<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\PasswordReset;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;
    public $email_id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email_id = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $randnum = $this->random_code(8);

        $data['url'] = $randnum;

        PasswordReset::create([
            'email'=>$this->email_id,
            'token'=>$randnum
        ]);

        return $this->view('emails.forgotPassword',$data);
    }

    function random_code($limit)
    {
      return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }
}
