<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\User;
use App\Token;
use App\Address;
use App\Product;
use App\ProductPrice;
use App\Subcategory;
use App\Banner;
use App\Theme;
use App\Brand;
use App\Flashsale;
use App\Color;
use App\Size;
use App\Wishlist;
use App\Subsubcategory;
use App\Priority;
use App\Cart;
use App\Seller;
use App\Offer;
use Image;
use Auth;
use Session;
use Validator;
use Intervention\Image\ImageServiceProvider;

class CartController extends Controller {
    private $baseurlslash;
    private $baseurl;

    public function __construct() {
        $this->baseurlslash = "http://mobuloustech.com/yodapi/public/";
        $this->baseurl = "http://mobuloustech.com/yodapi/public";
    }

    public function addtocart(Request $request) {

        $cartDatas = $request->all();
        $checkPro = Product::select("size","color")->where(["id"=>$cartDatas['productcart']])->first();
        if($request->is_variant == 2) {

            if(!empty($checkPro->color) && !empty($checkPro->size)) {
                $this->validate($request, [
                    'cartsize' => ['required'],
                    'cartcolor' => ['required']
                ]);

            } else if(!empty($checkPro->color) && empty($checkPro->size)) {
                $this->validate($request, [
                    'cartcolor' => ['required']
                ]);
            
            } else if(!empty($checkPro->size) && empty($checkPro->color)) {
                $this->validate($request, [
                    'cartsize' => ['required']
                ]);
            }
        }

        $offerArr = array();

        if(Auth::check()) {

            $haveCoupon = "false";
            $checkCoupons = Cart::where(['user_id' => Auth::user()->id])->get();
            foreach($checkCoupons as $checkCoupon) {
                if(strlen($checkCoupon->coupon_code) > 0) {
                    $haveCoupon = "true";
                }
            }
            if($haveCoupon == "true") {
                foreach($checkCoupons as $checkCoupon) {
                    $update = Cart::where(['id' => $checkCoupon->id])->first();
                    $update->discount = 0.00;
                    $update->coupon_code = "";
                    $update->save();
                }   
            }

            Wishlist::where(['product_id' => $cartDatas['productcart'], "user_id" => Auth::user()->id])->delete();

            $gerCatid = Product::select("subsubcategory_id","sp")->where(["id"=>$cartDatas['productcart']])->first();
            if($gerCatid) {
               $getoffer = Offer::where(["catid"=>$gerCatid->subsubcategory_id])->first();
                if($getoffer) {
                    $off = "";
                    $offerArr = ["haveOffer"=>1, "offertext"=>$off];
                } else {
                    $offerArr = ["haveOffer"=>0, "offertext"=>""];
                }
            }

            $cart = [
                "user_id"=>Auth::user()->id,
                "product_id"=>$cartDatas['productcart'],
                "size"=>$cartDatas['cartsize'],
                "color"=>$cartDatas['cartcolor'],
                "quantity"=>$cartDatas['cartqty']
            ];
        } else {
            if ($request->session()->has('cartsession')) {
                $session_id = session::get('cartsession');
            } else {
                $session_id = Session::getId();
                $request->session()->put('cartsession', $session_id);
            }

            $cart = [
                "user_id"=>$session_id,
                "product_id"=>$cartDatas['productcart'],
                "size"=>$cartDatas['cartsize'],
                "color"=>$cartDatas['cartcolor'],
                "quantity"=>1
            ];
        }

        $checkInCart = Cart::where(['user_id' => $cart['user_id'], "product_id"=>$cart['product_id']])->first();
        if($checkInCart) {

            $checkInCart->quantity = $checkInCart->quantity + 1;
            $checkInCart->save();

        } else {

            Cart::create($cart);
        }
        return redirect()->route('mybag');
    }

    public function cartlist(Request $request) {
        $currencyValue = Session::get('currency_value');
        if(empty($currencyValue)) {
            $currencyValue = 1;
        }

        $cartData = array();
        $sp_total = 0;
        $sp_price = 0;
        $mrp_total = 0;
        $mrp_price = 0;

        $subcatarr = array();

        if(Auth::check()) {
            $getCarts = Cart::where(["user_id"=>Auth::user()->id])->get();

            if($getCarts) {

                foreach($getCarts as $getCart) {
                    
                    $proDetail = Product::where(["id"=>$getCart->product_id])->first();
                    if($proDetail->is_variant == 2) {

                        if(!empty($getCart->color) && !empty($getCart->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size, "color"=>$getCart->color])->first();
                        } else if(!empty($getCart->color) && empty($getCart->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "color"=>$getCart->color])->first();
                        } else if(empty($getCart->color) && !empty($getCart->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size])->first();
                        }

                        $get_price_sp = $proPriceDetail->sp; 
                        $get_price_mrp = $proPriceDetail->mrp;
                    } else {
                        $get_price_sp = $proDetail->sp; 
                        $get_price_mrp = $proDetail->mrp;
                    }

                    $percentage = 100 - ($get_price_sp * $currencyValue *100)/$get_price_mrp * $currencyValue;
                    
                    $sellerdetails = Seller::find($proDetail->user_id);
                    if (!empty($sellerdetails)) {
                        $seller_name = $sellerdetails->name;
                    } else {
                        $seller_name = "";
                    }

                    if ($proDetail->images) {
                        $img = explode(',', $proDetail->images);
                        $proimage = $this->baseurl. $img[0];
                    } else {
                        $proimage = url('/public/') . "/img/imagenotfound.jpg";
                    }

                    $sp_price = $get_price_sp  * $currencyValue * $getCart->quantity;
                    $sp_total = $sp_price + $sp_total;

                    $mrp_price = $get_price_mrp * $currencyValue * $getCart->quantity;
                    $mrp_total = $mrp_price + $mrp_total;

                    $offerFind = 0;
                    $offerText = "";

                    $haveOffer = Offer::where(["catid"=>$proDetail->subsubcategory_id])->first();
                    if($haveOffer) {
                        $offerFind = 1;
                        $offerText = "Offer Buy ".$haveOffer->buy." Get ".$haveOffer->andget;
                        
                        $subcatarr[] = $proDetail->subsubcategory_id;
                    } else {
                        $offerFind = 0;
                        $offerText = "";
                    }

                    if(!empty($getCart->color)) {
                        $colorname = Color::where(["hexcode"=>$getCart->color])->first();
                        $colorname = $colorname->name;
                    } else {
                        $colorname = "";
                    }

                    $cartData[] = [
                        "cartid"=>$getCart->id,
                        "proid"=>$proDetail->id,
                        "productimage"=>$proimage,
                        "name"=>$proDetail->name,
                        "brand"=>$proDetail->brand,
                        "size"=>$getCart->size,
                        "color"=>$colorname,
                        "qty"=>$getCart->quantity,
                        "sp"=>$get_price_sp * $currencyValue,
                        "mrp"=>$get_price_mrp * $currencyValue,
                        "percentage"=> (int)$percentage,
                        "seller"=>$seller_name,
                        "offerFind"=>$offerFind,
                        "offerText"=>$offerText,
                        "size_label"=>$proDetail->size_label
                    ];
                }

            } else {
                $cartData = [];
            }
        } else {
            if ($request->session()->has('cartsession')) {
                $session_id = session::get('cartsession');

                $getCarts = Cart::where(["user_id"=>$session_id])->get();
                
                if($getCarts) {
                    $o = 1;
                    foreach($getCarts as $getCart) {

                        $proDetail = Product::where(["id"=>$getCart->product_id])->first();
                        if($proDetail->is_variant == 2) {

                            if(!empty($getCart->color) && !empty($getCart->size)) {
                                $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size, "color"=>$getCart->color])->first();
                            } else if(!empty($getCart->color) && empty($getCart->size)) {
                                $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "color"=>$getCart->color])->first();
                            } else if(empty($getCart->color) && !empty($getCart->size)) {
                                $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size])->first();
                            }

                            $get_price_sp = $proPriceDetail->sp; 
                            $get_price_mrp = $proPriceDetail->mrp;
                        } else {
                            $get_price_sp = $proDetail->sp; 
                            $get_price_mrp = $proDetail->mrp;
                        }

                        $percentage = 100 - ($get_price_sp * $currencyValue*100)/$get_price_mrp * $currencyValue;
                        
                        $sellerdetails = Seller::find($proDetail->user_id);
                        if (!empty($sellerdetails)) {
                            $seller_name = $sellerdetails->name;
                        } else {
                            $seller_name = "";
                        }

                        if ($proDetail->images) {
                            $img = explode(',', $proDetail->images);
                            $proimage = $this->baseurl. $img[0];
                        } else {
                            $proimage = url('/public/') . "/img/imagenotfound.jpg";
                        }

                        $sp_price = $get_price_sp * $currencyValue * $getCart->quantity;
                        $sp_total = $sp_price + $sp_total;

                        $mrp_price = $get_price_mrp * $currencyValue * $getCart->quantity;
                        $mrp_total = $mrp_price + $mrp_total;

                        $offerFind = 0;
                        $offerText = "";

                        $haveOffer = Offer::where(["catid"=>$proDetail->subsubcategory_id])->first();
                        if($haveOffer) {
                            $offerFind = 1;
                            $offerText = "Offer Buy ".$haveOffer->buy." Get ".$haveOffer->andget;

                        } else {
                            $offerFind = 0;
                            $offerText = "";
                        }

                        if(!empty($getCart->color)) {
                            $colorname = Color::where(["hexcode"=>$getCart->color])->first();
                            $colorname = $colorname->name;
                        } else {
                            $colorname = "";
                        }

                        $cartData[] = [
                            "cartid"=>$getCart->id,
                            "productimage"=>$proimage,
                            "name"=>$proDetail->name,
                            "brand"=>$proDetail->brand,
                            "size"=>$getCart->size,
                            "color"=>$colorname,
                            "qty"=>$getCart->quantity,
                            "sp"=>$get_price_sp * $currencyValue,
                            "mrp"=>$get_price_mrp * $currencyValue,
                            "percentage"=>(int)$percentage,
                            "seller"=>$seller_name,
                            "offerFind"=>$offerFind,
                            "offerText"=>$offerText,
                            "size_label"=>$proDetail->size_label
                        ];
                    }

                } else {
                    $cartData = [];
                }   

            } else {
                $cartData = [];
            }
        }

        $totalsaves = $mrp_total - $sp_total;

        $paying = [
            "amount"=>$sp_total,
            "payable"=>$sp_total,
            "discount"=>$totalsaves
        ];

        return view('addtocart', compact('cartData','paying'));
    }

    public function removefromcart($cid) {
        $getCarts = Cart::where(["id"=>$cid])->first();
        if($getCarts) {
            Cart::where(["id"=>$cid])->delete();
        }

        return redirect()->back();
    }

    public function cartupdate(Request $request) {
        

        $haveCoupon = "false";
        $checkCoupons = Cart::where(['user_id' => Auth::user()->id])->get();
        foreach($checkCoupons as $checkCoupon) {
            if(strlen($checkCoupon->coupon_code) > 0) {
                $haveCoupon = "true";
            }
        }
        if($haveCoupon == "true") {
            foreach($checkCoupons as $checkCoupon) {
                $update = Cart::where(['id' => $checkCoupon->id])->first();
                $update->discount = 0.00;
                $update->coupon_code = "";
                $update->save();
            }   
        }   
        if($request->qty == 0) {

            $getCarts = Cart::where(["id"=>$request->cid])->first();
            if($getCarts) {
                Cart::where(["id"=>$request->cid])->delete();
            }

        } else {

            $getCarts = Cart::where(["id"=>$request->cid])->first();
            if($getCarts) {
                $getCarts->quantity = $request->qty;
                $getCarts->save();
            }
        }

        echo true;
    }

    public function addtowishlist($id) {
        $check = Wishlist::where(["product_id"=>$id,"user_id"=>Auth::user()->id])->first();
        if($check) {} else {
            Wishlist::create([
                "product_id"=>$id,
                "user_id"=>Auth::user()->id,
            ]);
        }
        return redirect()->back();
    }

    
}
