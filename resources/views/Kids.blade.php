@extends('layouts.website')

@section('content')

    <section>
        <div class="DiscountArea">
            <div class="container">
                <div class="DiscountBox" style="background-image: url('{{url('/')}}/public/images/Women.png');">
                    <h1>GET 10 % EXTRA DISCOUNT</h1>
                    <h2>USE CODE: <span>WELCOME 10</span></h2>

                    <div class="row">
                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-1.png"></figure>
                                    <p>T-shirts</p>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-2.png"></figure>
                                    <p>shirts</p>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-3.png"></figure>
                                    <p>Jeans</p>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-4.png"></figure>
                                    <p>Jumpsuit</p>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-5.png"></figure>
                                    <p>Skirts</p>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-6.png"></figure>
                                    <p>Tops</p>
                                </a>
                            </div>
                        </div>
                    </div>

                    <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category" class="view">View all</a>

                </div>
            </div>
        </div>
    </section>
  
    <section>
        <div class="FlashArea">
            <div class="container">
                <h1><img src="{{url('/')}}/public/images/flash.png"> Flash Sale <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">View all</a></h1> 

                <div class="FlashBody">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="FlashBox">
                                <figure>
                                    <span>48%</span>
                                    <img src="{{url('/')}}/public/images/kid/kid-34.png">
                                </figure>
                                <aside>
                                    <p>Rs.809 <del>Rs.1799</del></p>
                                </aside>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="FlashBox">
                                <figure>
                                    <span>48%</span>
                                    <img src="{{url('/')}}/public/images/kid/kid-29.png">
                                </figure>
                                <aside>
                                    <p>Rs.809 <del>Rs.1799</del></p>
                                </aside>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="FlashBox">
                                <figure>
                                    <span>48%</span>
                                    <img src="{{url('/')}}/public/images/kid/kid-26.png">
                                </figure>
                                <aside>
                                    <p>Rs.809 <del>Rs.1799</del></p>
                                </aside>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="FlashBox">
                                <figure>
                                    <span>48%</span>
                                    <img src="{{url('/')}}/public/images/kid/kid-19.png">
                                </figure>
                                <aside>
                                    <p>Rs.809 <del>Rs.1799</del></p>
                                </aside>
                            </div>
                        </div>

                    </div>
                </div>
                    

            </div>
        </div>
    </section>
  

    <section>         
        <div class="container">
            <div class="OccasionArea">
                <h2>Shop by Occasion</h2>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="Occasionbox">
                            <figure><img src="{{url('/')}}/public/images/kid/kid-17.png"></figure>
                            <p>Gym Collection </p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="Occasionbox">
                            <figure><img src="{{url('/')}}/public/images/kid/kid-19.png"></figure>
                            <p>Jeans</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="Occasionbox">
                            <figure><img src="{{url('/')}}/public/images/kid/kid-24.png"></figure>
                            <p>Formal Dress</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="Occasionbox">
                            <figure><img src="{{url('/')}}/public/images/kid/kid-15.png"></figure>
                            <p>party Dress</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="OccasionArea"> 
                <div id="BuySlider" class="carousel slide" data-ride="carousel" style="background-image: url('{{url('/')}}/public/images/Kidslider.png');"> 
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="BuySliderBox">
                                <h3>Buy 4 Items</h3>
                                <h3>For <i class="fa fa-inr"></i> 1599</h3>
                                <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">Limited offer</a>
                            </div>
                        </div> 
                        <div class="item">
                            <div class="BuySliderBox">
                                <h3>Buy 2 Items</h3>
                                <h3>For <i class="fa fa-inr"></i> 1599</h3>
                                <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">Limited offer</a>
                            </div>
                        </div> 
                        <div class="item">
                            <div class="BuySliderBox">
                                <h3>Buy 5 Items</h3>
                                <h3>For <i class="fa fa-inr"></i> 1599</h3>
                                <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">Limited offer</a>
                            </div>
                        </div> 
                    </div>
                 
                    <a class="left carousel-control" href="#BuySlider" data-slide="prev">
                        <span class="glyphicon glyphicon-menu-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#BuySlider" data-slide="next">
                        <span class="glyphicon glyphicon-menu-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

        </div>         
    </section>




    <section>
        <div class="CollectionArea">
            <div class="container">

                <div class="CollectionList">
                    <h2>Ethnic Collection</h2>
                    <div id="demos">
                        <div class="owl-carousel owl-theme" id="Ethnic">
                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-15.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-16.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-17.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-18.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="CollectionList">
                    <h2>Dressess</h2>
                    <div id="demos">
                        <div class="owl-carousel owl-theme" id="jumpsuits">
                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-19.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-20.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-21.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-22.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="CollectionList">
                    <h2>Track Pants</h2>
                    <div id="demos">
                        <div class="owl-carousel owl-theme" id="lastest_top">
                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-23.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-24.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-25.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-26.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="CollectionList">
                    <h2>Footwear</h2>
                    <div id="demos">
                        <div class="owl-carousel owl-theme" id="Footwear">
                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-27.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-30.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-28.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-29.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="CollectionList">
                    <h2>kids bags</h2>
                    <div id="demos">
                        <div class="owl-carousel owl-theme" id="Watches">
                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-31.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-32.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-34.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-33.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="CollectionList">
                    <h2>Latest Iphone case</h2>
                    <div id="demos">
                        <div class="owl-carousel owl-theme" id="Stylish_Tops">
                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-35.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-36.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-37.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure><img src="{{url('/')}}/public/images/kid/kid-38.png"></figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
 
            </div>
        </div>
    </section>

    <section>
        <div class="FashionBuyArea">
            <div class="container">

                <h3>Fashion edit</h3>
                <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</h4>

                <div class="row">
                    <div class="col-sm-5">
                        <div class="FashionBuyLeft">
                            <h1>BUY 1 GET 1
                                <span>EXTRA 50% OFF</span> </h1>
                            <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">Shop now</a>
                        </div>
                    </div>

                    <div class="col-sm-7">
                        <ul>
                            <li>
                                <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/men/Men-38.png"></figure>
                                    <p>Casual wear</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/men/Men-39.png"></figure>
                                    <p>ethnic wear</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/men/Men-40.png"></figure>
                                    <p>accessories</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/men/Men-41.png"></figure>
                                    <p>gym Tees</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="ClothingArea">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="Clothing">
                            <figure><img src="{{url('/')}}/public/images/new/image-31.png"></figure>
                            <h3>clothing</h3>
                            <ul>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">KURTA PAJAMA</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">Jackets</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">nehru jacket</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">TRACKSUITS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">casual shirts</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">sweat shirts</a></li> 
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="Clothing">
                            <figure><img src="{{url('/')}}/public/images/new/image-32.png"></figure>
                            <h3>Sports</h3>
                            <ul>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">KURTA PAJAMA</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">Jackets</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">nehru jacket</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">TRACKSUITS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">casual shirts</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">sweat shirts</a></li> 
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="Clothing">
                            <figure><img src="{{url('/')}}/public/images/new/image-33.png"></figure>
                            <h3>Footwear</h3>
                            <ul>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">KURTA PAJAMA</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">Jackets</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">nehru jacket</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">TRACKSUITS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">casual shirts</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">sweat shirts</a></li> 
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="Clothing">
                            <figure><img src="{{url('/')}}/public/images/new/image-34.png"></figure>
                            <h3>Ethnic wear</h3>
                            <ul>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">KURTA PAJAMA</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">Jackets</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">nehru jacket</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">TRACKSUITS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">casual shirts</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=KIDS&filter_type=Category">sweat shirts</a></li> 
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
  
    @endsection