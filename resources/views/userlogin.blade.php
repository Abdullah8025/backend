@extends('layouts.website')

@section('content')
<style type="text/css">
	span.formerror {
    float: left;
    width: 100%;
    font-size: 10px;
    color: red;
    margin: 3px auto;
}
</style>
   <section>
        <div class="LoginArea">
            
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-md-8 col-md-offset-2 col-sm-offset-1">
                        <div class="LoginForm">
                            <form method="post" action="{{action('API\HomeController@userlogin')}}">
                                <h3>Login to Yard Of Deals</h3>
                                <h4>or</h4>
                                <ul>
                                    <li><a href="redirect/facebook"><img src="{{url('/')}}/public/images/Icon-1.png"> facebook</a></li>
                                    <li><a href="#"><img src="{{url('/')}}/public/images/Icon-2.png"> Google</a></li>
                                </ul>
                                <h5>- or using email -</h5>

                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Your Email Address">
                                    <span class="formerror"><?php
                                	if(isset($messages['email']['0']) && !empty($messages['email']['0'])){
                                		echo $messages['email']['0'];
                                	}
                                ?></span>

                                    <input type="password" name="password" class="form-control" placeholder="Enter Password">

                                    <span class="formerror"><?php
                                	if(isset($messages['password']['0']) && !empty($messages['password']['0'])){
                                		echo $messages['password']['0'];
                                	}
                                ?></span>

                                </div>

                                <button>Login</button>

                                <div class="Links">
                                    <a href="{{url('/')}}/forgot">Forgot password ?</a>
                                    <p>New To YOD? <a href="{{url('/')}}/signup">create Account</a></p>
                                </div>
                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection