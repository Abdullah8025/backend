<?php

namespace App\Http\Controllers\API;

use Illuminate\Pagination\LengthAwarePaginator;
use Propaganistas\LaravelPhone\LaravelPhoneServiceProvider;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use DB;
use App\Seller;
use App\User;
use App\Token;
use App\Address;
use App\Product;
use App\Order;
use App\Admin;
use App\Subcategory;
use App\Banner;
use App\Theme;
use App\Brand;
use App\Flashsale;
use App\Wishlist;
use App\Cart;
use App\Subsubcategory;
use App\Content;
use App\Faq;
use App\Priority;
use App\Offer;
use Image;
use Session;
use Validator;
use Mail;
use Socialite;
use Auth;
use Intervention\Image\ImageServiceProvider;
use Softon\Indipay\Facades\Indipay; 

class HomeController extends APIBaseController {

  private $baseurlslash;
  private $baseurl;

  public function __construct() {
      $this->baseurlslash = "http://mobuloustech.com/yodapi/public/";
      $this->baseurl = "http://mobuloustech.com/yodapi/public";
  }

  public function index(Request $request) {
      $return_array = array();
      $banners_array = array();
      $banner1 = array();
      $banner2 = array();
      $slider1 = array();
      $slider2 = array();
      $handpicked = array();
      $stylefeed = array();
      $brandinfocus = array();
      $hotdeal = array();
      $themedetailsarray = array();
      $announceArr = array();

      $bannerdetails = Banner::orderBy('id','DESC')->get();
      foreach ($bannerdetails as $value) {
          
          $catname = Subsubcategory::where('id',$value->subsubcategory_id)->first();
          //dd($catname);

          if($catname && $catname->status == 1) {

            if(!empty($value->image)) {
                if (strpos($value->image, $this->baseurlslash) == false) {
                    $value->image = $this->baseurl.$value->image;
                }
            } else {
              $value->image = url('/public/') . "/img/imagenotfound.jpg";
            }

            if($value->type == 'homeslider') {
              $banners_array[] = array("cat"=>$catname->category_id, "filter_data"=>$value->subsubcategory_id,"type"=>"subsubcategory_id","image"=>$value->image);
            }

            if($value->type == 'banner1') {
              $banner1[] = array("cat"=>$catname->category_id, "filter_data"=>$value->subsubcategory_id,"type"=>"subsubcategory_id","image"=>$value->image);
            }

            if($value->type == 'banner2') {
              $banner2[] = array("cat"=>$catname->category_id, "filter_data"=>$value->subsubcategory_id,"type"=>"subsubcategory_id","image"=>$value->image);
            }

            if($value->type == 'slider1') {
              $slider1[] = array("cat"=>$catname->category_id, "filter_data"=>$value->subsubcategory_id,"type"=>"subsubcategory_id","image"=>$value->image);
            }

            if($value->type == 'slider2') {
              $slider2[] = array("cat"=>$catname->category_id, "filter_data"=>$value->subsubcategory_id,"type"=>"subsubcategory_id","image"=>$value->image);
            }

            if($value->type == 'handpicked') {
              $handpicked[] = array("cat"=>$catname->category_id, "filter_data"=>$value->subsubcategory_id,"type"=>"subsubcategory_id","image"=>$value->image);
            }

            if($value->type == 'stylefeed') {
              $stylefeed[] = array("cat"=>$catname->category_id, "filter_data"=>$value->subsubcategory_id,"type"=>"subsubcategory_id","image"=>$value->image);
            }

            if($value->type == 'hotdeal') {
              $hotdeal[] = array("cat"=>$catname->category_id, "filter_data"=>$value->subsubcategory_id,"type"=>"subsubcategory_id","image"=>$value->image);
            }
          }
      }

      $deatilsbrand = Brand::where('focus_status','1')->orderBy('updated_at','DESC')->get();
      foreach ($deatilsbrand as $value) {
        if(!empty($value->image)){
            if (strpos($value->image, $this->baseurlslash) == false) {
                $value->image = $this->baseurlslash.$value->image;
            }
        } else {
          $value->image = url('/public/') . "/img/imagenotfound.jpg";
        } 

        $brandinfocus[] = array("filter_data"=>$value->name,"type"=>"brand","image"=>$value->image);
      }

      
      $themedetails = Theme::get();
      foreach ($themedetails as $value) {
        if(!empty($value->imagemain)){
          if (strpos($value->imagemain, $this->baseurlslash) == false) {
              $value->imagemain = $this->baseurl.$value->imagemain;
          }
        } else {
          $value->imagemain = url('/public/') . "/img/imagenotfound.jpg";
        } 

        if(!empty($value->herimage)){
          if (strpos($value->herimage, $this->baseurlslash) == false) {
              $value->herimage = $this->baseurl.$value->herimage;
          }
        } else {
          $value->herimage = url('/public/') . "/img/imagenotfound.jpg";
        } 

        if(!empty($value->himimage)){
          if (strpos($value->himimage, $this->baseurlslash) == false) {
              $value->himimage = $this->baseurl.$value->himimage;
          }
        } else {
          $value->himimage = url('/public/') . "/img/imagenotfound.jpg";
        } 

        $themedetailsarray[] = array("image"=>$value->imagemain,"himimage"=>$value->himimage,"herimage"=>$value->herimage,"title"=>$value->title,"description"=>$value->description,"filter_data"=>(string)$value->id,"type"=>"theme_id"); 
      }

      $flash_sale = array();
        $findflashdetails = Flashsale::where(['id' => 1, 'status' => '1'])->first();
        
        if (!empty($findflashdetails)) {
            $flashdatenew = date('Y-m-d H:i:s', strtotime($findflashdetails->time));
            $flashStr = strtotime($flashdatenew);
            $currentDate = date('Y-m-d H:i:s');
            $currentStr = strtotime($currentDate);

            if($currentStr >= $flashStr) {

                $flashdateend = date('Y-m-d H:i:s', strtotime($findflashdetails->endtime));
                $flashStrEnd = strtotime($flashdateend);
                $currentDateEnd = date('Y-m-d H:i:s');
                $currentStrEnd = strtotime($currentDateEnd);

                if($currentStrEnd <= $flashStrEnd) {


                    $findflashdetailsstatus = $findflashdetails->status;
                    $flashtime = date('d-m-Y, h:i:s', strtotime($findflashdetails->endtime));
                    $flashtime = strtotime($findflashdetails->endtime);
                    $currentDate = date('d-m-Y, h:i:s');
                    $currentDateStr = strtotime($currentDate);

                    $timeDiff = abs($flashtime - $currentDateStr);
                    $findflashdetailstime = $timeDiff;

                    $flashsaleendtimeget = $findflashdetails->endtime;

                    $findflashdetails->products = trim($findflashdetails->products);

                    if(strlen($findflashdetails->products)>=1) {
                      $productdetails = Product::whereIn('id', explode(",", $findflashdetails->products))->where(["deleteStatus"=>1,"status"=>1])->orderBy('id','DESC')->get();
                    } else {
                      $productdetails = Product::where(['subsubcategory_id'=>$findflashdetails->subsubcategory])->where(["deleteStatus"=>1,"status"=>1])->orderBy('id','DESC')->get();
                    }
                    
                    foreach ($productdetails as $value) {
                        $imagearray = explode(",", $value->images);
                        if (!empty($imagearray)) {
                            if (!empty($imagearray[0])) {
                                if (strpos($imagearray[0], $this->baseurlslash) == false) {
                                    $imagearray[0] = $this->baseurl . $imagearray[0];
                                }
                            } else {
                                $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                            }
                        } else {
                            $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                        }

                        if(Auth::check()) {
                          $findflagofwishlist = Wishlist::where(['user_id' => Auth::user()->id, "product_id" => $value->id])->first();
                          
                          if (!empty($findflagofwishlist)) {
                              $like_Status = "1";
                          } else {
                              $like_Status = "0";
                          }
                        } else {
                            $like_Status = "0";
                        }

                        $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
                        $flash_sale[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
                    }
                }  else {
                    $flash_sale = [];
                    $findflashdetailsstatus = '';
                    $findflashdetailstime = '';
                    $flashsaleendtimeget = '';
                } 
            } else {
                $flash_sale = [];
                $findflashdetailsstatus = '';
                $findflashdetailstime = '';
                $flashsaleendtimeget = ''; 
            }
        } else {
            $flash_sale = [];
            $findflashdetailsstatus = '';
            $findflashdetailstime = '';
            $flashsaleendtimeget = '';
        }

      $womansubcategory = Subcategory::where('category_id','WOMEN')->take(3)->get();
      $mansubcategory = Subcategory::where('category_id','MEN')->take(3)->get();
      $kidsubcategory = Subcategory::where('category_id','KIDS')->take(3)->get();
      
      $announceGets = Offer::where(['offer_type' => 3])->orderBy('id','ASC')->get();
      if($announceGets) {
          foreach($announceGets as $announceGet) {
              $getCat = Subsubcategory::select('category_id','status')->where(["id"=>$announceGet->catid])->first();
              if($getCat) {
                if($getCat->status == 1) {
                    $announceArr[] = array("cat"=>$getCat->category_id,"subcat" => $announceGet->catid, "type" => "subsubcategory_id", "content" => $announceGet->description);
                }
              }
          }
      }

      $return_array = ["banners"=>$banners_array,
                       "handpicked"=>$handpicked,
                       "banner1"=>$banner1,
                       "banner2"=>$banner2,
                       "hotdeal"=>$hotdeal,
                       "slider1"=>$slider1,
                       "slider2"=>$slider2,
                       "stylefeed"=>$stylefeed,
                       "brandinfocus"=>$brandinfocus,
                       "themedetails"=>$themedetailsarray,
                       "flash_sale"=>$flash_sale,
                       "flash_sale_status"=>$findflashdetailsstatus,
                       "flash_sale_time"=>$flashsaleendtimeget,
                    ];

      $priority = Priority::orderby('section_number','asc')->get();

      //dd($womansubcategory);

      return view('index', compact('return_array','womansubcategory','mansubcategory','kidsubcategory','announceArr','priority'));
  }

  public function autocomplete(Request $request) {
      $input = $request->all();
      $validator = Validator::make($input, [
          'key'=>'required',
      ]);

      $content ='';
      $brandArr = array();
      $catArr = array();

      $getBrands = Product::select('brand','subsubcategory_id', 'category_id')->where('brand', 'like', '%' . $input['key'] . '%')->distinct('subsubcategory_id')->where(["deleteStatus"=>1,"status"=>1])->get();
      
      foreach($getBrands as $getBrand) {
          $subsubname = Subsubcategory::select('name','status')->where('id',$getBrand->subsubcategory_id)->first(); 
          if($subsubname) {
            if($subsubname->status == 1) {
              $text = $getBrand->brand.' '.$subsubname->name.' '.$getBrand->category_id;
              $brandArr[] =  ["name"=>$text, "subsubcatid"=> base64_encode($getBrand->subsubcategory_id), "catid"=>$getBrand->category_id, 'brand'=> $getBrand->brand];
            }
          }
      }

      $getCats = Subsubcategory::select('id', 'name', 'category_id','status')->where('name', 'like', '%' . $input['key'] . '%')->get();

      foreach($getCats as $getCat) {
          if($getCat->status == 1) {
            $text = $getCat->name.' '.$getCat->category_id;
            $catArr[] = ["name"=>$text, "subsubcatid"=>  base64_encode($getCat->id), "catid"=>$getCat->category_id, 'brand'=>'0'];
          }
      }
      
      $finalarray = array_merge($brandArr, $catArr);
      sort($finalarray);
      return response()->json($finalarray);
  }


 
}
