@extends('layouts.app')

@section('content')

   <section>
        <div class="TermsArea">
            <div class="container">
                <h3>@if($lang=='ar') الشروط والأحكام @else Terms & Condition @endif</h3>

               <?php if($lang=='ar') {echo ucfirst($detail['ar_value']);}else{echo $detail['value'];} ?>            </div>
        </div>
    </section>

@endsection