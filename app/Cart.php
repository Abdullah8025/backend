<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    
    protected $fillable = [ 'user_id','product_id', 'seller_id', 'quantity', 'discount', 'coupon_code'];
}
