<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Couponcode extends Model
{
    
    protected $fillable = [ 'name','name_ar','discount','discount_ar','type','min_price','min_price_ar','startdate','expdate'];
}
