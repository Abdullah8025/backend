<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use DB;
use App\User;
use App\Token;
use App\Address;
use App\Product;
use App\ProductPrice;
use App\Subcategory;
use App\Banner;
use App\Theme;
use App\Brand;
use App\Flashsale;
use App\Color;
use App\Size;
use App\Wishlist;
use App\Subsubcategory;
use App\Priority;
use App\Offer;
use App\Seller;
use App\Order;
use App\OrderDetail;
use App\SizeChart;
use App\ProductImageColor;
use App\Cart;
use App\Attribute;
use App\Promotion;
use App\PromotionList;
use App\Cuponcode;
use Image;
use Auth;
use Session;
use Validator;
use View;
use Intervention\Image\ImageServiceProvider;
use GuzzleHttp\Client;

class ProductController extends Controller {
    private $baseurlslash;
    private $baseurl;
    private $currencyValue;
    private $getCurrency;

    public function __construct() {
        $this->baseurlslash = "http://mobuloustech.com/yodapi/public/";
        $this->baseurl = "http://mobuloustech.com/yodapi/public";

        //$this->middleware(function ($request, $next) {
            // $cCode = Session::get('currency_code');
            // if(isset($cCode) && !empty($cCode)) {
            //     $this->getCurrency = Session::get('currency_code');
            // } else {
            //     $this->getCurrency = "USD";
            // }

        //     return $next($request);
        // });

        //var_dump($this->getCurrency);die;

        //$http = new Client();
        //$generateWBN = $http->get(url("https://api.ratesexchange.eu/client/latest?apiKey=1ad9de46-fc6c-47f5-b147-432046d3501e&base_currency=INR&currencies=$this->getCurrency"));
        //$dataResult = json_decode($generateWBN->getBody());
        //var_dump($dataResult);      die;
    }

    public function innerproductlist(Request $request, $catid, $cattype, $cid, $color=null, $brand=null) {

        $input = $request->all();
        $return_array = array();
        $catid = $catid;
        $catype = base64_decode($cattype);
        $cid = base64_decode($cid);

        $sortcolumn_name = "id";
        $incorder_name = "DESC";
        $fsort = "";

        $query = Product::query();

        if($catype == "subcat") {

            $query->where(["subcategory_id"=>$cid]);

        } else if($catype == "subsubcat") {

            $color = base64_decode($color);
            $brand = base64_decode($brand);

            if($color != '') {
               // echo $color;die;
                $query->where(["subsubcategory_id"=>$cid, "defaultcolor"=>$color]);

            } elseif($brand != 0) {
                $query->where(["subsubcategory_id"=>$cid, "brand"=>$brand]);
            } else {
                $query->where(["subsubcategory_id"=>$cid]);
            }
        }

        $productids = $query->where(["deleteStatus"=>1,"status"=>1])->orderby($sortcolumn_name, $incorder_name)->get();
        $arrayids = array();
        foreach($productids as $productid) {
            $arrayids[] = $productid->id;
        }

        $productdetails = Product::whereIn('id',$arrayids)->orderby($sortcolumn_name, $incorder_name)->get();

        foreach ($productdetails as $value) {
            $check_status_unique = 0;
            $imagearray = explode(",", $value->images);
            
            if (!empty($imagearray)) {
            
                if (!empty($imagearray[0])) {
            
                    if (strpos($imagearray[0], $this->baseurlslash) == false) {
                        $imagearray[0] = $this->baseurl . $imagearray[0];
                    }
                } else {
                    $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                }
            
            } else {
                $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
            }

            if(!empty($input['user_id'])) {
                $findflagofwishlist = Wishlist::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
                
                if (!empty($findflagofwishlist)) {
                    $like_Status = "1";
                } else {
                    $like_Status = "0";
                }
            } else {
                $like_Status = "0";
            }

            $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
            
            $return_array[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
        }

        if($catype == "subsubcat") {
            $filters = $this->getFilters('subsubcat', $cid);
            $productdetails = Subsubcategory::where('id', $cid)->first();
            $urlname = ['first'=>$catid, 'second'=>$productdetails->name, 'third'=>'subsubcat', 'id'=> $cid, 'urltype'=>'common'];
        
        } else {
            $filters = $this->getFilters('subcat', $cid);
            $productdetails = Subcategory::where('id', $cid)->first();
            $urlname = ['first'=>$catid, 'second'=>$productdetails->name, 'third'=>'subcat', 'id'=> $cid, 'urltype'=>'common'];
        }

        $postFilters = ["fsort"=>$fsort];

        return view('plist', compact('return_array', 'urlname', 'filters', 'postFilters'));
    }

    public function brandproductlist(Request $request, $brand) {

        $input = $request->all();
        $return_array = array();
        $sortcolumn_name = "id";
        $incorder_name = "DESC";

        $productdetails = Product::where(["brand"=>$brand])->orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();

        if($productdetails) {
            
            foreach ($productdetails as $value) {
                $check_status_unique = 0;
                $imagearray = explode(",", $value->images);
                
                if (!empty($imagearray)) {
                
                    if (!empty($imagearray[0])) {
                
                        if (strpos($imagearray[0], $this->baseurlslash) == false) {
                            $imagearray[0] = $this->baseurl . $imagearray[0];
                        }
                    } else {
                        $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                    }
                
                } else {
                    $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                }

                if(!empty($input['user_id'])) {
                    $findflagofwishlist = Wishlist::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
                    
                    if (!empty($findflagofwishlist)) {
                        $like_Status = "1";
                    } else {
                        $like_Status = "0";
                    }
                } else {
                    $like_Status = "0";
                }

                $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
                
                $return_array[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
            }

        } else {
            $return_array =[];
        }

        $filters = $this->getFilters('brand', $brand);
        $urlname = ['first'=>'', 'second'=>$brand, 'id'=> $brand, 'urltype'=>'homebrand'];

        return view('plist', compact('return_array', 'urlname', 'filters'));
    }

    public function searchproductlist(Request $request, $catid, $subcatid, $brand) {

        $input = $request->all();
        $return_array = array();
        $catid = $catid;
        $subcatid = base64_decode($subcatid);
        $brand = $brand;

        $sortcolumn_name = "id";
        $incorder_name = "DESC";

        //dd($brand);

        $query = Product::query();
        $query->where(["category_id"=>$catid])->where(["subsubcategory_id"=>$subcatid]);
        if($brand != 0 || !empty($brand)) {
            $query->where(["brand"=>$brand]);
        }
        
        $productids = $query->where(["deleteStatus"=>1,"status"=>1])->orderby($sortcolumn_name, $incorder_name)->get();
        $arrayids = array();
        foreach($productids as $productid) {
            $arrayids[] = $productid->id;
        }

        //dd($arrayids);

        $productdetails = Product::whereIn('id',$arrayids)->orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();

        foreach ($productdetails as $value) {
            $check_status_unique = 0;
            $imagearray = explode(",", $value->images);
            
            if (!empty($imagearray)) {
            
                if (!empty($imagearray[0])) {
            
                    if (strpos($imagearray[0], $this->baseurlslash) == false) {
                        $imagearray[0] = $this->baseurl . $imagearray[0];
                    }
                } else {
                    $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                }
            
            } else {
                $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
            }

            if(!empty($input['user_id'])) {
                $findflagofwishlist = Wishlist::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
                
                if (!empty($findflagofwishlist)) {
                    $like_Status = "1";
                } else {
                    $like_Status = "0";
                }
            } else {
                $like_Status = "0";
            }

            $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
            
            $return_array[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
        }

        $filters = $this->getFilters('brand', $subcatid);
        $productdetails = Subsubcategory::where('id', $subcatid)->first();
        $urlname = ['first'=>$catid, 'second'=>$productdetails->name, 'third'=>'subsubcat','four'=>$brand, 'id'=> $subcatid, 'urltype'=>'search'];

        return view('plist', compact('return_array', 'urlname', 'filters'));
    }

    public function filterproductlist(Request $request, $catid, $cattype, $cid) {

        $input = $request->all();
        $return_array = array();

        $catid = $catid;
        $catype = base64_decode($cattype);
        $cid = base64_decode($cid);

        $filter_data = array();
        $filter_data[0] = "none";

        if(isset($request->sort)) {
            if (!empty($request->sort)) {

                if ($request->sort == 'new') {
                    $sortcolumn_name = "id";
                    $incorder_name = "desc";
                } else if ($request->sort == "popularity") {
                    $sortcolumn_name = "id";
                    $incorder_name = "desc";
                } else if ($request->sort == "discount") {
                    $sortcolumn_name = "discount";
                    $incorder_name = "desc";
                } else if ($request->sort == "low_to_high") {
                    $sortcolumn_name = "sp";
                    $incorder_name = "ASC";
                } else if ($request->sort == "high_to_low") {
                    $sortcolumn_name = "sp";
                    $incorder_name = "desc";
                } else if ($request->sort == "delivery_time") {
                    $sortcolumn_name = "ships_in";
                    $incorder_name = "asc";
                }
                $fsort = "";
            } else {
                $sortcolumn_name = "id";
                $incorder_name = "DESC";
                $fsort = "";
            }
        } else {
            $sortcolumn_name = "id";
            $incorder_name = "DESC";
            $fsort = "";
        }

        $query = Product::query();

        if($catype == "subcat") {

            $query->where(["subcategory_id"=>$cid]);

        } else if($catype == "subsubcat") {

            if(!empty($input['brand']) && !empty($input['defaultcolor'])) {

                $query->where(["subsubcategory_id"=>$cid, "brand"=>$input['brand'], "defaultcolor"=>$input['defaultcolor']]);

            } elseif(!empty($input['defaultcolor']) && empty($input['brand'])) {

                $query->where(["subsubcategory_id"=>$cid, "defaultcolor"=>$input['defaultcolor']]);
            
            } elseif(empty($input['defaultcolor']) && !empty($input['brand'])) {

                $query->where(["subsubcategory_id"=>$cid, "brand"=>$input['brand']]);

            } else {
                $query->where(["subsubcategory_id"=>$cid]);
            }

        } else if($catype == "search") {
            $query->where(["category_id"=>$catid])->where(["subsubcategory_id"=>$cid]);

        } else if($catype == "brand") {
            $query->where(["brand"=>$input['brand']]);
        }

        if(isset($input['minprice'])) {
            if($input['minprice'] == 100 && $input['maxprice'] != 10000) {
                $query->where('sp', '<=', $input['maxprice']);

            } else if($input['minprice'] != 100 && $input['maxprice'] == 10000) {
                $query->where('sp', '>=', $input['minprice']);

            } else if($input['minprice'] != 100 && $input['maxprice'] != 10000) {
                $query->where('sp', '>=', $input['minprice'])->where('sp', '<=', $input['maxprice']);
            }
        }

        $productids = $query->where(["deleteStatus"=>1,"status"=>1])->orderby($sortcolumn_name, $incorder_name)->get();
        $arrayids = array();
        foreach($productids as $productid) {
            $arrayids[] = $productid->id;
        }

        if(isset($request->sizearr)) {
            if(count($request->sizearr) > 0) {
                $filtersizes = $request->sizearr;
                $sizearrayids = $arrayids;
                $arrayids = array();
                foreach($filtersizes as $filtersize) {
                    $fsizes = Product::select('id')->whereIn('id',$sizearrayids)->where('size','like','%'.$filtersize.'%')->where(["deleteStatus"=>1,"status"=>1])->get();
                    foreach($fsizes as $fsize) {
                        $arrayids[] = $fsize->id;
                    }
                }
                $arrayids = array_unique($arrayids);
            }
        }

        if(isset($request->colorarr)) {
            if(count($request->colorarr) > 0) {
                $filtercolors = $request->colorarr;
                $colorarrayids = $arrayids;
                $arrayids = array();
                foreach($filtercolors as $filtercolor) {
                    $fcolors = Product::select('id')->where('color','like','%'.$filtercolor.'%')->where(["deleteStatus"=>1])->whereIn('id',$colorarrayids)->get();
                    foreach($fcolors as $fcolor) {
                        $arrayids[] = $fcolor->id;
                    }
                }
                $arrayids = array_unique($arrayids);
            }
        }

        if(isset($request->brandarr)) {
            if(count($request->brandarr) > 0) {
                $filterbrands = $request->brandarr;
                $brandarrayids = $arrayids;
                $arrayids = array();
                foreach($filterbrands as $filterbrand) {
                    $fbrands = Product::select('id')->whereIn('id',$brandarrayids)->where('brand','like','%'.$filterbrand.'%')->where(["deleteStatus"=>1])->get();
                    foreach($fbrands as $fbrand) {
                        $arrayids[] = $fbrand->id;
                    }
                }
                $arrayids = array_unique($arrayids);
            }
        }

        $productdetails = Product::whereIn('id',$arrayids)->orderby($sortcolumn_name, $incorder_name)->get();

        foreach ($productdetails as $value) {
            $check_status_unique = 0;
            $imagearray = explode(",", $value->images);
            
            if (!empty($imagearray)) {
            
                if (!empty($imagearray[0])) {
            
                    if (strpos($imagearray[0], $this->baseurlslash) == false) {
                        $imagearray[0] = $this->baseurl . $imagearray[0];
                    }
                } else {
                    $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                }
            
            } else {
                $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
            }

            if(!empty($input['user_id'])) {
                $findflagofwishlist = Wishlist::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
                
                if (!empty($findflagofwishlist)) {
                    $like_Status = "1";
                } else {
                    $like_Status = "0";
                }
            } else {
                $like_Status = "0";
            }

            $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
            
            $return_array[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
        }

        if($catype == "subsubcat") {
            $filters = $this->getFilters('subsubcat', $cid);
            $productdetails = Subsubcategory::where('id', $cid)->first();
            $urlname = ['first'=>$catid, 'second'=>$productdetails->name, 'id'=> $cid];
        
        } else {
            $filters = $this->getFilters('subcat', $cid);
            $productdetails = Subcategory::where('id', $cid)->first();
            $urlname = ['first'=>$catid, 'second'=>$productdetails->name, 'id'=> $cid];
        }

        $view = View::make('filterspro', compact('return_array'));
        return $contents = $view->render();
    }

    public function getFilters($type, $key) {
        $colorData = array();
        $hexData = array();
        $sizeData = array();
        $sizeDataa = array();
        $brandData = array();
        $brandDataa = array();

        $getColors = Color::select("name","hexcode")->get();
        foreach ($getColors as $getColor) {
            $colorData[] = ['name'=>$getColor->name,'hex'=>$getColor->hexcode];
            $hexData[] = $getColor->hexcode;
        }
        
        if($type == "flashsale") {
            
            $findflashdetails = Flashsale::where(['id' => 1, 'status' => '1'])->first();
            $brandGets = Product::select("brand")->whereIn('id', explode(",", $findflashdetails->products))->where(["deleteStatus"=>1,"status"=>1])->get();

            foreach ($brandGets as $brandGet) {
                $brandData[] = $brandGet->brand;
            }
            $brandDatas = array_unique($brandData);
            foreach($brandDatas as $brandData) {
                $brandDataa[] = $brandData;
            }

        } else if($type == "subcat" || $type == "subsubcat") {

            if($type == "subcat") {
                $whereType = ["subcategory_id"=>$key];
            } else {
                $whereType = ["subsubcategory_id"=>$key];
            }

            $getSizes = Product::select("size")->where($whereType)->get();
            foreach ($getSizes as $getSize) {
                $sizeStrings = explode(',',$getSize->size);
                
                foreach($sizeStrings as $sizeString) {
                    $sizeData[] = $sizeString;
                }
            }
            $sizeDatas = array_values(array_unique($sizeData));

            foreach($sizeDatas as $sizeData) {
                $sizeDataa[] = $sizeData;
            }

            $brandGets = Product::select("brand")->where($whereType)->get();
            foreach ($brandGets as $brandGet) {
                $brandData[] = $brandGet->brand;
            }
            $brandDatas = array_unique($brandData);
            foreach($brandDatas as $brandData) {
                $brandDataa[] = $brandData;
            }
        } else if($type == "brand") {

            $whereType = ["subsubcategory_id"=>$key];
            $getSizes = Product::select("size")->where($whereType)->get();
            foreach ($getSizes as $getSize) {
                $sizeStrings = explode(',',$getSize->size);
                
                foreach($sizeStrings as $sizeString) {
                    $sizeData[] = $sizeString;
                }
            }
            $sizeDatas = array_values(array_unique($sizeData));

            foreach($sizeDatas as $sizeData) {
                $sizeDataa[] = $sizeData;
            }
            $brandData[] = $key;
        }
        return ["size" => $sizeDataa, "color" => $colorData, "hexcode"=>$hexData, "brand" => $brandDataa];
    }

    public function productdetails(Request $request, $id) {
        
        $currencyValue = Session::get('currency_value');
        if(empty($currencyValue)) {
            $currencyValue = 1;
        }
        $id = base64_decode($id);
        $input['product_id'] = $id;
        $productImageColor = ProductImageColor::where('product_id',$id)->get();
        $productdetails_list = Product::find($id);

        $arrayimage = explode(",",$productdetails_list->images);
        $arrayimage1 = array();
        foreach ($arrayimage as $value) {
            if(!empty($value)){
                if (strpos($value, $this->baseurlslash) == false) {
                    $arrayimage1[] = $this->baseurl.$value;
                }
             } else {
              $arrayimage1[] = url('/public/') . "/img/imagenotfound.jpg";
             } 
        }

        $bankOfferArr = array();
        $getBankOffers = Offer::where('offer_type',1)->get();
        if($getBankOffers) {
            foreach($getBankOffers as $getBankOffer) {
                $bankOfferArr[] = $getBankOffer->description;
            }
        }

        $sellerdetails = Seller::find($productdetails_list->user_id);
        if (!empty($sellerdetails)) {
            $seller_name = $sellerdetails->name;
        } else {
            $seller_name = "";
        }


        $otherOfferArr = array();
        $checkPromotions = Promotion::where(["seller_id"=>$sellerdetails->id])->get();

        if($checkPromotions) {
            foreach($checkPromotions as $checkPromotion) {

                $checkProductPromotion = PromotionList::where(["product_id"=>$productdetails_list->id,'status'=>1])->first();

                if($checkProductPromotion) {
                    $getOtherOffers = Offer::where(['id'=>$checkPromotion->offer_id, 'offer_type'=>2, 'catid'=> $productdetails_list->subsubcategory_id])->first();

                        if($getOtherOffers) {
                            $date1 = strtotime($getOtherOffers->enddate);
                            $date2 = strtotime(date("d-m-Y H:i:s"));
                            if($date2 <= $date1) {
                                $getCoupon = Cuponcode::where('id', $getOtherOffers->coupon_id)->first();
                                if($getCoupon) {
                                    $otherOfferArr[] = $getOtherOffers->description .'- CODE : '.$getCoupon->name;
                                }
                            }
                        }   
                    
                }
            }
        }

        //Similar products

        $similar_list = Product::where('subsubcategory_id', $productdetails_list->subsubcategory_id)->where(["deleteStatus"=>1,"status"=>1])->get();
        $similar = array();
        foreach ($similar_list as $val) {
            if ($val->id != $input['product_id']) {
        
                if ($val->images) {
                    $img = explode(',', $val->images);
                    $res['image'] = $this->baseurl. $img[0];
                } else {
                    $res['image'] = url('/public/') . "/img/imagenotfound.jpg";
                }
                $res['product_id'] = $val->id;
                $res['name'] = $val->name;
                $res['mrp'] = $val->mrp * $currencyValue;
                $res['discount'] = round($val->discount);
                $res['brand'] = $val->brand;
                $percentage = 100 - ($val->sp * $currencyValue * 100) / $val->mrp * $currencyValue;
                $res['percentage'] = round($percentage);
                $res['sp'] = $val->sp * $currencyValue;
                $similar[] = $res;
            }
        }

        // customer also like products
        $oldOrders = Order::select("product_id")->distinct('product_id')->get();
        if(count($oldOrders) > 0) {
            $arrOldOrder = array();
            foreach ($oldOrders as $oldOrder) {
                $arrOldOrder[] = $oldOrder->product_id;
            }

            $liked_product = Product::whereIn('id', $arrOldOrder)->where(["deleteStatus"=>1,"status"=>1])->get();
            $curtomer_liked = array();
            
            foreach ($liked_product as $ke) {
            
                if ($ke->images) {
                    $img = explode(',', $ke->images);
                    $res['image'] = $this->baseurl . $img[0];
                } else {
                    $res['image'] = url('/public/') . "/img/imagenotfound.jpg";
                }
                $res['product_id'] = $ke->id;
                $res['name'] = $ke->name;
                $res['mrp'] = $ke->mrp * $currencyValue;
                $res['discount'] = round($ke->discount);
                $res['brand'] = $ke->brand;
                $percentage = 100 - ($val->sp * $currencyValue * 100) / $val->mrp * $currencyValue;
                $res['percentage'] = round($percentage);
                $res['sp'] = $val->sp * $currencyValue;
                $curtomer_liked[] = $res;
            }
        } else {
            $liked_product = Product::where('subsubcategory_id', $productdetails_list->subsubcategory_id)->where(["deleteStatus"=>1,"status"=>1])->get();
            $curtomer_liked = array();
            
            foreach ($liked_product as $ke) {
            
                if ($ke->images) {
                    $img = explode(',', $ke->images);
                    $res['image'] = $this->baseurl . $img[0];
                } else {
                    $res['image'] = url('/public/') . "/img/imagenotfound.jpg";
                }
                $res['product_id'] = $ke->id;
                $res['name'] = $ke->name;
                $res['mrp'] = $ke->mrp * $currencyValue;
                $res['discount'] = round($ke->discount);
                $res['brand'] = $ke->brand;
                $percentage = 100 - ($val->sp * $currencyValue * 100) / $val->mrp * $currencyValue;
                $res['percentage'] = round($percentage);
                $res['sp'] = $val->sp * $currencyValue;
                $curtomer_liked[] = $res;
            }
        }

        // Cart data
        $cartData = 0;
        if(Auth::check()) {
            $getCart = Cart::where(["user_id"=>Auth::user()->id, "product_id"=>$id])->first();

            if($getCart) {
                $cartData = 1;    
            } else {
                $cartData = 0;
            }
        } else {
            if ($request->session()->has('cartsession')) {
                $session_id = session::get('cartsession');

                $getCart = Cart::where(["user_id"=>$session_id, "product_id"=>$id])->first();
                if($getCart) {
                    $cartData = 1;    
                } else {
                    $cartData = 0;
                }   

            } else {
                $cartData = 0;
            }
        }

        $attriArr = array();
        $getAttri = Attribute::select('label','labeltext')->where(["product_id"=>$id])->get();
        if($getAttri) {
            foreach($getAttri as $getAttr) {
                $attriArr[]= ["labal"=>$getAttr->label,"text"=>$getAttr->labeltext];
            }
        }

        if(!empty($productdetails_list->defaultcolor)) {
            $colorName = Color::select('name')->where(["hexcode"=>$productdetails_list->defaultcolor])->first();
            if($colorName) {
                $defaultcolorname = $colorName->name;
            } else {
                $defaultcolorname = "";
            }
        } else {
            $defaultcolorname = "";
        }

        $saleingPrice = $productdetails_list->sp * $currencyValue;
        $mrpPrice = $productdetails_list->mrp * $currencyValue;

        $percentage = 100 - ($saleingPrice*100)/$mrpPrice;

        $return_array = array("product_id"=>$id,
                              "image_list"=>$arrayimage1,
                              "brand"=>$productdetails_list->brand,
                              "name"=>$productdetails_list->name,
                              "description"=>$productdetails_list->description,
                              "mrp"=>$mrpPrice,
                              "sp"=>$saleingPrice,
                              "qty"=>$productdetails_list->quantity,
                              "percentage"=>round($percentage),
                              "size"=>explode(",",$productdetails_list->size),
                              "color"=>explode(",",$productdetails_list->color),
                              "defaultcolor"=>$productdetails_list->defaultcolor,
                              "defaultcolorname"=>$defaultcolorname,
                              "defaultcolorcode"=>$productdetails_list->defaultcolor,
                              "sizechart"=>$productdetails_list->sizechart?$this->baseurl . $productdetails_list->sizechart:'',
                              "expected"=>$productdetails_list->ships_in,
                              "bestoffer"=>$otherOfferArr,
                              "bankoffer"=>$bankOfferArr,
                              "productcode"=>$productdetails_list->hsn_code,
                              "sold_by" => $seller_name,
                              "fit"=>$productdetails_list->fit,
                              "length"=>$productdetails_list->length,
                              "similars"=>$similar,
                              "likedpros"=>$curtomer_liked,
                              "haveCart"=>$cartData,
                              "attributes"=>$attriArr,
                              "is_variant"=>$productdetails_list->is_variant,
                              "variant_type"=>$productdetails_list->variant_type,
                              "size_label"=>$productdetails_list->size_label,
                        );
       // echo "<pre>";print_r($return_array);die;

        $getdata = SizeChart::where('subsub_cat_id',$productdetails_list->subsubcategory_id)->get()->toArray();
        $subName = Subcategory::where('id',$productdetails_list->subcategory_id)->first();
        $subsubName = Subsubcategory::where('id',$productdetails_list->subsubcategory_id)->first();
        $urlname = ['first'=>$productdetails_list->category_id, 'second'=>$subName->name, 'subid'=> $subName->id, 'subsubname'=>$subsubName->name, 'subsubid'=>$subsubName->id];

        return view('pdesc', compact('return_array','urlname','getdata','productImageColor'));
    }


    function getColorImage(Request $request) {
       //echo "<pre>";print_r($request->toArray());die;
       $color = $request->color;
       $pro_id = $request->pro_id;
       $product_images = ProductImageColor::where(['product_id'=>$pro_id,'color'=>$color])->first();
       $colorName = Color::select('name')->where(["hexcode"=>$color])->first();
       $color_name = $colorName->name;
       $productdetails_list = Product::find($pro_id);
       $subName = Subcategory::where('id',$productdetails_list->subcategory_id)->first();
       $subsubName = Subsubcategory::where('id',$productdetails_list->subsubcategory_id)->first();
        $urlname = ['first'=>$productdetails_list->category_id, 'second'=>$subName->name, 'subid'=> $subName->id, 'subsubname'=>$subsubName->name, 'subsubid'=>$subsubName->id];
        //echo $subsubName->name;die;
      // echo $subsubname = $urlname['subsubname'];die;
       $final =  $color_name.' '.$urlname['subsubname'];
       $anchor = "<a href=".route('products', [$urlname['first'], base64_encode('subsubcat'), base64_encode($urlname['subsubid']), base64_encode($color), base64_encode('0') ]).">"; 
       $anchor .= "More  $final <i class='fa fa-angle-right'></i>";
       $anchor .= "</a>";
       
       if($color_name){
        $color_name1 = $color_name;
       } else {
        $color_name1 ='';
       }
       $return_array = explode(',',$product_images['images']);
       //echo "<pre>";print_r($return_array);die;

       $view = View::make('color_image',compact('return_array'));
        $contents = $view->render();
       $returnArr = array('color_name'=>$color_name1,'contents'=>$contents,'url'=>$anchor);
       return json_encode($returnArr);

    }

    public function getVariantPrice(Request $request) {
        $input = $request->all();
        $productPrices = ProductPrice::where(["product_id"=>$request->pro_id, "size"=>$request->size, "color"=>$request->color])->first();
        if($productPrices) {
            $returnArr = array('status'=>'success', 'sp'=>$productPrices->sp, 'mrp'=>$productPrices->mrp, 'discount'=>$productPrices->discount);
        } else {
            $returnArr = array('status'=>'error', 'sp'=>'', 'mrp'=>'', 'discount'=>'');
        }
        return json_encode($returnArr);
    }

}
