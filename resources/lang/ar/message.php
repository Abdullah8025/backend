<?php
//arabic
return [
 
 'phone_dones_not_exit' => 'Phone Number does not exist.',
 'login_success' => 'تسجيل الدخول بنجاح. ',
 'user_not_exit' => 'المستخدم غير موجود ',
 'register_sucess' => 'تم التسجيل بنجاح ',
 'inactive_account'=>'حسابك غير نشط ',
 'token_expire'=>'انتهت صلاحية رمز تسجيل الدخول ',
 'old_pass_wrong'=>'كلمة السر القديمة غير صحيحة. ',
 'password_changed'=>'تم تغيير الرقم السري بنجاح ',
 'logout_sucess'=>'تم تسجيل الخروج بنجاح ',
 'profile_retrived' => 'تمت استعادة الملف الشخصي بنجاح ',
 'profile_update' => 'تم تحديث الملف الشخصي بنجاح ',
 'address_retrived' => 'تم استرداد قائمة العناوين بنجاح ',
 'address_error' => 'غير قادر على حفظ أكثر من ثلاثة عناوين ',
 'address_added' => 'تمت إضافة العنوان بنجاح ',
 'address_updated' => 'تم تحديث العنوان بنجاح ',
 'address_not_found' => 'لم يتم العثور على العنوان ',
 'address_removed' => 'تمت إزالة العنوان بنجاح ',
 'notification_status' => 'تم تغيير حالة الإشعار بنجاح ',
 'notification_retrieved' => 'تم استرداد قائمة الإشعارات بنجاح ',
 'detail_retrived' => 'تم استرداد التفاصيل بنجاح ',
 'categiry_retrived' => 'تم استرداد قائمة الفئات بنجاح ',
 'list_retrieved' => 'تم استرداد القائمة بنجاح ',
 'product_removed' => 'تمت إزالة المنتج من قائمة الرغبات بنجاح ',
 'product_added' => 'تمت إضافة المنتج إلى قائمة الرغبات بنجاح ',
 'store_removed' => 'تمت إزالة المتجر من قائمة الرغبات بنجاح ',
 'store_added' => 'تمت إضافة المتجر إلى قائمة الرغبات بنجاح ',
 'wishlist_retrieved' => 'تم استرداد قائمة الرغبات بنجاح ',
 'item_already_added' => 'تمت إضافة العنصر بالفعل في سلة التسوق ',
 'product_added' => 'تمت إضافة المنتج إلى سلة التسوق بنجاح ',
 'item_already_added_store'=>'تمت إضافة العنصر بالفعل في سلة التسوق من المتجر ',
 'added_cart' => 'تمت إضافة العنصر بالفعل في سلة التسوق ',
 'item_removed_cart' =>'تمت إزالة العنصر من سلة التسوق ',
 'cart_items' => 'تم تحديث عناصر سلة التسوق ',
 'out_of_stock' => 'العنصر نفد مخزونه ',
 'cart_items_updated' => 'تم تحديث عناصر سلة التسوق ',
 'item_not_found' => 'العنصر غير موجود في سلة التسوق ',
 'no_item_in_cart' => 'لا يوجد عناصر في عربة التسوق ',
 'coupen_retrived' => 'تم استرداد قائمة القسائم بنجاح ',
 'coupen_used' => 'لقد استخدمت هذه القسيمة بالفعل! ',
 'coupen_not_found' => 'رمز القسيمة غير موجود ',
 'unlock_coupen' => 'تسوق أكثر لفتح رمز القسيمة! ',
 'invalid_coupen' => 'رمز القسيمة غير صالح لهذه المنتجات! ',
 'coupen_applied' => 'تم تطبيق القسيمة بنجاح ',
 'coupen_not_apply' => 'لا يمكن تطبيق رمز القسيمة ',
 'coupen_cancel' => 'تم إلغاء القسيمة بنجاح ',
 'order_placed' => 'تم إتمام طلبك بنجاح ',
 'order_retrived' => 'تم استرداد قائمة الطلبات بنجاح. ',
  'order_detail_retrived' => 'تم استرداد تفاصيل الطلب بنجاح ',
 'already_rated'=>'لقد قمت بالتقييم بالفعل عند الطلب  ',
 'rating_saved' => 'تم حفظ التقييم بنجاح ',
 'cancel_request' => 'لقد سبق لك طلب الإلغاء! ',
 'request_submitted' => 'تم إرسال طلبك، سنرد عليك قريبًا. ',
 'reorder_sucess' => 'تمت إعادة الطلب بنجاح ',
 'review_added' => 'تمت إضافة المراجعة بنجاح ',
 'review_retrived' => 'تمت استعادة قائمة المراجعات بنجاح ',
 'cart_retrived' => 'Cart list retrieved successfully',

 'order_new' => 'تم تقديم طلبك الجديد',
 'order_shiped' => 'وقد تم شحن طلبك',
 'order_ready' => 'لقد كان طلبك جاهزًا',
 'order_deliverd' => 'تم تسليم طلبك',
 'order_cancel' => 'تم إلغاء طلبك',
 ];