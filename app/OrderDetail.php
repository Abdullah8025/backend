<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    
    protected $fillable = ['order_id', 'product_id', 'quantity', 'price', 'sale_price', 'discount_price', 'total_price', 'seller_shipping', 'valuable_tax','gst_tax','tcs_tax','final_price','seller_amount','admin_commision','total_shipping','rating'];
}
