<?php
//english
return [
 
 'phone_dones_not_exit' => 'Phone Number does not exist.',
 'login_success' => 'Login successfully.',
 'user_not_exit' => 'User does not exist',
 'register_sucess' => 'Registered successfully',
 'inactive_account'=>'Your account is inactive',
 'token_expire'=>'Login Token Expire',
 'old_pass_wrong'=>'Old password is incorrect.',
 'password_changed'=>'Password changed successfully',
 'logout_sucess'=>'Logout successfully',
 'profile_retrived' => 'Profile retrived successfully',
 'profile_update' => 'Profile updated successfully',
 'address_retrived' => 'Address list retrieved successfully',
 'address_error' => 'Address not able to save more than three',
 'address_added' => 'Address added successfully',
 'address_updated' => 'Address updated successfully',
 'address_not_found' => 'Address not found',
 'address_removed' => 'Address removed successfully',
 'notification_status' => 'Notification status change successfully',
 'notification_retrieved' => 'Notification list retrieved successfully',
 'detail_retrived' => 'Details retrieved successfully',
 'categiry_retrived' => 'Category list retrieved successfully',
 'list_retrieved' => 'List retrieved successfully',
 'product_removed' => 'Product removed from wishlist successfully',
 'product_added_wishlist' => 'Product added to wishlist successfully',
 'store_removed' => 'Store removed from wishlist successfully',
 'store_added' => 'Store added to wishlist successfully',
 'wishlist_retrieved' => 'Wishlist list retrieved successfully',
 'item_already_added' => 'Item already added in cart',
 'product_added' => 'Product added to Cart successfully',
 'item_already_added_store'=>'Item already added in cart from store',
 'added_cart' => 'Item already added in cart from store',
 'item_removed_cart' =>'Item removed from cart',
 'cart_items' => 'Cart items updated',
 'out_of_stock' => 'Item out of stock',
 'cart_items_updated' => 'Cart items updated',
 'item_not_found' => 'Item not found in cart',
 'no_item_in_cart' => 'No Item in a cart',
 'coupen_retrived' => 'Coupon list retrieved successfully',
 'coupen_used' => 'You have already used this coupon!',
 'coupen_not_found' => 'Coupon Code Not Found',
 'unlock_coupen' => 'Shop more to unlock coupon code!',
 'invalid_coupen' => 'Coupon code not valid for this products!',
 'coupen_applied' => 'Coupon applied successfully',
 'coupen_not_apply' => 'Coupon Code Not able to apply',
 'coupen_cancel' => 'Coupon canceled successfully',
 'order_placed' => 'Your order has been placed successfully',
 'order_retrived' => 'Order list retrieved successfully.',
 'order_detail_retrived' => 'Order detail retrieved successfully.',
 'already_rated'=>'Already rated on order',
 'rating_saved' => 'Rating saved successfully',
 'cancel_request' => 'You have already request for cancellation!',
 'request_submitted' => 'Your request submited, We notify soon.',
 'reorder_sucess' => 'Reoder successfully',
 'review_added' => 'Review added successfully',
 'review_retrived' => 'Review list retrived successfully',
 'cart_retrived' => 'Cart list retrieved successfully',

 'order_new' => 'Your new order has been placed',
 'order_shiped' => 'Your order has been Shipped',
 'order_ready' => 'Your order has been Ready',
 'order_deliverd' => 'Your order has been Delivered',
 'order_cancel' => 'Your order has been cancelled',
 ];