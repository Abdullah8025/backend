<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// Users API

Route::get('anas','API\User\UserController@anas')->middleware('Localization');

Route::post('verify_phone','API\User\UserController@verifyphone');
Route::post('verify_signup','API\User\UserController@verifysignup');
Route::post('login','API\User\UserController@login');
Route::post('signup','API\User\UserController@signup');
Route::post('sociallogin','API\User\UserController@sociallogin');
Route::post('user_password_change','API\User\UserController@changepassword');
Route::post('user_profile','API\User\UserController@profile');
Route::post('user_edit_profile','API\User\UserController@editprofile');

Route::post('logout','API\User\UserController@logout');
Route::post('notifystatus','API\User\UserController@notifystatus');

Route::post('user_address','API\User\UserController@address');
Route::post('user_add_address','API\User\UserController@addaddress');
Route::post('user_edit_address','API\User\UserController@editaddress');
Route::post('user_remove_address','API\User\UserController@removeaddress');
Route::post('default_address','API\User\UserController@defaultaddress');

Route::get('faq','API\User\StaticController@faq');
Route::get('about-us/{type?}','API\User\StaticController@aboutus');
Route::get('tandc/{type?}','API\User\StaticController@tandc');
Route::get('privacy-policy/{type?}','API\User\StaticController@privacypolicy');
Route::get('supprot','API\User\StaticController@supprot');
Route::get('marketing','API\User\StaticController@marketing');
Route::post('contact-us','API\User\StaticController@contact');

Route::post('home_product','API\User\ShopController@homeproduct');
Route::get('categorylist','API\User\ShopController@categorylist');
Route::post('search','API\User\ShopController@searching');
Route::post('product_detail','API\User\ShopController@productdetail');
Route::post('products_type','API\User\ShopController@productsbytype');
Route::post('homesearch','API\User\ShopController@homesearch');
Route::post('offer_detail','API\User\ShopController@offerDetail');
Route::get('testnotification','API\User\ShopController@testnotification');

Route::post('store_detail','API\User\ShopController@storedetail');

Route::post('addtowish','API\User\ShopController@addtowish');
Route::post('wishlistproduct','API\User\ShopController@wishlistproduct');

Route::post('addtocart','API\User\ShopController@addtocart');
Route::post('removetocart','API\User\ShopController@removetocart');
Route::post('cart_list','API\User\ShopController@cartlist');
Route::post('updatecart','API\User\ShopController@updatecart');

Route::get('coupons','API\User\ShopController@coupons');
Route::post('apply_coupon','API\User\ShopController@apply_coupon');
Route::post('cancel_coupon','API\User\ShopController@cancel_coupon');

Route::post('place_order',"API\User\ShopController@placeorder");

Route::post('upcoming_orders',"API\User\ShopController@upcomingorders");
Route::post('past_orders',"API\User\ShopController@pastorders");
Route::post('order_detail',"API\User\ShopController@orderdetail");
Route::post('order_rating',"API\User\ShopController@orderrating");
Route::post('cancel_order','API\User\ShopController@cancelorder');
Route::post('reorder','API\User\ShopController@reorder');
Route::post('add_review','API\User\ShopController@addreview');
Route::any('review_list/{type}/{id}','API\User\ShopController@reviewlist');

Route::post('notficationlist','API\User\UserController@notficationlist');//
Route::post('notiication_count','API\User\ShopController@notiicationCount');


Route::any('referal_list/{id}','API\ServiceController@referal_list');
Route::any('generateordercode',"API\ServiceController@generateOrderid");
Route::any('myorderhelp',"API\ServiceController@myorderhelp");

Route::any('cartoffer',"API\ServiceController@cartoffer");
Route::get('codavailable/{id}',"API\ServiceController@codavailable");









Route::post('allrequestlist1','API\ServiceController@allrequestlist1');

Route::post('getImageByColor','API\ServiceController@getImageByColor');

Route::post('set_as_default','API\ServiceController@set_as_default');

//Route::get('catdeleteb/{id}','API\ServiceController@catdeleteb');
//Route::get('catdelete11/{id}','API\ServiceController@catdelete11');
Route::get('catdelete113/{id}','API\ServiceController@catdelete113');  // coupon
Route::get('catdelete11pp/{id}','API\ServiceController@catdelete11pp'); // Flash sale


Route::get('order-invoice/{oid}','API\ServiceController@orderinvoice');
Route::get('order-manifiest/{oid}','API\ServiceController@ordermanifest');
Route::get('order-manifiest1/{oid}','API\ServiceController@ordermanifest1');

Route::get('orderlistingseller/{id}','API\ServiceController@orderlistingseller');

Route::get('buyerorderlisting/{id}','API\ServiceController@buyerorderlisting');

Route::get('bannerdetailsbyid/{id}','API\ServiceController@bannerdetailsbyid');

Route::get('catlistforadmin1234','API\ServiceController@catlistforadmin1234');


Route::get('productids','API\ServiceController@productids');


Route::post('changepasswordbyphone','API\ServiceController@changepasswordbyphone');

Route::post('checkphonedata','API\ServiceController@checkphonedata');

Route::post('setdefault','API\ServiceController@setdefault');

Route::get('orderrequest/{id}','API\ServiceController@orderrequest');

Route::any('indipay/response','API\ServiceController@response');



// Common for admin and seller

Route::get('getproductdetails/{id}','API\ServiceController@getproductdetails');
Route::get('productstatuschange/{id}','API\ServiceController@productstatuschange'); // product inactive
Route::post('updatestock','API\ServiceController@updatestock');
Route::post('updateprice','API\ServiceController@updateprice');
Route::get('productdelete/{id}','API\ServiceController@productdelete');

Route::get('orderdetails/{id}','API\ServiceController@orderdetails');

Route::get('category_list_dropdown','API\ServiceController@catlistfordropdown');
Route::get('ar_category_list_dropdown','API\ServiceController@arcatlistfordropdown');
Route::get('sizelist_fordropdown','API\SellerController@sizelistfordropdown');
Route::get('ar_sizelist_fordropdown','API\ServiceController@ar_sizelist_fordropdown');

Route::post('fileuploade1','API\ServiceController@fileuploade1');  // sellergstnumfile
Route::post('fileuploadebanner','API\ServiceController@fileuploadebanner');  // Banner
Route::post('fileuploadecat','API\ServiceController@fileuploadecat');  // catimages
Route::post('fileuploade','API\ServiceController@fileuploade');  // Product images

// Admin

Route::any("adminlogin","API\AdminController@adminlogin");
Route::post('updateadminprofile/{id}','API\AdminController@updateadminprofile');
Route::post('changepasswordforadmin/{id}','API\AdminController@changepasswordforadmin');

Route::any('dashoardforadmin','API\AdminController@dashoardforadmin');
Route::any('rating_list','API\AdminController@ratingList');
Route::post('addSubAdmin','API\AdminController@addSubAdmin');
Route::get('subAdminList','API\AdminController@subAdminList');
Route::post('subAdminEdit','API\AdminController@subAdminEdit');
Route::post('subAdminUpdate','API\AdminController@subAdminUpdate');
Route::get('subadminstatuschange/{id}','API\AdminController@subadminstatuschange'); //active and inactive

Route::any('payment_list','API\AdminController@paymentList');
Route::any('payout_list','API\AdminController@payoutList');
Route::post('payoutSubmit','API\AdminController@payoutSubmit');
Route::get('payout_history/{id}','API\AdminController@payoutHistory');

Route::get('userlistforadmin','API\AdminController@userlistforadmin');
Route::get('userviewforadmin/{id}','API\AdminController@viewprofileuseradmin');
Route::get('userdelete/{id}','API\AdminController@userdelete');
Route::get('userstatuschange/{id}','API\AdminController@userstatuschange');

Route::get('sellerlistforadmin','API\AdminController@sellerlistforadmin');
Route::get('sellerviewforadmin/{id}','API\AdminController@viewprofileselleradmin');
Route::get('seller_status_change/{id}','API\AdminController@sellerstatuschange'); //seller active / inactive
Route::get('sellerloginbyadmin/{id}','API\AdminController@sellerloginbyadmin');
Route::get('sellerdelete/{id}','API\AdminController@sellerdelete');
Route::post('updatecommission','API\AdminController@updatecommission');

Route::get('userlistfornotification','API\AdminController@userlistfornotification');
Route::post('sendnotification','API\AdminController@sendpushnotification');

Route::get('category_list/{id?}','API\AdminController@getcategorylist');

Route::get('category_single/{id}','API\AdminController@getcategorysingle');
Route::post('add_category','API\AdminController@addcategory');
Route::post('category_update/{id}','API\AdminController@categoryupdate');
Route::get('category_delete/{id}','API\AdminController@catdelete');
Route::get('category_status_change/{id}','API\AdminController@categorystatuschange'); // category active / inactive

Route::get('productlistforadmin/{id}','API\AdminController@productlistforadmin');
// Route::post('editproductbyadmin/{id}','API\AdminController@editproductbyadmin');
Route::post('productdisablecod','API\AdminController@disablecod');

Route::post('orderlisting/{id}','API\AdminController@orderlisting');

Route::get('getcontent/{type}','API\AdminController@getcontent');
Route::post('content','API\AdminController@content');

Route::get('faqlist','API\AdminController@faqlist');
Route::post('addfaq','API\AdminController@addfaq');
Route::get('deletefaq/{id}','API\AdminController@deletefaq');

Route::post('createbanner','API\AdminController@createbanner');
Route::get('bannerlistforadmin','API\AdminController@bannerlistforadmin');
Route::get('deleteBanner/{id}','API\AdminController@deleteBanner');

Route::get('allsellerpayment','API\AdminController@allsellerpayment');

Route::get('aboundedlist','API\AdminController@abounded');

Route::post('createcode','API\AdminController@createcode');
Route::get('cuponlist','API\AdminController@cuponlist');
Route::get('cuponDelete/{id}','API\AdminController@cuponDelete');

//offer mang

Route::get('seller_rating_list/{id}','API\SellerController@ratingList');
Route::any('seller_payment_list/{id}','API\SellerController@paymentList');
Route::post('productlistforoffer','API\SellerController@productlistforoffer');
Route::post('addproductoffer','API\SellerController@addproductoffer');
Route::get('offerlistingforseller/{id}','API\SellerController@offerlistingforseller');


Route::post('addoffer','API\ServiceController@addoffers');
Route::get('offerlist','API\ServiceController@offerlists');
Route::get('offerdelete/{id}','API\ServiceController@offerdelete');

Route::post('updaterefer','API\ServiceController@updaterefer');
Route::get('getrefer','API\ServiceController@getrefer');

Route::get('adminsupportticketlist','API\ServiceController@adminsupportticketlist');
Route::post('supportticketupdate','API\ServiceController@supportticketupdate');


//Route::post('createtheme','API\ServiceController@createtheme');
//Route::get('themelistforadmin','API\ServiceController@themelistforadmin');
//Route::get('catdeletet/{id}','API\ServiceController@catdeletet');

//Route::get('catlistforadmin','API\ServiceController@catlistforadmin');
//Route::get('catlistforadmin1/{id}','API\ServiceController@catlistforadmin1');


Route::get('exchange-order/{id}/{status}',"API\ServiceController@exchangeorder");
Route::get('orderreturnadmin','API\ServiceController@orderreturnadmin');
Route::get('ordercanceladmin','API\ServiceController@ordercanceladmin');

//Route::post('productSearchForFlashSale','API\AdminController@productSearchForFlashSale');

//Route::get('categoryapprovallist','API\AdminController@categoryApprovalList');

//Route::post('changecategoryapproval','API\AdminController@changecategoryapproval')

Route::get('shippingorderlisting','API\AdminController@shippingorderlisting');

//Route::any('adminpromotionlist',"API\AdminController@promotionlist");
//Route::any('adminpromotionproductlist/{id}',"API\AdminController@promotionproductlist");
//Route::any('adminpromotionproductstatus/{id}',"API\AdminController@promotionproductstatus");

//Subcatlist for offer
//Route::any('sublistforoffer/{id}',"API\AdminController@sublistforoffer");





//Seller
Route::post('checkphoneseller','API\SellerController@checkphoneseller');
Route::post('checkemailseller','API\SellerController@checkemailseller');

Route::post("sellersignup","API\SellerController@signup");
Route::any("sellersignup1","API\SellerController@signupother");
Route::any("sellerlogin","API\SellerController@sellerlogin");
Route::any("sellerlogin1","API\SellerController@sellerlogin1");

Route::any("getsellerlogin","API\SellerController@getsellerlogin");

Route::any("forgotpasswordseller","API\SellerController@forgotpasswordseller");
Route::post('updatesellerprofile/{id}','API\SellerController@updatesellerprofile');
Route::post('changepasswordforseller/{id}','API\SellerController@changepasswordforseller');

Route::any('dashoardforseller/{id}','API\SellerController@dashoardforseller');

Route::get('productlistforseller/{id}','API\SellerController@productlistforseller');
Route::post('csv_uploade_for_product/{id}','API\SellerController@csv_uploade_for_product');

Route::get('checkcategoryapproval/{id}','API\SellerController@checkcategoryapproval');
Route::post('categoryapproval','API\SellerController@categoryapproval');

Route::post('sellercategorylist','API\SellerController@sellerCategoryList');
Route::post('sellersubcategorylist','API\SellerController@sellerSubCategoryList');
Route::post('sellersubsubcategorylist','API\SellerController@sellerSubSubCategoryList');

Route::post('unique_sku','API\SellerController@uniquesku');
Route::post('addproductbyseller/{id}','API\SellerController@addproductbyseller');
Route::post('editproductbyseller/{id}','API\SellerController@editproductbyseller');

Route::post('addsize','API\SellerController@addsize');
Route::post('getsize','API\SellerController@getsize');
Route::get('sizelist','API\SellerController@sizelist');
Route::get('sizelist_dropdown','API\SellerController@sizelistfordropdown');
Route::get('sizedelete/{id}','API\SellerController@sizedelete');

Route::any('sellerorderlisting/{id}','API\SellerController@sellerorderlisting');
Route::any('sellerorderlisting1/{id}','API\SellerController@sellerorderlisting1'); // Aceepted
Route::any('sellerorderlisting2/{id}','API\SellerController@sellerorderlisting2'); // dispatched - RTD
Route::any('sellerorderlisting3/{id}','API\SellerController@sellerorderlisting3');  // transit
Route::any('sellerorderlisting4/{id}','API\SellerController@sellerorderlisting4');  // Delivered
// Route::get('sellerorderlisting5/{id}','API\ServiceController@sellerorderlisting5');  // Pending

Route::post('changethestatusoforder','API\ServiceController@changethestatusoforder');
Route::get('deleteorder/{id}','API\ServiceController@deleteorder');
Route::get('orderreturnseller/{id}','API\ServiceController@orderreturnseller');
Route::get('ordercancelseller/{id}','API\ServiceController@ordercancelseller');

Route::any('payreport/{id}',"API\SellerController@payreport");
Route::get('performanceseller/{id}','API\SellerController@performanceseller');

Route::get('sellersupportticketlist/{id}','API\SellerController@sellersupportticketlist');
Route::post('supportticketadd/{id}','API\SellerController@supportticketadd');


Route::any('promotionoffer',"API\SellerController@promotionoffer");
Route::any('promotion/{id}',"API\SellerController@promotion");
Route::any('promotionlist/{id}',"API\SellerController@promotionlist");
Route::any('promotiondelete/{id}',"API\SellerController@promotiondelete");

Route::post('sellerordercsv/{id}','API\ServiceController@sellerordercsv');

Route::post('langtypesave','API\ServiceController@langtypesave');

// Route::get('attribute-groups/{id}','API\SellerController@attributegroupList');
// Route::post('attribute-group','API\SellerController@attributegroupadd');
// Route::get('attribute-edit/{id}','API\SellerController@attributegroupedit');
// Route::post('attribute-edit','API\SellerController@attributegroupupdate');

// Route::get('attribute-options/{id}','API\SellerController@attributeoptionList');
// Route::post('attribute-option','API\SellerController@attributeoptionadd');
// Route::get('attribute-option-edit/{id}','API\SellerController@attributeoptionedit');
// Route::post('attribute-option-edit','API\SellerController@attributeoptiondate');





// CRON JOB

Route::get('checkthestatusoforder','API\CronController@checkthestatusoforder');
Route::get('checkthestatusofmanifestorder','API\CronController@checkthestatusofmanifestorder');
Route::get('performance','API\CronController@performance');
Route::get('payout','API\CronController@payout');
Route::get('orderpendingcheck','API\CronController@orderpendingcheck');
// Currency Rate
Route::get('exchangerate','API\CronController@exchangerate');

Route::any('pincodecheck','API\ServiceController@pincodecheck');
