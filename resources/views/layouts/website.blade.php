<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <title>Honey App</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
   @include('includes.head')
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

</head>

<body>

    <header>
        @include('includes.header')
       
    </header>

     @yield('content')


    <footer>
    	 @include('includes.footer')

    </footer>

    <script src="{{URL('/')}}/public/js/owl.carousel.min.js"></script>
    <script src="{{URL('/')}}/public/js/bootstrap.min.js"></script>

    @stack('script')   

</body>
</html>