<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    //

    protected $fillable = [
         'image', 'name', 'focus_status','admin_status','description','seller_id','deleteStatus'
    ];
}
