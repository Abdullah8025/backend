@extends('layouts.app')

@section('content')

  <section>
        <div class="TermsArea">
            <div class="container">
                <h3>@if($lang=='ar') سياسة خاصة @else Privacy policy @endif</h3>

                <?php if($lang=='ar') {echo ucfirst($detail['ar_value']);}else{echo $detail['value'];} ?>            </div>
        </div>
    </section>


@endsection