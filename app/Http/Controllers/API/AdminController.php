<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use DB;
use App\Seller;
use App\User;
use App\Token;
use App\Address;
use App\Product;
use App\Order;
use App\OrderDetail;
use App\OrderRequest;
use App\Admin;
use App\Category;
use App\Banner;
use App\Theme;
use App\Flashsale;
use App\Wishlist;
use App\Cart;
use App\Couponcode;
use App\Content;
use App\Faq;
use App\Notification;
use App\Size;
use App\Offer;
use App\ProductOffer;
use App\Refer;
use App\Promotion;
use App\PromotionList;
use App\Attribute;
use App\ReferEarn;
use App\Support;
use App\SizeChart;
use App\ProductImageColor;
use App\Manifest;
use App\SellerPerformance;
use App\SellerMistake;
use App\PayoutRecord;
use App\SellerApprove;
use App\OrderRating;
use App\SellerWallet;
use App\SellerHistory;
use Image;
use Validator;
use Mail;
use Intervention\Image\ImageServiceProvider;
use Softon\Indipay\Facades\Indipay;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use PDF;

class AdminController extends APIBaseController {

    private $baseurlslash;
    private $baseurl;
    
    public function __construct() {
        $this->baseurlslash = "http://54.152.130.226/honey_app/public/";
        $this->baseurl = "http://54.152.130.226/honey_app/public";
    }

    public function adminlogin(Request $request) {
        $input = $request->all();
        $detailsother = Admin::where(['email' => $input['username'], 'password' => $input['password']])->first();
        if (!empty($detailsother)) {
            return $this->sendResponse($detailsother, 'Logged in successfully.', $request->path());
        } else {
            return $this->sendError($request->path(), "Your Username or password is incorrect");
        }
    }

    public function updateadminprofile(Request $request, $id) {
        $input = $request->all();
        $details = Admin::find($id);
        $details->name = $input['name'];
        if ($input['image'] != $details->image) {
            $details->image = $this->baseurlslash . $input['image'];
        }
        $details->phone = $input['phone'];
        $details->email = $input['email'];
        $details->save();

        return $this->sendResponse1($details, 'Profile updated successfully', $request->path());
    }

    public function changepasswordforadmin(Request $request, $id) {
        $input = $request->all();
        $post = Admin::find($id);
        if ($post->password != $input['oldpassword']) {
            return $this->sendError($request->path(), "You enter incorrect old password");
        }
        $post->password = $input['password'];
        $post->save();
        return $this->sendResponse(['status' => 'success'], 'Password updated successfully.', $request->path());
    }

    public function dashoardforadmin(Request $request) {
        $result = array();

        $orderids = array();
        $getOrderids = Order::select('id')->get();
        foreach($getOrderids as $getOrderid) {
            $orderids[] = $getOrderid->order_id;
        }

        $orderid1s = array();
        $getOrderid1s = Order::select('id')->where(["status"=>'3'])->get();
        foreach($getOrderid1s as $getOrderid1) {
            $orderid1s[] = $getOrderid1->order_id;
        }

        if(isset($request->dateframe)) {
            $dateFrame = $request->dateframe;

            if($dateFrame == 6) {
              //  echo Carbon::now()->subMonths(6);die;
                $totalOrder = Order::where("created_at",">", Carbon::now()->subMonths(1))->count();

               // $revenue = OrderDetail::select("final_price")->whereIn('order_id', $orderid1s)->where("created_at",">", Carbon::now()->subMonths(6))->sum('final_price');
                $revenue  = Order::where('status','3')->where("created_at",">", Carbon::now()->subMonths(6))->sum('final_amt');
                $livePro = Product::where(["status"=>1])->where("created_at",">", Carbon::now()->subMonths(6))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where("created_at",">", Carbon::now()->subMonths(6))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(6))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(6))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 12) {
                $totalOrder = Order::where('created_at', '>=', Carbon::now()->subMonth(12))->count();
                $revenue  = Order::where('status','3')->where("created_at",">", Carbon::now()->subMonths(12))->sum('final_amt');       
                $livePro = Product::where(["status"=>1])->where("created_at",">", Carbon::now()->subMonths(12))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where("created_at",">", Carbon::now()->subMonths(12))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(12))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(12))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 18) {
                $totalOrder = Order::where('created_at', '>=', Carbon::now()->subMonth(18))->count();
                //$revenue = OrderDetail::select("final_price")->where("created_at",">", Carbon::now()->subMonths(18))->whereIn('id', $orderid1s)->sum('final_price');
                $revenue  = Order::where('status','3')->where("created_at",">", Carbon::now()->subMonths(18))->sum('final_amt');
                $livePro = Product::where(["status"=>1])->where("created_at",">", Carbon::now()->subMonths(18))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where("created_at",">", Carbon::now()->subMonths(18))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(18))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(18))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 24) {
                $totalOrder = Order::where('created_at', '>=', Carbon::now()->subMonth(24))->count();
               // $revenue = OrderDetail::select("final_price")->where("created_at",">", Carbon::now()->subMonths(24))->whereIn('id', $orderid1s)->sum('final_price');
                $revenue  = Order::where('status','3')->where("created_at",">", Carbon::now()->subMonths(24))->sum('final_amt');
                $livePro = Product::where(["status"=>1])->where("created_at",">", Carbon::now()->subMonths(24))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where("created_at",">", Carbon::now()->subMonths(24))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(24))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(24))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 30) {
                $totalOrder = Order::where('created_at', '>=', Carbon::now()->subMonth(30))->count();
               // $revenue = OrderDetail::select("final_price")->where("created_at",">", Carbon::now()->subMonths(30))->whereIn('id', $orderid1s)->sum('final_price');
                 $revenue  = Order::where('status','3')->where("created_at",">", Carbon::now()->subMonths(30))->sum('final_amt');
                $livePro = Product::where(["status"=>1])->where("created_at",">", Carbon::now()->subMonths(30))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where("created_at",">", Carbon::now()->subMonths(30))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(30))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(30))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }
            }
        } else {
            $totalOrder = Order::where("created_at",">", $request->startdate)->where("created_at","<", $request->enddate)->count();
          //  $revenue = OrderDetail::select("final_price")->where("created_at",">", $request->startdate)->where("created_at","<", $request->enddate)->whereIn('id', $orderid1s)->sum('final_price');
             $revenue  = Order::where('status','3')->where("created_at",">", $request->startdate)->where("created_at","<", $request->enddate)->sum('final_amt');
            $livePro = Product::where(["status"=>1])->where("created_at",">", $request->startdate)->where("created_at","<", $request->enddate)->count();
            $nonlivePro = Product::whereNotIn("status", [1])->where("created_at",">", $request->startdate)->where("created_at","<", $request->enddate)->count();

            $stateRecords = array();
            $getStates = Order::select('state')->distinct('state')->where("created_at",">", $request->startdate)->where("created_at","<", $request->enddate)->whereIn('id', $orderids)->get();
            $getOrderCount = Order::where("created_at",">", $request->startdate)->where("created_at","<", $request->enddate)->whereIn('id', $orderids)->count();

            foreach($getStates as $getState) {
                $stateCount = Order::where(["state"=>$getState->state])->count();
                $orderper = round(($stateCount/$getOrderCount) * 100);
                $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
            }            
        }

        $orderRecords = OrderDetail::select(
            DB::raw('sum(final_price) as sums'), 
            DB::raw("DATE_FORMAT(created_at,'%m') as months"))->whereYear('created_at', date('Y'))->whereIn('order_id', $orderids)->groupBy('months')->get();
        if(count($orderRecords) > 0) {
            for($i=0;$i<12;$i++) {
                $c = $i+1;
                if(isset($orderRecords[$i])) {
                    $graphArr[] = ["g$c" => (int)$orderRecords[$i]['sums']];
                } else {
                    $graphArr[] = ["g$c" => 0];
                }
            }
        } else {
            for($i=0;$i<12;$i++) {
                $c = $i+1;
                $graphArr[] = ["g$c" => 0];
            }
        }

        $result = [
            "orders" => $totalOrder,
            "revenue" => $revenue,
            "live_product" => $livePro,
            "nonlive_product" => $nonlivePro,
            "state_records"=>$stateRecords,
            "graph"=>$graphArr
        ];
        return $this->sendResponse($result, "Dashboard Response", $request->path());
    }

    public function userlistforadmin(Request $request) {
        $details = User::select("image", "name", "email", "phone", "status", "created_at", "id")->orderby('id', 'desc')->get();
        $return_array = array();
        foreach ($details as $met) {
            if (!empty($met->image)) {
                if (strpos($met->image, $this->baseurlslash) == false) {
                    $met->image = asset('public/'.$met->image);
                }
            } else {
                $met->image = asset("public/img/imagenotfound.jpg");
            }

            $return_array[] = array($met->image, $met->name, $met->phone, date('d-m-Y', strtotime($met->created_at)), "$met->id",$met->status);
            // $return_array[] = array(
            //              'id' => $met->id,
            //              'name' => $met->name,
            //              'email' => $met->email,
            //              'phone' => $met->phone,
            //              'date' => date('d-m-Y', strtotime($met->created_at)),
            //              'image' => $met->image,
                        
            //         );

            
        }
      //  echo "<pre>";print_r($return_array);die;
        return $this->sendResponse1($return_array, 'Buyer list retrieved successfully', $request->path());
    }

    public function userdelete(Request $request, $userid) {
        $details = User::find($userid);
        
        User::where('id', $userid)->delete();
        Cart::where('user_id', $userid)->delete();
        Notification::where('user_id', $userid)->delete();
        Order::where('user_id', $userid)->delete();
        Token::where('user_id', $userid)->delete();
        Wishlist::where('user_id', $userid)->delete();
        return $this->sendResponse1([], 'Buyer deleted successfully', $request->path());
    }

    public function viewprofileuseradmin(Request $request, $id) {

        if(empty($id)) {
            return $this->sendError($request->path(), 'User not found');
        }

        $check_token = Token::where(['user_id' => $id])->first();   
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'User not found');
        }

        $userdetails = User::find($id);
        if (!empty($userdetails->image)) {
            $userdetails->image = asset('public'.$userdetails->image);
        } else {
            $userdetails->image = asset("/img/imagenotfound.jpg");
        }
        
        if (empty($userdetails->dob)) {
            $userdetails->dob = "";
        }
        if (empty($userdetails->gender)) {
            $userdetails->gender = "";
        }
       // $x = $this->getBuyerOrder(2);
        //print_r($x);die;
        $return_array = array("user_id" => (string)$userdetails->id, "image" => $userdetails->image, "name" => $userdetails->name, "email" => $userdetails->email, "phone" => $userdetails->phone, "dob" => $userdetails->dob, "gender" => $userdetails->gender);
        return $this->sendResponse($return_array, 'User details retrieved successfully', $request->path());
    }
    //get buyer order list
    public function getBuyerOrder($userId){
        $details = Order::join('order_details as details', 'orders.id', '=', 'details.order_id')->join('users', 'users.id', '=', 'orders.user_id')->select("orders.id as id","orders.created_at as created", "orders.*", "users.name as buyer",'details.quantity')->where(['orders.user_id' => $userId])->orderBy('orders.id','DESC')->get();

        $return_array = array();
    
        foreach ($details as $met) {
            
            $getSeller = Seller::where(['id' => $met->seller_id])->first();
            $orderCount = 1;
            $final_amount = 0;

            $cartfindall = OrderDetail::where(['order_id' => $met->id])->get();
            if($cartfindall) {

                foreach($cartfindall as $cartfind) {
                
                    $final_amount =  $final_amount + $cartfind->final_price;
                    $orderCount++;
                }
            }

            $getD = strtotime($met->created_at);
            $addedDate = strtotime("+7 day", $getD);
            $currentDate = date('d/m/y');
            $currentConvert = strtotime($currentDate);

            if($currentConvert > $addedDate) {
                $exchangeStatus = 0;
            } else {
                $exchangeStatus = 1;
            }
           // echo $met->status;
            if($met->status==0){
              $order_status = "Pending";
            }else if($met->status == 1) {
                $order_status = "Confirmed";  
            }else if($met->status == 2) {
                $order_status = "Ready";
             }else if($met->status == 3) {
                $order_status = "Out of Delivery ";      
            }else if($met->status == 4) {
                $order_status = "Delivered";
            } else if($met->status == 5) {
                $order_status = "Cancelled";
            } else if($met->status == 6) {
                $order_status = "Return";
            } else if($met->status == 7) {
                $order_status = "Exchange";
            } 

                $return_array[] = array(
                    "id" => $met->id,
                    "image" => $getSeller->image,
                    "item_count"=> $orderCount,
                    "order_number" => $met->order_number,
                    "seller_name" => $getSeller->name, 
                    "dispatch_at" => date("d/m/Y", strtotime($met->dispatch_at)),
                    "order_date" => date("d/m/Y", strtotime($met->created)), 
                    "status" => $order_status, 
                    'quantity' =>$met->quantity,
                    "payment_type" => $met->payment_type,
                    'location' => $met->location, 
                    "amount" => $final_amount,
                );

        }
        return $return_array;
    }

    public function userstatuschange(Request $request, $userid) {
        $details = User::find($userid);
        
        if ($details->status) {
            $details->status = "";
        } else {
            $details->status = "1";
        }
        $details->save();
        return $this->sendResponse1([$details->status, $userid], 'Buyer status updated successfully', $request->path());
    }

    public function sellerlistforadmin(Request $request) {
        $details = Seller::select("cover_image", "name", "email", "phone", "status", "id","commission")->get();
        $return_array = array();
        foreach ($details as $met) {
            if (!empty($met->cover_image)) {
                $met->cover_image = asset('public/'.$met->cover_image);
             } else {
                $met->cover_image = asset("public/img/imagenotfound.jpg");
            }

            $comm = $met->commission;
            if($met->status == 1) {
                $metstatus = 1;
            } else {
                $metstatus = '';
            }
            
            $return_array[] = array($met->cover_image, $met->name, $met->email, $met->phone, [$metstatus, $met->id], $comm, [$met->id,$comm], $met->id);
        }
        return $this->sendResponse1($return_array, 'Buyer list retrieved successfully', $request->path());
    }

    public function viewprofileselleradmin(Request $request, $id) {
        if(empty($id)) {
            return $this->sendError($request->path(), 'Seller not found');
        }
        $check_token = Seller::where(['id' => $id])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Seller not found');
        }

        $userdetails = Seller::find($id);
        if (!empty($userdetails->image)) {
            $userdetails->image = asset($userdetails->image);
        } else {
            $userdetails->image = asset("/img/imagenotfound.jpg");
        }
        
        $return_array = array("user_id" => (string)$userdetails->id, 
                             "image" => $userdetails->image,
                             "name" => $userdetails->name,
                             "email" => $userdetails->email,
                             "phone" => $userdetails->phone,
                             "dob" => $userdetails->gstnum,
                             "gender" => $userdetails->pan_num,
                             "address"=>$userdetails->address,
                             "brand"=>$userdetails->brand,
                        );

        return $this->sendResponse($return_array, 'Seller details retrieved successfully', $request->path());
    }

    public function sellerstatuschange(Request $request, $userid) {
        $details = Seller::find($userid);
        if ($details->status) {
            $details->status = "";
            Product::where(['user_id'=>$userid])->update(['deleteStatus' => 0]);
        } else {
            $details->status = "1";
            Product::where(['user_id'=>$userid])->update(['deleteStatus' => 1]);
        }
        $details->save();
        return $this->sendResponse1([$details->status, $userid], 'Seller status updated successfully', $request->path());
    }

    public function sellerloginbyadmin(Request $request, $id) {
        $detailsother = Seller::where(['id' => $id])->first();
        
        if (!empty($detailsother)) {
            return $this->sendResponse($detailsother, 'Login successfully.', $request->path());
        } else {
            return $this->sendError($request->path(), "Seller not found");
        }
    }

    public function sellerdelete(Request $request, $userid) {
        $details = Seller::find($userid);
        $seller_name = $details->name;
        Seller::where('id', $userid)->delete();
        Product::where('user_id', $userid)->update(['deleteStatus' => 0]);
        return $this->sendResponse1([], $seller_name . ' deleted successfully', $request->path());
    }

    public function updatecommission(Request $request) {
        $check_pro = Seller::where(['id' => $request->id])->first();
        if (empty($check_pro)) {
            return $this->sendError($request->path(), 'Seller not Found');
        }
        DB::table('sellers')->where(['id'=>$request->id])->update(['commission' => $request->commission]);
        return $this->sendResponse(['status' => "success"], 'Commission updated successfully', $request->path());
    }

    public function addSubAdmin(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['email' => 'required', 'name' => 'required', 'password' => 'required', 'phone' => 'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $verifyAdmin = Admin::where(["email"=>$input['email']])->first();

        if($verifyAdmin) {
            return $this->sendError($request->path(), 'Sub-Admin Already Added');
        } else {
            $arrayData = [
                "type"=>"subadmin",
                "email"=>$input['email'],
                "name"=>$input['name'],
                "password"=>$input['password'],
                "phone"=>$input['phone'],
                "permit"=>$input['permit'],
            ];
        }

        $obj = Admin::create($arrayData);
        return $this->sendResponse($obj, "Subadmin added successfully", $request->path());
    }

    public function subAdminList(Request $request) {
        $getSubAdmins = Admin::where(['type'=> 'subadmin'])->get();
        $subadminArray = array();

        if($getSubAdmins) {

            foreach($getSubAdmins as $getSubAdmin) {
                
                $subadminArray[] = [$getSubAdmin->email, $getSubAdmin->name, $getSubAdmin->phone, [$getSubAdmin->status,$getSubAdmin->id], $getSubAdmin->created_at, $getSubAdmin->id];
                // $return_array[] = array($met->cover_image, $met->name, $met->email, $met->phone, [$metstatus, $met->id], $comm, [$met->id,$comm], $met->id);
            }
        }
        return $this->sendResponse($subadminArray, "Subadmin listing", $request->path());
    }

    //sub admin status active or inactive
    public function subadminstatuschange(Request $request, $userid) {
        $details = Admin::find($userid);
        
        if ($details->status) {
            $details->status = "";
        } else {
            $details->status = "1";
        }
        $details->save();
        return $this->sendResponse1([$details->status, $userid], 'Subadmin status updated successfully', $request->path());
    }


    public function subAdminEdit(Request $request) {
        $getSubAdmin = Admin::where(['type'=> 'subadmin', 'id'=>$request->id])->first();
        
        if($getSubAdmin) {

        	$subadminArray = ["email"=>$getSubAdmin->email, "name"=>$getSubAdmin->name, "phone"=>$getSubAdmin->phone, "status"=>$getSubAdmin->status, "permit"=>$getSubAdmin->permit, "created_at"=>$getSubAdmin->created_at];
        }
        return $this->sendResponse($subadminArray, "Subadmin Detail", $request->path());
    }

    public function subAdminUpdate(Request $request) {
        $getSubAdmin = Admin::where(['type'=> 'subadmin', 'id'=>$request->id])->first();
        
        if($getSubAdmin) {

        	$getSubAdmin->name = $request->name; 
        	$getSubAdmin->phone = $request->phone;
            $getSubAdmin->status = $request->status;
        	$getSubAdmin->permit = $request->permit;
            $getSubAdmin->save();
        }
        return $this->sendResponse($getSubAdmin, "Subadmin Detail Updated", $request->path());
    }

    public function sendpushnotification(Request $request) {

        $usersString = $request->user_ids;
        $userArray = explode(',', $usersString);

        if(count($userArray) > 0) {

            foreach($userArray as $userid) {
                $getUsers = User::where(["id"=>$userid])->first();
                if($getUsers) {

                    $notifyData = [
                        'user_id'=>$userid,
                        'order_id'=>0,
                        'order_number'=>0,
                        'type'=>'Promotional Notification',
                        'title'=> $request->title,
                        'content'=>$request->message
                    ];
                    Notification::create($notifyData);
                    $userDevice = Token::select('deviceType', 'deviceToken')->where(['user_id'=>$userid])->first();
                    if($userDevice) {
                        $badge = Notification::where(["user_id"=>$userid, 'status'=>0])->count();
                        $notifyResponse = $this->pushNotification($notifyData, $userDevice, $badge);
                    }
                }
            }
        }
        return $this->sendResponse1([], 'Notification Send Successfully.', $request->path());
  	}


    public function addcategory(Request $request) {
        $input = $request->all();
        $verifyCat = Category::where(["name"=>$input['name']])->first();

        if($verifyCat) {
            return $this->sendError($request->path(), 'Category Already Added');
        } else {

            if(isset($input['seller_id'])) {
                $getCat = Seller::where('id',$input['seller_id'])->first();
                $category_en = $getCat->type;
                $category_ar = $getCat->ar_type;
                $category_ar1 = ($category_ar)?$category_ar.','.$input['ar_name']:$input['ar_name'];
                $category_en1 = ($category_en)?$category_en.','.$input['name']:$input['name'];

                $details = Category::create(['name'=>$input['name'],'ar_name'=>$input['ar_name'],'seller_id'=>$input['seller_id']]);
                
                Seller::where('id',$input['seller_id'])->update(['type'=>$category_en1,'ar_type'=>$category_ar1]);
            
            } else {
                $details = Category::create(['name'=>$input['name'],'ar_name'=>$input['ar_name']]);
            }

            return $this->sendResponse1($input, 'Category added successfully', $request->path());
        }
    }

    public function getcategorylist(Request $request,$id=null) {

        if($id != null) {
            $details = Category::where('seller_id',$id)->orderBy('id','DESC')->get();
        } else {
            $details = Category::orderBy('id','DESC')->get();
        }

            $return_array = array();
            foreach ($details as $value) {
                
                if (!empty($value->image)) {
                    $value->image = asset($value->image);
                } else {
                    $value->image = asset("/img/imagenotfound.jpg");
                }

                $return_array[] = array($value->name,$value->ar_name,date('d-m-Y',strtotime($value->created_at)), $value->id);
            }
        return $this->sendResponse($return_array, 'Category list retrieved successfully', $request->path());
    }

    public function getcategorysingle(Request $request, $id) {
        $details = Category::where('id', $id)->first();
        $return_array = array();

        $return_array[] = array($details->id, $details->name,$details->ar_name, $details->status);
        
        return $this->sendResponse($return_array, 'Category retrieved successfully', $request->path());
    }

    public function categoryupdate(Request $request, $id) {
        $input = $request->all();

        $findsubcatg = Category::find($id);

        $findsubcatg->name = $input['name'];
        $findsubcatg->ar_name = $input['ar_name'];

        $findsubcatg->save();
        return $this->sendResponse1(["category" => $findsubcatg], 'Category updated successfully', $request->path());
    }

    public function catdelete(Request $request, $id) {
        $verifyProduct = Product::where(["category_id"=>$id])->first();
        if($verifyProduct) {
            return $this->sendError($request->path(), 'Category not able to delete because related product available');
        } else {

            $verifyBanner = Banner::where(["category_id"=>$id])->first();
            if($verifyBanner) {
                return $this->sendError($request->path(), 'Category not able to delete because related banner available');
            } else {
                $details = DB::table("categories")->where('id', $id)->delete();
               // $category_name = $details->name;
               // DB::table("subcategories")->where('id', $id)->delete();
                return $this->sendResponse1([], 'Category deleted successfully', $request->path());
            }
        }
    }

    public function categorystatuschange(Request $request, $userid) {
        $details = Category::find($userid);
        if ($details->status) {
            $details->status = "";
            Product::where(["category_id"=>$userid])->update(["status"=>'']);
        } else {
            $details->status = "1";
            Product::where(["category_id"=>$userid])->update(["status"=>1]);
        }
        $details->save();
        return $this->sendResponse1([$details->status, $userid], 'Category status updated successfully', $request->path());
    }

    public function productlistforadmin(Request $request, $userid) {
        $details = Product::select("sku","user_id", "name", "category_id", "sp","mrp", "size", "quantity", "status", "id")->where(["deleteStatus"=>1])->orderBy('id','DESC')->get();
        $return_array = array();

        foreach ($details as $met) {
            $getSeller = Seller::where(["id"=>$met->user_id])->first();
            if($getSeller) {
                $comm = $getSeller->commission;
            } else {
                $comm = 0;
            }

            if(strlen($met->size) > 0) {
                $sizeValue = $met->size;
            } else {
                $sizeValue = "-";
            }
            
            $getCat = Category::where("id", $met->category_id)->first();
            if($getCat) {
                $getCatt = $getCat->name;
            } else {
                $getCatt = "";
            }

            $return_array[] = array($met->sku, $met->name, $getCatt, $sizeValue, $met->sp, $met->mrp, $met->quantity, [$met->status, $met->id], $met->id,$met->id);
        }
        return $this->sendResponse1($return_array, 'Product list retrieved successfully', $request->path());
    }

    // public function editproductbyadmin(Request $request, $idk) {
    //     $input = $request->all();
       
    //     $finddetails = Product::find($idk);
    //     $finddetails->name = $input['name'];
    //     $finddetails->category_id = $input['category_id'];
    //     $finddetails->description = $input['description'];
    //     $finddetails->weight = $input['weight'];
    //     $finddetails->ships_in = $input['ships_in'];
    //     $finddetails->sku = $input['sku'];
        
    //     if(isset($input['payment_mode'])) {
    //         $finddetails->payment_mode = $input['payment_mode'];
    //     }

    //     $finddetails->save();

    //     return $this->sendResponse1($finddetails, $input['name'] . ' updated successfully', $request->path());
    // }


    public function orderlisting(Request $request, $id) {
        
        if ($id == "admin") {
            $getids = Order::select('id')->distinct('id')->get();

            $query = Order::query();
            $query->select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id');

            if (($request->has('start_date') && !empty($request->start_date)) && ($request->has('end_date') && !empty($request->end_date))) {
                $start = Carbon::parse($request->start_date);
                $end = Carbon::parse($request->end_date);
                $query->whereDate('orders.created_at','<=',$end->format('Y-m-d'))->whereDate('orders.created_at','>=',$start->format('Y-m-d'));
            }

            $details = $query->orderBy('orders.id','DESC')->get();
             // print_r($details->toArray());die;
            $return_array = array();
            
            foreach ($details as $met) {

                $getSeller = Seller::select("name")->where(['id' => $met->seller_id])->first();
                $detail = OrderDetail::where(['order_id'=>$met->id])->first();

                $product = Product::select("name","ships_in","sku","sp")->where(['id'=>$detail->product_id])->first();
                if($product) {
                    // if($product->is_variant ==2) {

                    //     $proPriceDetail = ProductPrice::where(["product_id"=>$met->product_id])->first();
                    //     if($proPriceDetail) {
                    //         $get_price_sp = $proPriceDetail->sp; 
                    //         $get_price_mrp = $proPriceDetail->mrp;
                    //     } else {
                    //         $get_price_sp = $product->sp; 
                    //         $get_price_mrp = $product->mrp;    
                    //     }
                    // } else {
                    //     $get_price_sp = $product->sp; 
                    //     $get_price_mrp = $product->mrp;
                    // }
                      $get_price_sp = $product->sp; 
                         $get_price_mrp = $product->mrp;  
                    if($product) {
                        $productName = $product->name;
                    } else {
                        $productName = "";
                    }

                    if($met->gift_wrap_status == 1) {
                        $gift_wrap_status = "Yes";
                    } else {
                        $gift_wrap_status = "No";
                    }

                    if($met->status == 0) {
                          $status = "Pending";

                    } else if($met->status == 1) {
                        $status = "Confrimed";

                    } else if($met->status == 2) {
                        $status = "Processing";

                    } else if($met->status == 3) {
                        $status = "Shipped";
                    }
                    else if($met->status==4){
                        $status = "Delivered";
                    }
                     else if($met->status==5){
                        $status = "Cancelled";
                    }

                    $shipsINDAYS = "+". $product->ships_in ."days";
                    $oDate = date('d-m-y',strtotime($met->created_at));
                    $shipsIN = date('d-m-Y', strtotime($oDate . $shipsINDAYS));

                    $return_array[] = array($met->order_number, $getSeller->name, $productName, $product->sku,$met->final_amt,$met->shipping_charges, $met->buyer, $met->location, $met->payment_type, $detail->quantity ,$status, date('d/m/y H:i',strtotime($met->created_at)),$met->dispatch_at, $met->id);
                }
            }
        } else {

        }

        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function getcontent(Request $request, $type) {
        $input = $request->all();
        $details_of_content = Content::select('type','value','ar_value')->where(['type' => $type])->first();
        return $this->sendResponse1($details_of_content, 'Successfully updated content', $request->path());
    }

    public function content(Request $request) {
        $input = $request->all();
        $details_of_content = Content::where(['type' => $input['type']])->first();
        if ($details_of_content) {
            $details_of_content->type = $request->type;
            $details_of_content->value = $request->value;
            $details_of_content->ar_value = $request->ar_value;
            $details_of_content->save();
        } else {
            Content::create($input);
        }
        return $this->sendResponse1($input['value'], 'Successfully updated content', $request->path());
    }

    public function faqlist(Request $request) {
        $details = Faq::get();
        $return_array = array();
        foreach ($details as $met) {
            $return_array[] = array($met->question, $met->ans, $met->id);
        }
        return $this->sendResponse1($return_array, 'Faq list retrieved successfully', $request->path());
    }
    public function addfaq(Request $request) {
        $input = $request->all();
        Faq::create($input);
        return $this->sendResponse1(['success' => '1'], 'Faq added successfully', $request->path());
    }
    public function deletefaq(Request $request, $id) {
        $details = Faq::find($id);
        Faq::where('id', $id)->delete();
        return $this->sendResponse1([], 'Faq deleted successfully', $request->path());
    }

    public function createbanner(Request $request) {
        $input = $request->all();
    
        if (isset($input['idm']) && !empty($input['idm'])) {
            $findbannerdetails = Banner::find($input['idm']);
    
            if (!empty($input['image'])) {
                $findbannerdetails->image = $input['image'];
                $findbannerdetails->save();
            }

           // DB::table('banners')->where(['id'=> $findbannerdetails->id])->update(["image"=> $input['type']]);

            return $this->sendResponse([], 'Banner updated successfully', $request->path());
        } else {
            Banner::create($input);
            return $this->sendResponse([], 'Banner created successfully', $request->path());
        }
    }

    public function bannerlistforadmin(Request $request) {
        $details = Banner::all();
        $return_array = array();
        
        if($details) {
            foreach ($details as $met) {

                $subcat = Category::where(['id'=>$met->category_id])->first();
                if($subcat) {
                    $subcatname = $subcat->name;
                } else {
                    $subcatname = '';
                }
            
                $return_array[] = array($met->image,$met->id);
            }
        }
        return $this->sendResponse1($return_array, 'Banner list retrieved successfully', $request->path());
    }

    //delete banner
     public function deleteBanner(Request $request, $id) {
        $details = Banner::find($id);
        Banner::where('id', $id)->delete();
        return $this->sendResponse1([], 'Banner deleted successfully', $request->path());
    }

  	public function allsellerpayment(Request $request) {
        $sellers = Seller::where(['status'=>1])->get();
        $sellerArray = array();

        if($sellers) {
            foreach($sellers as $seller) {
                $totalBuyerPayment = 0;
                $totalsellerPayment = 0;
                $totalOrders = 0;

                $orders = Order::where(['seller_id'=>$seller->id, 'status'=>'delivered'])->get();
                if($orders) {

                    foreach($orders as $order) {
                        $orderDetail = OrderDetail::where(['order_id'=>$order->id])->first();
                        $totalBuyerPayment = $totalBuyerPayment + $orderDetail->final_price;
                        $totalsellerPayment = $totalsellerPayment + $orderDetail->seller_amount;
                        $totalOrders++;
                    }
                }
                if($totalOrders > 0) {

                    $sellerArray[] = [$seller->name, $seller->email, $seller->state, $totalOrders, $totalsellerPayment, $totalBuyerPayment];
                }
            }
        }
        return $this->sendResponse1($sellerArray, 'Payment Lists', $request->path());
    }

    public function cuponlist(Request $request) {
         $details = Couponcode::orderBy('id','DESC')->get();
        // print  
         $return_array = array();
        foreach ($details as $met) {

            $return_array[] = array($met->name, $met->name_ar, $met->min_price, $met->discount, date("M d Y", strtotime($met->startdate)), date("M d Y", strtotime($met->expdate)), $met->id);
        }
        return $this->sendResponse1($return_array, 'Coupon list retrieved successfully', $request->path());
    }

    public function createcode(Request $request) {
        $input = $request->all();
        $details = Couponcode::create($input);
        return $this->sendResponse1($input, 'Coupon added successfully', $request->path());
    }


  public function cuponDelete(Request $request, $id){
     $delete = Couponcode::where('id',$id)->delete();
     return $this->sendResponse1([], 'Coupon deleted successfully', $request->path());
  }




    public function productSearchForFlashSale(Request $request) {
        $keyword = $request->keyword;
        $productArray = array();
        $productArrayOne = array();
        $productArrayTwo = array();

        $getProductNames = Product::select('id', 'name')->where('name', 'like', '%' . $keyword . '%')->get();
        if($getProductNames) {
            foreach($getProductNames as $getProductName) {
                $productArrayOne[] = ["id"=>$getProductName->id, "name"=>$getProductName->name];
            }
        }

        $getProductSKUs = Product::select('id', 'name')->where('sku', 'like', '%' . $keyword . '%')->get();
        if($getProductSKUs) {
            foreach($getProductSKUs as $getProductSKU) {
                $productArrayTwo[] = ["id"=>$getProductSKU->id, "name"=>$getProductSKU->name];
            }
        }

        $productArray = array_merge($productArrayOne, $productArrayTwo);

        return $this->sendResponse1($productArray, 'Product Lists', $request->path());
    }

    public function supportticketupdate(Request $request, $id) {
        $input = $request->all();
        $validator = Validator::make($input, ['id'=>'required','status'=>'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $checkSupport = Support::where(['id'=>$input['id']])->first();
        if(!$checkSupport) {
            return $this->sendError($request->path(), 'Ticket not Found');
        } else {
            $checkSupport->status = $input['status'];
            $checkSupport->save();

            return $this->sendResponse($checkSupport, "Ticket updated successfully", $request->path());
        }
    }

    public function categoryApprovalList(Request $request) {
        $getDatas = SellerApprove::orderBy('id','DESC')->get();
        $return_array = array();
        if($getDatas) {

            foreach($getDatas as $getData) {

                $seller = Seller::find($getData->seller_id);
                $subCat = Subcategory::find($getData->subcategory);
                $subSubCat = Subsubcategory::find($getData->subsubcategory);

                if($getData->status == 1) {
                    $status = "Not Approved";
                } else {
                    $status = "Approved";
                }

                $return_array[] = [$getData->id, 
                            $seller->name, 
                            $seller->email, 
                            $getData->category, 
                            $subCat->name, 
                            $subSubCat->name, 
                            $status, 
                            $getData->id];
            }

            return $this->sendResponse($return_array, 'List retrieved successfully', $request->path());

        } else {
            return $this->sendError($request->path(), 'List Not Found!');
        }
    }

    public function changecategoryapproval(Request $request) {
        $getDatas = SellerApprove::where(["id"=>$request->id])->first();
        if($getDatas) {

            if($getDatas->status == 1) {
                $getDatas->status = 2;
            } else {
                $getDatas->status = 1;
            }
            $getDatas->save();

            return $this->sendResponse($getDatas, 'Status change successfully', $request->path());

        } else {
            return $this->sendError($request->path(), 'Not Found!');
        }
    }

    public function userlistfornotification(Request $request) {
        $details = User::select("name", "email", "id")->orderby('id', 'desc')->get();
        $return_array = array();
        foreach ($details as $met) {

            $return_array[] = [$met->name, $met->email, $met->id];
        }
        return $this->sendResponse1($return_array, 'Buyer list retrieved successfully', $request->path());
    }

    public function abounded(Request $request) {
        $datas = Cart::orderBy('id','DESC')->get();
        $result = array();
        foreach($datas as $data) {

            if(is_numeric($data->user_id)) {
                
                $user = User::where('id', $data->user_id)->first();
                if($user) {
                    $product = Product::where('id', $data->product_id)->first();
                    if($product) {

                        if(!empty($data->color)) {
                            $colorname = Color::where(["hexcode"=>$data->color])->first();
                            $colorname = $colorname->name;
                        } else {
                            $colorname = "";
                        }

                        $result[] = [$user->name, $user->email, $user->phone, $product->sku, $product->name, $data->size, $colorname, $data->quantity, date('d-m-Y H:i:s', strtotime($data->created_at))];
                    }
                }
            }
        }
        return $this->sendResponse1($result, 'List retrieved successfully', $request->path());
    }

    public function disablecod(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['products'=>'required', 'payment_mode'=>'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $productids = explode(',', $input['products']);

        foreach($productids as $products) {

            DB::table('products')->where(['id'=>$products])->update(['payment_mode'=>$input['payment_mode']]);
        }

        return $this->sendResponse1([], 'Products updated Successfully.', $request->path());
    }


    public function shippingorderlisting(Request $request) {  //  In Transit list
        
        $details = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["orders.status"=>"transit"])->orderBy('orders.id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {

            $detail = OrderDetail::where(['order_id'=>$met->id])->first();

            $product = Product::select("name")->where(['id'=>$met->product_id])->first();
            if($product) {
                $productName = $product->name;
            } else {
                $productName = "";
            }

            $return_array[] = array($met->order_number, $detail->wbn_num, $productName, $met->buyer, $met->state, $met->city, $met->payment_type, $detail->quantity ,$met->status , date('d-m-y H:i',strtotime($met->created_at)),$met->id, $met->id);
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function promotionlist(Request $request) {

        $getPros = Promotion::orderBy('id','DESC')->get();
        $arrData = array();

        if($getPros) {

            foreach($getPros as $getPro) {

                $getseller = Seller::where(["id"=>$getPro->seller_id])->first();
                
                $getOffer = Offer::select('catid','description','startdate','enddate','coupon_id')->where(["id"=>$getPro->offer_id])->first();
                $getPromotionCount = PromotionList::where(["promotion_id"=>$getPro->id])->count();

                $getcoupon = Cuponcode::where(["id"=>$getOffer->coupon_id])->first();
                $getcat = Subsubcategory::where(["id"=>$getOffer->catid])->first();

                if($getOffer) {

                    if($getcoupon) {
                        $getcouponname = $getcoupon->name;
                    } else {
                        $getcouponname = "";
                    }

                    if($getcat) {
                        $getcatname = $getcat->name;
                    } else {
                        $getcatname = "";
                    }

                    $date1 = strtotime($getOffer->enddate);
                    $date2 = strtotime(date("d-m-Y H:i:s"));
                    if($date2 < $date1) {
                        $arrData[] = [
                            $getseller->name,
                            $getseller->email,
                            $getcatname,
                            $getOffer->description,
                            $getOffer->startdate,
                            $getOffer->enddate,
                            $getcouponname,
                            $getPromotionCount,
                            $getPro->id
                        ];
                    }
                }
            }
        }

        return $this->sendResponse($arrData, "Promotion listing", $request->path());
    }


    public function promotionproductlist(Request $request, $id) {

        $getPros = PromotionList::where(["promotion_id"=>$id])->orderBy('id','DESC')->get();
        $arrData = array();

        if($getPros) {

            foreach($getPros as $getPro) {

                $getproduct = Product::where(["id"=>$getPro->product_id])->first();
                
                if($getproduct) {

                    $getcat = Subsubcategory::where(["id"=>$getproduct->subsubcategory_id])->first();
                    
                    if($getPro->status == 0) {
                        $proStatus = "Inactive";
                    } else {
                        $proStatus = "Active";
                    }

                    $arrData[] = [
                        $getproduct->sku,
                        $getproduct->name,
                        $getcat->name,
                        $proStatus,
                        $getPro->id,
                    ];
                }
            }
        }

        return $this->sendResponse($arrData, "Promotion Product listing", $request->path());
    }

    public function promotionproductstatus(Request $request) {
        $getDatas = PromotionList::where(["id"=>$request->id])->first();
        if($getDatas) {

            if($getDatas->status == 0) {
                $getDatas->status = 1;
            } else {
                $getDatas->status = 0;
            }
            $getDatas->save();

            return $this->sendResponse($getDatas, 'Status change successfully', $request->path());

        } else {
            return $this->sendError($request->path(), 'Not Found!');
        }
    }


    public function sublistforoffer(Request $request, $id) {
        $details = Subsubcategory::where(['subcategory_id' => $id])->get();
        $return_array = array();
        foreach ($details as $value) {

            if($value->status == 1) {
                $checkOffer = Offer::where('catid', $value->id)->first();
                if($checkOffer) {

                } else {
                    if($value->occasion_status == 1) {
                        $name = $value->name . ' - occasion';
                    } else {
                        $name = $value->name;
                    }
                    $return_array[] = [$value->id, $name];
                }
            }
        }
        return $this->sendResponse($return_array, 'Sub-subcategory retrieved successfully', $request->path());
    }

   
   //rating list for admin
    public function ratingList(Request $request){
        $query = OrderRating::query();
        $query->orderBy('id','DESC');
        $return_array = [];
        $ratings = $query->get();
        if(count($ratings)>0){
            foreach($ratings as $rating){
             $seller  = Seller::where('id',$rating->seller_id)->first();
             $seller_name = ($seller)?$seller->name:'';
             $order  = Order::where('id',$rating->order_id)->first();
             $order_number = ($order)?$order->order_number:'';
             $return_array[] = array($order_number,$seller_name,$rating->product_rate,$rating->store_rate,$rating->comment);
        
            }
        }  
        return $this->sendResponse1($return_array, 'Rating list retrieved successfully.', $request->path());    
    }

    //admin payout

    public function payoutList(Request $request){
       $query = SellerWallet::query();
       $query->select("sellers.name","sellers.phone","sellers.id","receivable_amt","payable_amt","actual_amt")->join('sellers', 'sellers.id', '=', 'seller_wallets.seller_id');
      if (($request->has('to') && !empty($request->to)) && ($request->has('from') && !empty($request->from))) {
         $to = Carbon::parse($request->to);
         $from = Carbon::parse($request->from);
        $query->whereDate('seller_wallets.created_at','<=',$to->format('Y-m-d'))->whereDate('seller_wallets.created_at','>=',$from->format('Y-m-d'));
            } 
        $query->groupBy('seller_wallets.seller_id');    
      $details = $query->orderBy('seller_wallets.id','DESC')->get();  
      // print_r($details->toArray());
      $return_array = [];
      $total_payable_amt = 0;
      $total_receivable_amt = 0;
       if(count($details)>0){    
            foreach ($details as $met) {
                $total_payable_amt = $total_payable_amt+$met->payable_amt;
                $total_receivable_amt = $total_receivable_amt+$met->receivable_amt;
                $return_array[] = array($met->name,$met->phone,$met->actual_amt,$met->receivable_amt,$met->payable_amt,$met->id);
                 }  

            } 
           $result = ['total_receivable_amt'=>$total_receivable_amt,'total_payable_amt'=>$total_payable_amt,'list'=>$return_array];

             return $this->sendResponse1($result, 'Payout list retrieved successfully.', $request->path());       

    }

    //payout
    public function payoutSubmit(Request $request){
       $input = $request->all();
        $validator = Validator::make($input, ['seller_id' => 'required','amount'=>'required','type'=>'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }  

       $payment_type = $request->type;
       if($payment_type=='paid'){ 
        //echo "hi";die;
        //for paid
        $msg  = "Payment paid successfully.";
        $check_seller_wallet = SellerWallet::where('seller_id',$request->seller_id)->first();
        $payable_amtt =  $check_seller_wallet->payable_amt;
        

     if($payable_amtt>=$request->amount){
         $payable_amttt = $payable_amtt-$request->amount;
         $update = SellerWallet::where('seller_id',$request->seller_id)->update(['payable_amt'=>$payable_amttt]) ;  
        }else{
       return $this->sendError($request->path(),'Please enter valid amount');  
        }
    }else{

        //for unpaid
        $msg = "Payment recived successfully";
        $check_seller_wallet = SellerWallet::where('seller_id',$request->seller_id)->first();
      //  if($check_seller_wallet){
           $receivable_amtt =  $check_seller_wallet->receivable_amt;
        // }else{
        //   $receivable_amtt = 0;
        // }

     if($receivable_amtt>=$request->amount){
         $receivable_amttt =  $receivable_amtt-$request->amount;
         $update = SellerWallet::where('seller_id',$request->seller_id)->update(['receivable_amt'=>$receivable_amttt]) ;  
        }else{
       return $this->sendError($request->path(),'Please enter valid amount');  
        }

       }
       //seller history
       $save_history = SellerHistory::create(['seller_id'=>$request->seller_id,'amount'=>$request->amount,'type'=>$request->type]);

       return $this->sendResponse1([],$msg, $request->path());

    }


//payout history
   public function payoutHistory(Request $request,$id){
     $history = SellerHistory::where('seller_id',$id)->orderBy('id','DESC')->get();
     $return_array = [];
     if(count($history)>0){
        foreach($history as $values){
         $return_array[] = array($values->amount,$values->type,date('d-m-Y',strtotime($values->created_at)));
        }
     }

     return $this->sendResponse1($return_array,'History retrieved successfully', $request->path());
   }


    //payment list
   public function paymentList(Request $request){
           // $getids = Order::select('id')->distinct('id')->get();

            $query = Order::query();
            $query->select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id');

            if (($request->has('to') && !empty($request->to)) && ($request->has('from') && !empty($request->from))) {
               $to = Carbon::parse($request->to);
               $from = Carbon::parse($request->from);
              // echo $end->format('Y-m-d');die;
              // echo $to->format('Y-m-d');die;
                $query->whereDate('orders.created_at','<=',$to->format('Y-m-d'))->whereDate('orders.created_at','>=',$from->format('Y-m-d'));
            }
           if(($request->has('payment_type') && !empty($request->payment_type))){
             $query->where('payment_type',$request->payment_type);
            }

            $details = $query->orderBy('orders.id','DESC')->get();
           //   print_r($details->toArray());die;
            $return_array = array();
            $return_array = [];
            $total_earning = 0;
            if(count($details)>0){    
            foreach ($details as $met) {

                $getSeller = Seller::select("name")->where(['id' => $met->seller_id])->first();
                $seller_name = ($getSeller)?$getSeller->name:'';
                $detail = OrderDetail::where(['order_id'=>$met->id])->first();
                $total_earning = $total_earning+$met->total_admin_comission;
               // $invoic_amt = $met->shipping_charges+$met->final_amt;
                //$sub_total = $met->final_amt+$met->discount+$met->shipping_charges;
                $return_array[] = array($met->order_number,date('d/m/y',strtotime($met->created_at)),$met->buyer,$seller_name,$met->sub_tot_amt,$met->shipping_charges,$met->discount,$met->final_amt,round($met->total_seller_earning),round($met->total_admin_comission),$met->payment_type,$met->transcation_id,$met->id);

                    //print_r($return_array);die;
                //}
                  }  

            }
             $result = ['total_earning'=>round($total_earning),'list'=>$return_array];

             return $this->sendResponse1($result, 'Payment list retrieved successfully.', $request->path());
        }

    
   //}
    


    public function pushNotification($notifyData, $userDevice, $badge) {
        $url = 'https://fcm.googleapis.com/fcm/send';  
        
        $message = array("id"=>$notifyData['order_id'],'title' => $notifyData['title'], "body"=>$notifyData['content'], "type"=>$notifyData['type'], 'sound' => 'default', 'badge'=>$badge);
        
        $registatoin_ids = array($userDevice->deviceToken);
        $fields = array('registration_ids' => $registatoin_ids, 'data' => $message, 'notification'=>$message);

        $GOOGLE_API_KEY = "AAAAvtlLWfU:APA91bHkKDPg59guB8VXYN8zeJc5w8vfrYuCRYL6J4Hlmdz2SSo15JotADzXH-RBF9fdGxPgci7W_422oGlJz6I_KW1mZ99dy7fqJHxtiDy3_FT199aH8VkUgZmA5Zf8NaGeL6rnnKW8";   
        $headers = array('Authorization: key=' . $GOOGLE_API_KEY , 'Content-Type: application/json',);
        
        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);    
        
        if ($result === false) {
            return $abc = 'FCM Send Error: ' . curl_error($ch);
        } else {
            return $abc = "Notification Send";
        }
        curl_close($ch);
    }

}

?>