@extends('layouts.website')

@section('content')
<style>
    .alert{border: 1px solid #e8e8e8!important;}
</style>
 <section>
        <div class="DashboardArea">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="SideNavbar">
                            <ul>
                                <li ><a href="{{ route('profile') }}"><i class="fa fa-user"></i>My Profile</a></li> 
                                <li class="active"><a href="{{ route('notifications') }}"><i class="fa fa-bell"></i>Notifications</a></li>  
                                <li><a href="{{ route('refer') }}"><i class="fa fa-money"></i>Refer $ Earn</a></li>
                                <li><a href="{{ route('coupons') }}"><i class="fa fa-money"></i>Coupons</a></li>  
                                <li ><a href="{{ route('address') }}"><i class="fa fa-home"></i>Address</a></li> 
                                <li><a href="{{ route('change-password') }}"><i class="fa fa-refresh"></i> Change password</a></li>
                                <li><a href="{{ route('orders') }}"><i class="fa fa-refresh"></i> My Orders </a></li>
                                <li><a href="{{ route('wishlist') }}"><i class="fa fa-refresh"></i> Whishlist </a></li>
                                <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> logout</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-9">
                        <div class="UserDashboard">

                            <div class="ReferAreaa">
                            @foreach($notifyArr as $notify)
                                <div class="alert alert-secondary">
                                    <p><strong>{{ $notify['title'] }}</strong> {{ $notify['content'] }}</p>
                                </div>
                            @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection