@extends('layouts.app')

@section('content')

  <section>
        <div class="AboutArea">
            <div class="container">
                <h3>@if($lang=='ar')  معلومات عنا @else About Us @endif</h3>

                <?php if($lang=='ar') {echo ucfirst($detail['ar_value']);}else{echo $detail['value'];} ?>
            </div>
        </div>
    </section>


@endsection