<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferEarn extends Model
{
    //

    protected $fillable = [
        'price'
    ];
}
