<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //
    protected $fillable = [
         'user_id', 'description', 'status','Review','type','rating','product_id'
    ];
}
