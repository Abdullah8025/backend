@extends('layouts.website')

@section('content')
<style type="text/css">
    span.formerror {
    float: left;
    width: 100%;
    font-size: 10px;
    color: red;
    margin: 3px auto;
}
</style>
 <section>
        <div class="DashboardArea">
            <div class="container">
                <div class="row">
                    @if(Session::has('message_add'))
                            <p class="alert alert-info">{{ Session::get('message_add') }}</p>
                    @endif
                    <div class="col-sm-3">
                        <div class="SideNavbar">
                            <ul>
                                <li><a href="{{ route('profile') }}"><i class="fa fa-user"></i>My Profile</a></li> 
                                <li><a href="{{ route('notifications') }}"><i class="fa fa-bell"></i>Notifications</a></li>  
                                <li><a href="{{ route('refer') }}"><i class="fa fa-money"></i>Refer $ Earn</a></li>
                                <li><a href="{{ route('coupons') }}"><i class="fa fa-money"></i>Coupons</a></li>  
                                 <li class="active"><a href="{{ route('address') }}"><i class="fa fa-home"></i>Address</a></li> 
                                <li><a href="{{ route('change-password') }}"><i class="fa fa-refresh"></i> Change password</a></li>
                                <li><a href="{{ route('orders') }}"><i class="fa fa-refresh"></i> My Orders </a></li>
                                <li><a href="{{ route('wishlist') }}"><i class="fa fa-refresh"></i> Whishlist </a></li>
                                <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> logout</a></li>
                            </ul>
                        </div>
                    </div>
                   <div class="col-sm-9">
                        <div class="UserDashboard">
                            
                            <div class="UserAddress">
                                <h2>Our Address 
                                <?php if(sizeof($detailsaddress) < 2){ ?>
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#Location">Add New Address</a> <?php } ?></h2>
                               
                                @if(!empty($detailsaddress))
                                    @foreach ($detailsaddress as $value)
                                
                                <label class="Address">    
                                    <h3>{{$value->name}}</h3>
                                    <p>{{$value->address}},{{$value->town}}
                                        <br> {{$value->city}}, {{$value->state}} - {{$value->pincode}}</p>
                                    <span class="Location">{{$value->type}}</span>
                                    <ul>
                                        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#EditAddress{{$value->id}}"><i class="fa fa-pencil"></i></a></li>
                                        <li><a href="{{route('removeaddress', $value->id)}}"><i class="fa fa-trash-o"></i></a></li>
                                    </ul>
                                </label>

                                @endforeach
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="Location" class="modal fade" role="dialog">
        <div class="modal-dialog"> 
            <div class="modal-content"> 
                <div class="modal-body">
                    <div class="AddAddress">
                        <h3>Add Address</h3>
                        <form action="{{ route('add-address') }}" method="post">
                            <div class="form-group col-sm-6">
                                <label>Full Name</label>
                                <input name="name" id="adrs_name" type="text" class="form-control">
                                 <span id="adrs_name_error" style="color: red;"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Mobile Number</label>
                                <input name="phone" id="adrs_phone" type="text" class="form-control">
                                <span id="adrs_phone_error" style="color: red;"></span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label>Address</label>
                                <textarea name="address" id="adrs_address" rows="3" class="form-control"></textarea>
                                <span id="adrs_address_error" style="color: red;"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Town</label>
                                <input name="town" id="adrs_town" type="text" class="form-control">
                                <span id="adrs_town_error" style="color: red;"></span> 
                            </div>
                            <div class="form-group col-sm-6">
                                <label>City</label>
                                <input name="city" id="adrs_city" type="text" class="form-control">
                                <span id="adrs_city_error" style="color: red;"></span> 
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Pincode</label>
                                <input name="pincode" id="adrs_pincode" type="text" class="form-control">
                                <span id="adrs_pincode_error" style="color: red;"></span> 
                            </div>
                            <div class="form-group col-sm-6">
                                <label> State</label>
                                <input name="state" id="adrs_state" type="text" class="form-control">
                                <span id="adrs_state_error" style="color: red;"></span> 
                            </div>

                            <div class="form-group col-sm-12">
                                <label>Address Type</label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="Address">
                                            <input type="radio" value="Home" name="type" checked>
                                            <span class="checkmark"></span>
                                            <h4>Home <span>(All Day Delivery)</span></h4>
                                        </label>
                                    </div>

                                    <div class="col-sm-6">
                                        <label class="Address">
                                            <input value="Office" type="radio" name="type">
                                            <span class="checkmark"></span>
                                            <h4>Office <span>(Between 10 AM to  7 PM)</span></h4>
                                        </label>
                                    </div>
                                </div>
                            </div>
                                <div class="form-group col-sm-12">
                                    <input type="checkbox" name="remark" style="font-size: 17px;    font-weight: 700;"> Make it Default<br>
                                </div>
                            {{-- </div> --}}
                            <div class="form-group col-sm-12">
                                <button type="button" class="btn" id="addnewaddress">Save Address</button>
                            </div>
                           
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        </form>

                        <div class="clear"></div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    

    @if(!empty($detailsaddress))
        @foreach ($detailsaddress as $value)

    <div id="EditAddress{{$value->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog"> 
            <div class="modal-content"> 
                <div class="modal-body">
                    <div class="AddAddress">
                        <h3>Change Address</h3>
                        <form action="{{ route('update-address') }}" method="post">
                             <div class="form-group col-sm-6">
                                <label>Full Name</label>
                                <input name="name" id="adrs_name{{$value->id}}" type="text" class="form-control" value="{{$value->name}}">
                                 <span id="adrs_name_error{{$value->id}}" style="color: red;"></span>
                            </div>
                            <input type="hidden" name="id" value="{{$value->id}}">
                            <div class="form-group col-sm-6">
                                <label>Mobile Number</label>
                                <input name="phone" value="{{$value->phone}}" id="adrs_phone{{$value->id}}" type="text" class="form-control">
                                <span id="adrs_phone_error{{$value->id}}" style="color: red;"></span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label>Address</label>
                                <textarea name="address" id="adrs_address{{$value->id}}" rows="3" class="form-control">{{$value->address}}</textarea>
                                <span id="adrs_address_error{{$value->id}}" style="color: red;"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Town</label>
                                <input name="town" value="{{$value->town}}" id="adrs_town{{$value->id}}" type="text" class="form-control">
                                <span id="adrs_town_error{{$value->id}}" style="color: red;"></span> 
                            </div>
                            <div class="form-group col-sm-6">
                                <label>City</label>
                                <input name="city" value="{{$value->city}}" id="adrs_city{{$value->id}}" type="text" class="form-control">
                                <span id="adrs_city_error{{$value->id}}" style="color: red;"></span> 
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Pincode</label>
                                <input name="pincode" value="{{$value->pincode}}" id="adrs_pincode{{$value->id}}" type="text" class="form-control">
                                <span id="adrs_pincode_error{{$value->id}}" style="color: red;"></span> 
                            </div>
                            <div class="form-group col-sm-6">
                                <label>State</label>
                                <input name="state" value="{{$value->state}}" id="adrs_state{{$value->id}}" type="text" class="form-control">
                                <span id="adrs_state_error{{$value->id}}" style="color: red;"></span> 
                            </div>

                            <div class="form-group col-sm-12">
                                <label>Address Type</label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="Address">
                                            <input type="radio" value="Home" name="type" checked>
                                            <span class="checkmark"></span>
                                            <h4>Home <span>(All Day Delivery)</span></h4>
                                        </label>
                                    </div>

                                    <div class="col-sm-6">
                                        <label class="Address">
                                            <input value="Office" type="radio" name="type">
                                            <span class="checkmark"></span>
                                            <h4>Office <span>(Between 10 AM to  7 PM)</span></h4>
                                        </label>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group col-sm-6">
                             <input type="checkbox" name="remark" <?php if($value->remark == '1'){ ?>checked<?php }  ?>> Make it Default<br>
                             </div>
                            <div class="form-group col-sm-12">
                                <button type="button" id="addnewaddress{{$value->id}}">Change Address</button>
                            </div>
                             <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        </form>

                        <div class="clear"></div>
                    </div>
                </div> 
            </div>
        </div>
    </div>

    <script type="text/javascript">
       $("#addnewaddress{{$value->id}}").click(function(){
           
             if($("#adrs_name{{$value->id}}").val() == ""){
                 $("#adrs_name_error{{$value->id}}").text("Full Name is required.")
             }

            if($("#adrs_phone{{$value->id}}").val() == ""){
                 $("#adrs_phone_error{{$value->id}}").text("Phone Number is required.")
            } 

            if($("#adrs_address{{$value->id}}").val() == ""){
                 $("#adrs_address_error{{$value->id}}").text("Address is required.")
            } 

            if($("#adrs_town{{$value->id}}").val() == ""){
                 $("#adrs_town_error{{$value->id}}").text("Town is required.")
            } 
            
            if($("#adrs_city{{$value->id}}").val() == ""){
                 $("#adrs_city_error{{$value->id}}").text("City is required.")
            } 
            
            if($("#adrs_pincode{{$value->id}}").val() == ""){
                 $("#adrs_pincode_error{{$value->id}}").text("Pincode is required.")
            } 

           if($("#adrs_state{{$value->id}}").val() == ""){
                 $("#adrs_state_error{{$value->id}}").text("State is required.")
            }  

           if($("#adrs_name{{$value->id}}").val() != "" && $("#adrs_phone{{$value->id}}").val() != "" && $("#adrs_address{{$value->id}}").val() != "" && $("#adrs_town{{$value->id}}").val() != "" && $("#adrs_city{{$value->id}}").val() != "" && $("#adrs_pincode{{$value->id}}").val() != "" && $("#adrs_state{{$value->id}}").val() != ""){
                  $("#addnewaddress{{$value->id}}").attr("type","submit");
                  $("#addnewaddress{{$value->id}}").click();
           }     
       });
    </script>

@endforeach
@endif

    <script type="text/javascript">
       $("#addnewaddress").click(function(){
           
             if($("#adrs_name").val() == ""){
                 $("#adrs_name_error").text("Full Name is required.")
             }

            if($("#adrs_phone").val() == ""){
                 $("#adrs_phone_error").text("Phone Number is required.")
            } 

            if($("#adrs_address").val() == ""){
                 $("#adrs_address_error").text("Address is required.")
            } 

            if($("#adrs_town").val() == ""){
                 $("#adrs_town_error").text("Town is required.")
            } 
            
            if($("#adrs_city").val() == ""){
                 $("#adrs_city_error").text("City is required.")
            } 
            
            if($("#adrs_pincode").val() == ""){
                 $("#adrs_pincode_error").text("Pincode is required.")
            } 

           if($("#adrs_state").val() == ""){
                 $("#adrs_state_error").text("State is required.")
            }  

           if($("#adrs_name").val() != "" && $("#adrs_phone").val() != "" && $("#adrs_address").val() != "" && $("#adrs_town").val() != "" && $("#adrs_city").val() != "" && $("#adrs_pincode").val() != "" && $("#adrs_state").val() != ""){
                  $("#addnewaddress").attr("type","submit");
                  $("#addnewaddress").click();
           }     
       });
    </script>

@endsection