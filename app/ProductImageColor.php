<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImageColor extends Model
{
    //

    protected $fillable = [
         'product_id', 'images', 'status','size'];
}
