<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    
    protected $fillable = [ 'id','name','ar_name','seller_id', 'image', 'status'];
}
