<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Symfony\Component\HttpFoundation\Response;
use DB;
use App\Seller;

use App\User;
use App\Token;
use App\Address;

use App\Category;
use App\Product;
use App\ProductPrice;
use App\Order;
use App\OrderDetail;
use App\OrderRequest;
use App\Admin;
use App\Subcategory;
use App\Banner;
use App\Theme;
use App\Brand;
use App\Flashsale;
use App\Wishlist;
use App\Cart;
use App\Subsubcategory;
use App\Cuponcode;
use App\Content;
use App\Faq;
use App\Priority;
use App\Notification;
use App\Size;
use App\Color;
use App\Offer;
use App\Announcement;
use App\Refer;
use App\Promotion;
use App\PromotionList;
use App\Attribute;
use App\ReferEarn;
use App\Support;
use App\SizeChart;
use App\ProductImageColor;
use App\Manifest;
use App\SellerPerformance;
use App\SellerMistake;
use App\PayoutRecord;
use Image;
use Validator;
use Mail;
use Intervention\Image\ImageServiceProvider;
use Softon\Indipay\Facades\Indipay;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use PDF;

class ServiceController extends APIBaseController {

    private $baseurlslash;
    private $baseurl;
    
    public function __construct() {
        $this->baseurlslash = "http://54.152.130.226/honey_app/public/";
        $this->baseurl = "http://54.152.130.226/honey_app/public";
    }

    public function addcategory(Request $request) {
        $input = $request->all();
        $verifyCat = Category::where(["name"=>$input['name']])->first();

        if($verifyCat) {
            return $this->sendError($request->path(), 'Category Already Added');
        } else {
            $details = Category::create($input);
            return $this->sendResponse1($input, 'Category added successfully', $request->path());
        }
    }

    public function getproductdetails(Request $request, $idk) {
    
        $details = Product::find($idk);
        $product_name = $details->name;
        
        $attriArr = array();
        $getAttri = Attribute::select('label','ar_label','labeltext','ar_labeltext')->where(["product_id"=>$idk])->get();
        if($getAttri) {
            foreach($getAttri as $getAttr) {
                $attriArr[]= ["labal"=>$getAttr->label,'ar_label'=>$getAttr->ar_label,"text"=>$getAttr->labeltext,'ar_text'=>$getAttr->ar_labeltext];
            }

            $details->attributes = $attriArr;
        } else {
            $details->attributes = $attriArr;
        }

        $getCat = Category::where(["id"=>$details->category_id])->first();
        if($getCat) {
            $details->category_id = $getCat->name;
            $details->categoryid = $getCat->id;
            $details->ar_category_id = $getCat->ar_name;
        } else {
            $details->category_id = "";
            $details->ar_category_id = "";
            $details->categoryid = "";
        }

        return $this->sendResponse1($details, $product_name . ' details retrieved successfully', $request->path());
    }

    public function productstatuschange(Request $request, $userid) {
        $details = Product::find($userid);
        if ($details->status) {
            $details->status = "";
        } else {
            $details->status = "1";
        }
        $details->save();
        return $this->sendResponse1([$details->status, $userid], 'Product status updated successfully', $request->path());
    }

    public function updatestock(Request $request) {
        
        if($request->productprice_id == 0) {
            $check_pro = Product::where(['id' => $request->id])->first();
            if (empty($check_pro)) {
                    return $this->sendError($request->path(), 'Product not Found');
            }
            DB::table('products')->where(['id'=>$request->id])->update(['quantity' => $request->stock]);
        } else {
            $check_pro = ProductPrice::where(['id' => $request->productprice_id])->first();
            if (empty($check_pro)) {
                    return $this->sendError($request->path(), 'Product not Found');
            }
            DB::table('product_prices')->where(['id'=>$request->productprice_id])->update(['quantity' => $request->stock]);
        }
        return $this->sendResponse(['status' => "success"], 'Stock updated successfully', $request->path());
    }

    public function updateprice(Request $request) {

        if($request->productprice_id == 0) {
            $check_pro = Product::where(['id' => $request->id])->first();
            if (empty($check_pro)) {
                return $this->sendError($request->path(), 'Product not Found');
            }
            DB::table('products')->where(['id'=>$request->id])->update(['sp'=>$request->sp, "commision"=>$request->commission, "tax"=>$request->gst]);
        } else {

            $check_pro = ProductPrice::where(['id' => $request->productprice_id])->first();
            if (empty($check_pro)) {
                return $this->sendError($request->path(), 'Product not Found');
            }
            DB::table('product_prices')->where(['id'=>$request->productprice_id])->update(['sp'=>$request->sp, "commision"=>$request->commission, "tax"=>$request->gst]);

        }
        return $this->sendResponse(['status' => "success"], 'Selling price updated successfully', $request->path());
    }

    public function productdelete(Request $request, $userid) {
        $details = Product::find($userid);
        $product_name = $details->name;
        Product::where(['id'=>$details->id])->update(['deleteStatus' => 0]);
        return $this->sendResponse1([], $product_name . 'Deleted successfully', $request->path());
    }

    public function orderdetails(Request $request, $id) {

        $details = Order::join('users', 'users.id', '=', 'orders.user_id')->select("orders.id as id", "orders.*", "users.name", "users.email", "users.phone")->where(["orders.id"=>$id])->first();
      //  echo "<pre>";print_r($details);die;

        if($details) {
            $orderDetails = array();
            $getDetails = OrderDetail::where(['order_id'=>$details->id])->get();
            foreach($getDetails as $getDetail){
          //  print_r($getDetail);die;
            $seller = Seller::where(['id'=>$details->seller_id])->first();

            $pro = Product::where(['id'=>$getDetail->product_id])->first();
           // print_r($pro);die;
            $imgArr = [];
            if(empty($pro->images)) {
                $img = asset("public/img/imagenotfound.jpg");
            } else {
                $imgArr = explode(',', $pro->images);
                $img1 = $imgArr[0];
               $img =  asset('public'.$img1);
            }

            $notifyArr = array();
            $getNotify = Notification::where(['order_id'=>$details->id, 'type'=>'order'])->get();
            if($getNotify) {
                foreach($getNotify as $notify) {
                    $notifyArr[] = ["date"=> date("d M", strtotime($notify->updated_at)), "content"=>$notify->content];
                }
            }

            if($details->gift_wrap_status == 1) {
                $details->gift_wrap_status = "Yes";
            } else {
                $details->gift_wrap_status = "No";
            }

            $orderDetails[] = [
                    "id"=>$details->id,
                    "order_id"=>$details->id,
                    "product_id"=>$getDetail->product_id,
                    "product_name"=>$pro->name,
                    "product_image"=> $img,
                    "product_sku"=>$pro->sku,
                    "size"=>$pro->size,
                    "quantity"=>$getDetail->quantity,
                    "sale_price"=>$getDetail->sale_price,
                    "discount_price"=>$getDetail->discount_price,
                    "shipping"=>$getDetail->seller_shipping,
                    "gst"=>$getDetail->gst_tax,
                    "total_price"=>$getDetail->final_price,
                    "gift_wrap_status"=>$details->gift_wrap_status,
                    "dispatch_by"=>$details->dispatch_at,
                    "payment_status"=>$details->payment_status,
                    "status"=>$details->status,
                    "seller"=>$seller->name,
                    "seller_email"=>$seller->email,
                    "seller_phone"=>$seller->phone,
                    "trackorder" => $notifyArr
            ];

        }

            $orderfetch = [
                "id"=>$details->id,
                "order_number"=>$details->order_number,
                "location"=>$details->location,
                "payment_type"=>$details->payment_type,
                "transcation_id"=>$details->transcation_id,
                "coupen_code"=>(!empty($details->coupon_code))?$details->coupon_code:'-----',
                "created_at"=>(!empty($details->created_at))?date('d-m-Y',strtotime($details->created_at)):'-----',
                "expacted_date"=>(!empty($details->dispatch_at))?$details->dispatch_at:'-----',
                'total_price' => $details->final_amt,
                'discount' => $details->discount,
                'shipping_charge' => $details->shipping_charges,
                'tracking_id' => (!empty($details->tracking_id))?$details->tracking_id:'-----',
                'tracking_url' => (!empty($details->tracking_url))?$details->tracking_url:'-----',
                "user_name"=>$details->name,
                "user_email"=>$details->email,
                "phone"=>$details->phone,
                "details"=>$orderDetails,
            ];
        } else {
            $orderfetch = [];
        }

        return $this->sendResponse1($orderfetch, 'Order details retrieved successfully.', $request->path());
    }

    public function catlistfordropdown(Request $request) {
        $details = DB::table('categories')->select("id", "name","ar_name")->orderBy('id','DESC')->get();
        $return_array = array();
        foreach ($details as $met) {
            $return_array[] = ["name"=>$met->name,"id"=>$met->id];
        }
        return $this->sendResponse1($return_array, 'Category list retrieved successfully', $request->path());
    }

     public function arcatlistfordropdown(Request $request) {
        $details = DB::table('categories')->select("id", "ar_name",'name')->orderBy('id','DESC')->get();
        $return_array = array();
        foreach ($details as $met) {
            $return_array[] = ["ar_name"=>$met->ar_name, "id"=>$met->id];
        }
        return $this->sendResponse1($return_array, 'Category list retrieved successfully', $request->path());
    }

    public function sizelist_fordropdown(Request $request) {
        $details = DB::table('sizes')->select("id", "name")->orderBy('id','DESC')->get();
        $return_array = array();
        foreach ($details as $met) {
            $return_array[] = ["name"=>$met->name, "id"=>$met->name];
        }
        return $this->sendResponse1($return_array, 'Size list retrieved successfully', $request->path());
    }

     public function ar_sizelist_fordropdown(Request $request) {
        $details = DB::table('sizes')->select("id", "size",'ar_size')->orderBy('id','DESC')->get();
        $return_array = array();
        foreach ($details as $met) {
            $return_array[] = ["name"=>$met->ar_size, "id"=>$met->ar_size];
        }
        return $this->sendResponse1($return_array, 'Size list retrieved successfully', $request->path());
    }

    public function fileuploade1(Request $request) {
        $input = $request->all();
        $fileuploadedpath = "";
        if ($request->gstnumfile) {
            $thumbnail_img = $request->gstnumfile;
            list($type, $thumbnail_img) = explode(';', $thumbnail_img);
            list(, $thumbnail_img) = explode(',', $thumbnail_img);
            $thumbnail_img = base64_decode($thumbnail_img);
            $thumbnail_img_name = rand(1000000, 9999999) . date('ymdhis') . '.png';
            $thumbnail_img_name_dir = public_path('sellergstnumfile/' . $thumbnail_img_name);
            file_put_contents($thumbnail_img_name_dir, $thumbnail_img);
            $fileuploadedpath = url('/') . '/public/sellergstnumfile/' . $thumbnail_img_name;
        }
        return $this->sendResponse1($fileuploadedpath, 'Image uploaded successfully', $request->path());
    }

    public function fileuploadebanner(Request $request) {
        $input = $request->all();
        $fileuploadedpath = "";
        if ($request->gstnumfile) {
            $thumbnail_img = $request->gstnumfile;
            list($type, $thumbnail_img) = explode(';', $thumbnail_img);
            list(, $thumbnail_img) = explode(',', $thumbnail_img);
            $thumbnail_img = base64_decode($thumbnail_img);
            $thumbnail_img_name = rand(1000000, 9999999) . date('ymdhis') . '.png';
            $thumbnail_img_name_dir = public_path('banner/' . $thumbnail_img_name);
            file_put_contents($thumbnail_img_name_dir, $thumbnail_img);
            $fileuploadedpath =  '/banner/' . $thumbnail_img_name;
        }
        return $this->sendResponse1($fileuploadedpath, 'Image uploaded successfully', $request->path());
    }

    public function fileuploadecat(Request $request) {
        $input = $request->all();
        $fileuploadedpath = "";
        
        if ($request->gstnumfile) {
            $thumbnail_img = $request->gstnumfile;
            
            list($type, $thumbnail_img) = explode(';', $thumbnail_img);
            list(, $thumbnail_img) = explode(',', $thumbnail_img);
            $thumbnail_img = base64_decode($thumbnail_img);
            $thumbnail_img_name = rand(1000000, 9999999) . date('ymdhis') . '.png';
            $thumbnail_img_name_dir = public_path('catimages/' . $thumbnail_img_name);
            file_put_contents($thumbnail_img_name_dir, $thumbnail_img);
            $fileuploadedpath = '/catimages/' . $thumbnail_img_name;
        }
        return $this->sendResponse1($fileuploadedpath, 'Image uploaded successfully', $request->path());
    }

    public function fileuploade(Request $request) {
        $input = $request->all();
        $fileuploadedpath = "";
        
        if ($request->gstnumfile) {
            $thumbnail_img = $request->gstnumfile;
            
            list($type, $thumbnail_img) = explode(';', $thumbnail_img);
            list(, $thumbnail_img) = explode(',', $thumbnail_img);
            $thumbnail_img = base64_decode($thumbnail_img);
            $thumbnail_img_name = rand(1000000, 9999999) . date('ymdhis') . '.png';
            $thumbnail_img_name_dir = public_path('productimages/' . $thumbnail_img_name);
            file_put_contents($thumbnail_img_name_dir, $thumbnail_img);
            $fileuploadedpath = 'productimages/' . $thumbnail_img_name;
        }
        return $this->sendResponse1($fileuploadedpath, 'Image uploaded successfully', $request->path());
    }








    






    public function innerpageapi(Request $request, $id) {
        $input = $request->all();
        $subcategories_list = Subcategory::where('category_id', $id)->get();
        $subcategorylist_array = array();
        $othercatlist = array();
        
        $px=1;

        foreach ($subcategories_list as $value1) {
        
            if (empty($value1->image)) {
                $value1->image = url('/public/') . "/img/imagenotfound.jpg";
            }
            $subcategorylist_array[] = array("filter_type" => "", "filter_data" => $value1->id, "image" => $value1->image, "name" => $value1->name);

            if($px <= 4) {
                $productdetails = Product::where(['subcategory_id' => $value1->id])->where(["deleteStatus"=>1,"status"=>1])->orderBy('id','DESC')->limit(20)->get();
                $temp_datalist = array();
                
                if(count($productdetails) >0) {
                    foreach ($productdetails as $value) {
                        $imagearray = explode(",", $value->images);
                
                        if (!empty($imagearray)) {
                
                            if (!empty($imagearray[0])) {
                
                                if (strpos($imagearray[0], $this->baseurlslash) == false) {
                                    $imagearray[0] = $this->baseurl . $imagearray[0];
                                }
                            } else {
                                $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                            }
                        } else {
                            $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                        }
                        $findflagofwishlist = Wishlist::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
                        if (!empty($findflagofwishlist)) {
                            $like_Status = "1";
                        } else {
                            $like_Status = "0";
                        }
                        $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
                        
                        if (!empty($value->status)) {
                            $temp_datalist[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
                        }
                    }
                    $othercatlist[] = array("section_name" => $value1->name, "subcat_id" => $value1->id, "data" => $temp_datalist);
                }
            }

            $px++;
        }

        $flash_sale = array();
        $findflashdetails = Flashsale::where(['id' => 1, 'status' => '1'])->first();
        
        if (!empty($findflashdetails)) {

            $flashdatenew = date('Y-m-d H:i:s', strtotime($findflashdetails->time));
            $flashStr = strtotime($flashdatenew);
            $currentDate = date('Y-m-d H:i:s');
            $currentStr = strtotime($currentDate);

            if($currentStr >= $flashStr) {

                $flashdateend = date('Y-m-d H:i:s', strtotime($findflashdetails->endtime));
                $flashStrEnd = strtotime($flashdateend);
                $currentDateEnd = date('Y-m-d H:i:s');
                $currentStrEnd = strtotime($currentDateEnd);

                $timeDiff = abs($flashStrEnd - $currentStrEnd);
                $findflashdetailstime = $timeDiff;

                if($currentStrEnd <= $flashStrEnd) {

                    $findflashdetails->products = trim($findflashdetails->products);

                    if(strlen($findflashdetails->products)>=1) {
                      $productdetails = Product::whereIn('id', explode(",", $findflashdetails->products))->where(["category_id"=>$id, "deleteStatus"=>1,"status"=>1])->orderBy('id','DESC')->get();
                    } else {
                      $productdetails = Product::where(['subsubcategory_id'=>$findflashdetails->subsubcategory])->where(["category_id"=>$id,"deleteStatus"=>1,"status"=>1])->orderBy('id','DESC')->get();
                    }
                
                    foreach ($productdetails as $value) {
                        $imagearray = explode(",", $value->images);
                
                        if (!empty($imagearray)) {
                            if (!empty($imagearray[0])) {
                                if (strpos($imagearray[0], $this->baseurlslash) == false) {
                                    $imagearray[0] = $this->baseurl . $imagearray[0];
                                }
                            } else {
                                $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                            }
                        } else {
                            $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                        }
                        $findflagofwishlist = Wishlist::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
                        if (!empty($findflagofwishlist)) {
                            $like_Status = "1";
                        } else {
                            $like_Status = "0";
                        }
                        $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
                        $flash_sale[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
                    }
                } else {
                    $flash_sale = [];  
                    $findflashdetailstime ="";  
                }
            } else {
                $flash_sale = [];
                $findflashdetailstime ="";
            }
        }
        
        $subcategories_list1 = Subsubcategory::where(['category_id' => $id, 'occasion_status' => '1'])->orderBy('updated_at','DESC')->get();
        $subcategorylist_array1 = array();
        foreach ($subcategories_list1 as $value) {
            if (empty($value->image)) {
                $value->image = url('/public/') . "/img/imagenotfound.jpg";
            }
            $subcategorylist_array1[] = array("filter_type" => "", "filter_data" => $value->id, "image" => $value->image, "name" => $value->name);
        }
        
        $allinnerbanner = Banner::where('type', $id . "_inner")->orderBy('id', 'ASC')->get();
        $subcategorylist_array2 = array();
        foreach ($allinnerbanner as $value) {
            if (empty($value->image)) {
                $value->image = url('/public/') . "/img/imagenotfound.jpg";
            } else {
                $value->image = url('/public/') . $value->image;
            }

            $getCat = Subcategory::select("name")->where(['id'=>$value->subcategory_id])->first();
            
            $subcategorylist_array2[] = array("filter_type" => "", "filter_data" => $value->subcategory_id, "image" => $value->image, "name" => $getCat->name);
        }

        return $this->sendResponse1(['subcategory_slider' => $subcategorylist_array, 'flash_sale' => $flash_sale, "occasion_list" => $subcategorylist_array1, "bannerlist" => $subcategorylist_array2, "other_section" => $othercatlist,"flash_sale_time" => $findflashdetailstime], 'Inner page details retrieved successfully', $request->path());
    }

    public function referal_list(Request $request, $id) {
        $referArr = array();
        $user = User::where(['id' => $id])->select('referal_code')->first();
        $referArr = ['referal_code'=>$user->referal_code];

        $refers = Refer::where(['refercode' => $user->referal_code])->get();
        
        if(count($refers) > 0) {
            foreach($refers as $refer) {
                $referUser = User::where(['id' => $refer->referto])->select('name','email')->first();
                $referArr['list'][] = ["name"=>$referUser->name, "email"=>$referUser->email];
            }
        } else {
            $referArr['list'] = [];
        }

        $referArr['referal_price'] = 100;

        return $this->sendResponse($referArr, 'Referal list retrieved successfully', $request->path());
    }

    public function checkemail($email) {
        $email_inmembers = DB::table('sellers')->where('email', $email)->first();
        if (!empty($email_inmembers)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function themeorder(Request $request) {
        $details = Priority::orderby('section_number', 'asc')->pluck('image');
        return $this->sendResponse1($details, 'Successfully updated data', $request->path());
    }

    public function setthemeorder(Request $request) {
        $input = $request->all();
        // $return_array = explode(",", $input['simpleList']);
        // dd($return_array);
        $ikmj = 1;
        foreach ($input['simpleList'] as $value) {
            $finddetails = Priority::where('image', $value)->first();
            $finddetails->section_number = $ikmj;
            $finddetails->save();
            $ikmj++;
        }
        // dd(var_dump($input['simpleList']));
        $details = Priority::orderby('section_number', 'asc')->pluck('image');
        return $this->sendResponse1($details, 'Successfully updated data', $request->path());
    }

    public function checkphone1($email) {
        $email_inmembers = DB::table('users')->where('phone', $email)->first();
        if (!empty($email_inmembers)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function checkemail1($email) {
        $email_inmembers = DB::table('users')->where('email', $email)->first();
        if (!empty($email_inmembers)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function myorderhelp(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', ]);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
    
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
    
        $details = Order::join('users', 'users.id', '=', 'orders.user_id')->where(['orders.user_id'=>$input['user_id']])->select("orders.id as id", "orders.*", "users.name as buyer")->orderby('orders.id', 'DESC')->get();
        $return_array = array();
        
        if($details) {
            foreach ($details as $met) {
                $cartfindall = OrderDetail::where(['order_id'=> $met->id])->first();
                
                    $detailsproduct = Product::where('id', $met->product_id)->first();
                    $coupen = Cuponcode::select('discount')->where('name', $met->coupen_code)->first();
                    if($coupen) {
                        $coupendiscount = (int)$coupen->discount;
                    } else {
                        $coupendiscount = 0;
                    }
                    $percentagetemp = 100 - ($detailsproduct->sp * 100) / $detailsproduct->mrp;
                        
                    $getD = strtotime($met->created_at);
                    $addedDate = strtotime("+7 day", $getD);
                    $currentDate = date('d/m/y');
                    $currentConvert = strtotime($currentDate);

                    if($currentConvert > $addedDate) {
                        $exchangeStatus = 0;
                    } else {
                        $exchangeStatus = 1;
                    }

                        $return_array[] = array(
                            "image" => $this->baseurl . explode(",", $detailsproduct->images) [0], 
                            "sku" => $detailsproduct->sku, 
                            "brand" => $detailsproduct->brand, 
                            "product_id" => $detailsproduct->id, 
                            "id" => $met->id,
                            "order_number" => $met->order_number,
                            "Buyer" => $met->buyer, 
                            "location" => $met->location, 
                            "order_date" => date("d/m/y", strtotime($met->created_at)), 
                            "dispatch_by" => date("d/m/y", strtotime($met->dispatch_at)), 
                            "payment_type" => $met->payment_type, 
                            "coupan_code" => !empty($met->coupen_code) ? $met->coupen_code : '', 
                            "status" => $met->status,
                            "product_name" => $detailsproduct->name, 
                            "amount" => $cartfindall->final_price,
                            "percentage" => $coupendiscount,
                            "mrp" => $cartfindall->sale_price, 
                            "quantity" => $cartfindall->quantity,
                            "color" => $cartfindall->color, 
                            "size" => $cartfindall->size, 
                            "exchangeStatus"=>$exchangeStatus,
                            "size_label"=>$detailsproduct->size_label,
                        );
            }
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function exchangeorder($id, $status) {

        if($status == "reject") {

            $details = Orderrequest::where(['order_id' => $id])->first();
            if ($details) {
                return $this->sendError($request->path(), 'Order not found');
            }

            $orderData = Order::where(["id"=>$id])->first();
            $notifyData = [
                'user_id'=>$orderData->user_id,
                'order_id'=>$orderData->id,
                'order_number'=>$orderData->order_number,
                'type'=>'Order',
                'title'=>'Request cancel',
                'content'=>'Your order request for exchange has been cancelled'
            ];
            Notification::create($notifyData);
            $userDevice = Token::select('deviceType', 'deviceToken')->where(['user_id'=>$orderData->user_id])->first();
            $badge = Notification::where(["user_id"=>$orderData->user_id, 'status'=>0])->count();
            $notifyResponse = $this->pushNotification($notifyData, $userDevice, $badge);

            Orderrequest::where(['order_id' => $id])->update(["status"=>1]);

            return $this->sendResponse(["status" => "success",'msg'=>$notifyResponse], "Order for exchange completed", $request->path());

        } else {
            
            $requestDetail = Orderrequest::where(['order_id' => $id])->first();

            if($requestDetail->request_type == "exchange") {

                $orderData = Order::where(["id"=>$id])->first();

                $http = new Client;
                $generateWBN = $http->get(url('https://staging-express.delhivery.com/waybill/api/bulk/json/?cl='.$userGet->name.'&token=dcafa28eb05642c39f31b14724e28cf45f91256a&count=1'));
                $data_wbn = json_decode($generateWBN->getBody());

                $findp = Product::find($orderData->product_id);
                $shippingDate = $findp->ships_in;
                $shipDate = "+$shippingDate days";

                $orderData = array("order_number"=> $orderData->order_number.'-Re',
                                    "product_id" => $orderData->product_id,
                                    "seller_id" => $orderData->seller_id,
                                    "user_id" => $orderData->user_id,
                                    "location" => $orderData->location,
                                    "state" => $orderData->state,
                                    "city" => $orderData->city,
                                    "country" => $orderData->country,
                                    "pincode" => $orderData->pincode,
                                    "phone" => $orderData->phone,
                                    "name" => $orderData->name,
                                    "address_type" => $orderData->address_type,
                                    "dispatch_at" => date('Y-m-d', strtotime($shipDate)),
                                    "payment_type" => $orderData->payment_type,
                                    "transcation_id" => '',
                                    "coupen_code" => $orderData->coupen_code,
                                    "gift_wrap_status" => $orderData->gift_wrap_status,
                                    "payment_status" => 'success',
                                    "status" => 'reorder',
                                );
                    $oid = Order::create($orderData);

                $orderDetailData = OrderDetail::where(["order_id"=>$id])->first();

                $orderProducts = array("order_id" => $oid->id,
                                    "size" => $requestDetail->size,
                                    "color" => $requestDetail->color,
                                    "quantity" => $orderDetailData->quantity,
                                    "price" => $orderDetailData->price,
                                    "sale_price" => $orderDetailData->sale_price,
                                    "discount_price" => $orderDetailData->discount_price,
                                    "total_price" => $orderDetailData->total_price,
                                    "refer_discount"=> $orderDetailData->refer_discount,
                                    "giftwrap_amount"=> $orderDetailData->giftwrap_amount,
                                    "seller_shipping"=> $orderDetailData->seller_shipping,
                                    "valuable_tax" => $orderDetailData->valuable_tax,
                                    "gst_tax" => $orderDetailData->gst_tax,
                                    "tcs_tax" => $orderDetailData->tcs_tax,
                                    "final_price" => $orderDetailData->final_price,
                                    "seller_amount" => $orderDetailData->seller_amount,
                                    "admin_commision" => $orderDetailData->admin_commision,
                                    "total_shipping"=> $orderDetailData->total_shipping,
                                    "wbn_num" => $data_wbn
                                );
                    OrderDetail::create($orderProducts);
                
                $notifyData = [
                    'user_id'=>$orderData->user_id,
                    'order_id'=>$oid->id,
                    'order_number'=>$orderData->order_number.'-Re',
                    'type'=>'Order',
                    'title'=>'Re-Order - '.$orderNum,
                    'content'=>'Your order request for exchange has been confirmed'
                ];
                Notification::create($notifyData);
                $userDevice = Token::select('deviceType', 'deviceToken')->where(['user_id'=>$orderData->user_id])->first();
                $badge = Notification::where(["user_id"=>$orderData->user_id, 'status'=>0])->count();
                $notifyResponse = $this->pushNotification($notifyData, $userDevice, $badge);

                Orderrequest::where(['order_id' => $id])->update(["status"=>2]);
                DB::table('orders')->where(['id' => $input['id']])->update(['status' => "cancel"]);
            
            } else {

                $order = Order::where(["id"=>$id])->first();
                $details = OrderDetail::where(["order_id"=>$id])->first();
                $sellerDetail = Seller::where(['id' => $orderData->seller_id])->first();
                $prod = Product::where(['id'=>$orderData->product_id])->first();
                
                if($order->payment_type == "Cash On Delivery") {
                    $paymentType = "COD";
                    $codamt = $details->final_price;
                } else {
                    $paymentType = "Prepaid";
                    $codamt = 0;
                }

                $jsonData['shipments'][] = [
                            "add" => $order->location,
                            "address_type" => $order->address_type,
                            "phone" => $order->phone,
                            "payment_mode"=> "Pickup",
                            "name" => $order->name,
                            "city" => $order->city,
                            "state" => $order->state,
                            "pin" => $order->pincode,
                            "country" => $order->country,
                            "order" => $order->order_number,
                            "hsn_code" => $prod->sku,
                            "client" => "THEBRANDSTANDSURFACE-B2C",
                            "shipment_width" =>$prod->width,
                            "seller_gst_tin" => $sellerDetail->gstnum,
                            "seller_name" =>$sellerDetail->name,
                            "seller_add" =>$sellerDetail->address,
                            "return_state"=> $order->state,
                            "return_city"=> $order->city,
                            "return_country"=> $order->country,
                            "return_add"=> $order->location,
                            "return_pin"=> $order->pincode,
                            "extra_parameters"=> [
                              "return_reason"=> $requestDetail->reason
                            ],
                            "return_name"=>$order->name,

                            "weight" => $prod->weight,
                            "cod_amount" => $codamt,
                            "dangerous_good" => "False",
                            "waybill" => $details->wbn_num,
                            "order_date" =>$order->created_at,
                            "total_amount" => $details->final_price,
                            "quantity" =>$details->quantity,
                ];

                $jsonData['pickup_location'] = [
                        "name" => $sellerDetail->name,
                        "city" => $sellerDetail->city,
                        "pin" => $sellerDetail->pincode,
                        "country" => $sellerDetail->country,
                        "phone" => $sellerDetail->phone,
                        "add" => $sellerDetail->address
                ];

                $jsonarray = 'format=json&data='. json_encode($jsonData);
                $this->shippingapiordercreate($jsonarray);

                $jsonarray = [
                    "pickup_time"=>date("H:i:s", strtotime('+12 hours')),
                    "pickup_date"=>date("H:i:s", strtotime('+12 hours')),
                    "pickup_location"=>$sellerDetail->name,
                    "expected_package_count"=>1,
                ];
                $this->shippingapiorderpickup($jsonarray);
                DB::table('orders')->where(['id' => $id])->update(['status' => "return"]);
            }

            return $this->sendResponse(["status" => "success",'msg'=>$notifyResponse], "Order request updated", $request->path());
        }
    }

    public function generateOrderid(Request $request) {
        $orderNum = 'YOD'. $this->random_strings(5);
        $order = ["order_number"=>$orderNum];
        return $this->sendResponse($order, 'Notification list retrieved successfully', $request->path());
    }


    public function changepasswordbyphone(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['phone' => 'required', 'password' => 'required|min:6', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $post = User::where('phone', $input['phone'])->first();
        $post->password = Hash::make($input['password']);
        $post->save();
        return $this->sendResponse(['status' => 'success'], 'Password updated successfully.', $request->path());
    }

    public function set_as_default(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['user_id' => 'required', 'token' => 'required', 'address_id' => 'required', 'remark' => 'required', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
        if (isset($input['remark']) && !empty($input['remark']) && $input['remark'] == 1) {
            DB::table('addresses')->where(['user_id' => $input['user_id'], 'remark' => '1'])->update(['remark' => '0']);
        }
        $detailsadd = Address::find($input['address_id']);
        $detailsadd->remark = $input['remark'];
        $detailsadd->save();
        return $this->sendResponse(['status' => "success"], 'Address set as default successfully', $request->path());
    }
    

    public function homeapi(Request $request) {
        $input = $request->all();
        $return_array = array();
        $banners_array = array();
        $banner1 = array();
        $banner2 = array();
        $men = array();
        $women = array();
        $kids = array();
        $slider1 = array();
        $slider2 = array();
        $handpicked = array();
        $stylefeed = array();
        $brandinfocus = array();
        $hotdeal = array();
        $themedetailsarray = array();
        $announceArr = array();
        $num_of_addtocart = Cart::where(['user_id' => $input['user_id']])->count();
        
        $bannerdetails = Banner::get();
        foreach ($bannerdetails as $value) {

            $checkSubSubcat = Subsubcategory::where('id', $value->subsubcategory_id)->first();

            if($checkSubSubcat && $checkSubSubcat->status == 1) {

                if (!empty($value->image)) {
                    if (strpos($value->image, $this->baseurlslash) == false) {
                        $value->image = $this->baseurl . $value->image;
                    }
                } else {
                    $value->image = url('/public/') . "/img/imagenotfound.jpg";
                }

                if ($value->type == 'homeslider') {
                    $banners_array[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
                if ($value->type == 'banner1') {
                    $banner1[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
                if ($value->type == 'banner2') {
                    $banner2[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
                if ($value->type == 'slider1') {
                    $slider1[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
                if ($value->type == 'slider2') {
                    $slider2[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
                if ($value->type == 'handpicked') {
                    $handpicked[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
                if ($value->type == 'stylefeed') {
                    $stylefeed[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
                if ($value->type == 'hotdeal') {
                    $hotdeal[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
            }
        }
        
        $deatilsbrand = Brand::where('focus_status', '1')->get();
        foreach ($deatilsbrand as $value) {
            if (!empty($value->image)) {
                if (strpos($value->image, $this->baseurlslash) == false) {
                    $value->image = $this->baseurl . $value->image;
                }
            } else {
                $value->image = url('/public/') . "/img/imagenotfound.jpg";
            }
            $brandinfocus[] = array("filter_data" => $value->name, "type" => "brand", "image" => $value->image);
        }
        
        $themedetails = Theme::get();
        foreach ($themedetails as $value) {
            if (!empty($value->imagemain)) {
                if (strpos($value->imagemain, $this->baseurlslash) == false) {
                    $value->imagemain = $this->baseurl . $value->imagemain;
                }
            } else {
                $value->imagemain = url('/public/') . "/img/imagenotfound.jpg";
            }
            if (!empty($value->herimage)) {
                if (strpos($value->herimage, $this->baseurlslash) == false) {
                    $value->herimage = $this->baseurl . $value->herimage;
                }
            } else {
                $value->herimage = url('/public/') . "/img/imagenotfound.jpg";
            }
            if (!empty($value->himimage)) {
                if (strpos($value->himimage, $this->baseurlslash) == false) {
                    $value->himimage = $this->baseurl . $value->himimage;
                }
            } else {
                $value->himimage = url('/public/') . "/img/imagenotfound.jpg";
            }
            $themedetailsarray[] = array("image" => $value->imagemain, "himimage" => $value->himimage, "herimage" => $value->herimage, "title" => $value->title, "description" => $value->description, "filter_data" => (string)$value->id, "type" => "theme_id");
        }
        
        $flash_sale = array();
        $findflashdetails = Flashsale::where(['id' => 1, 'status' => '1'])->first();
        
        if (!empty($findflashdetails)) {
            $flashdatenew = date('Y-m-d H:i:s', strtotime($findflashdetails->time));
            $flashStr = strtotime($flashdatenew);
            $currentDate = date('Y-m-d H:i:s');
            $currentStr = strtotime($currentDate);

            if($currentStr >= $flashStr) {

                $flashdateend = date('Y-m-d H:i:s', strtotime($findflashdetails->endtime));
                $flashStrEnd = strtotime($flashdateend);
                $currentDateEnd = date('Y-m-d H:i:s');
                $currentStrEnd = strtotime($currentDateEnd);

                if($currentStrEnd <= $flashStrEnd) {

                    $findflashdetailsstatus = $findflashdetails->status;
                    $flashtime = date('d-m-Y, h:i:s', strtotime($findflashdetails->endtime));
                    $flashtime = strtotime($findflashdetails->endtime);
                    $currentDate = date('d-m-Y, h:i:s');
                    $currentDateStr = strtotime($currentDate);

                    $timeDiff = abs($flashtime - $currentDateStr);
                    $findflashdetailstime = $timeDiff;

                    $findflashdetails->products = trim($findflashdetails->products);

                    if(strlen($findflashdetails->products)>=1) {
                      $productdetails = Product::whereIn('id', explode(",", $findflashdetails->products))->where(["deleteStatus"=>1,"status"=>1])->orderBy('id','DESC')->get();
                    } else {
                      $productdetails = Product::where(['subsubcategory_id'=>$findflashdetails->subsubcategory])->where(["deleteStatus"=>1,"status"=>1])->orderBy('id','DESC')->get();
                    }
                    
                    foreach ($productdetails as $value) {
                        $imagearray = explode(",", $value->images);
                        if (!empty($imagearray)) {
                            if (!empty($imagearray[0])) {
                                if (strpos($imagearray[0], $this->baseurlslash) == false) {
                                    $imagearray[0] = $this->baseurl . $imagearray[0];
                                }
                            } else {
                                $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                            }
                        } else {
                            $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                        }
                        $findflagofwishlist = Wishlist::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
                        
                        if (!empty($findflagofwishlist)) {
                            $like_Status = "1";
                        } else {
                            $like_Status = "0";
                        }
                        $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
                        $flash_sale[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
                    }
                }  else {
                    $flash_sale = [];
                    $findflashdetailsstatus = '';
                    $findflashdetailstime = '';    
                } 
            } else {
                $flash_sale = [];
                $findflashdetailsstatus = '';
                $findflashdetailstime = '';    
            }
        } else {
            $flash_sale = [];
            $findflashdetailsstatus = '';
            $findflashdetailstime = '';
        }

        $announceGets = Offer::where(['offer_type' => 3])->orderBy('id','ASC')->get();
        if($announceGets) {
            foreach($announceGets as $announceGet) {
                $announceArr[] = array("filter_data" => $announceGet->catid, "type" => "subsubcategory_id", "content" => $announceGet->description);
            }
        }


        $men = Subcategory::where(['category_id' => 'MEN'])->skip(0)->take(5)->get();
        $women = Subcategory::where(['category_id' => 'WOMEN'])->skip(0)->take(5)->get();
        $kids = Subcategory::where(['category_id' => 'KIDS'])->skip(0)->take(5)->get();
        
        $return_array = array("banners" => $banners_array,
                              "handpicked" => $handpicked, 
                              "banner1" => $banner1, 
                              "banner2" => $banner2, 
                              "hotdeal" => $hotdeal, 
                              "slider2" => $slider2, 
                              "slider1" => $slider1, 
                              "stylefeed" => $stylefeed, 
                              "brandinfocus" => $brandinfocus, 
                              "themedetails" => $themedetailsarray, 
                              "flash_sale" => $flash_sale, 
                              "flash_sale_status" => $findflashdetailsstatus, 
                              "flash_sale_time" => $findflashdetailstime,
                              "num_of_addtocart" => $num_of_addtocart, 
                              'announcement_bar' => $announceArr, 
                              'men' => $men, 
                              'women' => $women, 
                              'kids' => $kids);
        
        return $this->sendResponse($return_array, 'Home details retrieved successfully', $request->path());
    }


    public function themelistforadmin(Request $request) {
        $details = Theme::select("id", "imagemain", "himimage", "herimage", "title", "description")->get();
        $return_array = array();
        foreach ($details as $met) {
            if (!empty($met->imagemain)) {
                if (strpos($met->imagemain, $this->baseurlslash) == false) {
                    $met->imagemain = $this->baseurl . $met->imagemain;
                }
            } else {
                $met->imagemain = url('/public/') . "/img/imagenotfound.jpg";
            }
            if (!empty($met->himimage)) {
                if (strpos($met->himimage, $this->baseurlslash) == false) {
                    $met->himimage = $this->baseurl . $met->himimage;
                }
            } else {
                $met->himimage = url('/public/') . "/img/imagenotfound.jpg";
            }
            if (!empty($met->herimage)) {
                if (strpos($met->herimage, $this->baseurlslash) == false) {
                    $met->herimage = $this->baseurl . $met->herimage;
                }
            } else {
                $met->herimage = url('/public/') . "/img/imagenotfound.jpg";
            }
            $return_array[] = array($met->title, $met->description, $met->imagemain, $met->himimage, $met->herimage, $met->id);
        }
        return $this->sendResponse1($return_array, 'Theme list retrieved successfully', $request->path());
    }

    public function brandlistforadmin(Request $request) {
        $details = Brand::select("id", "name", "image", "description", "focus_status", "admin_status")->get();
        $return_array = array();
        foreach ($details as $met) {
            if (!empty($met->image)) {
                if (strpos($met->image, $this->baseurlslash) == false) {
                    $met->image = $met->image;
                }
            } else {
                $met->image = url('/public/') . "/img/imagenotfound.jpg";
            }
            $return_array[] = array($met->name, $met->description, $met->image, [$met->admin_status, $met->id], [$met->focus_status, $met->id], $met->id);
        }
        return $this->sendResponse1($return_array, 'Brand list retrieved successfully', $request->path());
    }

    public function brandviewforadmin(Request $request, $id) {
        $details = Brand::select("id", "name", "image", "description", "focus_status", "admin_status")->where(["id"=>$id])->first();
        $return_array = array();

        if (!empty($details->image)) {
            
                $details->image = $details->image;
            
        } else {
            $details->image = url('/public/') . "/img/imagenotfound.jpg";
        }
        $return_array = ["name"=>$details->name, "description"=>$details->description, "image"=> $details->image,"id"=> $details->id];
        
        return $this->sendResponse1($return_array, 'Brand retrieved successfully', $request->path());
    }

    public function brandeditforadmin(Request $request, $id) {
        $details = Brand::where(["id"=>$id])->first();

        if($details) {
            
            $details->name = $request->name;
            $details->image = $request->image;
            $details->description = $request->description;
            $details->save();

            return $this->sendResponse(['status' => 'success'], 'Brand updated successfully', $request->path());
        } else {
            return $this->sendError($request->path(), 'Brand not found');
        }
    }

    public function brandapprovalstatuschange(Request $request, $userid) {
        $details = Brand::find($userid);
        if ($details->admin_status) {
            $details->admin_status = "";
        } else {
            $details->admin_status = "1";
        }
        $details->save();
        return $this->sendResponse1([$details->admin_status, $userid], 'Brand Approval status updated successfully', $request->path());
    }
    public function brandfocusstatuschange(Request $request, $userid) {
        $details = Brand::find($userid);
        if ($details->focus_status) {
            $details->focus_status = "";
        } else {
            $details->focus_status = "1";
        }
        $details->save();
        return $this->sendResponse1([$details->focus_status, $userid], 'Brand Infocus status updated successfully', $request->path());
    }

    public function subsubcatdelete(Request $request, $userid) {
        $verifyProduct = Product::where(["subsubcategory_id"=>$userid])->first();
        if($verifyProduct) {
            return $this->sendError($request->path(), 'Sub-SubCategory not able to delete because related product available');
        } else {

            $verifyBanner = Subsubcategory::where(["id"=>$userid])->first();
            if($verifyBanner) {
                return $this->sendError($request->path(), 'Sub-SubCategory not available');
            } else {
                DB::table("subsubcategories")->where('id', $userid)->delete();
                return $this->sendResponse1([], $product_name . ' Deleted successfully', $request->path());
            }
        }
    }
    
    public function catdeletebrand(Request $request, $userid) {
        $details = DB::table("brands")->where('id', $userid)->first();

        if($details) {
            $findbrand = Product::where(["brand"=>$details->name])->first();
            
            if($findbrand) {
                return $this->sendError($request->path(), 'Brand not able to delete because related product available');
            } else {
                $product_name = $details->name;
                DB::table("brands")->where('id', $userid)->delete();
                return $this->sendResponse1([], $product_name . ' deleted successfully', $request->path());
            }
        } else {
            return $this->sendError($request->path(), 'Brand Not found');
        }
    }

    public function catdeleteb(Request $request, $userid) {
        DB::table("banners")->where('id', $userid)->delete();
        return $this->sendResponse1([], 'A banner deleted successfully', $request->path());
    }
    
    public function catdeletet(Request $request, $userid) {
        $verifyTheme = Product::where(["theme_id"=>$userid])->first();
        
        if($verifyTheme) {
            return $this->sendError($request->path(), 'Theme not able to delete because related product available');
        }else {
            DB::table("themes")->where('id', $userid)->delete();
            return $this->sendResponse1([], 'A Theme deleted successfully', $request->path());
        }
    }

    public function catdelete11(Request $request, $userid) {
        
        $details = DB::table("subsubcategories")->where('id', $userid)->first();
        $product_name = $details->name;
        DB::table("subsubcategories")->where('id', $userid)->delete();
        return $this->sendResponse1([], $product_name . ' deleted successfully', $request->path());
    }
    public function catdelete113(Request $request, $userid) {
        $details = Cuponcode::where('id', $userid)->first();
        if($details) {
            $product_name = $details->name;
            $checkinOffer = Offer::where(["coupon_id"=>$userid])->first();
            if($checkinOffer) {
                return $this->sendResponse1([], 'Coupon added in offers! Please remove first from offers', $request->path());
            } else {
                Cuponcode::where('id', $userid)->delete();
                return $this->sendResponse1([], $product_name . ' deleted successfully', $request->path());
            }
        } else {
            return $this->sendResponse1([], 'Coupon not found', $request->path());
        }
    }
    public function catdelete11pp(Request $request, $userid) {
        $details = Flashsale::find(1);
        if(strlen($details->products) > 0) {
            $newarray = explode(",", $details->products);
            $return_array = array();
            foreach ($newarray as $value) {
                if ($value != $userid) {
                    $return_array[] = $value;
                }
            }
            $details->products = implode(",", $return_array);
        } else {
            $details->subsubcategory = "";
        }
        $details->save();
        return $this->sendResponse1([], 'Product removed from flash sale successfully', $request->path());
    }


    function getImageByColor(Request $request) {

     $input = $request->all();
     $validator = Validator::make($input, ['product_id'=>'required']);
     if ($validator->fails()) {
         return $this->sendError($request->path(), $validator->errors()->first());
     }

     $colorCode = $request->color_code;
     $image  = ProductImageColor::where(['color'=>$colorCode,'product_id'=>$request->product_id])->first();
     $return_new_array = array();
     
        if(!empty($image)) {
            
            $image_array = explode(",", $image->images);

            foreach ($image_array as $key => $value) {
                $return_new_array[] = url('public/').$value;
            }
        }

        $priceDetail = array();
        $proPriceDetail = ProductPrice::where(["product_id"=>$request->product_id, "size"=>$request->size, "color"=>$request->color_code])->first();
        if($proPriceDetail) {
            $priceDetail = ["sp"=>$proPriceDetail->sp, "mrp"=>$proPriceDetail->mrp, "percentage"=>$proPriceDetail->discount];
        } else {
            $priceDetail = ["sp"=>"", "mrp"=>"", "percentage"=>""];

            return $this->sendError($request->path(), 'Product not available with this variants');
        }

        return $this->sendResponse1(['image_list'=>$return_new_array, 'priceDetail'=>$priceDetail],'image show successfully', $request->path());
    }

    public function fileuploadebrand(Request $request) {
        $input = $request->all();
        $fileuploadedpath = "";
        if ($request->gstnumfile) {
            $thumbnail_img = $request->gstnumfile;
            list($type, $thumbnail_img) = explode(';', $thumbnail_img);
            list(, $thumbnail_img) = explode(',', $thumbnail_img);
            $thumbnail_img = base64_decode($thumbnail_img);
            $thumbnail_img_name = rand(1000000, 9999999) . date('ymdhis') . '.png';
            $thumbnail_img_name_dir = public_path('brandimg/' . $thumbnail_img_name);
            file_put_contents($thumbnail_img_name_dir, $thumbnail_img);
            $fileuploadedpath =  '/brandimg/' . $thumbnail_img_name;
        }
        return $this->sendResponse1($fileuploadedpath, 'Image uploaded successfully', $request->path());
    }
    
    public function sublistbycat(Request $request, $id) {
        $details = Subcategory::where(['category_id' => $id])->get();
        $return_array = array();
        foreach ($details as $value) {
            $return_array[] = [$value->id, $value->name];
        }
        return $this->sendResponse($return_array, 'Subcategory list retrieved successfully', $request->path());
    }

    public function subsubcatbyid(Request $request, $id) {
        $details = SubSubcategory::where(['id' => $id])->first();
        return $this->sendResponse($details, 'Subsubcategory details retrieved successfully', $request->path());
    }

    public function sublistbycatremark(Request $request, $id) {
        $details = Subsubcategory::where(['subcategory_id' => $id])->get();
        $return_array = array();
        foreach ($details as $value) {
            if($value->status == 1) {
                if($value->occasion_status == 1) {
                    $name = $value->name . ' - occasion';
                } else {
                    $name = $value->name;
                }
                $return_array[] = [$value->id, $name];
            }
        }
        return $this->sendResponse($return_array, 'Remark list retrieved successfully', $request->path());
    }

    public function brandlistforseller(Request $request) {
        $details = Brand::get();
        $return_array = array();
        foreach ($details as $value) {
            if (!empty($value->admin_status)) {
                $return_array[] = [$value->id, $value->name];
            }
        }
        return $this->sendResponse($return_array, 'Brand list retrieved successfully', $request->path());
    }

    public function searchlist(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['key' => 'required']);
        $brandArr = array();
        $catArr = array();

        $getBrands = Product::select('brand','subsubcategory_id', 'category_id')->where('brand', 'like', '%' . $input['key'] . '%')->distinct('subsubcategory_id')->where(["deleteStatus"=>1,"status"=>1])->get();
        
        foreach($getBrands as $getBrand) {
            $subsubname = Subsubcategory::select('name','status')->where('id',$getBrand->subsubcategory_id)->first();
            if($subsubname) {

                if($subsubname->status == 1) {
                    $text = $getBrand->brand.' '.$subsubname->name.' '.$getBrand->category_id;
                    $brandArr[] =  ["name"=>$text, "subsubcatid"=> "$getBrand->subsubcategory_id", "catid"=>$getBrand->category_id, 'brand'=>$getBrand->brand];
                }
            }
        }

        $getCats = Subsubcategory::select('id', 'name', 'category_id','status')->where('name', 'like', '%' . $input['key'] . '%')->get();

        foreach($getCats as $getCat) {

            if($getCat->status == 1) {
                $text = $getCat->name.' '.$getCat->category_id;
                $catArr[] = ["name"=>$text, "subsubcatid"=> "$getCat->id", "catid"=>$getCat->category_id, 'brand'=>'0'];
            }
        }
    
        $finalarray = array_merge($brandArr, $catArr);
        sort($finalarray);
        return $this->sendResponse($finalarray, 'List retrieved successfully', $request->path());
    }

    public function themelistforseller(Request $request) {
        $details = Theme::get();
        $return_array = array();
        foreach ($details as $value) {
            $return_array[] = [$value->id, $value->title];
        }
        return $this->sendResponse($return_array, 'Brand list retrieved successfully', $request->path());
    }

    public function addtocart1($user_id, $arrayofproduct, $arrayofsize, $arrayofcolor, $arrayofqty) {
        $q = 0;
        foreach ($arrayofproduct as $value) {
            $checkCart = Cart::where(["product_id"=>$value,"user_id"=>$user_id])->first();

            if($checkCart) {
                Cart::where(["product_id"=>$value, "user_id" => $user_id])->delete();
            }
            $input = array("product_id" => $value, "user_id" => $user_id, "size" => $arrayofsize[$q], "color" => $arrayofcolor[$q], "quantity" => $arrayofqty[$q]);
            Cart::create($input);
            $q++;
        }
    }

    public function langtypesave(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['user_id' => 'required', 'lang' => 'required']);
        
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $user = User::where('id', $input["user_id"])->first();
        if($user) {
            $user->lang_type = $input["lang"];
            $user->save();
        }

        return $this->sendResponse([], 'Language saved successfully', $request->path());
    }

    public function productdetails(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['product_id' => 'required', ]);
        
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $num_of_addtocart = Cart::where(['user_id' => $input['user_id']])->count();
        
        $productdetails_list = Product::find($input['product_id']);
        
        // Similar products
        $similar_list = Product::where('subsubcategory_id', $productdetails_list->subsubcategory_id)->get();
        //Size chart start
        $subSubCat_id = $productdetails_list->subsubcategory_id;
        $getdata = SizeChart::where('subsub_cat_id',$subSubCat_id)->get();
        $newData = array();
        foreach($getdata as $data) {
           $heading = explode(',', $data->heading); 
           $heading_values = explode(',', $data->heading_values); 
           //echo "<pre>";print_r($heading);die;
           foreach($heading as $k=>$v) {

           // echo "<pre>";print_r($k);
            @$newData[$v][] = $heading_values[$k]; 
           }
       }

        foreach($newData as $k1=>$v1) {
            $new = [];
            foreach($v1 as $c){
                if($c==''){
                   $t = 'N/A';
                }else{
                 $t = $c;
                }
                $new[] = $t;
            }

          $sizeChartRes[] = array(
                         'key' => $k1?$k1:'N/A',
                         'value' => $new
          );
       }

       if(!empty($sizeChartRes)) {
        $sizeChartRes1 = $sizeChartRes;
       } else {
         $sizeChartRes1 = [];
       }
       //end size chart
        $similar = array();
        foreach ($similar_list as $val) {
            if ($val->id != $input['product_id']) {
        
                if ($val->images) {
                    $img = explode(',', $val->images);
                    $res['image'] = $this->baseurl. $img[0];
                } else {
                    $res['image'] = url('/public/') . "/img/imagenotfound.jpg";
                }
                $res['product_id'] = $val->id;
                $res['name'] = $val->name;
                $res['mrp'] = $val->mrp;
                $res['discount'] = round($val->discount);
                $res['brand'] = $val->brand;
                $percentage = 100 - ($val->sp * 100) / $val->mrp;
                $res['percentage'] = round($percentage);
                $res['sp'] = $val->sp;
                $similar[] = $res;
            }
        }

        // customer also like products
        $oldOrders = Order::select("product_id")->distinct('product_id')->get();
        if(count($oldOrders) > 0) {
            $arrOldOrder = array();
            foreach ($oldOrders as $oldOrder) {
                $arrOldOrder[] = $oldOrder->product_id;
            }

            $liked_product = Product::whereIn('id', $arrOldOrder)->where(["deleteStatus"=>1,"status"=>1])->get();
            $curtomer_liked = array();
            
            foreach ($liked_product as $ke) {
            
                if ($ke->images) {
                    $img = explode(',', $ke->images);
                    $res['image'] = $this->baseurl . $img[0];
                } else {
                    $res['image'] = url('/public/') . "/img/imagenotfound.jpg";
                }
                $res['product_id'] = $ke->id;
                $res['name'] = $ke->name;
                $res['mrp'] = $ke->mrp;
                $res['discount'] = round($ke->discount);
                $res['brand'] = $ke->brand;
                $percentage = 100 - ($val->sp * 100) / $val->mrp;
                $res['percentage'] = round($percentage);
                $res['sp'] = $val->sp;
                $curtomer_liked[] = $res;
            }
        } else {
            $liked_product = Product::where('subsubcategory_id', $productdetails_list->subsubcategory_id)->where(["deleteStatus"=>1,"status"=>1])->get();
            $curtomer_liked = array();
            
            foreach ($liked_product as $ke) {
            
                if ($ke->images) {
                    $img = explode(',', $ke->images);
                    $res['image'] = $this->baseurl . $img[0];
                } else {
                    $res['image'] = url('/public/') . "/img/imagenotfound.jpg";
                }
                $res['product_id'] = $ke->id;
                $res['name'] = $ke->name;
                $res['mrp'] = $ke->mrp;
                $res['discount'] = round($ke->discount);
                $res['brand'] = $ke->brand;
                $percentage = 100 - ($val->sp * 100) / $val->mrp;
                $res['percentage'] = round($percentage);
                $res['sp'] = $val->sp;
                $curtomer_liked[] = $res;
            }
        }

        $solderdetails = Seller::find($productdetails_list->user_id);
        
        if (!empty($solderdetails)) {
            $solder_name = $solderdetails->name;
        } else {
            $solder_name = "";
        }
        $arrayimage = explode(",", $productdetails_list->images);
        $arrayimage1 = array();
        
        foreach ($arrayimage as $value) {
        
            if (!empty($value)) {
        
                if (strpos($value, $this->baseurlslash) == false) {
                    $arrayimage1[] = $this->baseurl . $value;
                }
            } else {
                $arrayimage1[] = url('/public/') . "/img/imagenotfound.jpg";
            }
        }

        if(strlen($productdetails_list->sizechart) > 1) {
            $sizeimg = $this->baseurlslash . $productdetails_list->sizechart;
        } else {
            $sizeimg = '';
        }
        $percentage = 100 - ($productdetails_list->sp * 100) / $productdetails_list->mrp;
        
        $bankOfferArr = array();
        $getBankOffers = Offer::where('offer_type',1)->get();
        if($getBankOffers) {
            foreach($getBankOffers as $getBankOffer) {
                $bankOfferArr[] = $getBankOffer->description;
            }
        }
        
        $otherOfferArr = array();
        $checkPromotions = Promotion::where(["seller_id"=>$solderdetails->id])->get();
        if($checkPromotions) {
            foreach($checkPromotions as $checkPromotion) {

                $checkProductPromotion = PromotionList::where(["product_id"=>$productdetails_list->id,'status'=>1])->first();
                if($checkProductPromotion) {
                    $getOtherOffers = Offer::where(['id'=>$checkPromotion->offer_id, 'offer_type'=>2, 'catid'=> $productdetails_list->subsubcategory_id])->first();
                    if($getOtherOffers) {

                        $getCoupon = Cuponcode::where('id', $getOtherOffers->coupon_id)->first();
                        if($getCoupon) {
                            $otherOfferArr[] = $getOtherOffers->description .'- CODE : '.$getCoupon->name;
                        }
                    }
                }
            }
        }


        if(empty($input['user_id'])) {
            $wishHave = "0";
        } else {
            $wishCheck = Wishlist::where(["user_id"=>$input['user_id'],"product_id"=>$input['product_id']])->first();
            if($wishCheck) {
                $wishHave = "1";
            } else {
                $wishHave = "0";
            }
        }

        $catName = Subsubcategory::select('name','id')->where(["id"=>$productdetails_list->subsubcategory_id])->first();
        
        if(!empty($productdetails_list->defaultcolor)) {
            $colorName = Color::select('name')->where(["hexcode"=>$productdetails_list->defaultcolor])->first();
            if($colorName) {
                $defaultcolorname = $colorName->name;
                if($colorName) {
                    $colorname = $colorName->name;
                } else {
                    $colorname = "";  
                }
            } else {
                $defaultcolorname = "";
                $colorname = "";  
            }
        } else {
            $defaultcolorname = "";
            $colorname = "";  
        }


        $attributeArr = array();
        $getAttris = Attribute::where(["product_id"=>$input['product_id']])->get();
        foreach($getAttris as $getAttri) {
            $attributeArr[] = ["keyName"=>$getAttri->label, "keyValue"=>$getAttri->labeltext];
        }
        if($productdetails_list->brand) {
            $brand = $productdetails_list->brand;
        } else {
            $brand = "";  
        }
        if($catName->name) {
            $catname = $catName->name;
        } else {
            $catname = "";  
        }

        if(!empty($productdetails_list->size)) {
            $getSizearr = explode(",", $productdetails_list->size);
        }else {
            $getSizearr = [];
        }
        if(!empty($productdetails_list->color)) {
            $getColorarr = explode(",", $productdetails_list->color);
        }else {
            $getColorarr = [];
        }

        $return_array = array("image_list" => $arrayimage1, 
                            'quantity' => $productdetails_list->quantity, 
                            "brand" => $productdetails_list->brand,
                            "morebrand"=>[
                                        "More $catName->name By $brand",
                                        "More $colorname $catname",
                                        "More $catname",
                                    ],
                            "subsubcatid"=>$catName->id,
                            "name" => $productdetails_list->name, 
                            "description" => $productdetails_list->description, 
                            "fit" => $productdetails_list->fit?$productdetails_list->fit:'',
                            "mrp" => $productdetails_list->mrp, 
                            "sp" => $productdetails_list->sp, 
                            "percentage" => round($percentage), 
                            "size" => $getSizearr, 
                            "color" => $getColorarr, 
                            "defaultcolor" => $productdetails_list->defaultcolor,
                            "defaultcolorname" => $defaultcolorname,
                            "num_of_addtocart" => $num_of_addtocart, 
                            "sold_by" => $solder_name, 
                            "hsn_code" => $productdetails_list->hsn_code, 
                            "delivery_time" => $productdetails_list->ships_in, 
                            'similar_product' => $similar, 
                            'customer_also_liked' => $curtomer_liked,
                            'offers' => $bankOfferArr,
                            "details_offer" => $otherOfferArr,
                            "sizechart"=>$sizeimg,
                            "wishHave"=>$wishHave,
                            "attributes"=>$attributeArr,
                            'sizechart' => $sizeChartRes1,
                            "is_variant"=>$productdetails_list->is_variant,
                            "variant_type"=>$productdetails_list->variant_type,
                            "size_label"=>$productdetails_list->size_label,

                        );
        return $this->sendResponse($return_array, 'Product details retrieved successfully', $request->path());
    }

    public function filter_section(Request $request) {
        $input = $request->all();
        $colorData = array();
        $hexData = array();
        $sizeData = array();
        $sizeDataa = array();
        $brandData = array();
        $brandDataa = array();

        $getColors = Color::select("name","hexcode")->get();
        foreach ($getColors as $getColor) {
            $colorData[] = $getColor->name;
            $hexData[] = $getColor->hexcode;
        }
        
        if($input['type'] == "flashsale") {
            
            $findflashdetails = Flashsale::where(['id' => 1, 'status' => '1'])->first();
            $brandGets = Product::select("brand")->whereIn('id', explode(",", $findflashdetails->products))->where(["deleteStatus"=>1,"status"=>1])->get();

            foreach ($brandGets as $brandGet) {
                $brandData[] = $brandGet->brand;
            }
            $brandDatas = array_unique($brandData);
            foreach($brandDatas as $brandData) {
                $brandDataa[] = $brandData;
            }

        } else if($input['type'] == "theme") {
            
            $proid = array();
            $brandGets = Product::select('brand')->where(["theme_id"=>$input['key'], "category_id"=>$input['type']])->where(["deleteStatus"=>1,"status"=>1])->get();

            foreach ($brandGets as $brandGet) {
                $brandData[] = $brandGet->brand;
            }
            $brandDatas = array_unique($brandData);
            foreach($brandDatas as $brandData) {
                $brandDataa[] = $brandData;
            }

        } else if($input['type'] == "subcat" || $input['type'] == "subsubcat") {

            if($input['type'] == "subcat") {
                $whereType = ["subcategory_id"=>$input['key']];
            } else {
                $whereType = ["subsubcategory_id"=>$input['key']];
            }

            $getSizes = Product::select("size")->where($whereType)->where(["deleteStatus"=>1,"status"=>1])->get();
            foreach ($getSizes as $getSize) {
                $sizeStrings = explode(',',$getSize->size);
                
                foreach($sizeStrings as $sizeString) {
                    $sizeData[] = $sizeString;
                }
            }
            $sizeDatas = array_unique($sizeData);
            foreach($sizeDatas as $sizeData) {
                $sizeDataa[] = $sizeData;
            }

            $brandGets = Product::select("brand")->where($whereType)->where(["deleteStatus"=>1,"status"=>1])->get();
            foreach ($brandGets as $brandGet) {
                $brandData[] = $brandGet->brand;
            }
            $brandDatas = array_unique($brandData);
            foreach($brandDatas as $brandData) {
                $brandDataa[] = $brandData;
            }
        }
        return $this->sendResponse(["size" => $sizeDataa, "color" => $colorData, "hexcode"=>$hexData, "brand" => $brandDataa], 'Filter details retrieved successfully', $request->path());
    }

    public function productlist(Request $request) {
        $input = $request->all();
        
        if (!empty($input['sortby'])) {
        
            if ($input['sortby'] == 'new') {
                $sortcolumn_name = "id";
                $incorder_name = "desc";
            } else if ($input['sortby'] == 'popularity') {
                $sortcolumn_name = "id";
                $incorder_name = "desc";
            } else if ($input['sortby'] == 'discount') {
                $sortcolumn_name = "discount";
                $incorder_name = "desc";
            } else if ($input['sortby'] == 'low_to_high') {
                $sortcolumn_name = "sp";
                $incorder_name = "asc";
            } else if ($input['sortby'] == 'high_to_low') {
                $sortcolumn_name = "sp";
                $incorder_name = "desc";
            } else if ($input['sortby'] == 'delivery_time') {
                $sortcolumn_name = "ships_in";
                $incorder_name = "asc";
            }
        } else {
            $sortcolumn_name = "id";
            $incorder_name = "desc";
        }

        if (!empty($input['filter_data'])) {
            
            if (empty($input['filter_type'])) {
                $productdetails = Product::where(['subcategory_id' => $input['filter_data']])->where(["deleteStatus"=>1,"status"=>1])->get();
            
            } else if ($input['filter_type'] == "Category") {
                $productdetails = Product::where(["category_id" => $input['filter_data']])->where(["deleteStatus"=>1,"status"=>1])->orderby($sortcolumn_name, $incorder_name)->get();
            
            } else if ($input['filter_type'] == "search") {
                $productdetails = Product::where('name', 'like', '%' . $input['filter_data'] . '%')->orwhere('category_id', 'like', '%' . $input['filter_data'] . '%')->orwhere('brand', 'like', '%' . $input['filter_data'] . '%')->where(["deleteStatus"=>1,"status"=>1])->orderby($sortcolumn_name, $incorder_name)->get();
            
            } else if ($input['filter_type'] == "flash") {
                $findflashdetails = Flashsale::where(['id' => 1, 'status' => '1'])->first();
            
                if (!empty($findflashdetails)) {
                    $finsalearray = explode(",", $findflashdetails->products);
                } else {
                    $finsalearray = array("0");
                }
                $productdetails = Product::whereIn('id', $finsalearray)->orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();
            
            } else {
            
                if (!empty($input['option'])) {
                    $productdetails = Product::where([$input['filter_type'] => $input['filter_data'], "category_id" => $input['option']])->where(["deleteStatus"=>1,"status"=>1])->orderby($sortcolumn_name, $incorder_name)->get();
                } else {
                    $productdetails = Product::where([$input['filter_type'] => $input['filter_data']])->where(["deleteStatus"=>1,"status"=>1])->orderby($sortcolumn_name, $incorder_name)->get();
                }
            }
        } else {

            $productdetails = Product::orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();
        }

        $return_array = array();
        $color_array = array();
        $size_array = array();
        $brand_array = array();
        
        if (!empty($input['extra_filter_type'])) {
            $color_array = explode(",", $input['color']);
            $size_array = explode(",", $input['size']);
            $brand_array = explode(",", $input['brand']);
        }
        
        foreach ($productdetails as $value) {
            $check_status_unique = 0;
            $imagearray = explode(",", $value->images);
            
            if (!empty($imagearray)) {
            
                if (!empty($imagearray[0])) {
            
                    if (strpos($imagearray[0], $this->baseurlslash) == false) {
                        $imagearray[0] = $this->baseurl . $imagearray[0];
                    }
                } else {
                    $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                }
            
            } else {
                $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
            }
            $findflagofwishlist = Wishlist::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
            
            if (!empty($findflagofwishlist)) {
                $like_Status = "1";
            } else {
                $like_Status = "0";
            }
            $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
            $associated_color_array = explode(",", $value->color);
            $associated_size_array = explode(",", $value->size);
            
            if (!empty($color_array) && sizeof(array_intersect($associated_color_array, $color_array)) > 0) {
                $check_status_unique = 1;
            }
            
            if (!empty($size_array) && sizeof(array_intersect($associated_size_array, $size_array)) > 0) {
                $check_status_unique = 1;
            }
            
            if (!empty($brand_array) && in_array($value->brand, $brand_array)) {
                $check_status_unique = 1;
            }
            
            if (isset($input['extra_filter_type']) && !empty($input['extra_filter_type'])) {
                if (!empty($value->status) && $check_status_unique == 1) {
                    $return_array[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
                }
            } else {
                if (!empty($value->status)) {
                    $return_array[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
                }
            }
        }
        return $this->sendResponse($return_array, 'Product list retrieved successfully', $request->path());
    }

    public function moreproductlist(Request $request) {
        $input = $request->all();
        $return_array = array();

        if (!empty($input['sortby'])) {

            if ($input['sortby'] == 'new') {
                $sortcolumn_name = "id";
                $incorder_name = "desc";
            } else if ($input['sortby'] == "popularity") {
                $sortcolumn_name = "id";
                $incorder_name = "desc";
            } else if ($input['sortby'] == "discount") {
                $sortcolumn_name = "discount";
                $incorder_name = "desc";
            } else if ($input['sortby'] == "low_to_high") {
                $sortcolumn_name = "sp";
                $incorder_name = "ASC";
            } else if ($input['sortby'] == "high_to_low") {
                $sortcolumn_name = "sp";
                $incorder_name = "desc";
            } else if ($input['sortby'] == "delivery_time") {
                $sortcolumn_name = "ships_in";
                $incorder_name = "asc";
            }
        } else {
            $sortcolumn_name = "id";
            $incorder_name = "DESC";
        }

        $query = Product::query();

        if(!empty($input['subcatid']) && empty($input['subsubcatid']) && empty($input['catid']) && empty($input['flash_sale']) && empty($input['theme'])) {
            
            $query->where(["subcategory_id"=>$input['subcatid']]);

        } else if(empty($input['subcatid']) && !empty($input['subsubcatid']) && empty($input['catid']) && empty($input['flash_sale']) && empty($input['theme'])) {

            if(!empty($input['brand']) && !empty($input['defaultcolor'])) {
                $query->where(["subsubcategory_id"=>$input['subsubcatid'], "brand"=>$input['brand'], "defaultcolor"=>$input['defaultcolor']]);

            } elseif(!empty($input['defaultcolor']) && empty($input['brand'])) {
                $query->where(["subsubcategory_id"=>$input['subsubcatid'], "defaultcolor"=>$input['defaultcolor']]);
            
            } elseif(empty($input['defaultcolor']) && !empty($input['brand'])) {
                $query->where(["subsubcategory_id"=>$input['subsubcatid'], "brand"=>$input['brand']]);

            } else {
                $query->where(["subsubcategory_id"=>$input['subsubcatid']]);
            }

        } else if(empty($input['subcatid']) && !empty($input['catid']) && !empty($input['subsubcatid']) && empty($input['flash_sale']) && empty($input['theme'])) {
            $query->where(["category_id"=>$input['catid']])->where(["subsubcategory_id"=>$input['subsubcatid']]);

        } else if(empty($input['subcatid']) && empty($input['subsubcatid']) && empty($input['catid']) && !empty($input['flash_sale']) && empty($input['theme'])) {
            $flashData = Flashsale::where(['id'=>1, 'status'=>1])->first();
            if(strlen($findflashdetails->products)>=1) {
                $fData = explode(',', $flashData->products);
                $query->whereIn("id", $fData);
            } else {
                $query->where(['subsubcategory_id'=>$flashData->subsubcategory]);
            }
        
        } else if(empty($input['subcatid']) && empty($input['subsubcatid']) && empty($input['catid']) && empty($input['flash_sale']) && !empty($input['theme'])) {
            $proid = array();
            $productids = Product::select('id')->where(["theme_id"=>$input['theme_id'], "category_id"=>$input['theme']])->get();
            foreach ($productids as $productid) {
                $proid[] = $productid->id;
            }
            $query->whereIn("id", $proid);
        
        }

        if(!empty($input['filter_data'])) {
            if(!empty($input['filter_brand'])) {
                $filterBrand = explode(',',$input['filter_brand']);
            }

            if(!empty($input['filter_brand'])) {
                $query->whereIn('brand', $filterBrand);
            }

            if($input['minValue'] == 100 && $input['maxValue'] != 10000) {
                $query->where('sp', '<=', $input['maxValue']);

            } else if($input['minValue'] != 100 && $input['maxValue'] == 10000) {
                $query->where('sp', '>=', $input['minValue']);

            } else if($input['minValue'] != 100 && $input['maxValue'] != 10000) {
                $query->where('sp', '>=', $input['minValue'])->where('sp', '<=', $input['maxValue']);
            }
        }
        $productids = $query->orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();
        $arrayids = array();
        foreach($productids as $productid) {
            $arrayids[] = $productid->id;
        }

        if(!empty($input['filter_data'])) {
            $filtersizesarr = array();

            if(!empty($input['filter_size'])) {
                $filtersizes = explode(',',$input['filter_size']);
                foreach($filtersizes as $filtersize) {
                    $fsizes = Product::select('id')->whereIn('id',$arrayids)->where('size','like','%'.$filtersize.'%')->where(["deleteStatus"=>1,"status"=>1])->get();
                    foreach($fsizes as $fsize) {
                        $filtersizesarr[] = $fsize->id;
                    }
                }
                $filtersizesarr = array_unique($filtersizesarr);
                $productdetails = Product::whereIn('id',$filtersizesarr)->orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();
            } else {
                $filtersizesarr = [];
            }
        }

        if(!empty($input['filter_data'])) {
            $filtercolorsarr = array();
            
            if(!empty($input['filter_color'])) {
                $filtercolors = explode(',',$input['filter_color']);

                foreach($filtercolors as $filtercolor) {
                    if(!empty($input['filter_size'])) {
                        $fcolors = Product::select('id')->whereIn('id',$filtersizesarr)->where('color','like','%'.$filtercolor.'%')->where(["deleteStatus"=>1,"status"=>1])->get();
                    } else {
                        $fcolors = Product::select('id')->whereIn('id',$arrayids)->where('color','like','%'.$filtercolor.'%')->where(["deleteStatus"=>1,"status"=>1])->get();
                    }

                    foreach($fcolors as $fcolor) {
                        $filtercolorsarr[] = $fcolor->id;
                    }
                }
                $filtercolorsarr = array_unique($filtercolorsarr);
                $productdetails = Product::whereIn('id',$filtercolorsarr)->orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();
            } else {
                $filtercolorsarr = [];
            }
        }

        if(!empty($input['filter_data'])) {

            if(empty($input['filter_color']) && empty($input['filter_size'])) {
                $productdetails = Product::whereIn('id',$arrayids)->orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();
            }
        } else {
            $productdetails = Product::whereIn('id',$arrayids)->orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();
        }

        foreach ($productdetails as $value) {
            
            $verifySeller = Seller::where(["id"=>$value->user_id])->whereIn("status", [1])->first();

            if($verifySeller) {
                $check_status_unique = 0;
                $imagearray = explode(",", $value->images);
                
                if (!empty($imagearray)) {
                
                    if (!empty($imagearray[0])) {
                
                        if (strpos($imagearray[0], $this->baseurlslash) == false) {
                            $imagearray[0] = $this->baseurl . $imagearray[0];
                        }
                    } else {
                        $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                    }
                
                } else {
                    $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                }

                if(!empty($input['user_id'])) {
                    $findflagofwishlist = Wishlist::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
                    
                    if (!empty($findflagofwishlist)) {
                        $like_Status = "1";
                    } else {
                        $like_Status = "0";
                    }
                } else {
                    $like_Status = "0";
                }

                $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
                
                $return_array[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
            }

        }

        return $this->sendResponse($return_array, 'Product list retrieved successfully', $request->path());
    }

    public function checkphonedata(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['phone' => 'required', 'deviceToken' => '', 'deviceType' => '', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $details = User::where('phone', $input['phone'])->first();
        $token_s = str_random(25);
        $findtokencolm = Token::where('user_id', $details['id'])->first();
        if (!empty($findtokencolm)) {
            $token_saver = Token::where('user_id', $details['id'])->update(['token' => $token_s, 'deviceToken' => $input['deviceToken'], 'deviceType' => $input['deviceType']]);
        } else {
            $token_saver = Token::create(array("user_id" => $details['id'], "token" => $token_s, "deviceToken" => $input['deviceToken'], "deviceType" => $input['deviceType']));
        }
        $details['user_id'] = (string)$details['id'];
        unset($details['id']);
        if (!empty($details['image'])) {
            $details['image'] = url('/public/') . "/" . $details['image'];
        } else {
            $details['image'] = url('/public/') . "/img/imagenotfound.jpg";
        }
        $details['token'] = $token_s;
        return $this->sendResponse($details, 'User login successfully.', $request->path());
    }

    public function codavailable(Request $request, $id) {

        $addtocarts = Cart::where(['user_id' => $id])->get();
        $flag = "true";
        $xx = 0;
        $yy = 0;
        foreach($addtocarts as $addtocart) {

            $value = Product::where('id', $addtocart->product_id)->first();
            
            if($value->payment_mode == 1) {
                $yy++;
            }

            if($value->payment_mode == 2) {
                $xx++;
            }
        }

        if($xx == $yy) {
            $flag = "true";
        
        } else if($xx >= $yy) {
            $flag = "false";
        
        } else if($xx <= $yy) {
            $flag = "true";
        }

        $return_array['cod_available'] = $flag;
        return $this->sendResponse($return_array, 'COD retrieved successfully', $request->path());
    }
    
    public function allrequestlist1(Request $request) {
        // $input = $request->all();
        // switch ($input['type']) {
        //     case "Cancel":
        //         $detailsoforder = Order::find($input['id']);
        //         $detailsoforder->status = "canceled";
        //         $detailsoforder->save();
        //         $orderrequestdetails = DB::table('orderrequests')->where(['order_id' => $input['id']])->update(['request_type' => "canceled"]);
        //     break;
        //     case "return":
        //         $detailsoforder = Order::find($input['id']);
        //         $detailsoforder->status = "returned";
        //         $detailsoforder->save();
        //         $orderrequestdetails = DB::table('orderrequests')->where(['order_id' => $input['id']])->update(['request_type' => "returne"]);
        //     break;
        //     default:
        //         $detailsoforder = Order::find($input['id']);
        // }
        // return $this->sendResponse1([], 'Order list retrieved successfully.', $request->path());
    }

    public function orderreturnadmin(Request $request) {
        $details = Orderrequest::whereIn("request_type", ["exchange", "return"])->orderBy('id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {
        
            $findallorderdetails = Order::find($met->order_id);
            $orderdetails = OrderDetail::where(["order_id" => $met->order_id])->first();
            $finduserdetails = User::find($findallorderdetails->user_id);

            if($met->status == 0) {
                $orderstatus = "None";
            } else if($met->status == 1) {
                $orderstatus = "Reject";
            } else if($met->status == 2) {
                $orderstatus = "Accept";
            }
            
            $return_array[] = array($findallorderdetails->order_number, $finduserdetails->name, date('d-m-Y', strtotime($findallorderdetails->created_at)), $met->reason, $met->description, $orderdetails->final_price, $findallorderdetails->payment_type, $findallorderdetails->transcation_id, $met->request_type, $orderstatus, $met->order_id);
        }
        
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function ordercanceladmin(Request $request) {
        $details = Orderrequest::where(["request_type"=>"cancel"])->orderBy('id','DESC')->get();
        $return_array = array();
        
        if($details) {
            foreach ($details as $met) {
            
                $findallorderdetails = Order::find($met->order_id);
                $orderdetails = OrderDetail::where(["order_id" => $met->order_id])->first();
                $finduserdetails = User::find($met->user_id);

                $return_array[] = array($findallorderdetails->order_number, 
                    $finduserdetails->name, 
                    $met->refund_by, 
                    date('d-m-Y', strtotime($findallorderdetails->created_at)), 
                    $met->reason, 
                    $met->description, 
                    $orderdetails->final_price, 
                    $findallorderdetails->payment_type, 
                    $findallorderdetails->transcation_id);
            }
        }
        
        return $this->sendResponse1($return_array, 'Order Cancel list retrieved successfully.', $request->path());
    }

    public function orderreturnseller(Request $request, $id) {
        $details = Orderrequest::whereIn("request_type", ["exchange", "return"])->orderBy('id','DESC')->get();
        $return_array = array();
        
        if($details) {
            foreach ($details as $met) {
            
                $findallorderdetails = Order::find($met->order_id);

                if($findallorderdetails->seller_id == $id) {
                    $orderdetails = OrderDetail::where(["order_id" => $met->order_id])->first();
                    $finduserdetails = User::find($findallorderdetails->user_id);

                    $return_array[] = array($findallorderdetails->order_number, $finduserdetails->name, date('d-m-Y', strtotime($findallorderdetails->created_at)), $met->reason, $met->description, $orderdetails->final_price, $findallorderdetails->payment_type, $findallorderdetails->transcation_id, $met->request_type, $met->status);
                }
            }
        }
        
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function ordercancelseller(Request $request, $id) {
        $details = Orderrequest::where(["request_type"=>"cancel"])->orderBy('id','DESC')->get();
        $return_array = array();
        
        if($details) {
            foreach ($details as $met) {
            
                $findallorderdetails = Order::find($met->order_id);
                if($findallorderdetails) {
                    if($findallorderdetails->seller_id == $id) {
                        $orderdetails = OrderDetail::where(["order_id" => $met->order_id])->first();
                        $finduserdetails = User::find($findallorderdetails->user_id);

                        $return_array[] = array($findallorderdetails->order_number, $finduserdetails->name, $met->refund_by, date('d-m-Y', strtotime($findallorderdetails->created_at)), $met->reason, $met->description, $orderdetails->final_price, $findallorderdetails->payment_type, $findallorderdetails->transcation_id);
                    }
                }
            }
        }
        
        return $this->sendResponse1($return_array, 'Order Cancel list retrieved successfully.', $request->path());
    }

    

    public function buyerorderlisting(Request $request, $id) {
        
            $details = Order::join('users', 'users.id', '=', 'orders.user_id')->select("orders.id as id", "orders.order_number", "users.name as buyer", "orders.location", "orders.payment_type", "orders.status", "orders.created_at")->where(["orders.user_id"=>$id])->get();
            $return_array = array();
            
            foreach ($details as $met) {

                $return_array[] = array($met->order_number,$met->buyer,$met->location,$met->payment_type, 1, $met->status, date('d-m-y H:i',strtotime($met->created_at)),$met->id);   
            }

        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    
    public function sellerordercsv(Request $request, $id) {
        
        $input = $request->all();
        $inputOrder = $request->orderids;
        $inputOrderArr = explode(',', $inputOrder);

        $query = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["orders.seller_id"=>$id])->whereIn("orders.id", $inputOrderArr);
        $details = $query->orderBy('id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {

            $detail = OrderDetail::where(['order_id'=>$met->id])->first();
            $product = Product::select("name","ships_in","sku","sp")->where(['id'=>$met->product_id])->first();
            if($product) {
                $productName = $product->name;
            } else {
                $productName = "";
            }
            $shipsINDAYS = "+". $product->ships_in ."days";
            $oDate = date('d-m-y',strtotime($met->created_at));
            $shipsIN = date('d-m-Y', strtotime($oDate . $shipsINDAYS));

            $return_array[] = array("order_number"=>$met->order_number, "product_name"=>$productName, "sku"=>$product->sku, "sale_price"=>$product->sp, "buyer_name"=>$met->buyer, "location"=>$met->location, "payment_type"=>$met->payment_type, "quantity"=>$detail->quantity, "status"=>$met->status, "order_date"=>date('d-m-y H:i',strtotime($met->created_at)), "Shipsin"=>$shipsIN);
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }



    // Seller Order listings by status functions


    

    public function sellerorderlisting5(Request $request, $id) {  //  Pending
        
        $details = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["orders.status"=>"pending", "seller_id"=>$id])->orderBy('orders.id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {

            $detail = OrderDetail::where(['order_id'=>$met->id])->first();

            $product = Product::select("name")->where(['id'=>$met->product_id])->first();
            if($product) {
                $productName = $product->name;
            } else {
                $productName = "";
            }
            $return_array[] = array($met->order_number, $detail->wbn_num, $productName, $met->buyer, $met->state, $met->city, $met->payment_type, $met->quantity ,$met->status , date('d-m-y H:i',strtotime($met->created_at)),$met->id, $met->id);
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    //change order status api developed by anas
    public function changethestatusoforder(Request $request){
         $oids = $request->order_id;
         
        foreach($oids as $oid) {
          $order = Order::where('id',$oid)->first();
          $msg = "";
          if($request->status == 1) {
                $msg = "Your order has been Ready";
                $msg_ar = 'لقد كان طلبك جاهزًا';
                //update expected date here
                // $date = str_replace('/', '-',$request->expected_date);  21/12/2020
                // $dispatch_at =  date('Y-m-d', strtotime($date));
               $res = explode("/", $request->expected_date);
               $dispatch_at = $res[2]."-".$res[1]."-".$res[0];
               $update = Order::where('id',$oid)->update(['dispatch_at'=>$dispatch_at]);
           
           } else if($request->status == 2) {
                $msg = "Your order has been Shipped";
                $msg_ar = 'وقد تم شحن طلبك';
                //update tracking id and url here
                $update = Order::where('id',$oid)->update(['tracking_id'=>$request->tracking_id,'tracking_url'=>$request->tracking_url]);

            } else if($request->status == 3) {
                $msg = "Your order has been Delivered";
                $msg_ar = 'تم تسليم طلبك';

            } else if($request->status == 4) {
                $msg = "Your order has been cancelled";
                $msg_ar = 'تم إلغاء طلبك';

            } else if($request->status == 5) {
                $msg = "Your order has been cancelled";
                $msg_ar = 'تم إلغاء طلبك';

                $cancelData = ["order_id"=>$order->id, "product_id"=>$order->product_id, "user_id"=>$order->user_id, "seller_id"=>$order->seller_id, "request_type"=>"cancel", "cancel_by"=>"Seller", "status"=>0,];
                Orderrequest::create($cancelData);
            }
          
            if(strlen($msg) > 1) {
                $notifyData = [
                    'user_id'=>$order->user_id,
                    'order_id'=>$order->id,
                    'order_number'=>$order->order_number,
                    'type'=>'Order',
                    'title'=>'Order - '.$order->order_number,
                    'content'=>$msg,
                    'title_ar'=>'ترتيب - '.$order->order_number,
                    'content_ar'=>$msg_ar
                ];
                Notification::create($notifyData);
                $userDevice = Token::select('deviceType','deviceToken')->where(['user_id'=>$order->user_id])->first();
                $badge = Notification::where(["user_id"=>$order->user_id,'status'=>0])->count();
                if($userDevice) {
                    if(strlen($userDevice->deviceToken) > 0) {

                        $userLang = User::select('lang_type')->where(['id'=>$order->user_id])->first();

                        $notifyResponse = $this->pushNotificationNew($notifyData, $userDevice, $badge, $userLang->lang_type);
                    }
                }
            }
            
            $status = Order::where('id',$oid)->update(['status'=>$request->status]);
        }
        return $this->sendResponse1([], 'Order Status Updated successfully.', $request->path());
    }

    public function changethestatusoforder1(Request $request) {
        $oids = $request->order_id;
        $jsonarray = array();
        $jsonData = array();
        $returnData = array();

        $sellerDetail = Seller::where(['id' => $request->seller_id])->first();

        $x=1;
        $getorderids = '';
        foreach($oids as $oid) {

            $cancelData = array();

            $order = Order::where(['id'=>$oid])->first();
            $details = OrderDetail::where(['order_id'=>$order->id])->first();

            if($request->status == 'dispatched') {
                $msg = "Your order has been out for delivery";

            } else if($request->status == 'delivered') {
                $msg = "Your order has been delivered";

            } else if($request->status == 'transit') {
                $msg = "Your order has been in-transit";

            } else if($request->status == 'cancelled') {
                $msg = "Your order has been cancelled";

                $cancelData = ["order_id"=>$order->id, "product_id"=>$order->product_id, "user_id"=>$order->user_id, "seller_id"=>$order->seller_id, "request_type"=>"cancel", "refund_by"=>"Seller", "status"=>0, "price"=>$details->final_price];
                Orderrequest::create($cancelData);

                $jsonarray = [
                    "waybill"=>$details->wbn_num,
                    "cancellation"=>"true"
                ];
                $this->shippingapiordercancle($jsonarray);
            
            } else if($request->status == 'return') {

                $msg = "Your order request for return has been confirmed";
            
            } else if($request->status == 'accepted') {
                
                $prod = Product::where(['id'=>$order->product_id])->first();
                if($order->payment_type == "Cash On Delivery") {
                    $paymentType = "COD";
                    $codamt = $details->final_price;
                } else {
                    $paymentType = "Prepaid";
                    $codamt = 0;
                }

                if($order->status == "reorder") {

                    $msg = "";
                    $jsonData['shipments'][] = [
                            "add" => $order->location,
                            "address_type" => $order->address_type,
                            "phone" => $order->phone,
                            "payment_mode"=> "REPL",
                            "name" => $order->name,
                            "city" => $order->city,
                            "state" => $order->state,
                            "pin" => $order->pincode,
                            "country" => $order->country,

                            "order" => $order->order_number,
                            "hsn_code" => $prod->sku,
                            "client" => "THEBRANDSTANDSURFACE-B2C",
                            "shipment_width" =>$prod->width,

                            "seller_gst_tin" => $sellerDetail->gstnum,
                            "seller_name" =>$sellerDetail->name,
                            "seller_add" =>$sellerDetail->address,

                            "return_city"=> $sellerDetail->city,
                            "return_country"=> $sellerDetail->country,
                            "return_add"=> $sellerDetail->location,
                            "return_pin"=> $sellerDetail->pincode,
                            "return_name"=>$sellerDetail->name,

                            "weight" => $prod->weight,
                            "cod_amount" => $codamt,
                            "dangerous_good" => "False",
                            "waybill" => $details->wbn_num,
                            "order_date" =>$order->created_at,
                            "total_amount" => $details->final_price,
                            "quantity" =>$details->quantity,
                    ];

                } else {

                    $msg = "Your order has been confirmed";
                
                    $jsonData['shipments'][] = [
                            "add" => $order->location,
                            "address_type" => $order->address_type,
                            "phone" => $order->phone,
                            "payment_mode"=> $paymentType,
                            "name" => $order->name,
                            "city" => $order->city,
                            "state" => $order->state,
                            "pin" => $order->pincode,
                            "country" => $order->country,

                            "order" => $order->order_number,
                            "hsn_code" => $prod->sku,
                            "client" => "THEBRANDSTANDSURFACE-B2C",
                            "shipment_width" =>$prod->width,

                            "seller_gst_tin" => $sellerDetail->gstnum,
                            "seller_name" =>$sellerDetail->name,
                            "seller_add" =>$sellerDetail->address,

                            "weight" => $prod->weight,
                            "cod_amount" => $codamt,
                            "dangerous_good" => "False",
                            "waybill" => $details->wbn_num,
                            "order_date" =>$order->created_at,
                            "total_amount" => $details->final_price,
                            "quantity" =>$details->quantity,
                    ];
                }

                    $jsonData['pickup_location'] = [
                        "name" => $sellerDetail->name,
                        "city" => $sellerDetail->city,
                        "pin" => $sellerDetail->pincode,
                        "country" => $sellerDetail->country,
                        "phone" => $sellerDetail->phone,
                        "add" => $sellerDetail->address
                    ];

                    $jsonarray = 'format=json&data='. json_encode($jsonData);

                    $this->shippingapiordercreate($jsonarray);
                    $this->shippingapipackingslip($details->wbn_num);

            } else if($request->status == 'shipped') {

                $msg = "Your order has been shipped";
            
            } else if($request->status == 'packed') {

                $msg = "Your order has been packed by seller and ready to ship";
                if($x==1) {
                    $getorderids = $order->id;
                } else {
                    $getorderids = $getorderids .','.$order->id;
                }
            }

            $orderUpdate = Order::where(['id'=>$order->id])->first();
            $orderUpdate->status = $request->status;
            $orderUpdate->save();

            if(strlen($msg) > 1) {
                $notifyData = [
                    'user_id'=>$order->user_id,
                    'order_id'=>$order->id,
                    'order_number'=>$order->order_number,
                    'type'=>'Order',
                    'title'=>'Order - '.$order->order_number,
                    'content'=>$msg
                ];
                Notification::create($notifyData);
                $userDevice = Token::select('deviceType','deviceToken')->where(['user_id'=>$order->user_id])->first();
                $badge = Notification::where(["user_id"=>$order->user_id,'status'=>0])->count();
                if($userDevice) {
                    if(strlen($userDevice->deviceToken) > 0) {
                        $notifyResponse = $this->pushNotification($notifyData, $userDevice, $badge);
                    }
                }
            }
            $x++;
        }

        if($request->status == 'packed') {
            $return_array = array(
                            "order_id" => $getorderids, 
                            "order_count" => $x - 1, 
                            "handover_date" => strtotime('+12 hours'),
                        );
            Manifest::create($return_array);

            $jsonarray = [
                "pickup_time"=>date("H:i:s", strtotime('+12 hours')),
                "pickup_date"=>date("H:i:s", strtotime('+12 hours')),
                "pickup_location"=>$sellerDetail->name,
                "expected_package_count"=>$x,
            ];

            $this->shippingapiorderpickup($jsonarray);
        }
        
        return $this->sendResponse1([], 'Order Status Updated successfully.', $request->path());
    }


    public function shippingapiordercreate($jsonarray) {   //   order create

        $headers['headers'] = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Token dcafa28eb05642c39f31b14724e28cf45f91256a',
            'Accept' => 'application/json',
        ];

        $url = "https://staging-express.delhivery.com/api/cmu/create.json";
        $myBody = $jsonarray;

        $client = new Client($headers);
        $response = $client->post($url, [
            'http_errors' => false,
            'body' => $myBody,
        ]);
        $status = $response->getStatusCode();
        $data = json_decode($response->getBody()->getContents());
        return $data;
    }

    public function shippingapipackingslip($jsonarray) {  //  Packing Slip creation

        $checkDetails = OrderDetail::where(['wbn_num'=>$jsonarray])->first();

        $headers['headers'] = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Token dcafa28eb05642c39f31b14724e28cf45f91256a',
            'Accept' => 'application/json',
        ];
        $http = new Client($headers);
        $generateWBN = $http->get(url('https://staging-express.delhivery.com/api/p/packing_slip?wbns='.$jsonarray));
        $data_wbn = json_decode($generateWBN->getBody());

        if(strlen($checkDetails->barcode) <= 0 ) {

            $file_data = $data_wbn->packages[0]->barcode; 
            $file_name = 'image_'.time().'.png'; //generating unique file name; 
            $path = public_path() . "/barcode/" . $file_name;
            $img = $file_data;
            $img = substr($img, strpos($img, ",")+1);
            $data = base64_decode($img);
            file_put_contents($path, $data);
            $destinationPath = $this->baseurlslash . "barcode/" . $file_name;

            $details = OrderDetail::where(['wbn_num'=>$jsonarray])->first();
            $details->barcode = $destinationPath;
            $details->save();
        }

        if(strlen($checkDetails->barcode_more) <= 0 ) {

            $file_data1 = $data_wbn->packages[0]->oid_barcode; 
            $file_name1 = 'image_'.time().'.png'; //generating unique file name; 
            $path1 = public_path() . "/barcode/" . $file_name1;
            $img1 = $file_data1;
            $img1 = substr($img1, strpos($img1, ",")+1);
            $data1 = base64_decode($img1);
            file_put_contents($path1, $data1);
            $destinationPath1 = $this->baseurlslash . "barcode/" . $file_name1;

            $details = OrderDetail::where(['wbn_num'=>$jsonarray])->first();
            $details->barcode_more = $destinationPath1;
            $details->save();
        }
        return true;

    }

    public function shippingapiordercancle($jsonarray) {  //  cancle order

        $headers['headers'] = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Token dcafa28eb05642c39f31b14724e28cf45f91256a',
            'Accept' => 'application/json',
        ];

        $url = "https://staging-express.delhivery.com/api/p/edit";
        $myBody = $jsonarray;

        $client = new Client($headers);
        $response = $client->post($url, [
            'http_errors' => false,
            'json' => $myBody,
        ]);
        $status = $response->getStatusCode();
        $data = json_decode($response->getBody()->getContents());
        return $data;
    }


    public function shippingapiorderpickup($jsonarray) {  //  Pickup request

        $headers['headers'] = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Token dcafa28eb05642c39f31b14724e28cf45f91256a',
            'Accept' => 'application/json',
        ];

        $url = "https://staging-express.delhivery.com/api/p/edit";
        $myBody = $jsonarray;

        $client = new Client($headers);
        $response = $client->post($url, [
            'http_errors' => false,
            'json' => $myBody,
        ]);
        $status = $response->getStatusCode();
        $data = json_decode($response->getBody()->getContents());
        return $data;
    }

    public function orderinvoice($oids) {

        $datas = Order::whereIn('id', [$oids])->get();
        $jsonarry = array();

        foreach($datas as $data) {
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Token dcafa28eb05642c39f31b14724e28cf45f91256a',
                'Accept' => 'application/json',
            ];

            $order = OrderDetail::where(['order_id'=>$data->id])->first();

            $http = new Client([
                'headers' => $headers
            ]);
            $generateWBN = $http->get(url('https://staging-express.delhivery.com/api/p/packing_slip?wbns='.$order->wbn_num));
            $data_wbn = json_decode($generateWBN->getBody());

            $user = User::where(['id'=>$data->user_id])->first();
            $seller = Seller::where(['id'=>$data->seller_id])->first();
            $product = Product::where(['id'=>$data->product_id])->first();

            $jsonarry['invoice']['user'] = [
                    "name"=>$data->name,
                    "location"=>$data->location,
            ];

            $jsonarry['invoice']['seller'] = [
                    "name"=>$seller->name,
                    "location"=>$seller->address,
                    "gst"=>$seller->gstnum,
                    "pan_num"=>$seller->pan_num,
            ];

            $jsonarry['invoice']['courier'] = [
                    "name"=>'Delivery Express',
            ];

            $addDay = '+ '.$product->ships_in.' days';

            $jsonarry['invoice']['products'] = [
                    "name"=>$product->name,
                    "quantity"=>$order->quantity,
                    "desc"=>$product->hsn_code,
                    "sku"=>$product->sku,
                    
                    "price"=>$order->sale_price,
                    "discount"=>$order->discount_price,
                    "valuable_tax"=>$order->valuable_tax,
                    "total_tax"=>$order->gst_tax,
                    "total_price"=>$order->final_price,
                    "tracking_id"=>$order->wbn_num,
                    "order_number"=>$data->order_number,
                    "barcode"=>$order->barcode,
                    "payment_type"=>$data->payment_type,
                    "order_date"=>date('d-m', strtotime($data->created_at)),
                    "dispatch_date"=>date('d-m', strtotime($data->created_at . $addDay)),
            ];

            $jsonarry['invoice']['shipping'] = [
                "quantity" =>1,
                "amount"=>$order->seller_shipping,
            ];
        }

        $items = [];
        view()->share('jsonarry',$jsonarry);
        //if($download == "pdf") {
        $pdf = PDF::loadView('pdfinvoice');
        //return $pdf->download('pdfview.pdf');
        //}
        return $pdf->download('pdfinvoice.pdf');
        //return $pdf->stream('pdfinvoice.pdf');
    }


    public function ordermanifest($oids) {
        $manifest = Manifest::where(['id'=>$oids])->first();
        if($manifest) {

            $datas = OrderDetail::whereIn('id', explode(',',$manifest->order_id))->get();
            $listArray = array();
            foreach ($datas as $value) {
                $listArray[] = ['wbn'=>$value->wbn_num, "order_number"=>$value->order_number, "date"=>$manifest->created_at];
            }

            $items = ['handover_date'=>$manifest->handover_date, 'ordercount'=>$manifest->order_count, 'lists'=>$listArray];
        } else {
            $items =[];
        }
        view()->share('items',$items);
        
        $pdf = PDF::loadView('manifest');
        //return view('manifest');
        return $pdf->download('manifest.pdf');
        //return $pdf->stream('manifest.pdf');
    }

    public function ordermanifest1($oids) {
        $manifest = Manifest::where(['id'=>$oids])->first();
        if($manifest) {
            $items = ['handover_date'=>$manifest->handover_date, 'ordercount'=>$manifest->order_count];
        } else {
            $items =[];
        }
        view()->share('items',$items);
        
        $pdf = PDF::loadView('manifest');
        return view('manifest');
        //return $pdf->download('manifest.pdf');
        //return $pdf->stream('manifest.pdf');

    }

    public function deleteorder(Request $request, $id) {
        Order::where('id', $id)->delete();
        return $this->sendResponse1([], "Order Id " . $id . ' deleted successfully', $request->path());
    }

    //  End of functions


    public function orderlistingseller(Request $request, $id) {
        $orId = array();
        $getOrderIds = Order::select('id')->where(['seller_id'=>$id])->get();
        
        foreach($getOrderIds as $getOrderId) {
            $orId[] = $getOrderId->order_id;
        }

        $getorId = array_unique($orId);

        $details = Order::join('users', 'users.id', '=', 'orders.user_id')->select("orders.id as id", "orders.order_number", "users.name as buyer", "orders.location", "orders.payment_type", "orders.created_at")->whereIn("orders.id", $getorId)->get();
        $return_array = array();
        
        foreach ($details as $met) {
            $detailsaboutproduct = OrderDetail::where(['order_id'=>$met->id])->count();
            $checkDelivered = OrderDetail::where(['order_id'=>$met->id, 'status'=>'Delivered'])->count();
            
            if($detailsaboutproduct == $checkDelivered) {
                $status = "Delivered";
            } else {
                $status = "Pending";
            }

            $return_array[] = array($met->order_number,$met->buyer,$met->location,$met->payment_type, $detailsaboutproduct, $status, date('d-m-y H:i',strtotime($met->created_at)),$met->id);
        }

        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function orderrequest(Request $request, $id) {
        $details = Order::join('users', 'users.id', '=', 'orders.user_id')->select("orders.product_id", "orders.id as order_id", "users.name as buyer", "orders.location", "orders.dispatch_by", "orders.payment_type", "orders.order_date", "orders.amount", "orders.status")->get();
        // dd($details);
        $return_array = array();
        foreach ($details as $met) {
            $all_product = explode(",", $met->product_id);
            $check_request = Orderrequest::where('order_id', $met->order_id)->first();
            if (!empty($check_request)) {
                if ($check_request->request_type == "Exchange") {
                    $detailsaboutproduct = Product::whereIn('id', $all_product)->get();
                    if ($id == "admin") {
                        foreach ($detailsaboutproduct as $value) {
                            if ($value->user_type == $id) {
                                $return_array[] = array([$this->baseurl . explode(",", $value->images) [0], $value->sku, $value->brand, $value->id], $met->order_id, $met->buyer, $met->location, $met->order_date, $met->dispatch_by, $met->payment_type, $met->amount, [$met->status, $met->order_id], $met->order_id);
                            }
                        }
                    } else {
                        foreach ($detailsaboutproduct as $value) {
                            if ($value->user_id == $id) {
                                $return_array[] = array([$this->baseurl . explode(",", $value->images) [0], $value->sku, $value->brand, $value->id], $met->order_id, $met->buyer, $met->location, $met->order_date, $met->dispatch_by, $met->payment_type, $met->amount, [$met->status, $met->order_id], $met->order_id);
                            }
                        }
                    }
                }
            }
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function catlistforadmin1(Request $request, $id) {
        $detailsofsub = Subcategory::find($id);
        $details = DB::table('subsubcategories')->select("id", "name")->where(['subcategory_id' => $id])->get();
        $return_array = array();
        foreach ($details as $met) {
            $return_array[] = array($met->name, $met->id);
        }
        return $this->sendResponse1(["list" => $return_array, "list1" => $detailsofsub], 'Subcategory list retrieved successfully', $request->path());
    }
    
    public function bannerdetailsbyid(Request $request, $id) {
        $bannerArray = array();
        $bannerdetails = Banner::select('id','image')->where('id',$id)->first();
        $image = asset('public/').'/'.$bannerdetails->image;
       // $cartData = [$bannerdetails->category_id, $bannerdetails->category_id];

        // $subcatlists = Subcategory::where('category_id', $bannerdetails->category_id)->get();
        // $subcartData = array();
        // foreach($subcatlists as $subcatlist) {
        //     $subcartData[] = [$subcatlist->id, $subcatlist->name];
        // }

        // $subsubcatlists = Subsubcategory::where('subcategory_id', $bannerdetails->subcategory_id)->get();
        // $subsubcartData = array();
        // foreach($subsubcatlists as $subsubcatlist) {
        //     if($subsubcatlist->status == 1) {
        //         $subsubcartData[] = [$subsubcatlist->id, $subsubcatlist->name];
        //     }
        // }
        if($bannerdetails) {
       // if(count($subsubcartData) > 0) {
            $bannerArray = [
                    // "bannerdetails"=>$bannerdetails,
                    // "catlist"=>$cartData,
                    // "subcatlist"=>$subcartData,
                    // "subsubcatlist"=>$subsubcartData
                 $image,
                 $bannerdetails->id

                ];
        }

       // }
      //  return response()->json(['banner'=>$bannerArray,'status'=>'SUCESS','message'=>"Email id already registered. Try another"],Response::HTTP_OK); 
        return $this->sendResponse1($bannerArray, 'Banner list successfully', $request->path());
    }

    public function flashproductlist(Request $request) {
        $detailsnew = Flashsale::find(1);

        if(strlen($detailsnew->subsubcategory)>=1 && $detailsnew->subsubcategory != 0) {
            $subname = Subsubcategory::where('id', $detailsnew->subsubcategory)->first();
            $return_array[] = array($subname->name, $subname->id);
            $approval = "Selected category";
        } else {
            $newarray = explode(",", $detailsnew->products);
            $details = DB::table('products')->select("id", "name")->whereIn('id', $newarray)->get();
            $return_array = array();
            foreach ($details as $met) {
                $return_array[] = array($met->name, $met->id);
            }
            $approval = "Selected products";
        }
        return $this->sendResponse1(array("data" => $return_array, "status" => $detailsnew->status, "time" => $detailsnew->time, "endtime" => $detailsnew->endtime, "label"=>$approval), 'Flash sale list retrieved successfully', $request->path());
    }

    public function updateflashtime(Request $request) {
        $input = $request->all();
        $detailsnew = Flashsale::find(1);
        if(!empty($input['time'])) {
            $detailsnew->time = date("Y-m-d H:i:s", strtotime($input['time']));
        }
        if(!empty($input['endtime'])) {
            $detailsnew->endtime = date("Y-m-d H:i:s", strtotime($input['endtime']));
        }
        $detailsnew->status = $input['status'];
        $detailsnew->save();
        return $this->sendResponse1([], 'Flash sale End time updated successfully', $request->path());
    }
    public function catlistforadmin1234(Request $request) {
        $details = DB::table('subcategories')->select("id", "name")->get();
        $return_array = array();
        foreach ($details as $met) {
            $return_array[] = array($met->name, $met->id);
        }
        return $this->sendResponse1($return_array, 'Subcategory list retrieved successfully', $request->path());
    }
    public function productids(Request $request) {
        $details = DB::table('products')->select("id", "name")->get();
        $return_array = array();
        foreach ($details as $met) {
            $return_array[] = array($met->name, $met->id);
        }
        return $this->sendResponse1($return_array, 'Product list retrieved successfully', $request->path());
    }
    public function addptoflash(Request $request) {
        $input = $request->all();
        $details = Flashsale::find(1);
        $newarray = array();

        if(empty($input['product_id'])) {
            $details->subsubcategory = $input['subsubcatid'];
            $details->products = " ";
        
        } else {
            $details->subsubcategory = " ";
            $newarray = $details->products;
            $newarray = explode(",", $newarray);
            $newarray[] = $input['product_id'];
            $details->products = implode(",", array_unique($newarray));
        }
        
        $details->save();
        return $this->sendResponse1([], 'Product added to flash sale successfully', $request->path());
    }

    public function createtheme(Request $request) {
        $input = $request->all();
        Theme::create($input);
        return $this->sendResponse([], 'Theme created successfully', $request->path());
    }

    public function createBrand(Request $request) {
        $input = $request->all();
        $brandName = $input['name'];
        $brand = Brand::where(["name"=>$brandName])->first();

        if($brand) {
            return $this->sendError($request->path(), 'Brand Already Added');
        } else {
            $input['name'] = ucwords($brandName);
            Brand::create($input);
            return $this->sendResponse([], 'Brand created successfully', $request->path());
        }
    }

    public function editprofile(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', 'fullname' => 'required', 'email' => 'required', 'phone' => 'required', 'dob' => 'required', 'gender' => 'required']);
        
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
        
        $userdetails = User::find($input['user_id']);
        
        if ($request->image) {
            $filename = substr(md5($userdetails->id . '-' . time()), 0, 15) . '.' . $request->file('image')->getClientOriginalExtension();
            $path = public_path('users-photos/' . $filename);
            Image::make($request->image)->save($path);
            $userdetails->image = '/users-photos/' . $filename;
        }
        
        $userdetails->name = $input['fullname'];
        $userdetails->phone = $input['phone'];
        $userdetails->dob = $input['dob'];
        $userdetails->gender = $input['gender'];
        $checkemail = User::whereNotIn('id', array($input['user_id']))->where('email', $input['email'])->first();
        
        if (!empty($checkemail)) {
            return $this->sendError($request->path(), 'Email id Already Exist');
        }
        
        $userdetails->email = $input['email'];
        
        if (!empty($userdetails->image)) {
            $datanameimage = url('/public/') . $userdetails->image;
        } else {
            $datanameimage = url('/public/') . "/img/imagenotfound.jpg";
        }
        $userdetails->save();
        $userdetails1 = User::find($input['user_id']);
        
        if (!empty($userdetails1->image)) {
            $userdetails1->image = url('/public/') . $userdetails1->image;
        } else {
            $userdetails1->image = url('/public/') . "/img/imagenotfound.jpg";
        }
        
        $return_array = array("user_id" => (string)$userdetails1->id, "image" => $userdetails1->image, "fullname" => $userdetails1->fullname, "address" => $userdetails1->address, "email" => $userdetails1->email, "phone" => $userdetails1->phone);

        // $otp = $userdetails1->identicard;
        //   $email = $userdetails1->email;
        //            $subject = "Asec Identity Card";
        //         $postData ="";
        //          try{
        //              Mail::send('emails.otpverify3', ['otp' =>$otp], function($message) use ($postData,$email)
        //                         {
        //                           $message->from('asecclass@asecenglish.awsapps.com', 'TMASEC');
        //                           $message->to($email, 'TMASEC')->subject('TMASEC Identity Card');
        //                         });
        //          }
        //           catch(Exception $e){
        //                         }
        return $this->sendResponse($return_array, 'User profile updated successfully', $request->path());
    }

    public function addcolor(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['color' => 'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $input = $request->all();
        $hex = $input['hexcode'];
        $verifyColor = Color::where(["name"=>$input['color'], "hexcode"=>$hex])->first();

        if($verifyColor) {
            return $this->sendError($request->path(), 'Color Already Added');
        } else {
            $arrayData = [
                "name"=>$input['color'],
                "hexcode"=>$input['hexcode']
            ];

            $obj = Color::create($arrayData);
            return $this->sendResponse($obj, "Color added successfully", $request->path());
        }
    }

    public function getcolor(Request $request) {
        $objs = Color::select("name", "hexcode")->get();
        $colorData = array();

        foreach ($objs as $value) {
            $colorData[] = ["value"=>$value->hexcode, "label"=>"$value->name"];
        }
        return $this->sendResponse($colorData, "Color listing", $request->path());
    }

    public function colorlist(Request $request) {
        $objs = Color::select("id", "name", "hexcode")->get();
        $colorData = array();

        foreach ($objs as $value) {
            $colorData[] = [$value->name, $value->hexcode, $value->id];
        }
        return $this->sendResponse($colorData, "Color listing", $request->path());
    }

    public function colordelete(Request $request, $id) {
        $getcolor = Color::where(["id"=>$id])->first();

        if($getcolor) {
            $verifyColor = Product::where("color", 'like', '%'.$getcolor->hexcode.'%')->first();

            if($verifyColor) {
                return $this->sendError($request->path(), 'Color not able to delete because related product available');
            } else {
                Color::where(["id"=>$id])->delete();
                $objs = "delete";
                return $this->sendResponse($objs, "Color deleted", $request->path());
            }
        } else {
            return $this->sendError($request->path(), 'Color not found');
        }
    }

    public function addsubsubcat(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['name' => 'required','subcategory_id' => 'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $verifySubCat = Subsubcategory::where(["name"=>$input['name'], "subcategory_id"=>$input['subcategory_id']])->first();

        if($verifySubCat) {
            return $this->sendError($request->path(), 'Sub-subcategory Already Added');
        } else {
            $arrayData = [
                "name"=>$input['name'],
                "subcategory_id"=>$input['subcategory_id'],
                "category_id"=>$input['category_id'],
                "occasion_status"=>$input['occasion_status'],
                "image"=>$input['image'],
            ];
        }

        $obj = Subsubcategory::create($arrayData);
        return $this->sendResponse($obj, "Sub sub-category successfully", $request->path());
    }

    public function subsubcategoryupdate(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['name' => 'required','id' => 'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $getSubSub = Subsubcategory::where(["id"=>$input['id']])->first();
        if($getSubSub) {
            
                $getSubSub->name = $input['name'];
                //$getSubSub->subcategory_id = $input['subcategory_id'];
                //$getSubSub->category_id = $input['category_id'];
                $getSubSub->occasion_status = $input['occasion_status'];
                if(strlen($input['image']) > 1) {
                    $getSubSub->image = $input['image'];
                }
                $getSubSub->save();

                return $this->sendResponse($getSubSub, "Sub sub-category updated successfully", $request->path());
        }
    }

    public function subsubcatlist(Request $request) {
        $objs = Subsubcategory::orderBy('id','DESC')->get();
        $catData = array();

        foreach ($objs as $value) {
            if(strlen($value->subcategory_id) > 0) {
                $subName = Subcategory::select("name")->where(["id"=>$value->subcategory_id])->first();
                
                if($subName) {
                    $catData[] = [
                                $value->category_id,
                                $subName->name, 
                                $value->name,
                                $value->occasion_status,
                                [$value->status, $value->id],
                                $value->id
                            ];
                }
            }
        }
        return $this->sendResponse($catData, "Subsubcategory listing", $request->path());
    }

    public function subsubcatsingle(Request $request, $id) {
        $objs = Subsubcategory::where('id',$id)->first();
        $catData = array();

        if(strlen($objs->subcategory_id) > 0) {
            $subName = Subcategory::select("name")->where(["id"=>$objs->subcategory_id])->first();
            
            if($subName) {
                $catData = [$subName->name, 
                              $objs->name, 
                              $objs->id
                            ];
            }
        }

        return $this->sendResponse($catData, "Subsubcategory data", $request->path());
    }


    public function addoffers(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['offer_type'=>'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $startAt = date("d-m-Y H:i:s", strtotime($input['startdate']));
        $endAt = date("d-m-Y H:i:s", strtotime($input['enddate']));

        if($input['offer_type'] == 1) {
            $arrayData = [
                "offer_type"=>$input['offer_type'],
                "description"=>$input['description'],
                "startdate"=>$startAt,
                "enddate"=>$endAt,
            ];
        } else if($input['offer_type'] == 2) {

            $verifyOffer = Offer::where(["catid"=>$input['catid']])->first();

            if($verifyOffer) {
                return $this->sendError($request->path(), 'Already added this offer in this Sub-subcategory');                
            } else {
                $arrayData = [
                    "offer_type"=>$input['offer_type'],
                    "description"=>$input['description'],
                    "coupon_id"=>$input['coupon_id'],
                    "catid"=>$input['catid'],
                    "startdate"=>$startAt,
                    "enddate"=>$endAt,
                ];
            }
        } else {

            $verifyOffer = Offer::where(["catid"=>$input['catid']])->first();

            if($verifyOffer) {
                return $this->sendError($request->path(), 'Already added this offer in this Sub-subcategory');                
            } else {
                $arrayData = [
                    "offer_type"=>$input['offer_type'],
                    "description"=>$input['description'],
                    "coupon_id"=>$input['coupon_id'],
                    "catid"=>$input['catid'],
                    "startdate"=>$startAt,
                    "enddate"=>$endAt,
                ];
            }
        }

        $obj = Offer::create($arrayData);
        return $this->sendResponse($obj, "Offer added successfully", $request->path());
    }

    public function offerlists(Request $request) {
        $arrayData = array();
        $objs = Offer::orderBy('id','DESC')->get();
        
        foreach($objs as $obj) {

            if($obj->offer_type == 1) {
                $objType = "Bank offers";
            } else if($obj->offer_type == 2) {
                $objType = "Discounted offer";
            } else {
                $objType = "Announcment offer";
            }

            $getcat = Subsubcategory::where(["id"=>$obj->catid])->first();
            if($getcat) {
                $getcatname = $getcat->name;
            } else {
                $getcatname = "-";
            }

            $getcoupon = Cuponcode::where(["id"=>$obj->coupon_id])->first();
            if($getcoupon) {
                $getcouponname = $getcoupon->name;
            } else {
                $getcouponname = "";
            }

            $arrayData[] = [$objType, $obj->description, $getcatname, $getcouponname, $obj->id];
        }
        
        return $this->sendResponse($arrayData, "Offer added successfully", $request->path());
    }

    public function offerdelete(Request $request, $id) {
        $arrayData = array();
        $objs = Offer::where(["id"=>$id])->delete();
        $objs = Promotion::where(["offer_id"=>$id])->delete();
        return $this->sendResponse(["status" => "success"], "Offer deleted successfully", $request->path());
    }

    public function getrefer(Request $request) {
        $referArr = array();
        $getRefer = ReferEarn::where(["id"=>1])->first();
        $referArr = [$getRefer->price];

        return $this->sendResponse($referArr, "Refer and Earn successfully", $request->path());
    }

    public function updaterefer(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['price'=>'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $getRefer = ReferEarn::where(["id"=>1])->first();
        $getRefer->price = $request->price;
        $getRefer->save();

        return $this->sendResponse($getRefer, "Refer and Earn updated successfully", $request->path());
    }

    public function adminsupportticketlist(Request $request) {
        $supportArr = array();
        $getSupports = Support::orderBy("id","DESC")->get();
        foreach($getSupports as $getSupport) {
            $seller = Seller::where(["id"=>$getSupport->seller_id])->first();
            $supportArr[] = [$seller->name,$seller->email,$getSupport->ticket_id, $getSupport->title, $getSupport->content, $getSupport->status, $getSupport->id];
        }

        return $this->sendResponse($supportArr, "Support Listing Retrive successfully", $request->path());
    }

    public function supportticketupdate(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['id'=>'required','status'=>'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $support = Support::where('id',$request->id)->first();
        $support->status = $request->status;
        $support->save();

        return $this->sendResponse($obj, "Ticket updated successfully", $request->path());
    }

    //Payment response

    public function response(Request $request) {
        $result = array();
        $response = Indipay::response($request);

        $result = [
            "order_id" => $response['order_id'],
            "tracking_id" => $response['tracking_id'],
            "order_status" => $response['order_status'],
            "status_message" => $response['status_message'],
        ];
        return $this->sendResponse($result, "Payment Response", $request->path());
    }


    public function cartoffer(Request $request) {
        $offerArr = array();
        $catArrs = $request->catlist;

        foreach($catArrs as $catArr) {
            $objs = Offer::where(["catid"=>$catArr, "offer_type"=>2])->first();

            if($objs) {
                $offerArr[] =  [
                    "id"=>$objs->id,
                    "buy"=>$objs->buy,
                    "get"=>$objs->andget,
                    "catid"=>$objs->catid,
                ];
            }
        }
        return $this->sendResponse($offerArr, "Offer added successfully", $request->path());
    }

    function random_strings($length_of_string) {
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }


   function addsizechart(Request $request) {
 
     $input = $request->all();
     $validator = Validator::make($input, ['heading' => 'required','heading_values'=>'required','user_id'=>'required','subsub_cat_id'=>'required','type'=>'required']);
    
    if ($validator->fails()) {
         return $this->sendError($request->path(), $validator->errors()->first());
    }
    
    $headingArr = explode(',',$request->heading);
    if($request->type=='add') { 
     $checkSubcat =   SizeChart::where('subsub_cat_id',$request->subsub_cat_id)->first(); 
     if(!$checkSubcat) {
    foreach ($request->heading_values as $val) {
        $finalVal = implode(',',$val);
         
          $size = SizeChart::create([
                         'heading'=> $request->heading,
                         'heading_values' => $finalVal,
                         'user_id'=> $request->user_id,
                         'subsub_cat_id'=>$request->subsub_cat_id,
                         'status'=>1,
                ]);
    }
   
    return $this->sendResponse(['status' => "success"], 'Size chart added successfully', $request->path());
  } else {
    return $this->sendError($request->path(), "Size chart already added this sub sub category"); 
  }
   
   } else {
       SizeChart::where('subsub_cat_id',$request->subsub_cat_id)->delete();
       foreach ($request->heading_values as $val) {
        $finalVal = implode(',',$val);
         
          $size = SizeChart::create([
                         'heading'=> $request->heading,
                         'heading_values' => $finalVal,
                         'user_id'=> $request->user_id,
                         'subsub_cat_id'=>$request->subsub_cat_id,
                         'status'=>1,
                ]);

      }
      return $this->sendResponse(['status' => "success"], 'Size chart updated successfully', $request->path());

   }
   } 


  public function updateSizeChart(Request $request) {
    
    $subsubcatid  = $request->subsub_cat_id;
  
     $input = $request->all();
     $validator = Validator::make($input, ['subsub_cat_id'=>'required']);
    
    if ($validator->fails()) {
         return $this->sendError($request->path(), $validator->errors()->first());
    }  
    $subsubcatid  = $request->subsub_cat_id;
    $getdata = SizeChart::where('subsub_cat_id',$subsubcatid)->get();
    //echo "<pre>";
    //print_r($getdata->toArray());die;
    $j=0;
    $obj=[];

    foreach($getdata as $res) {
        $headingArr = explode(',',$res->heading); 
        $head=[];
        foreach($headingArr as $k=>$v) {
              $head[] = $v;
        }

        $valArr = explode(',',$res->heading_values); 

        for($i=0; $i<count($valArr); $i++) {
            $obj[$j][$i] = $valArr[$i];
        }

        $j++;
    }


    $final = array(
        'head'=>$head,
        'val'=> $obj,
    );
   
   return $this->sendResponse(["sizeChart"=>$final], "size chart details", $request->path());    

  } 


  public function showSizeChart(Request $request) {
     
        $subsubcatid  = $request->subsub_cat_id;
        $input = $request->all();
        $subsubcatid  = $request->subsub_cat_id;
   
        $getdata =   DB::table('size_charts as chart')
                ->select('sub.name','sub.id','chart.heading','chart.heading_values')
                ->join('subsubcategories as sub','sub.id','=','chart.subsub_cat_id')
                 ->groupBy('subsub_cat_id')
                ->get();


              foreach($getdata as $value) {
                 $head = explode(',',$value->heading) ; 
                    $res[] = [
                                //$value->category_id,
                                $value->name, 
                               // $value->name,
                                $head,
                               
                                $value->id
                            ];
                
              }  
        return $this->sendResponse(["sizeChart"=>$res], "size chart details", $request->path());   
  }

  public function deleteChart($id){
    $delete = SizeChart::where('subsub_cat_id',$id)->delete();
    return $this->sendResponse(['status' => "success"], 'Size chart deleted successfully', $request->path());
  }


  public function pincodecheck(Request $request) {
        
        $requestpincode = "201001";

        $headers['headers']  = [
            "Content-Type" => "application/json",
            "Authorization" => "Token 6898a0f95f6e4a463df8864f0286b3cc5c712cd5",
        ];
        
        $url = "https://staging-express.delhivery.com/api/backend/clientwarehouse/create/";
        $body = $jsonarray;

        $client = new Client($headers);
        $urlpincode = "https://staging-express.delhivery.com/c/api/pin-codes/json/?filter_codes=".$requestpincode;
        $generateWBN = $http->get(url($urlpincode));
        $data_wbn = json_decode($generateWBN->getBody());

        return $this->sendResponse(["pincodecheck"=>$data_wbn], "test api details", $request->path());
  }


  public function pushNotification($notifyData, $userDevice, $badge) {
        $url = 'https://fcm.googleapis.com/fcm/send';  
        
        $message = array("id"=>$notifyData['order_id'],'title' => $notifyData['title'], "body"=>$notifyData['content'], "type"=>$notifyData['type'], 'sound' => 'default', 'badge'=>$badge);
        
        $registatoin_ids = array($userDevice->deviceToken);
        $fields = array('registration_ids' => $registatoin_ids, 'data' => $message, 'notification'=>$message);

        $GOOGLE_API_KEY = "AAAAKT7Oo5k:APA91bEcQ-uqB42hPjRktyQv2CMKrLXdsJhYWGjy0_PDhGRmsFD8OBqJ0cErEJp0rStYoHBJa1jrmZjeMblMCM6SQ1JxmdT00lZg7e4aBungPYiUHPTY7PaXhfGXzlhQ857agWaJ9Z7P";   
        $headers = array('Authorization: key=' . $GOOGLE_API_KEY , 'Content-Type: application/json',);
        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);    
        
        if ($result === false) {
            return $abc = 'FCM Send Error: ' . curl_error($ch);
        } else {
            return $abc = "Notification Send";
        }
        curl_close($ch);
    }

}
