@extends('layouts.app')

@section('content')

     <section>
        <div class="FaqArea">
            
            <div class="container">
                <div class="row">
                    <div class="col-sm-10">
                        <div class="FaqLeft">
                            <h1>frequently asked questions</h1>

                            <div class="panel-group" id="accordion">

                              <?php  
                                    $x=1;
                              		foreach ($detail as $value) {
                              	
                               ?>

                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true">
                                        <h4><span>Ques : </span>{{$value->question}}</h4>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <p>{{$value->ans}}</p>  
                                        </div>
                                    </div>
                                </div>

                              <?php $x++; } ?>

                            </div>  
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>


@endsection