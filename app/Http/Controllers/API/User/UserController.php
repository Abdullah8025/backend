<?php
namespace App\Http\Controllers\API\User;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use DB;

use App\User;
use App\Token;
use App\Address;
use App\Notification;
use App\Cart;
use Image;
use Mail;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserController extends APIBaseController {

	public function __construct() {

    }

    function anas(){
   //     $data = [
   //   'message' => trans('message.greeting')
   // ];
   // return response()->json($data, 200);
        //echo 'App::setLocale();
       // App::setlocale();
    }

    public function verifyphone(Request $request) {
        $input = $request->all();
        $lang = app()->getLocale();
        $validator = Validator::make($input, ['phone' => 'required', 'deviceToken' => 'required', 'deviceType' => 'required|in:android,ios']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $userdetails = User::where('phone', $input['phone'])->first();
        if (!empty($userdetails)) {
           // echo "hi";die;
            $userdetails->image = asset('public'.$userdetails->image);
            $token_s = $this->generateToken();
            $findtokencolm = Token::where(['user_id'=>$userdetails->id, 'deviceToken' => $request->deviceToken, 'deviceType' => $request->deviceType])->first();

            if (!empty($findtokencolm)) {
                $token_saver = Token::where('user_id', $userdetails->id)->update(['token' => $token_s, 'deviceToken' => $request->deviceToken, 'deviceType' => $request->deviceType]);
            } else {
                $token_saver = Token::create(array("user_id" => $userdetails->id, "token" => $token_s, "deviceToken" => $request->deviceToken, "deviceType" => $request->deviceType));
            }

            $userid = (string)$userdetails->id;
            if (!empty($input['cartids'])) {
                $this->guest_addtocart($userid, explode(",", $input['cartids']), explode(",", $input['sellers']), explode(",", $input['quantity']));
            }

            return $this->sendResponse(['status' => 'success', 'user'=>$userdetails, 'token'=>$token_s], trans('message.login_success'), $request->path());
        
        } else {

            return $this->sendResponse(['status' => 'success'],trans('message.phone_dones_not_exit'), $request->path());
        }
    }

    public function verifysignup(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['email' => 'required|unique:users,email', 'phone' => 'required|unique:users,phone']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        return $this->sendResponse(['status' => 'success'], trans('message.user_not_exit'), $request->path());
    }

    public function signup(Request $request) {
            // print_r($request->toArray());die;
         $input = $request->all();
       // return $this->sendResponse(['data'=>$input], 'show data.', $request->path());
       
        $validator = Validator::make($request->all(), [
            'phone' => 'required|unique:users,phone',
            'phone_code' => 'required',
            'deviceType' => 'required',
            'deviceToken' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $user = User::create([
        	"phone_code"=> $request->phone_code,
        	"phone"=> $request->phone,
        	"country" => $request->country,
        ]);
        
        $newToken = $this->generateToken();

        Token::create([
        	"user_id" => $user->id,
        	"token" => $newToken,
        	"deviceType" => $request->deviceType,
        	"deviceToken" => $request->deviceToken,
        ]);

        $userid = (string)$user->id;
        if (!empty($input['cartids'])) {
            //echo "<pre>";print_r(explode(",", $input['cartids']));die;
            $this->guest_addtocart($userid, explode(",", $input['cartids']), explode(",", $input['sellers']), explode(",", $input['quantity']));
        }
        
        return $this->sendResponse(['status' => 'success', 'user'=>$user, 'token'=>$newToken], trans('message.register_sucess'), $request->path());
    }

    public function guest_addtocart($user_id, $arrayofproduct, $arrayofseller, $arrayofqty) {

        Cart::where(["user_id" => $user_id])->delete();

        $q = 0;
        foreach ($arrayofproduct as $value) {
            //$checkCart = Cart::where(["product_id"=>$value, "user_id"=>$user_id])->first();

            $input = array("user_id"=>$user_id, "product_id"=>$value, "seller_id"=>$arrayofseller[$q], "quantity" => $arrayofqty[$q]);
          //  print_r($input);die;
            Cart::create($input);
            $q++;
        }
    }
    
    public function sociallogin(Request $request) {

        $validator = Validator::make($request->all(), [
            'social_id' => 'required',
            'social_type' => 'required',
            'deviceToken' => 'required',
            'deviceType' => 'required|in:android,ios', 
        ]);

        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $checkUser = User::where(['social_id'=>$request->social_id, 'social_type'=>$request->social_type])->first();
        $token_s = $this->generateToken();

        if ($checkUser) {
               
            if (empty($checkUser->status)) {
                return $this->sendError($request->path(), trans('message.inactive_account'));
            }

            $findtokencolm = Token::where(['user_id'=> $checkUser->id,'deviceToken' => $request->deviceToken, 'deviceType' => $request->deviceType])->first();
            if (!empty($findtokencolm)) {
                $token_saver = Token::where('user_id', $checkUser->id)->update(['token' => $token_s, 'deviceToken' => $request->deviceToken, 'deviceType' => $request->deviceType]);
            } else {
                $token_saver = Token::create(array("user_id" => $checkUser->id, "token" => $token_s, "deviceToken" => $request->deviceToken, "deviceType" => $request->deviceType));
            }

            // $userid = (string)$checkUser->id;
            // if (!empty($input['cartlist'])) {
            //     $this->addtocart1($userid, explode(",", $input['cartlist']), explode(",", $input['size']), explode(",", $input['color']), explode(",", $input['quantity']));
            // }

            return $this->sendResponse(['status' => 'success', 'user'=>$checkUser, 'token'=>$token_s], trans('message.login_success'), $request->path());
        
        } else {

            if(!empty($request->email)) {
                $validator= Validator::make($request->all(),[
                  'email' =>"required|unique:users,email",
                ]);
            }
            if ($validator->fails()) {
                return $this->sendError($request->path(), $validator->errors()->first());
            }

            $user = User::create([
                "social_id"=> $request->social_id,
                "social_type"=> $request->social_type,
	        	"phone_code"=> $request->phone_code,
	        	"phone"=> $request->phone,
	        	"referal_code" => 'honey-'. $this->random_referal_string(4),
	        ]);

	        Token::create([
	        	"user_id" => $user->id,
	        	"token" => $this->generateToken(),
	        	"deviceType" => $request->deviceType,
	        	"deviceToken" => $request->deviceToken,
	        ]);

            // if (!empty($input['cartlist'])) {
            //     $this->addtocart1($insertmember->id, explode(",", $input['cartlist']), explode(",", $input['size']), explode(",", $input['color']), explode(",", $input['quantity']));
            // }

            $findtokencolm = Token::where(['user_id'=> $user->id,'deviceToken' => $request->deviceToken, 'deviceType' => $request->deviceType])->first();
            if (!empty($findtokencolm)) {
                $token_saver = Token::where('user_id', $user->id)->update(['token' => $token_s, 'deviceToken' => $request->deviceToken, 'deviceType' => $request->deviceType]);
            } else {
                $token_saver = Token::create(array("user_id" => $user->id, "token" => $token_s, "deviceToken" => $request->deviceToken, "deviceType" => $request->deviceType));
            }

            if (!empty($user->image)) {
                $user->image = asset($user->image);
            } else {	
                $user->image = asset("/img/imagenotfound.jpg");
            }

            return $this->sendResponse(['status' => 'success', 'user'=>$user, 'token'=>$token_s],trans('message.login_success'), $request->path());
        }
    }

    public function changepassword(Request $request) {

        $validator = Validator::make($request->all(), ['token' => 'required', 'new_password' => 'required|min:6', 'old_password' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $request->token])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $user = User::find($check_token->user_id);

        if(!Hash::check($request->old_password, $user->password)) {
            return $this->sendError($request->path(), trans('message.old_pass_wrong'));
        }
        $user->password = Hash::make($request->new_password);
        $user->save();
        return $this->sendResponse(['status' => 'success'], trans('message.password_changed'), $request->path());
    }

    public function logout(Request $request) {

        $validator = Validator::make($request->all(), ['token' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $request->token])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        Token::where(['token' => $request->token])->delete();
        
        return $this->sendResponse([], trans('message.logout_sucess'), $request->path());
    }

    public function profile(Request $request) {
        
        $validator = Validator::make($request->all(), ['token' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $request->token])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(),  trans('message.token_expire'));
        }
        
        $userdetails = User::find($check_token->user_id);
        
        if (!empty($userdetails->image)) {
            $userdetails->image = asset('public'.$userdetails->image);
        } else {
            $userdetails->image = 'https://i.ibb.co/y6sCcvm/user-thumbnail.jpg';//asset('public'."/img/imagenotfound.jpg");
        }
        
        return $this->sendResponse($userdetails,  trans('message.profile_retrived'), $request->path());
    }

    public function editprofile(Request $request) {
        
        $validator = Validator::make($request->all(), ['token' => 'required', 'name' => 'required', 'phone' => 'required']);
        
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $request->token])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }
        
        $userdetails = User::find($check_token->user_id);
        
        if ($request->image) {
            $filename = substr(md5($userdetails->id . '-' . time()), 0, 15) . '.' . $request->file('image')->getClientOriginalExtension();
            $path = public_path('users-photos/' . $filename);
            Image::make($request->image)->save($path);
            $userdetails->image = '/users-photos/' . $filename;
        }
        
        $userdetails->name = $request->name;
        $userdetails->phone = $request->phone;
    
        // $checkemail = User::whereNotIn('id', [$userdetails->id])->where('email', $request->email)->first();
        // if (!empty($checkemail)) {
        //     return $this->sendError($request->path(), 'Email id Already Exist');
        // }
        //comment by anas
        
        $userdetails->email = $request->email;
        $userdetails->save();

        $user = User::find($userdetails->id);
        if (!empty($user->image)) {
            $user->image = asset('public'.$user->image);
        } else {
            $user->image = asset('public'."/img/imagenotfound.jpg");
        }
        
        return $this->sendResponse($user, trans('message.profile_update'), $request->path());
    }

    public function address(Request $request) {

        $validator = Validator::make($request->all(), ['token' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $request->token])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }

        $details = Address::where(['user_id' => $check_token->user_id])->get();
        return $this->sendResponse($details, trans('message.address_retrived'), $request->path());
    }

    public function addaddress(Request $request) {
        
        $validator = Validator::make($request->all(), ['token' => 'required', 'name' => 'required','address' => 'required', 'city' => 'required', 'state' => 'required', 'latitude' => 'required', 'longitude' => 'required', 'pin_code' => 'required']);
        
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $request->token])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $checkAddress = Address::where(["user_id" => $check_token->user_id])->count();
        if ($checkAddress == 3) {
            return $this->sendError($request->path(), trans('message.address_error'));
        } else {
                
            //$checkRemark = Address::where(["user_id" => $check_token->user_id, 'remark' => $request->remark])->first();
             
            Address::create([
                'user_id' => $check_token->user_id, 
                'name' => $request->name, 
                'phone' => $request->phone, 
                'address' => $request->address, 
                'street' => $request->street, 
                'city' => $request->city, 
                'state' => $request->state,
                'type' => $request->type, 
                'remark' => $request->remark, 
                'latitude' => $request->latitude, 
                'longitude' => $request->longitude,
                'pin_code' => $request->pin_code,
            ]);
         
            return $this->sendResponse(['status' => "success"], trans('message.address_added'), $request->path());
        }
    }

    public function editaddress(Request $request) {
        
        $validator = Validator::make($request->all(), ['token' => 'required', 'address_id'=>'required', 'name' => 'required', 'phone' => 'required', 'address' => 'required', 'street' => 'required', 'city' => 'required', 'state' => 'required', 'latitude' => 'required', 'longitude' => 'required', 'pin_code' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $request->token])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $detailsadd = Address::find($request->address_id);
        if($detailsadd) {
            $detailsadd->name = $request->name;
            $detailsadd->phone = $request->phone;
            $detailsadd->address = $request->address;
            $detailsadd->street = $request->street;
            $detailsadd->city = $request->city;
            $detailsadd->state = $request->state;
            $detailsadd->latitude = $request->latitude;
            $detailsadd->longitude = $request->longitude;
            $detailsadd->remark = $request->remark;
            $detailsadd->pin_code = $request->pin_code;
            $detailsadd->save();
            return $this->sendResponse(['status' => "success"], trans('message.address_updated'), $request->path());
        
        } else {
            return $this->sendError($request->path(), "Address not found");
        }
    }

    public function removeaddress(Request $request) {
        
        $validator = Validator::make($request->all(), ['token' => 'required', 'address_id' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $request->token])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(),  trans('message.token_expire'));
        }

        Address::where(['id' => $request->address_id])->delete();

        return $this->sendResponse(['status' => "success"],  trans('message.address_removed'), $request->path());
    }

    public function defaultaddress(Request $request) {
        
        $validator = Validator::make($request->all(), ['token' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $request->token])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $getAddress = Address::where(["user_id"=>$check_token->user_id, 'remark'=> 2])->orderBy('updated_at','DESC')->limit(1)->first();
        if(!$getAddress) {

            $getAddress = Address::where(["user_id"=>$check_token->user_id])->orderBy('updated_at','DESC')->limit(1)->first();
            if(!$getAddress) {

                return $this->sendError($request->path(), trans('message.address_not_found'));
            }
        }
        return $this->sendResponse($getAddress, trans('message.address_removed'), $request->path());
    }

    public function notifystatus(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $userdetails = User::find($check_token->user_id);
        if ($userdetails->notify_status == "0") {
            $userdetails->notify_status = "1";
            $userdetails->save();
            return $this->sendResponse(['status' => "1"], trans('message.notification_status'), $request->path());
        } else {
            $userdetails->notify_status = "0";
            $userdetails->save();
            return $this->sendResponse(['status' => "0"], trans('message.notification_status'), $request->path());
        }
    }


    public function notficationlist(Request $request) {
        $input = $request->all();
        $notifyArray = array();

        $validator = Validator::make($input, ['token' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $userLang = User::select('lang_type')->where(['id'=>$check_token->user_id])->first();

        Notification::where(['user_id'=> $check_token->user_id])->update(['status' => 1]);

        $alldetails = Notification::select('id','order_id','order_number', 'title','content','title_ar','content_ar', 'created_at')->where(['user_id'=> $check_token->user_id])->orderBy('id','DESC')->get();

        foreach($alldetails as $alldetail) {
               
               if($userLang->lang_type == "en") {
                    $langTitle = $alldetail->title;
                    $langContent = $alldetail->content;
               } else {
                    $langTitle = $alldetail->title_ar;
                    $langContent = $alldetail->content_ar;
               }

               $notifyArray[] = [
                    "id" => $alldetail->id,
                    "order_id" => $alldetail->order_id,
                    "order_number" => $alldetail->order_number,
                    "title" => $langTitle,
                    "content" => $langContent,
                    "created_at" => $alldetail->created_at,
               ];
        }

        return $this->sendResponse($notifyArray, trans('message.notification_retrieved'), $request->path());
    }


    public function generateToken() {

    	$str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
    	$token = substr(str_shuffle($str_result), 0, 10);
    	$this->checkGenerateToken($token);

    	return $token;
    }

    public function checkGenerateToken($token) {
    	$tokenCheck = Token::where('token', $token)->first();
    	if($tokenCheck) {
    		$this->generteToken();
    	} else {
    		return true;
    	}
    }

    function random_referal_string($length_of_string) {
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }
}