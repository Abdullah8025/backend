<!DOCTYPE html>
<html>
<head>
	
	<title></title>

	<!--<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">-->
	<!--<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">-->

	<style type="text/css">
		*{ box-sizing: border-box;}
		@media print {
		   body {
		      -webkit-print-color-adjust: exact;
		   }
		}
	</style>

</head>
<body style="font-family: 'Poppins', sans-serif; margin: 0; color: #3a3a3a;"> 

	<div style="width: 600px; margin: 20px auto 0; background-color: #fff;  padding: 0 10px; box-sizing: border-box; border: 2px dashed #555; border-top: none; border-bottom: none;">

		<h5 style=" background-color: #f3f3f3; padding: 10px 10px; font-weight: 500; font-family: Roboto; margin: 0 0 8px 0;">
			COD Collect amount : Rs. <?php if($jsonarry['invoice']['products']['payment_type'] == "Cash On Delievery") { ?>   {{ $jsonarry['invoice']['products']['total_price'] }} <?php } else { echo 0; } ?>
		</h5>

		<aside style=" float: left; width: 60%; padding: 0 15px 0 15px; border-right: 2px solid #ddd; min-height: 200px;">
			<h3 style=" margin: 0 0 1px; font-weight: 600; font-size: 15px; color: #000; font-family: Roboto;">
				DELIVERY ADDRESS:
			</h3>
			<p style="margin: 0 0 1px; font-size: 14px; color: #3a3a3a; font-family: roboto; display: block; line-height:14px;">
				{{ $jsonarry['invoice']['user']['name'] }},  <br>
				{{ $jsonarry['invoice']['user']['location'] }}
			</p>
		</aside>

		<aside style=" width: 15%; float: right;">
			
		</aside>
		<div style="clear: both;"></div>

		<div style=" border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; padding: 5px 5px;">
			<aside style="float: left; width: 50%">
				<p style="margin: 0 0 0px; font-size: 14px; color: #000; font-family: roboto; display: block; line-height:18px;">
					<strong style="font-weight: 500">Courier Name : </strong>  Delhivery Express
				</p>
				<p style="margin: 0 0 0px; font-size: 14px; color: #000; font-family: roboto; display: block; line-height:18px;">
					<strong style="font-weight: 500">Courier AWB No :</strong> 8562254863
				</p>        
			</aside>
			<aside style="float: right;">
				<p style="margin: 0 0 0px; font-size: 14px; color: #000; font-family: roboto; display: block; line-height:18px;">
					<strong style="font-weight: 500">HBD : </strong> {{ $jsonarry['invoice']['products']['order_date'] }}
				</p>
				<p style="margin: 0 0 0px; font-size: 14px; color: #000; font-family: roboto; display: block; line-height:18px;">
					<strong style="font-weight: 500">CPD :</strong> {{ $jsonarry['invoice']['products']['dispatch_date'] }}
				</p>
			</aside>
			<div style="clear: both;"></div>
		</div> 

		<div style=" padding: 10px 10px 5px; border-bottom: 2px solid #ddd;">
			<p style="margin: 0 0 0px; font-size: 14px; color: #000; font-family: roboto; display: block; line-height:18px;">
				<strong style="font-weight: 500">Sold By :</strong> 
				{{ $jsonarry['invoice']['seller']['name'] }}, {{ $jsonarry['invoice']['seller']['location'] }}
			</p>
		</div>

		<div style=" padding: 5px 10px; border-bottom: 2px solid #ddd;">
			<p style="margin: 0 0 0px; font-size: 14px; color: #000; font-family: roboto; display: block; line-height:18px;">
				<strong style="font-weight: 500">GSTIN No :</strong> 
				{{ $jsonarry['invoice']['seller']['gst'] }}
			</p>
		</div>

		<div style="margin: 0 0 0px 0;">
	    	<table style=" box-sizing: border-box; width: 100%; border-collapse: collapse; border-spacing: 0;border: 1px solid #ddd;">
	    		<tr style=" box-sizing: border-box;">
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left;">
	    				Product
	    			</th>
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left; ">
	    				Qty
	    			</th>
	    		</tr>
	    		<tr>
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				{{ $jsonarry['invoice']['products']['sku'] }} | {{ $jsonarry['invoice']['products']['name'] }}
	    			</td>
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				{{ $jsonarry['invoice']['products']['quantity'] }}
	    			</td>
	    		</tr>
	    		<tr>
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				<strong style="font-weight: 700">Total</strong> 
	    			</td>
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				<strong style="font-weight: 700">{{ $jsonarry['invoice']['products']['quantity'] }}</strong> 
	    			</td>
	    		</tr>

	    	</table>
	    </div>

	    <div style=" padding: 10px 10px">

	    	<aside style="float: left; width: 50%">
		    	<p style=" margin: 0;">
		    		<span style=" background-color: #000; color: #fff; font-family: Roboto; font-size: 13px; padding: 8px 20px; margin: 0 0 0 -10px; display: inline-block;">
			    		Handover to Delhivery Express
			    	</span>
		    		<span style="float: right; font-weight: 600; color: #000;">REG</span>
		    		<div style="clear: both;"></div>
		    	</p>
		    	<p style="margin: 0 0 0px; font-size: 14px; color: #000; font-family: roboto; display: block; line-height:24px;">
					<strong style="font-weight: 500">Tracking ID :</strong> 
					{{ $jsonarry['invoice']['products']['tracking_id'] }}
				</p>

				<p style="margin: 0 0 0px; font-size: 14px; color: #000; font-family: roboto; display: block; line-height:24px;">
					<strong style="font-weight: 500">Order ID :</strong> 
					{{ $jsonarry['invoice']['products']['order_number'] }}
				</p>
			</aside>

			<aside style="float: left; width: 49%">
	        	<figure style=" margin: 15px 0 15px 0; width: 150px;">
					<img src="{{ $jsonarry['invoice']['products']['barcode'] }}" style="width: 100%">
				</figure>
	        </aside>

	    </div>

		<div style="clear: both;"></div>

	</div>

	<div style=" width: 100%; border-top: 2px dashed #555; margin: 20px 0;"></div>

	<div class="BGColor" style="width: 100%; margin: auto; background-color: #fff; padding: 10px; box-sizing: border-box;">
	    <div style="margin: 0 0 15px 0; border: 1px solid #ddd; padding: 10px;">
	        
	        <aside style="float: left; width: 33.33%">
	        	<h3 style=" margin: 0 0 10px; font-weight: 600; font-size: 16px; color: #000; font-family: Roboto;">Tax invoice</h3>
	        </aside>

	        <aside style="float: left; width: 33.33%">
	        	<p style="margin: 0 0 9px; font-size: 14px; color: #3a3a3a; font-family: roboto; display: block; line-height:15px;">
	        		Order Id: {{ $jsonarry['invoice']['products']['order_number'] }},
	        	</p>
	        	<p style="margin: 0 0 9px; font-size: 14px; color: #3a3a3a; font-family: roboto; display: block; line-height:15px;">
	        		Order_date: {{ $jsonarry['invoice']['products']['order_date'] }}
	        	</p>
	        </aside>

	        <aside style="float: left; width: 33.33%">
	        	<p style="margin: 0 0 9px; font-size: 14px; color: #3a3a3a; font-family: roboto; display: block; line-height:15px;">
	        		GSTIN: {{ $jsonarry['invoice']['seller']['gst'] }},
	        	</p>
	        	<p style="margin: 0 0 9px; font-size: 14px; color: #3a3a3a; font-family: roboto; display: block; line-height:15px;">
	        		PAN: {{ $jsonarry['invoice']['seller']['pan_num'] }}
	        	</p>
	        </aside>

	        <div style="clear: both"></div>

	        <aside style="float: left; width: 33.33%">
	        	<h3 style=" margin: 0 0 10px; font-weight: 600; font-size: 16px; color: #000; font-family: Roboto;">Sold By</h3>
	        	<p style="margin: 0 0 9px; font-size: 14px; color: #3a3a3a; font-family: roboto; display: block; line-height:15px;">
	        		{{ $jsonarry['invoice']['seller']['name'] }}
	        	</p>
	        	<p style="margin: 0 0 9px; font-size: 14px; color: #3a3a3a; font-family: roboto; display: block; line-height:15px;">
	        		{{ $jsonarry['invoice']['seller']['location'] }}
	        	</p> 
	        </aside>

	        <aside style="float: left; width: 33.33%">
	        	<h3 style=" margin: 0 0 10px; font-weight: 600; font-size: 16px; color: #000; font-family: Roboto;">Shipping Address</h3>
	        	<p style="margin: 0 0 9px; font-size: 14px; color: #3a3a3a; font-family: roboto; display: block; line-height:15px;">
	        		{{ $jsonarry['invoice']['user']['name'] }},
	        	</p>
	        	<p style="margin: 0 0 9px; font-size: 14px; color: #3a3a3a; font-family: roboto; display: block; line-height:15px;">
	        		{{ $jsonarry['invoice']['user']['location'] }}
	        	</p>
	        </aside>

	        <aside style="float: left; width: 33.33%">
	        	<h3 style=" margin: 0 0 10px; font-weight: 600; font-size: 16px; color: #000; font-family: Roboto; ">Billing Address</h3>
	        	<p style="margin: 0 0 9px; font-size: 14px; color: #3a3a3a; font-family: roboto; display: block; line-height:15px;">
	        		{{ $jsonarry['invoice']['user']['name'] }},
	        	</p>
	        	<p style="margin: 0 0 9px; font-size: 14px; color: #3a3a3a; font-family: roboto; display: block; line-height:15px;">
	        		{{ $jsonarry['invoice']['user']['location'] }}
	        	</p>
	        </aside>

	        <div style="clear: both"></div>
	    </div>     
	    <div style="margin: 0 0 15px 0;">
	    	<table style=" box-sizing: border-box; width: 100%; border-collapse: collapse; border-spacing: 0;border: 1px solid #ddd;">
	    		<tr style=" box-sizing: border-box;">
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left; width: 40%">
	    				Product
	    			</th>
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left;">
	    				Description
	    			</th>
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left;">
	    				Qty
	    			</th>
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left;">
	    				Gross Amount
	    			</th>
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left;">
	    				Discount
	    			</th>
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left;">
	    				Taxable Value
	    			</th> 
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left;">
	    				IGST
	    			</th> 
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left;">
	    				Total
	    			</th>  
	    		</tr>

	    		<tr>
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				{{ $jsonarry['invoice']['products']['name'] }} | {{ $jsonarry['invoice']['products']['sku'] }}
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				HSN: {{ $jsonarry['invoice']['products']['desc'] }} | IGST: 18%
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				 {{ $jsonarry['invoice']['products']['quantity'] }}
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				{{ $jsonarry['invoice']['products']['price'] }}
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				-{{ $jsonarry['invoice']['products']['discount'] }}
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				{{ $jsonarry['invoice']['products']['valuable_tax'] }}
	    			</td> 

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				{{ $jsonarry['invoice']['products']['total_tax'] }}
	    			</td> 

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				{{ $jsonarry['invoice']['products']['total_price'] - $jsonarry['invoice']['shipping']['amount'] }}
	    			</td>  
	    		</tr>

	    		<tr>
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				Shipping Charges
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				 {{ $jsonarry['invoice']['shipping']['quantity'] }}
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				{{ $jsonarry['invoice']['shipping']['amount'] }}
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				
	    			</td> 

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				
	    			</td> 

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				{{ $jsonarry['invoice']['shipping']['amount'] }}
	    			</td>  
	    		</tr>

	    		<tr>
	    			<td colspan="4" style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				<span style="color: #000; font-weight: 600">
	    					 
	    				</span> 
	    			</td>

	    			<td colspan="2" style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				<span style="color: #000; font-weight: 600">
	    					Paid Amount  :
	    				</span> 
	    			</td> 

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;font-family: Roboto;">
	    				0
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;font-family: Roboto;">
	    				{{ $jsonarry['invoice']['products']['total_price'] }}
	    			</td> 
	    		</tr>
	    	</table>
	    </div> 
	    <div>
	    	<div style="float: left; width: 50%">
		    	<h3 style=" margin: 0 0 5px; font-weight: 600; font-size: 15px; color: #000; font-family: Roboto;">Seller Registered Address</h3>
		    	<p style="margin: 0 0 5px; font-size: 14px; color: #3a3a3a; font-family: roboto; display: block; line-height:20px;">
	        		{{ $jsonarry['invoice']['seller']['name'] }}, {{ $jsonarry['invoice']['seller']['location'] }}
	        	</p>

		    	<h3 style=" margin: 0 0 5px; font-weight: 600; font-size: 15px; color: #000; font-family: Roboto;">Declaration</h3>
		    	<p style="margin: 0 0 5px; font-size: 14px; color: #3a3a3a; font-family: roboto; display: block; line-height:18px;">
	        		The goods sold are intended for end user consumption and not for resale.
	        	</p>
	        </div>
	        <div style="float: right; width: 50%; text-align: right;">
	        	<h3 style=" margin: 0 0 5px; font-weight: 600; font-size: 17px; color: #000; font-family: Roboto;">{{ $jsonarry['invoice']['seller']['name'] }}</h3>
		    	<p style="margin: 0 0 5px; font-size: 15px; color: #3a3a3a; font-family: roboto; display: block; line-height:24px;">
	        		Authorized Signature
	        	</p>
	        </div>
	        <div style="clear: both;"></div>
	    </div>
    </div>

</body>
</html>