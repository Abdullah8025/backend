<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerPerformance extends Model
{
    //

    protected $fillable = [
        'seller_id','type','from_date','to_date','total_order','total_return','total_breach','total_reattempt','metric','target','rating'
    ];
}
