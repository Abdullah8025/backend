<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    
    protected $fillable = ['image', 'name', 'section_number'];
}
