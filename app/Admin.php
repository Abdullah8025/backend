<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    //

    protected $fillable = [
         'type', 'email', 'name', 'password', 'image', 'phone','status','permit'
    ];
}
