
@extends('layouts.website')

@section('content')

<?php
$currencyCode = Session::get('currency_code');
if($currencyCode == "INR") {
    $currencyCodeLabel = "Rs";
} else {
    $currencyCodeLabel = $currencyCode;
}
?>
<section>
    <div class="AccountArea">
        <div class="container">
            <div class="AccountHead">
                <h1>Account</h1>
                <h2>{{ $userData->name }} | {{ $userData->phone }}</h2>
            </div>

            <div class="row">
                
                <div class="col-sm-8">
                    <div class="AccountLeft">
                        <form method="post" action="{{ route('paying') }}">
                            @csrf
                        <div class="panel-group" id="accordion">
                            <div class="panel">
                                <div class="panel-heading Active" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    <h4>Delivery Address</h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="AddressBox">

                                        @if(count($detailsaddress) > 0)
                                        @php $x=1; @endphp
                                        @foreach ($detailsaddress as $value)
                                            <label class="Address">
                                                <input type="radio" name="location" value="{{$value->id}}" @if($value->remark == 1) checked="checked"  @else @if($x == 1) checked="checked"  @endif @endif>
                                                <span class="checkmark"></span>
                                                <h3>{{$value->name}}</h3>
                                                <p> {{ $value->phone }} </p>
                                                <p>{{$value->address}},{{$value->town}}
                                                <br> {{$value->city}}, {{$value->state}} - {{$value->pincode}}</p>

                                                <span class="Location">{{$value->type}}</span>
                                                <ul>
                                                    <li><a href="#" data-toggle="modal" data-target="#EditAddress{{$value->id}}"><i class="fa fa-pencil"></i></a>
                                                    </li>
                                                    <li><a href="{{route('removeaddress', $value->id)}}"><i class="fa fa-trash-o"></i></a></li>
                                                </ul>
                                            </label>
                                            @php $x++; @endphp
                                        @endforeach
                                        @endif

                                        @if(count($detailsaddress) > 0)
                                            {{-- <aside>
                                                <a href="javascript:void(0)" id="colaptwo">Delivery Here</a>
                                            </aside> --}}
                                        @else

                                            <aside>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#Locationnn"> Add Address </a>
                                            </aside>

                                        @endif

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-heading Active" id="secondpanel">
                                    <h4>Order Details</h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="BagOrder">
                                            <article>
                                            @foreach($cartData as $cart)
                                                <aside>
                                                    <figure>
                                                        <img src="{{ $cart['productimage'] }}">
                                                    </figure>
                                                    <figcaption>
                                                        <h2>{{ $cart['name'] }}</h2>
                                                         <h3>{{ $cart['brand'] }}</h3>
                                                         <h4> @if(!empty($cart['size'])) {{ $cart['size_label'] }}: {{ $cart['size'] }} | @endif  @if(!empty($cart['color'])) Size: {{ $cart['color'] }} | @endif Qty: {{ $cart['qty'] }} </h4>
                                                         <h5>{{ $currencyCodeLabel }} {{ $cart['sp'] }} @if($cart['percentage'] != 0) <del>{{ $currencyCodeLabel }} {{ $cart['mrp'] }}</del> <span>Saved {{ $cart['percentage'] }}%</span> @endif </h5>
                                                         <h6><span>Sold By:</span> {{ $cart['seller'] }}</h6>
                                                         @if(strlen($cart['coupon_applied']) > 0)
                                                            <h6 class="text-right" style="color: #20bd99;">{{ $cart['coupon_applied'] }}</h6>
                                                         @else
                                                            <h6 class="text-right"> Coupon not applied</h6>
                                                         @endif
                                                    </figcaption>
                                                    <div class="Spinner">
                                                    </div>
                                                </aside>
                                            @endforeach
                                            </article>

                                            <div class="Links">
                                                {{-- <a href="javascript:void(0)" id="colapthree">Payment Detail</a> --}}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-heading Active" id="thirdpanel" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                    <h4>Payment Details</h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="BagOrder">
 
                                                <div class="col-md-4 col-md-offset-2">
                                                    <div class="radio">
                                                        <label> <input class="shipclass" type="radio" name="payment_type" value="online" @if($checkCodAvailable == "false") checked @endif> Online Payment</label>
                                                        <label style="color: #20bd99;"> Free Shipping </label>
                                                    </div>
                                                    
                                                </div>
                                                @if($checkCodAvailable == "true")
                                                <div class="col-md-6">
                                                    <div class="radio">
                                                        <label> <input class="shipclass" type="radio" name="payment_type" value="Cash On Delivery" checked> Cash on delivery</label>
                                                        <label style="color: #20bd99;"> Rs. {{ $currencyCodeLabel }} {{ $paying['shipping'] }}/- Extra Shipping </label>
                                                    </div>
                                                </div>
                                                @else
                                                
                                                @endif

                                            <div class="clear"></div>

                                            <div class="Links">
                                                <input type="hidden" name="coupan_code" id="ccode" value="{{ $paying['is_coupenApplied'] }}">
                                                <input type="hidden" name="gift_wrap" id="cwrap" value="no">
                                                @if(count($detailsaddress) > 0)
                                                    <button type="submit" class="placebutton">Place Order</button>
                                                @else
                                                    <button type="button" class="placebutton" disabled="">Place Order</button>
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    {{-- <div class="BagsRight" id="BagsRightshow" style="display:none;"> --}}
                    <div class="BagsRight" id="BagsRightshow">
                        <aside>
                            <h4>Apply Coupon</h4>
                        </aside>
                        <article>
                            <p> <input id="couponid" type="text" name="coupon" class="form-control" placeholder="coupon code"> </p>
                            <span id="coupenerror" class="text-danger"> </span>
                            <span id="coupensuccess" class="text-success"> </span>
                            <p> <button type="button" id="couponbuttonid" class="btn btn-primary" style="float:right;"> Apply Coupon </button> </p>
                        </article>
                    </div>
                    {{-- <div class="BagsRight" id="WrapRightshow" style="display:none;"> --}}
                    <div class="BagsRight" id="WrapRightshow">
                        <aside>
                            <h4>Gift Wrap</h4>
                        </aside>
                        <article>
                            <div class="col-md-4 col-md-offset-2">
                                <div class="radio">
                                    <label> <input type="radio" value="yes" onclick="giftclick(this.value)" name="wrap" class="wrapclass" @if($paying['giftwrap'] > 0 ) checked @endif> Yes</label>
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label> <input type="radio" value="no" name="wrap" onclick="giftclick(this.value)" class="wrapclass" @if($paying['giftwrap'] <= 0 ) checked @endif> No</label>
                                </div>
                                
                            </div>
                            <div class="clear"></div>
                        </article>
                    </div>

                    <div class="BagsRight">
                        <aside>
                            <h4>price Details</h4>
                        </aside>
                        <article>
                            <h4>Price ({{ count($cartData) }} item) <span id="csp">{{ $currencyCodeLabel }} {{ $paying['amount'] }}</span></h4>
                            
                            @if($checkCodAvailable == "true")
                            <h4 id="shipiingcharges">Delivery Charges <span>{{ $currencyCodeLabel }} {{ $paying['shipping'] }}</span></h4>
                            @endif

                            @if($paying['giftwrap'] > 0 )
                            <h4 id="wrapcharges">Gift Wrap Charges <span>{{ $currencyCodeLabel }} {{ $paying['giftwrap'] }}</span></h4>
                            @endif

                            @if(strlen($paying['is_coupenApplied']) > 0 ) 
                                <h4 id="cdis">Additional Discount <span id="ctdis">{{ $currencyCodeLabel }} {{ $paying['coupon_discount'] }}</span></h4>
                            @else
                                <h4 id="cdis" style="display: none;">Additional Discount <span id="ctdis">{{ $currencyCodeLabel }} 00</span></h4>
                            @endif

                            @if($checkCodAvailable == "true")
                            <h5 id="withShippping">Amount payable <span id="ctotal">{{ $currencyCodeLabel }} {{ $paying['payableWithShip'] }}</span></h5>
                            <h5 id="withoutshipping" style="display: none;">Amount payable <span id="ctotal">{{ $currencyCodeLabel }} {{ $paying['payable'] }}</span></h5>
                            
                            @else
                                <h5 id="withoutshipping">Amount payable <span id="ctotal">{{ $currencyCodeLabel }} {{ $paying['payable'] }}</span></h5>
                            @endif
                            
                            <h5 id="withshippingandWrap" style="display: none;">Amount payable <span id="ctotal">{{ $currencyCodeLabel }} {{ $paying['payableWithShipAndGift'] }}</span></h5>
                            <h5 id="withoutshippingandWrap" style="display: none;">Amount payable <span id="ctotal">{{ $currencyCodeLabel }} {{ $paying['payableWithGift'] }}</span></h5>

                        </article>
                        <div class="Saved">
                            <p>Your total savings on this order {{ $currencyCodeLabel }} <span id="cadddis">{{ $paying['discount'] }}</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@if(!empty($detailsaddress))
        @foreach ($detailsaddress as $value)

    <div id="EditAddress{{$value->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog"> 
            <div class="modal-content"> 
                <div class="modal-body">
                    <div class="AddAddress">
                        <h3>Change Address</h3>
                        <form action="{{ route('update-address') }}" method="post">
                             <div class="form-group col-sm-6">
                                <label>Full Name</label>
                                <input name="name" id="adrs_name{{$value->id}}" type="text" class="form-control" value="{{$value->name}}">
                                 <span id="adrs_name_error{{$value->id}}" style="color: red;"></span>
                            </div>
                            <input type="hidden" name="id" value="{{$value->id}}">
                            <div class="form-group col-sm-6">
                                <label>Mobile Number</label>
                                <input name="phone" value="{{$value->phone}}" id="adrs_phone{{$value->id}}" type="text" class="form-control">
                                <span id="adrs_phone_error{{$value->id}}" style="color: red;"></span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label>Address</label>
                                <textarea name="address" id="adrs_address{{$value->id}}" rows="3" class="form-control">{{$value->address}}</textarea>
                                <span id="adrs_address_error{{$value->id}}" style="color: red;"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Town</label>
                                <input name="town" value="{{$value->town}}" id="adrs_town{{$value->id}}" type="text" class="form-control">
                                <span id="adrs_town_error{{$value->id}}" style="color: red;"></span> 
                            </div>
                            <div class="form-group col-sm-6">
                                <label>City</label>
                                <input name="city" value="{{$value->city}}" id="adrs_city{{$value->id}}" type="text" class="form-control">
                                <span id="adrs_city_error{{$value->id}}" style="color: red;"></span> 
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Pincode</label>
                                <input name="pincode" value="{{$value->pincode}}" id="adrs_pincode{{$value->id}}" type="text" class="form-control">
                                <span id="adrs_pincode_error{{$value->id}}" style="color: red;"></span> 
                            </div>
                            <div class="form-group col-sm-6">
                                <label>State</label>
                                <input name="state" value="{{$value->state}}" id="adrs_state{{$value->id}}" type="text" class="form-control">
                                <span id="adrs_state_error{{$value->id}}" style="color: red;"></span> 
                            </div>

                            <div class="form-group col-sm-12">
                                <label>Address Type</label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="Address">
                                            <input type="radio" value="Home" name="type" checked>
                                            <span class="checkmark"></span>
                                            <h4>Home <span>(All Day Delivery)</span></h4>
                                        </label>
                                    </div>

                                    <div class="col-sm-6">
                                        <label class="Address">
                                            <input value="Office" type="radio" name="type">
                                            <span class="checkmark"></span>
                                            <h4>Office <span>(Between 10 AM to  7 PM)</span></h4>
                                        </label>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group col-sm-6">
                             <input type="checkbox" name="remark" <?php if($value->remark == '1'){ ?>checked<?php }  ?>> Make it Default<br>
                             </div>
                            <div class="form-group col-sm-12">
                                <button type="button" id="addnewaddress{{$value->id}}">Change Address</button>
                            </div>
                             <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        </form>

                        <div class="clear"></div>
                    </div>
                </div> 
            </div>
        </div>
    </div>

<script type="text/javascript">
   $("#addnewaddress{{$value->id}}").click(function(){
       
         if($("#adrs_name{{$value->id}}").val() == ""){
             $("#adrs_name_error{{$value->id}}").text("Full Name is required.")
         }

        if($("#adrs_phone{{$value->id}}").val() == ""){
             $("#adrs_phone_error{{$value->id}}").text("Phone Number is required.")
        } 

        if($("#adrs_address{{$value->id}}").val() == ""){
             $("#adrs_address_error{{$value->id}}").text("Address is required.")
        } 

        if($("#adrs_town{{$value->id}}").val() == ""){
             $("#adrs_town_error{{$value->id}}").text("Town is required.")
        } 
        
        if($("#adrs_city{{$value->id}}").val() == ""){
             $("#adrs_city_error{{$value->id}}").text("City is required.")
        } 
        
        if($("#adrs_pincode{{$value->id}}").val() == ""){
             $("#adrs_pincode_error{{$value->id}}").text("Pincode is required.")
        } 

       if($("#adrs_state{{$value->id}}").val() == ""){
             $("#adrs_state_error{{$value->id}}").text("State is required.")
        }  

       if($("#adrs_name{{$value->id}}").val() != "" && $("#adrs_phone{{$value->id}}").val() != "" && $("#adrs_address{{$value->id}}").val() != "" && $("#adrs_town{{$value->id}}").val() != "" && $("#adrs_city{{$value->id}}").val() != "" && $("#adrs_pincode{{$value->id}}").val() != "" && $("#adrs_state{{$value->id}}").val() != ""){
              $("#addnewaddress{{$value->id}}").attr("type","submit");
              $("#addnewaddress{{$value->id}}").click();
       }     
   });
</script>
@endforeach
@endif

<script type="text/javascript">
    $(document).ready(function(){
        $("#couponbuttonid").on("click", function() {
            var couponcode = $("#couponid").val();
            if(couponcode.length > 0) {
                $.ajax({
                    type:"POST",
                    url:"{{ route('apply_cupon') }}",
                    data:{code:couponcode,_token:'{{csrf_token()}}'},
                    success:function(response) {
                        console.log(response);
                        var data = JSON.parse(response);
                        if(data.status == 0) {
                            $("#coupenerror").html(data.msg);
                        } else {
                            $("#csp").html("Rs "+data.data[0]);
                            $("#cdis").css("display","block");
                            $("#ctdis").html("Rs "+data.data[1]);
                            $("#ctotal").html("Rs "+data.data[2]);
                            $("#cadddis").html("Rs "+data.data[3]);
                            $("#coupensuccess").html(data.msg);
                            $("#ccode").val(couponcode);
                            location.reload();
                        }
                    }
                });
            } else {
                $("#coupenerror").html("Please enter coupon code");
            }
        });

        $(".wrapclass").on("change",function() {
            var wrap = $(this).val();
            $.ajax({
                type:"POST",
                url:"{{ route('change_giftwrap') }}",
                data:{wrap:wrap,_token:'{{csrf_token()}}'},
                success:function(response) {
                    location.reload();
                }
            });
        });

        $(".shipclass").on("change",function() {
            var wrap = $(this).val();

            if(wrap == "online") {

                var seleted = $(".wrapclass:checked").val();
                if(seleted == "yes") {
                  
                    $('#shipiingcharges').css('display','none');
                    
                    $('#withoutshippingandWrap').css('display','block');
                    $('#withshippingandWrap').css('display','none');

                    $('#withShippping').css('display','none');
                    $('#withoutshipping').css('display','none');

                } else {
                  
                    $('#shipiingcharges').css('display','none');
                    
                    $('#withoutshippingandWrap').css('display','none');
                    $('#withshippingandWrap').css('display','none');

                    $('#withShippping').css('display','none');
                    $('#withoutshipping').css('display','block');
                }
            } else {

                var seleted = $(".shipclass:checked").val();
                if(seleted == "yes") {

                    $('#shipiingcharges').css('display','block');
                    
                    $('#withoutshippingandWrap').css('display','none');
                    $('#withshippingandWrap').css('display','block');

                    $('#withShippping').css('display','none');
                    $('#withoutshipping').css('display','none');
                
                } else {

                    $('#shipiingcharges').css('display','block');
                    
                    $('#withoutshippingandWrap').css('display','none');
                    $('#withshippingandWrap').css('display','none');

                    $('#withShippping').css('display','block');
                    $('#withoutshipping').css('display','none');
                }
            }
        });
    });
</script>

<div id="Locationnn" class="modal fade" role="dialog">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-body">
                <div class="AddAddress">
                    <h3>Add Address</h3>
                    <form action="{{ route('add-address') }}" method="post">
                        <div class="form-group col-sm-6">
                            <label>Full Name</label>
                            <input name="name" id="adrs_name" type="text" class="form-control">
                             <span id="adrs_name_error" style="color: red;"></span>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Mobile Number</label>
                            <input name="phone" id="adrs_phone" type="text" class="form-control">
                            <span id="adrs_phone_error" style="color: red;"></span>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Address</label>
                            <textarea name="address" id="adrs_address" rows="3" class="form-control"></textarea>
                            <span id="adrs_address_error" style="color: red;"></span>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Town</label>
                            <input name="town" id="adrs_town" type="text" class="form-control">
                            <span id="adrs_town_error" style="color: red;"></span> 
                        </div>
                        <div class="form-group col-sm-6">
                            <label>City</label>
                            <input name="city" id="adrs_city" type="text" class="form-control">
                            <span id="adrs_city_error" style="color: red;"></span> 
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Pincode</label>
                            <input name="pincode" id="adrs_pincode" type="text" class="form-control">
                            <span id="adrs_pincode_error" style="color: red;"></span> 
                        </div>
                        <div class="form-group col-sm-6">
                            <label> State</label>
                            <input name="state" id="adrs_state" type="text" class="form-control">
                            <span id="adrs_state_error" style="color: red;"></span> 
                        </div>

                        <div class="form-group col-sm-12">
                            <label>Address Type</label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="Address">
                                        <input type="radio" value="Home" name="type" checked>
                                        <span class="checkmark"></span>
                                        <h4>Home <span>(All Day Delivery)</span></h4>
                                    </label>
                                </div>

                                <div class="col-sm-6">
                                    <label class="Address">
                                        <input value="Office" type="radio" name="type">
                                        <span class="checkmark"></span>
                                        <h4>Office <span>(Between 10 AM to  7 PM)</span></h4>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <input type="checkbox" name="remark" > Make it Default<br>
                         </div>
                        <div class="form-group col-sm-12">
                            <button type="button" class="btn btn-primary" id="addnewaddress">Save Address</button>
                        </div>
                       
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    </form>

                    <div class="clear"></div>
                </div>
            </div> 
        </div>
    </div>
</div>

<script type="text/javascript">
       $("#addnewaddress").click(function(){
           
             if($("#adrs_name").val() == ""){
                 $("#adrs_name_error").text("Full Name is required.")
             }

            if($("#adrs_phone").val() == ""){
                 $("#adrs_phone_error").text("Phone Number is required.")
            } 

            if($("#adrs_address").val() == ""){
                 $("#adrs_address_error").text("Address is required.")
            } 

            if($("#adrs_town").val() == ""){
                 $("#adrs_town_error").text("Town is required.")
            } 
            
            if($("#adrs_city").val() == ""){
                 $("#adrs_city_error").text("City is required.")
            } 
            
            if($("#adrs_pincode").val() == ""){
                 $("#adrs_pincode_error").text("Pincode is required.")
            } 

           if($("#adrs_state").val() == ""){
                 $("#adrs_state_error").text("State is required.")
            }  

           if($("#adrs_name").val() != "" && $("#adrs_phone").val() != "" && $("#adrs_address").val() != "" && $("#adrs_town").val() != "" && $("#adrs_city").val() != "" && $("#adrs_pincode").val() != "" && $("#adrs_state").val() != ""){
                  $("#addnewaddress").attr("type","submit");
                  $("#addnewaddress").click();
           }     
       });
    </script>

@endsection