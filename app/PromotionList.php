<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionList extends Model {
    //

    protected $fillable = [
         'seller_id', 'promotion_id', 'sku', 'product_id', 'status'
    ];
}
