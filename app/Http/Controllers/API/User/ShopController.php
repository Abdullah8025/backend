<?php
namespace App\Http\Controllers\API\User;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use DB;
use App\User;
use App\Token;
use App\Category;
use App\Product;
use App\ProductImageColor;
use App\Attribute;
use App\Size;
use App\Wishlist;
use App\WishlistStore;
use App\Banner;
use App\Seller;
use App\Cart;
use App\Couponcode;
use App\Order;
use App\OrderDetail;
use App\Address;
use App\OrderRating;
use App\OrderRequest;
use App\Review;
use App\Notification;
use App\SellerWallet;
use App\ProductOffer;
use Image;
use Mail;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class ShopController extends APIBaseController {

    public function __construct() {

    }

    public function homeproduct(Request $request) {
        $lang = app()->getLocale();
        if(strlen($request->token) > 0) {
            $check_token = Token::where(['token' => $request->token])->first();
            if (empty($check_token)) {
                return $this->sendError($request->path(), 'Login Token Expire');
            }
        }
      //user latlong  
      $latitude = $request->latitude;
      $longitude = $request->longitude;
        //$banners = Banner::where("type","homeslider")->orderBy('id','DESC')->get();
        $banners = Banner::select('id','image','type','category_id')->orderBy('id','DESC')->get();
       // $bannerArr = [];
       // if(count($banners)>0){
            foreach($banners as $banner){
             $banner->image = asset('public/'.$banner->image); 
            //}
        }

        //offer
        $offers = ProductOffer::select('id','seller_id','name','image','description','mrp','offer as offer_price')->where('status',1)->orderBy('id','DESC')->get();
            foreach($offers as $offer){
                $offer->image = asset('public'.$offer->image);
            }
        $name = ($lang=='ar')?$lang.'_name as name':'name';  
        $type = ($lang=='ar')?$lang.'_type as type':'type';
        // Latest Stores
        $latest = Seller::select("id", "$name", "email", "image", "cover_image", "delivery_time", "$type", "shipping","rating")->where(["status"=>1])->where('created_at','>=',Carbon::now()->subdays(15))->orderBy('id','DESC')->limit(50)->get();
        foreach($latest as $lat) {
            //$lat->rating_count = OrderRating::where('seller_id', $lat->id)->count("id");
            $lat->rating_count = store_review_count($lat->id);
            $lat->image = asset('public/'.$lat->image);
            $lat->cover_image = asset('public/'.$lat->cover_image);
            if(strlen($request->token) > 0) {

                $haveLike = WishlistStore::where(['user_id'=>$check_token->user_id, "store_id"=>$lat->id])->first();
                if($haveLike) {
                    $haveFav = "yes";
                } else {
                    $haveFav = "no";
                }
            } else {
                $haveFav = "no";
            }

            $lat->favourite = $haveFav;

            if(strlen($lat->type) > 0) {
                $lat->type = explode(',', $lat->type);
            } else {
                $lat->type = [];
            }
        }

        //all store
         $allstores = Seller::select("id", "$name", "email", "image", "cover_image", "delivery_time", "$type", "shipping","rating")->where(["status"=>1])->orderBy('id','DESC')->get();
        foreach($allstores as $allstore) {
           // $allstore->rating_count = OrderRating::where('seller_id', $allstore->id)->count("id");
            $allstore->rating_count = store_review_count($allstore->id);

            $allstore->image = asset('public/'.$allstore->image);
            $allstore->cover_image = asset('public/'.$allstore->cover_image);
            if(strlen($request->token) > 0) {

                $haveLike = WishlistStore::where(['user_id'=>$check_token->user_id, "store_id"=>$allstore->id])->first();
                if($haveLike) {
                    $haveFav = "yes";
                } else {
                    $haveFav = "no";
                }
            } else {
                $haveFav = "no";
            }

            $allstore->favourite = $haveFav;

            if(strlen($allstore->type) > 0) {
                $allstore->type = explode(',', $allstore->type);
            } else {
                $allstore->type = [];
            }
        }
        // Popular Stores
        $sellerArr = array();
        $getSellers = DB::table('orders')
                 ->select('seller_id', DB::raw('count(seller_id) as sellertotal'))
                 ->groupBy('seller_id')
                 ->havingRaw("COUNT(seller_id) > 4")
                 ->get();

        foreach($getSellers as $getSeller) {
            $sellerArr[] = $getSeller->seller_id;
        }
        //echo $name;die;
        $popular = Seller::select("id", "$name", "email", "image", "cover_image", "delivery_time", "$type", "shipping","rating")->where(["status"=>1])->whereIn('id', $sellerArr)->orderBy('id','DESC')->limit(50)->get();
        foreach($popular as $pop) {
            //$pop->rating_count = OrderRating::where('seller_id', $pop->id)->count("id");
              $pop->rating_count = store_review_count($pop->id);

            if(strlen($request->token) > 0) {
                $haveLike = WishlistStore::where(['user_id'=>$check_token->user_id, "store_id"=>$pop->id])->first();
                if($haveLike) {
                    $haveFav = "yes";
                } else {
                    $haveFav = "no";
                }
            } else {
                $haveFav = "no";
            }

            $pop->favourite = $haveFav;

            if(strlen($pop->type) > 0) {
                $pop->type = explode(',', $pop->type);
            } else {
                $pop->type = [];
            }
        }
    
        // Distance Stores
       // $distance = Seller::select("id", "name", "email", "image", "cover_image", "delivery_time", "type", "shipping","rating")->where(["status"=>1])->orderBy('id','DESC')->limit(6)->get();
        $distances =   DB::table('sellers as s')
                      ->select("id", "$name", "email", "image", "cover_image", "delivery_time", "$type","shipping","rating")
                      ->selectRaw("IFNULL(111.111 * DEGREES(ACOS(LEAST(1.0, COS(RADIANS(s.latitude)) * COS(RADIANS($latitude))* COS(RADIANS(s.longitude - $longitude)) + SIN(RADIANS(s.latitude)) * SIN(RADIANS($latitude))))), 0) AS distance")
                      ->where('status',1)
                      ->having("distance", "<=", 50000)
                      ->orderBy('distance','ASC')
                      ->get();
                     //echo "<pre>";print_r($distances);die; 
     // $distances = [];                
    // if(!empty($distances)){
        foreach($distances as $dist) {
           // $dist->rating_count = OrderRating::where('seller_id', $dist->id)->count("id");
            $dist->rating_count = store_review_count($dist->id);
            $dist->distance = round($dist->distance,0);
            $dist->image = asset('public/'.$dist->image);
            $dist->$name = $dist->name;
            $dist->cover_image = asset('public/'.$dist->cover_image);
            if(strlen($request->token) > 0) {
                $haveLike = WishlistStore::where(['user_id'=>$check_token->user_id, "store_id"=>$dist->id])->first();
                if($haveLike) {
                    $haveFav = "yes";
                } else {
                    $haveFav = "no";
                }
            } else {
                $haveFav = "no";
            }

            $dist->favourite = $haveFav;

            if(strlen($dist->type) > 0) {
                $dist->type = explode(',', $dist->type);
            } else {
                $dist->type = [];
            }
        }
    //}  
      //distances  //popular
        return $this->sendResponse1(['banner' => $banners, 'latest' => $latest, "popular" => $latest,'allstores'=>$allstores, "offers" => $offers, "distance" => $latest], trans('message.detail_retrived'), $request->path());
    }

    //offer detail
    public function offerDetail(Request $request){
         $input = $request->all();
        $validator = Validator::make($input, ['offer_id' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        // if(strlen($request->token) > 0) {
        //     $check_token = Token::where(['token' => $request->token])->first();
        //     if (empty($check_token)) {
        //         return $this->sendError($request->path(), 'Login Token Expire');
        //     }
        // }
        $offer = ProductOffer::select('id','seller_id','name','product_id','qty','mrp','offer as offer_price','description','image','created_at')->where('id',$request->offer_id)->first();
        $seller = Seller::select('name','image','cover_image','address')->where('id',$offer->seller_id)->first();
      //  $seller->image = asset('public'.$seller->image);
       // $seller->cover_image = asset('public'.$seller->cover_image);
        $offer->seller_name = $seller->name;
        $offer->seller_address = $seller->address;
        $offer->seller_image = asset('public/'.$seller->image);
        $offer->image = asset('public'.$offer->image);
        $explodeProId = explode(',',$offer->product_id);
        $products = Product::select('id','name','mrp','category_id','images')->whereIn('id',$explodeProId)->get();
        $i=0;
        foreach($products as $product){
            $image = explode(",",$product->images);
            if(!empty($image)){
              $img = $image[0];
            }else{
              $img = asset('public/img/honey_app.png');
            }
            $qtyArr = explode(",",$offer->qty);
            $product->images = asset('public'.$img);
            $product->qty = $qtyArr[$i];

            $i++;
         }
        
           $offer->haveCart = "no";
           $offer->product = $products;
         return $this->sendResponse($offer, 'Offer detail retrieved successfully', $request->path());
    }

    public function categorylist(Request $request) {
        $lang = app()->getLocale();
        $name = ($lang=='ar')?$lang.'_name as name':'name';  
        $details = DB::table('categories')->select("id", "$name")->where('status',1)->get();
        return $this->sendResponse($details,trans('message.categiry_retrived'), $request->path());
    }

    //home search new api
    public function homesearch(Request $request){
        // echo "hi";die;
         $input = $request->all();
         $lang = app()->getLocale();
        if(strlen($request->token) > 0) {
            $check_token = Token::where(['token' => $request->token])->first();
            if (empty($check_token)) {
                return $this->sendError($request->path(), trans('message.token_expire'));
            }
        }
        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $name = ($lang=='ar')?$lang.'_name as name':'name';  
        $type = ($lang=='ar')?$lang.'_type':'type';
        // Stores
       $query =  DB::table('sellers as s');
       $query->select("s.id", "$name", "email", "image", "cover_image", "delivery_time", "$type", "shipping");
       $query->selectRaw(DB::raw('round(AVG(store_rate),0) as rating'));
       $query->selectRaw("IFNULL(111.111 * DEGREES(ACOS(LEAST(1.0, COS(RADIANS(s.latitude)) * COS(RADIANS($latitude))* COS(RADIANS(s.longitude - $longitude)) + SIN(RADIANS(s.latitude)) * SIN(RADIANS($latitude))))), 0) AS distance");
      $query->leftjoin('order_ratings as rate','s.id','=','rate.seller_id');
      $query->where('s.status',1);
      $query->groupBy('s.id');
      // $orderrating = OrderRating::where('seller_id',$all_cat_pro->seller_id)->avg('store_rate');
      if(!empty($request->from) && !empty($request->to)) {
        $query->having("distance", "<=", $request->from);
        $query->having("distance", ">=", $request->to);
       }else{
       	$query->having("distance", "<=", 50);
       } 

      if ($request->has('type') && strlen($request->type)>0) {
            $query->where($type, 'like', '%'.$request->type.'%');
        }

      if ($request->has('delivery_day') && strlen($request->delivery_day)>0) {
            $query->where('delivery_time',$request->delivery_day);
        }
      if(!empty($request->rating)){
        $query->where('rating',$request->rating);	
      }  
      $stores =  $query->get();
     // echo "<pre>";print_r($stores->toArray());die;
      foreach($stores as $store){
      	$store->distance = round($store->distance,0);
      	$store->image = asset('public/'.$store->image);
      	$store->cover_image = asset('public/'.$store->cover_image);
      	$store->rating = ($store->rating)?$store->rating:0;
      	
        if(strlen($store->$type) > 0) {
         $store->type = explode(',', $store->$type);
         } else {
          $store->type = [];
        }


        $getSellerCats = Category::where("seller_id", $store->id)->get();
        $sellerCat_arr = array();
        $sellerCat_typearr = array();
        foreach($getSellerCats as $getSellerCat) {
            if($lang=='ar') {
              $sellerCat_arr[] = $getSellerCat->ar_name;
              $sellerCat_typearr[] = ["name"=>$getSellerCat->ar_name, "id"=>$getSellerCat->id];
            } else {
              $sellerCat_arr[] = $getSellerCat->name;
              $sellerCat_typearr[] = ["name"=>$getSellerCat->name, "id"=>$getSellerCat->id];
            }
        }

        if(count($sellerCat_arr) > 0) {
            $store->type = $sellerCat_typearr;
        } else {
            $store->type = [];
        }

        $store->rating_count = OrderRating::where('seller_id', $store->id)->count("id");
        if(strlen($request->token) > 0) {
            $haveLike = WishlistStore::where(['user_id'=>$check_token->user_id, "store_id"=>$store->id])->first();
                if($haveLike) {
                    $haveFav = "yes";
                } else {
                    $haveFav = "no";
                }
            } else {
                $haveFav = "no";
            }

            $store->favourite = $haveFav;
      }
      // echo "<pre>";print_r($result->toArray()); die;
      return $this->sendResponse1([ "stores" => $stores],trans('message.detail_retrived'), $request->path());
    }

    public function searching(Request $request) {

        $input = $request->all();
        $lang = app()->getLocale();

        if(strlen($request->token) > 0) {
            $check_token = Token::where(['token' => $request->token])->first();
            if (empty($check_token)) {
                return $this->sendError($request->path(),trans('message.token_expire'));
            }
        }

        // Stores
        //DB::enableQueryLog();

        $name = ($lang=='ar')?$lang.'_name as name':'name';
        $search_name = ($lang=='ar')?$lang.'_name':'name';
        $type = ($lang=='ar')?$lang.'_type':'type';
        $query = Seller::query();
        $query->select("id", "$name", "email", "image", "cover_image", "delivery_time", "$type", "shipping", "rating")->where(["status"=>1]);

        if ($request->has('search_key') && strlen($request->search_key)>0) {
            $query->where($search_name, 'like', '%'.$request->search_key.'%');
        }

        if ($request->has('type') && strlen($request->type)>0) {
            $query->where($type, 'like', '%'.$request->type.'%');
        }

        if ($request->has('sort_by') && strlen($request->sort_by)>0) {

            if($request->sort_by == "popular") {
                $sellerArr = array();
                $getSellers = DB::table('orders')
                         ->select('seller_id', DB::raw('count(seller_id) as sellertotal'))
                         ->groupBy('seller_id')
                         ->havingRaw("COUNT(seller_id) > 4")
                         ->get();

                foreach($getSellers as $getSeller) {
                    $sellerArr[] = $getSeller->seller_id;
                }

                $stores = $query->whereIn('id', $sellerArr)->orderBy('id','DESC')->get();
            
            } else if($request->sort_by == "free_delivery") {

                $stores = $query->where('shipping', "yes")->orderBy('id','DESC')->get();
            
            } else if($request->sort_by == "nearest") {

                $stores = $query->orderBy('id','ASC')->get();
            
            } else if($request->sort_by == "delivery_time") {

                $stores = $query->orderBy('delivery_time','ASC')->get();
            
            } else {
                $stores = $query->orderBy('id','DESC')->get();
            }

        } else {
            $stores = $query->orderBy('id','DESC')->get();
        }
      // print_r(DB::getQueryLog());die;

      //echo "<pre>";print_r($stores);die;
        foreach($stores as $store) {
            $store->rating_count = OrderRating::where('seller_id', $store->id)->count("id");
            $store->image = asset('public/'.$store->image);
            $store->cover_image = asset('public/'.$store->cover_image);

            if(strlen($request->token) > 0) {
                $haveLike = WishlistStore::where(['user_id'=>$check_token->user_id, "store_id"=>$store->id])->first();
                if($haveLike) {
                    $haveFav = "yes";
                } else {
                    $haveFav = "no";
                }
            } else {
                $haveFav = "no";
            }

            $store->favourite = $haveFav;

            $getSellerCats = Category::where("seller_id", $store->id)->get();
            $sellerCat_arr = array();
            $sellerCat_typearr = array();
            foreach($getSellerCats as $getSellerCat) {
                if($lang=='ar') {
                  $sellerCat_arr[] = $getSellerCat->ar_name;
                  $sellerCat_typearr[] = ["name"=>$getSellerCat->ar_name, "id"=>$getSellerCat->id];
                } else {
                  $sellerCat_arr[] = $getSellerCat->name;
                  $sellerCat_typearr[] = ["name"=>$getSellerCat->name, "id"=>$getSellerCat->id];
                }
            }

            if(count($sellerCat_arr) > 0) {
                $store->$type = $sellerCat_typearr;
            } else {
                $store->$type = [];
            }
        }
          
        // Products
        $category_id = ($lang=='ar')?$lang.'_category_id':'category_id';
        $querypro = Product::query();
        $querypro->select("id", "user_id as seller_id", "$name", "images", "mrp","sp","discount","ships_in")->where(["status"=>1,"deleteStatus"=>1]);
        
        if ($request->has('search_key') && strlen($request->search_key)>0) {
            $querypro->where($search_name,'like', '%'.$request->search_key.'%');
        }

        if ($request->has('type') && strlen($request->type)>0) {
            $querypro->where($category_id, 'like', '%'.$request->type.'%');
        }

        if ($request->has('sort_by') && strlen($request->sort_by)>0) {

            if($request->sort_by == "popular") {
                $productArr = array();
                $getProducts = DB::table('order_details')
                         ->select('product_id', DB::raw('count(product_id) as producttotal'))
                         ->groupBy('product_id')
                         ->havingRaw("COUNT(product_id) > 4")
                         ->get();

                foreach($getProducts as $getProduct) {
                    $productArr[] = $getProduct->product_id;
                }

                $products = $querypro->whereIn('id', $productArr)->orderBy('id','DESC')->get();
            
            } else if($request->sort_by == "free_delivery") {

                $products = $querypro->orderBy('id','DESC')->get();
            
            } else if($request->sort_by == "nearest") {

                $products = $querypro->orderBy('id','ASC')->get();
            
            } else if($request->sort_by == "delivery_time") {

                $products = $querypro->orderBy('ships_in','ASC')->get();
            
            } else if($request->sort_by == "price_range") {

                $products = $querypro->where("sp>=", $request->price_low)->where("sp<=", $request->price_high)->orderBy('sp','ASC')->get();
            
            } else {
                $products = $querypro->orderBy('id','DESC')->get();
            }

        } else {
            $products = $querypro->orderBy('id','DESC')->get();
        }

        foreach($products as $pro) {

            $pro->images = asset('public'.$pro->images);

            if(strlen($request->token) > 0) {
                $haveLike = Wishlist::where(['user_id'=>$check_token->user_id, "product_id"=>$pro->id])->first();
                if($haveLike) {
                    $haveFav = "yes";
                } else {
                    $haveFav = "no";
                }
            } else {
                $haveFav = "no";
            }

            if(strlen($request->token) > 0) {
                $haveCart = Cart::where(['user_id'=>$check_token->user_id, "product_id"=>$pro->id])->first();
                if($haveCart) {
                    $haveCart = "yes";
                } else {
                    $haveCart = "no";
                }
            } else {
                $haveCart = "no";
            }

            $pro->favourite = $haveFav;
            $pro->havecart = $haveCart;
        }

        return $this->sendResponse1(["products" => $products, "stores" => $stores], 'Details retrieved successfully', $request->path());
    }

    public function productdetail(Request $request) {
        $input = $request->all();
        $lang = app()->getLocale();
        $validator = Validator::make($input, ['product_id' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        if(strlen($request->token) > 0) {
            $check_token = Token::where(['token' => $request->token])->first();
            if (empty($check_token)) {
                return $this->sendError($request->path(), trans('message.token_expire'));
            }
        }
        $name = ($lang=='ar')?$lang.'_name as name':'name';
        $sku = ($lang=='ar')?$lang.'_sku as sku':'sku';
        $sp = ($lang=='ar')?$lang.'_sp as sp':'sp';
        $quantity = ($lang=='ar')?$lang.'_quantity as quantity':'quantity';
        $mrp = ($lang=='ar')?$lang.'_mrp as mrp':'mrp';
        $category_id = ($lang=='ar')?$lang.'_category_id as category_id':'category_id';
        $description = ($lang=='ar')?$lang.'_description as description':'description';
        $weight = ($lang=='ar')?$lang.'_weight as weight':'weight';
        $size = ($lang=='ar')?$lang.'_size as size':'size';
       // $productRating = OrderRating::where('product_id',$request->product_id)->avg('product_rate');
        $product = Product::select("$name","$sku","$sp","$quantity","$mrp","$category_id","$description","$weight","$size", "user_id as seller_id","id",'images','rating')->where('id', $request->product_id)->first();
        
        if(!empty($product->images)){
          $explodeImg = explode(',',$product->images);
          $product->images = asset('public'.$explodeImg[0]);
        }else{
          $product->images =    asset('public/img/honey_app.png');
        }
        $attriArr = array();
        $label = ($lang=='ar')?$lang.'_label':'label'; 
        $labeltext = ($lang=='ar')?$lang.'_labeltext':'labeltext';
        $getAttri = Attribute::select("$label","$labeltext")->where(["product_id"=>$product->id])->get();
        if($getAttri) {
            foreach($getAttri as $getAttr) {
                $attriArr[]= ["labal"=>$getAttr->$label,"text"=>$getAttr->$labeltext,"image"=>""];
            }

            $product->attributes = $attriArr;
        } else {
            $product->attributes = $attriArr;
        }
       // echo $check_token->user_id;die;
        if(strlen($request->token) > 0) {
            $haveLike = Wishlist::where(['user_id'=>$check_token->user_id, "product_id"=>$product->id])->first();
            if($haveLike) {
                $haveFav = "yes";
            } else {
                $haveFav = "no";
            }
        } else {
            $haveFav = "no";
        }

        if(strlen($request->token) > 0) {
           // echo $product->id;die;
            $haveCartt = Cart::where(['user_id'=>$check_token->user_id, "product_id"=>$product->id])->first();
            if($haveCartt) {
                 $haveCart = "yes";
                $haveQTY = $haveCartt->quantity;
            } else {
                $haveCart = "no";
                $haveQTY = 0;
            }
        } else {
            $haveCart = "no";
            $haveQTY = 0;
        }
        //echo $haveCart;die;
        
        $product->favourite = $haveFav;
        $product->havecart = $haveCart;
        $product->haveQTY = $haveQTY;
        $product->review = getReview($request->product_id,0);
        $product->rating = $product->rating;
       // $product->total_rating = getRating($request->product_id,0);

        return $this->sendResponse1($product, trans('message.detail_retrived'), $request->path());
    }

    public function storedetail(Request $request) {
        $input = $request->all();
        $lang = app()->getLocale();
        $validator = Validator::make($input, ['store_id' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        if(strlen($request->token) > 0) {
            $check_token = Token::where(['token' => $request->token])->first();
            if (empty($check_token)) {
                return $this->sendError($request->path(), trans('message.token_expire'));
            }
        }
        $name = ($lang=='ar')?$lang.'_name as name':'name';  
        $type = ($lang=='ar')?$lang.'_type as type':'type'; 
        $all = ($lang=='ar')?'الكل':'All'; 

        $seller = Seller::select("id","$name","email","phone","image","cover_image","address","password","status","created_at","commission","latitude","longitude","city","pincode","country","delivery_time","shipping","$type","rating","type as type1")->where('id', $request->store_id)->first();
        
        $orderrating = OrderRating::where('seller_id',$seller->id)->avg('store_rate');
        $seller->image = asset('public/'.$seller->image);
        $seller->cover_image = asset('public/'.$seller->cover_image);
        $seller->rating = round($orderrating,1);

        $sellerCat_arrr = array();
        $sellerCatAR_arrr = array();
        $sellerID_array = array();

        $sellerCat_arrr[] = ["name"=>"All", "id"=>"All"];
        $sellerCatAR_arrr[] = ["name"=>"الكل", "id"=>"الكل"];

        $getSellerProCatID = Product::select("category_id")->where("user_id", $request->store_id)->groupBy("category_id")->get();
        foreach($getSellerProCatID as $getSellerProID) {
          $getuniquecatid[] = $getSellerProID->category_id;
        }
        //var_dump($getuniquecatid);die;
        $getSellerCats = Category::whereIn("id", $getuniquecatid)->get();
        foreach($getSellerCats as $getSellerCat) {

            $checkPro = Product::where(["user_id"=>$seller->id,"category_id"=>$getSellerCat->id])->first();
            if($checkPro) {
                $sellerCat_arrr[] = ["name"=>$getSellerCat->name, "id"=>$getSellerCat->id];
                $sellerCatAR_arrr[] = ["name"=>$getSellerCat->ar_name, "id"=>$getSellerCat->id];

                $sellerID_array[] = $getSellerCat->id;
            }
        }

        if($lang=='ar') {
          $seller->type = $sellerCatAR_arrr;
        } else {

          $seller->type = $sellerCat_arrr;
        }

        if(strlen($request->token) > 0) {
            $haveLike = WishlistStore::where(['user_id'=>$check_token->user_id, "store_id"=>$seller->id])->first();
            if($haveLike) {
                $haveFav = "yes";
            } else {
                $haveFav = "no";
            }
        } else {
            $haveFav = "no";
        }

        $seller->favourite = $haveFav;
        $pro_name = ($lang=='ar')?$lang.'_name as name':'name';
        $sku = ($lang=='ar')?$lang.'_sku as sku':'sku';
        $sp = ($lang=='ar')?$lang.'_sp as sp':'sp';
        $quantity = ($lang=='ar')?$lang.'_quantity as quantity':'quantity';
        $mrp = ($lang=='ar')?$lang.'_mrp as mrp':'mrp';
        $category_id = ($lang=='ar')?$lang.'_category_id as category_id':'category_id';
        $description = ($lang=='ar')?$lang.'_description as description':'description';
        $weight = ($lang=='ar')?$lang.'_weight as weight':'weight';
        $size = ($lang=='ar')?$lang.'_size as size':'size';
      
        // Latest Stores
        $latest = Product::select("id", "user_id as seller_id", "$pro_name", "images", "$category_id", "$mrp", "$sp", "discount", "ships_in")->where(["user_id"=>$seller->id, "status"=>1])->orderBy('id','DESC')->limit(10)->get();
      
        foreach($latest as $lat) {
          if(!empty($lat->images)){
             $explodeImg = explode(',',$lat->images);  
            $lat->images = asset('public'.$explodeImg[0]);
            }else{
             $lat->images = asset('public/img/honey_app.png');   
            }

            if(strlen($request->token) > 0) {
                $haveLike = Wishlist::where(['user_id'=>$check_token->user_id, "product_id"=>$lat->id])->first();
                if($haveLike) {
                    $haveFav = "yes";
                } else {
                    $haveFav = "no";
                }
            } else {
                $haveFav = "no";
            }

            if(strlen($request->token) > 0) {
                $haveCart = Cart::where(['user_id'=>$check_token->user_id, "product_id"=>$lat->id])->first();
                if($haveCart) {
                    $haveCart = "yes";
                } else {
                    $haveCart = "no";
                }
            } else {
                $haveCart = "no";
            }

            $lat->favourite = $haveFav;
            $lat->havecart = $haveCart;

            $getCats = Category::where("id", $category_id)->first();
            if($getCats) {
                $lat->$category_id = $getCats->$name;
            }
        }

        //  Category Wise
        $catArray = array();
        if(count($sellerID_array) > 0) {
           
            $all_cat_pros = Product::select("id", "user_id as seller_id", "$pro_name", "images", "$category_id", "$mrp", "$sp", "discount", "ships_in")->where(["user_id"=>$seller->id, "status"=>1])->whereIn('category_id', $sellerID_array)->orderBy('id','DESC')->limit(10)->get();
           
            foreach($all_cat_pros as $all_cat_pro) {
                $all_cat_pro->images = asset('public'.$all_cat_pro->images);

                if(strlen($request->token) > 0) {
                    $haveLike = Wishlist::where(['user_id'=>$check_token->user_id, "product_id"=>$all_cat_pro->id])->first();
                    if($haveLike) {
                        $haveFav = "yes";
                    } else {
                        $haveFav = "no";
                    }
                } else {
                    $haveFav = "no";
                }

                if(strlen($request->token) > 0) {
                    $haveCart = Cart::where(['user_id'=>$check_token->user_id, "product_id"=>$all_cat_pro->id])->first();
                    if($haveCart) {
                        $haveCart = "yes";
                    } else {
                        $haveCart = "no";
                    }
                } else {
                    $haveCart = "no";
                }

                $all_cat_pro->favourite = $haveFav;
                $all_cat_pro->havecart = $haveCart;

                $getCats = Category::where("id", $category_id)->first();
                if($getCats) {
                    $all_cat_pro->$category_id = $getCats->$name;
                }
            }

            $catArray['all'] = $all_cat_pros;
        }

        return $this->sendResponse1([ "store" => $seller, "featured" => $latest, "all"=>$catArray,'review'=>getReview($request->store_id,1),'rating'=>round($orderrating,1)], trans('message.detail_retrived'), $request->path());
    }

    public function productsbytype(Request $request) {
        $input = $request->all();
        $lang = app()->getLocale();
        $validator = Validator::make($input, ['store_id' => 'required', "type" => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        if(strlen($request->token) > 0) {
            $check_token = Token::where(['token' => $request->token])->first();
            if (empty($check_token)) {
                return $this->sendError($request->path(), trans('message.token_expire'));
            }
        }

        $seller = Seller::where('id', $request->store_id)->first();

        $getSellerCats = Category::where("seller_id", $request->store_id)->get();
        $sellerCat_arr = array();
        if(count($getSellerCats) > 0) {
          
          foreach($getSellerCats as $getSellerCat) {
            $sellerCat_arr[] = $getSellerCat->name;
          }
        } else {
            $catPros = Product::select("category_id")->where(["user_id"=>$request->store_id])->get();
            //dd($catPros);
            foreach($catPros as $catPro) {

                $sellerCat_arr[] = $catPro->category_id;
            }
        }
        $seller_type = $sellerCat_arr;
        $name = ($lang=='ar')?$lang.'_name as name':'name';
        $sku = ($lang=='ar')?$lang.'_sku as sku':'sku';
        $sp = ($lang=='ar')?$lang.'_sp as sp':'sp';
        $quantity = ($lang=='ar')?$lang.'_quantity as quantity':'quantity';
        $mrp = ($lang=='ar')?$lang.'_mrp as mrp':'mrp';
        $category_id = ($lang=='ar')?$lang.'_category_id as category_id':'category_id';
        $description = ($lang=='ar')?$lang.'_description as description':'description';
        $weight = ($lang=='ar')?$lang.'_weight as weight':'weight';
        $size = ($lang=='ar')?$lang.'_size as size':'size';
        $ships_in = ($lang=='ar')?$lang.'_ships_in as ships_in':'ships_in';
      
        $catArray = array();

        if(count($seller_type) > 0) {
            $all = ($lang=='ar')?"الكل":"all";
            if(strtolower($request->type) == $all) {
          
                $all_cat_pros = Product::select("id", "user_id as seller_id", "$name", "images", "category_id", "$mrp", "$sp", "discount", "$ships_in","rating")->where(["user_id"=>$seller->id, "status"=>1])->inRandomOrder()->limit(10)->get();
                //var_dump($all_cat_pros);die;
                //echo "<pre>";print_r($all_cat_pros->toArray());die;
               // print_r(DB::getQueryLog());die;

                foreach($all_cat_pros as $all_cat_pro) {

                    //var_dump($all_cat_pro->category_id);die;

                    $getCats = Category::where("id", $all_cat_pro->category_id)->get();
                      if($getCats) {
                        foreach($getCats as $getCat) {
                            if($lang == "ar") {
                                $all_cat_pro->category_id = $getCat->ar_name;
                            } else {
                                $all_cat_pro->category_id = $getCat->name;
                            }
                        }
                      }

                    if(!empty($all_cat_pro->images)){
                     $explodeImg = explode(',',$all_cat_pro->images);   
                     $all_cat_pro->images = asset('public'.$explodeImg[0]);
                    }else{
                     $all_cat_pro->images = asset('public/img/honey_app.png');   
                    }

                    if(strlen($request->token) > 0) {
                        $haveLike = Wishlist::where(['user_id'=>$check_token->user_id, "product_id"=>$all_cat_pro->id])->first();
                        if($haveLike) {
                            $haveFav = "yes";
                        } else {
                            $haveFav = "no";
                        }
                    } else {
                        $haveFav = "no";
                    }

                    if(strlen($request->token) > 0) {
                        $haveCart = Cart::where(['user_id'=>$check_token->user_id, "product_id"=>$all_cat_pro->id])->first();
                        if($haveCart) {
                            $haveCart = "yes";
                        } else {
                            $haveCart = "no";
                        }
                    } else {
                        $haveCart = "no";
                    }
                    //$orderrating = OrderRating::where('seller_id',$all_cat_pro->seller_id)->avg('store_rate');
                   // $productrating = OrderRating::where('product_id',$all_cat_pro->id)->avg('product_rate');
                    $all_cat_pro->favourite = $haveFav;
                    $all_cat_pro->havecart = $haveCart;
                    $all_cat_pro->review = getReview($all_cat_pro->id,0); //0 for product
                    $all_cat_pro->rating = $all_cat_pro->rating;
                }

            } else {
                $cat_filed_name = ($lang=='ar')?$lang.'_category_id':'category_id';
                
                $all_cat_pros = Product::select("id", "user_id as seller_id", "$name", "images", "category_id", "$mrp", "$sp", "discount", "$ships_in")->where(["user_id"=>$seller->id, "status"=>1])->where($cat_filed_name, $request->type)->inRandomOrder()->limit(10)->get();
                //var_dump($all_cat_pros);die;
                if($all_cat_pros) {

                    foreach($all_cat_pros as $all_cat_pro) {

                      $getCats = Category::where("id", $all_cat_pro->category_id)->get();
                      if($getCats) {
                        foreach($getCats as $getCat) {
                            if($lang == "ar") {
                                $all_cat_pro->category_id = $getCat->ar_name;
                            } else {
                                $all_cat_pro->category_id = $getCat->name;
                            }
                        }
                      }
                       
                      if(!empty($all_cat_pro->images)){
                         $explodeImg = explode(',', $all_cat_pro->images);
                         $all_cat_pro->images = asset('public'.$explodeImg[0]);
                      } else{
                         $all_cat_pro->images =  asset('public/img/honey_app.png');  
                      }

                      $getCat = Category::where("id", $all_cat_pro->category_id)->first();
                      if($getCat) {
                          if($lang=='ar') {

                              $all_cat_pros->$category_id = $getCat->ar_name;
                          } else {

                              $all_cat_pros->$category_id = $getCat->name;
                          }
                      }

                        if(strlen($request->token) > 0) {
                            $haveLike = Wishlist::where(['user_id'=>$check_token->user_id, "product_id"=>$all_cat_pro->id])->first();
                            if($haveLike) {
                                $haveFav = "yes";
                            } else {
                                $haveFav = "no";
                            }
                        } else {
                            $haveFav = "no";
                        }

                        if(strlen($request->token) > 0) {
                            $haveCart = Cart::where(['user_id'=>$check_token->user_id, "product_id"=>$all_cat_pro->id])->first();
                            if($haveCart) {
                                $haveCart = "yes";
                            } else {
                                $haveCart = "no";
                            }
                        } else {
                            $haveCart = "no";
                        }
                        
                        //$productrating = OrderRating::where('product_id',$all_cat_pro->id)->avg('product_rate');
                        $all_cat_pro->favourite = $haveFav;
                        $all_cat_pro->havecart = $haveCart;
                        $all_cat_pro->review = getReview($all_cat_pro->id,0); //0 for product
                        $all_cat_pro->rating = $all_cat_pro->rating;;
                    }
                }
            }
        } else {
            $all_cat_pros = [];
            //echo "hi";die;
        }
        if(!empty($request->token) ||  (!empty($request->cart_id) && !empty($request->quantity))){
         $card_details = $this->getCardDetails($request->token,$request->cart_id,$request->quantity); 
       }else{
        $card_details = ["sub_total"=>0, "tax"=>0, "discounted_amount"=>0, "total"=>0,'total_itmes'=>0];
       }
        
        if(!empty($card_details)){
          if($card_details['total_itmes']>0){
           $haveCart = "yes";
          }else{
          $haveCart = "no";
          }
        }else{
           $haveCart = "no";
        }
        return $this->sendResponse1(["list" => $all_cat_pros,'cart_details'=>$card_details,'haveCart'=>$haveCart],trans('message.list_retrieved'), $request->path());
       // return $this->sendResponse1(["list" => $all_cat_pros], trans('message.list_retrieved'), $request->path());

    }

    //other card detail in product type api
     public function getCardDetails($token="",$cart_id="",$quantity="") {
       // $input = $request->all();
        $lang = app()->getLocale();

        if(strlen($token) > 0) {
            $check_token = Token::where(['token' => $token])->first();
            if (empty($check_token)) {
                $status = "no";}
        } else {
            $check_token = "";
        }
//echo "Token ".$check_token;die;
        if (strlen($check_token)<=0) {

            // if(strlen($cart_id) <= 0) {
            //     $status = "no";
            // }

            $findproductids = explode(",", $cart_id);
           // print_r($findproductids);die;
            $findproductqty = explode(",", $quantity);
            // echo "hello";die;
        } else {
         // echo "hi";die;
            $findproductids = Cart::where(['user_id' => $check_token->user_id])->orderBy('id', 'asc')->pluck('product_id');
            if($findproductids) {
            } else {
                $status = "no";}
        }

       // $return_array = array();
        $cart_amount = array();
        $totalAmount = 0;
        $finalAmount = 0;
        $discounted_amount = 0;
        $q = 0;
      //  print_r($findproductids);die;
        foreach($findproductids as $findproductid) {
           // echo $findproductid;die;
            $value = Product::where('id', $findproductid)->first();
           // print_r($value);die;

                if (empty($check_token)) {
                    $carteachitem = "";    
                } else {
                    $carteachitem = Cart::where(['user_id'=>$check_token->user_id, "product_id"=>$value->id])->first();
                }

                if (empty($carteachitem)) {
                    $quantity = $findproductqty[$q];
                    $discountCoupon = 0;
                    $couponCode = "";
                } else {
                    $quantity = $carteachitem->quantity;
                    $discountCoupon = $carteachitem->discount;
                    $couponCode = $carteachitem->coupon_code??"";
                }

                $get_price_sp = $value->sp; 
                $get_price_mrp = $value->mrp;
                
                $total_price = $get_price_sp * $quantity;
                $totalAmount = $totalAmount + $total_price;
                $discounted_amount =  $discountCoupon;
               // $finalAmount = $finalAmount + $totalAmount - $discountCoupon;
               $finalAmount =  $totalAmount - $discountCoupon;
                if (empty($check_token)) {
                    
                    $num_of_addtocart = count($findproductids);

                } else {
                    $num_of_addtocart = Cart::where(['user_id' => $check_token->user_id])->count();
                }
                
                $q++;
        }

        $cart_amount = ["sub_total"=>$totalAmount, "tax"=>0, "discounted_amount"=>$discounted_amount, "total"=>$finalAmount,'total_itmes'=>$q];
        return $cart_amount;
    }

    public function addtowish(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'id' => 'required', 'type'=>"required"]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }
        
        if($input['type'] == "product") {
        
            $finddetails = Wishlist::where(['product_id' => $input['id'], "user_id" => $check_token->user_id])->first();
            if (!empty($finddetails)) {
                Wishlist::where(['product_id'=>$input['id'], "user_id"=>$check_token->user_id])->delete();
                return $this->sendResponse(["status" => "0"], trans('message.product_removed'), $request->path());
            
            } else {
                Cart::where(['product_id'=>$input['id'], "user_id"=>$check_token->user_id])->delete();
                Wishlist::create(['product_id'=>$input['id'], "user_id"=>$check_token->user_id]);
                return $this->sendResponse(["status" => "1"], trans('message.product_added_wishlist'), $request->path());
            }
        
        } else {

            $finddetails = WishlistStore::where(['store_id' => $input['id'], "user_id" => $check_token->user_id])->first();
            if (!empty($finddetails)) {
                WishlistStore::where(['store_id'=>$input['id'], "user_id"=>$check_token->user_id])->delete();
                return $this->sendResponse(["status" => "0"],  trans('message.store_removed'), $request->path());
            
            } else {
                WishlistStore::create(['store_id'=>$input['id'], "user_id"=>$check_token->user_id]);
                return $this->sendResponse(["status" => "1"], trans('message.store_added'), $request->path());
            }

        }
    }

    public function wishlistproduct(Request $request) {
        $input = $request->all();
        $lang = app()->getLocale();
        $validator = Validator::make($input, ['token' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }
        $name = ($lang=='ar')?$lang.'_name as name':'name'; 
        $type = ($lang=='ar')?$lang.'_type as type':'type'; 
        $description = ($lang=='ar')?$lang.'_description as description':'description'; 
        $category_id = ($lang=='ar')?$lang.'_category_id as category_id':'category_id'; 

        $findproductids = Wishlist::where('user_id', $check_token->user_id)->pluck('product_id');
        
        $productdetails = Product::select("id","$name","$description","mrp","quantity","$category_id","images","sp","discount","user_id as seller_id")->WhereIn('id', $findproductids)->where(["deleteStatus"=>1,"status"=>1])->get();
        foreach ($productdetails as $value) {
            
            if (!empty($value->images)) {
                $explode = explode(',',$value->images);
                $value->images = asset('public'.$explode[0]);
            } else {
                $value->images = asset("public/img/honey_app.png");
            }

            $getCats = Category::where("id", $value->category_id)->first();
            if($getCats) {
                  if($lang == "ar") {
                    $value->category_id = $getCats->ar_name;
                  } else {
                    $value->category_id = $getCats->name;
                  }
            }
        }

        $findsellerids = WishlistStore::where('user_id', $check_token->user_id)->pluck('store_id');
        $sellerdetails = Seller::select("id", "$name", "email", "image", "delivery_time", "$type", "shipping","rating")->WhereIn('id', $findsellerids)->where(["status"=>1])->get();

        foreach($sellerdetails as $sellerdetail) {
            if(strlen($sellerdetail->type) > 0) {
                $sellerdetail->type = explode(',', $sellerdetail->type);
            } else {
                $sellerdetail->type = [];
            }

            // $getSellerCats = Category::where("seller_id", $sellerdetail->id)->get();
            // $sellerCat_arr = array();
            // foreach($getSellerCats as $getSellerCat) {
            //     if($lang=='ar') {
            //       $sellerCat_arr[] = $getSellerCat->ar_name;
            //     } else {
            //       $sellerCat_arr[] = $getSellerCat->name;
            //     }
            // }

            // if(count($sellerCat_arr) > 0) {
            //     $sellerdetail->$type = $sellerCat_arr;
            // } else {
            //     $sellerdetail->$type = [];
            // }

            $getSellerCats = Category::where("seller_id", $sellerdetail->id)->get();
            $sellerCat_arr = array();
            $sellerCat_typearr = array();
            foreach($getSellerCats as $getSellerCat) {
                if($lang=='ar') {
                  $sellerCat_arr[] = $getSellerCat->ar_name;
                  $sellerCat_typearr[] = ["name"=>$getSellerCat->ar_name, "id"=>$getSellerCat->id];
                } else {
                  $sellerCat_arr[] = $getSellerCat->name;
                  $sellerCat_typearr[] = ["name"=>$getSellerCat->name, "id"=>$getSellerCat->id];
                }
            }

            if(count($getSellerCats) > 0) {
                $sellerdetail->type = $sellerCat_typearr;
            } else {
                $sellerdetail->type = [];
            }
        }

        return $this->sendResponse(["product_list"=>$productdetails, "store_list"=>$sellerdetails], trans('message.wishlist_retrieved'), $request->path());
    }

    public function addtocart(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required','product_id' => 'required', 'seller_id' => 'required', 'cart_empty' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $checkcart = Cart::where(["seller_id" => $input['seller_id'], "user_id" => $check_token->user_id])->first();
        if($checkcart) {

                $finddetails = Cart::where(['product_id' => $input['product_id'], "user_id" => $check_token->user_id])->first();
                if($finddetails) {
                    return $this->sendError($request->path(), trans('message.item_already_added'));
                } else {

                    // $haveCoupon = "false";
                    // $checkCoupons = Cart::where(['user_id' => $input['user_id']])->get();
                    // foreach($checkCoupons as $checkCoupon) {
                    //     if(strlen($checkCoupon->coupon_code) > 0) {
                    //         $haveCoupon = "true";
                    //     }
                    // }
                    // if($haveCoupon == "true") {
                    //     foreach($checkCoupons as $checkCoupon) {
                    //         $update = Cart::where(['id' => $checkCoupon->id])->first();
                    //         $update->discount = 0.00;
                    //         $update->coupon_code = "";
                    //         $update->save();
                    //     }   
                    // }

                    $wish = Wishlist::where(['product_id'=>$input['product_id'], "user_id"=>$check_token->user_id])->first();
                    if($wish) {
                        $wish->delete();
                    }
                    Cart::create(['product_id'=>$input['product_id'], 'seller_id'=>$input['seller_id'], "user_id"=>$check_token->user_id, "quantity"=>1]);
                    $num_of_addtocart = Cart::where(['user_id'=>$check_token->user_id])->count();

                    return $this->sendResponse(["status" => "1", "total_items_in_cart" => $num_of_addtocart], trans('message.product_added'), $request->path());
                }

        } else {

            if($input['cart_empty'] == "no") {
                $checkbyuser = Cart::where(["user_id" => $check_token->user_id])->first();
                if($checkbyuser) {
                    $seller = Seller::where('id', $checkbyuser->seller_id)->first();
                    return $this->sendError($request->path(), trans('message.item_already_added_store')." ".$seller->name);
                } else {

                    $finddetails = Cart::where(['product_id' => $input['product_id'], "user_id" => $check_token->user_id])->first();
                    if($finddetails) {
                        return $this->sendError($request->path(), trans('message.item_already_added'));
                    } else {

                        // $haveCoupon = "false";
                        // $checkCoupons = Cart::where(['user_id' => $input['user_id']])->get();
                        // foreach($checkCoupons as $checkCoupon) {
                        //     if(strlen($checkCoupon->coupon_code) > 0) {
                        //         $haveCoupon = "true";
                        //     }
                        // }
                        // if($haveCoupon == "true") {
                        //     foreach($checkCoupons as $checkCoupon) {
                        //         $update = Cart::where(['id' => $checkCoupon->id])->first();
                        //         $update->discount = 0.00;
                        //         $update->coupon_code = "";
                        //         $update->save();
                        //     }   
                        // }

                        $wish = Wishlist::where(['product_id'=>$input['product_id'], "user_id"=>$check_token->user_id])->first();
                        if($wish) {
                            $wish->delete();
                        }
                        Cart::create(['product_id'=>$input['product_id'], 'seller_id'=>$input['seller_id'], "user_id"=>$check_token->user_id, "quantity"=>1]);
                        $num_of_addtocart = Cart::where(['user_id'=>$check_token->user_id])->count();

                        return $this->sendResponse(["status" => "1", "total_items_in_cart" => $num_of_addtocart], trans('message.product_added'), $request->path());
                    }                

                }
            } else {

                cart::where(["user_id" => $check_token->user_id])->delete();
                $wish = Wishlist::where(['product_id'=>$input['product_id'], "user_id"=>$check_token->user_id])->first();
                if($wish) {
                    $wish->delete();
                }
                Cart::create(['product_id'=>$input['product_id'], 'seller_id'=>$input['seller_id'], "user_id"=>$check_token->user_id, "quantity"=>1]);
                $num_of_addtocart = Cart::where(['user_id'=>$check_token->user_id])->count();

                return $this->sendResponse(["status" => "1", "total_items_in_cart" => $num_of_addtocart], trans('message.product_added'), $request->path());
            }
        }

    }

    public function removetocart(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'product_id' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $cart = Cart::where(['product_id'=>$input['product_id'], "user_id"=>$check_token->user_id])->first();
        if($cart) {
            $cart->delete();

            return $this->sendResponse(['status' => "success"], trans('message.item_removed_cart'), $request->path());
        } else {
            return $this->sendError($request->path(), trans('message.item_not_found'));
        }
    }

    public function updatecart(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'product_id' => 'required', 'quantity' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $finddetails = Cart::where(['product_id' => $input['product_id'], "user_id" => $check_token->user_id])->first();
        if ($finddetails) {
            
            if($input['quantity'] <= 0) {
                $finddetails->delete();
                return $this->sendResponse(['status' => "success"], trans('message.cart_items_updated'), $request->path());

            } else {

                $product = Product::where("id", $input['product_id'])->first();
                if($product) {

                    if($product->quantity < $input['quantity']) {
                        return $this->sendError($request->path(), trans('message.out_of_stock'));    
                    }
                    $finddetails->quantity = $input['quantity'];
                    $finddetails->save();   
                    return $this->sendResponse(['status' => "success"], trans('message.cart_items_updated'), $request->path());
                } else {
                    return $this->sendError($request->path(), trans('message.item_not_found'));
                }
            }
       
        } else {
            return $this->sendError($request->path(), trans('message.item_not_found'));
        }
    }

    public function cartlist(Request $request) {
        $input = $request->all();
        $lang = app()->getLocale();

        if(strlen($input['token']) > 0) {
            $check_token = Token::where(['token' => $input['token']])->first();
            if (empty($check_token)) {
                return $this->sendError($request->path(), trans('message.token_expire'));
            }
        } else {
            $check_token = "";
        }

        if (empty($check_token)) {

            if(strlen($input['cart_id']) <= 0) {
                return $this->sendResponse(['status' => 'error'], trans('message.no_item_in_cart'), $request->path());
            }

            $findproductids = explode(",", $input['cart_id']);
            $findproductqty = explode(",", $input['quantity']);

        } else {
            $findproductids = Cart::where(['user_id' => $check_token->user_id])->orderBy('id', 'asc')->pluck('product_id');
            if($findproductids) {
            } else {
                return $this->sendResponse(['status' => 'error'], trans('message.no_item_in_cart'), $request->path());
            }
        }

        $return_array = array();
        $cart_amount = array();
        $totalAmount = 0;
        $finalAmount = 0;
        $discounted_amount = 0;
        $q = 0;

        foreach($findproductids as $findproductid) {

            $value = Product::where('id', $findproductid)->first();

                $solderdetails = Seller::find($value->user_id);
                if (!empty($solderdetails)) {
                    $name = ($lang=='ar')?$lang.'_name':'name';
                    $solder_name = $solderdetails->$name;
                    $latitude = $solderdetails->latitude;
                    $longitude = $solderdetails->longitude;
                } else {
                    $solder_name = "";
                    $latitude = "";
                    $longitude = "";
                }

                if (!empty($value->images)) {
                    $explodeImg = explode(',', $value->images);
                    $value->images = asset('public'.$explodeImg[0]);
                } else {
                   // $value->images = asset("public/img/imagenotfound.jpg");
                    $value->images = asset("public/img/honey_app.png");
                }

                if (empty($check_token)) {
                    $carteachitem = "";    
                } else {
                    $carteachitem = Cart::where(['user_id'=>$check_token->user_id, "product_id"=>$value->id])->first();
                }

                if (empty($carteachitem)) {
                    $quantity = $findproductqty[$q];
                    $discountCoupon = 0;
                    $couponCode = "";
                } else {
                    $quantity = $carteachitem->quantity;
                    $discountCoupon = $carteachitem->discount;
                    $couponCode = $carteachitem->coupon_code??"";
                }

                $get_price_sp = $value->sp; 
                $get_price_mrp = $value->mrp;
                
                $total_price = $get_price_sp * $quantity;
                $totalAmount = $totalAmount + $total_price;
                $discounted_amount =  $discountCoupon;
               // $finalAmount = $finalAmount + $totalAmount - $discountCoupon;
               $finalAmount =  $totalAmount - $discountCoupon;
                if (empty($check_token)) {
                    
                    $num_of_addtocart = count($findproductids);

                } else {
                    $num_of_addtocart = Cart::where(['user_id' => $check_token->user_id])->count();
                }
                 $name = ($lang=='ar')?'ar_name':'name';
                 $sp = ($lang=='ar')?'ar_sp':'sp';
                 $category_id = ($lang=='ar')?'ar_category_id':'category_id';

                 //var_dump($value->$category_id);die;

                 $getCats = Category::where("id", $value->$category_id)->first();

                $return_array[] = array(
                                        "seller_id" => $value->user_id,
                                        "product_id" => $value->id,
                                        "category_id" => $getCats->$name,
                                        "sold_by" => $solder_name,
                                        "image" => $value->images, 
                                        "name" => $value->$name, 
                                        'total_items' => $value->quantity,
                                        "mrp" => $get_price_mrp,
                                        "sp" => $value->$sp,
                                        "percentage" => $value->discount,
                                        "quantity" => $quantity, 
                                        "total_price" => round($total_price), 
                                        "discount_coupon" => round($discountCoupon),
                                        "coupon_code" => $couponCode,
                                        'latitude' => $latitude,
                                        'longitude' => $longitude,
                                );
                $q++;
        }

        $cart_amount = ["sub_total"=>round($totalAmount), "tax"=>0, "discounted_amount"=>round($discounted_amount), "total"=>round($finalAmount)];

        return $this->sendResponse(["cart_list"=>$return_array, "Cart_amount"=>$cart_amount, "total_items_in_cart"=>$q], trans('message.cart_retrived'), $request->path());
    }

    public function coupons(Request $request) {
        $details = Couponcode::get();

        $lang = app()->getLocale();

        $return_array = array();

        foreach ($details as $met) {
            $flashdatenew = date('Y-m-d H:i:s', strtotime($met->startdate));
            $flashStr = strtotime($flashdatenew);
            $currentDate = date('Y-m-d H:i:s');
            $currentStr = strtotime($currentDate);

            if($currentStr > $flashStr) {
                $flashdateend = date('Y-m-d H:i:s', strtotime($met->expdate));
                $flashStrEnd = strtotime($flashdateend);
                $currentDateEnd = date('Y-m-d H:i:s');
                $currentStrEnd = strtotime($currentDateEnd);

                if($currentStrEnd < $flashStrEnd) {

                    if($lang == "en") {
                      
                      $return_array[] = array("coupon_code" => $met->name, "discount" => $met->discount,"discount_type"=>$met->type, "expiry_date" => date("M d Y", strtotime($met->expdate)), "coupon_id" => $met->id, "min_price" => $met->min_price);
                    } else {

                      $return_array[] = array("coupon_code" => $met->name_ar, "discount" => $met->discount_ar, "discount_type"=>$met->type, "expiry_date" => date("M d Y", strtotime($met->expdate)), "coupon_id" => $met->id, "min_price" => $met->min_price_ar);
                    }
                }
            }
        }
        return $this->sendResponse1($return_array, trans('message.coupen_retrived'), $request->path());
    }

    public function apply_coupon(Request $request) {
        $input = $request->all();

        $validator = Validator::make($input, ['token' => 'required', 'code' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $detailsofcupon = Couponcode::where('id', $input['code'])->first();
        $data = array();

        if (empty($detailsofcupon)) {
            return $this->sendError($request->path(), trans('message.coupen_not_found'));
        } else {

            $check_code_reused = Order::where(['user_id' => $check_token->user_id, 'coupon_code' => $detailsofcupon->name])->first();
            if ($check_code_reused) {
                return $this->sendError($request->path(), trans('message.coupen_used'));
            }

            $totalAmount = 0;
            $getCarts = Cart::where(["user_id"=>$check_token->user_id])->get();

            foreach ($getCarts as $getCart) {
                $pro = Product::select('sp', 'mrp', 'category_id')->where(["id"=>$getCart->product_id])->first();
                $get_price_sp = $pro->sp; 
                $get_price_mrp = $pro->mrp;
                
                $cartAmount = $get_price_sp * $getCart->quantity;
                $totalAmount = $totalAmount + $cartAmount;
            }
            // echo $totalAmount;die;
            if($totalAmount > 0) {
                if ($totalAmount < $detailsofcupon->min_price) {
                    return $this->sendError($request->path(), trans('message.unlock_coupen'));
                    die;
                }
            } else {
                return $this->sendError($request->path(), trans('message.invalid_coupen'));
                die;
            }

            $totalAmount = 0;
            $totalSave = 0;
            $total_price = 0;
            $final_discounted_price = 0;

            $getCarts = Cart::where(["user_id"=>$check_token->user_id])->get();

            if(count($getCarts) > 0) {

                foreach ($getCarts as $getCart) {
                
                    $total_discounted_price = 0;
                    $pro = Product::select('sp','mrp','category_id')->where(["id"=>$getCart->product_id])->first();
            
                    $get_price_sp = $pro->sp; 
                    $get_price_mrp = $pro->mrp;

                    $cartAmount = $get_price_sp * $getCart->quantity;
                    $totalAmount = $totalAmount + $cartAmount;

                    $totalMrp = $get_price_mrp * $getCart->quantity;
                    $cal = $totalMrp - $cartAmount;
                    $totalsavediff = $totalSave + $cal;

                    if($detailsofcupon->type == "percent") {
                        $dicounted_price = ($totalAmount * $detailsofcupon->discount) / 100;
                        $total_price = $total_price + round($totalAmount - $dicounted_price);
                        $totalSave = $totalSave +  $totalsavediff + round($dicounted_price);

                        $final_discounted_price = $final_discounted_price + round($dicounted_price);
                    } else {
                        $dicounted_price = $detailsofcupon->discount;
                        $total_price = $total_price + round($totalAmount - $dicounted_price);
                        $totalSave = $totalSave + $totalsavediff + round($dicounted_price);
                        
                        $final_discounted_price = $final_discounted_price + round($dicounted_price);
                    }

                    $updateCart = Cart::where(["id"=>$getCart->id])->first();
                    $updateCart->discount = $dicounted_price;
                    $updateCart->coupon_code = $detailsofcupon->name;
                    $updateCart->save();
                }

                $data = ["total_price"=>$totalAmount, "discounted_amount"=>$final_discounted_price, "final_price"=>$total_price, "total_save"=>$totalSave];
                return $this->sendResponse($data,trans('message.coupen_applied'), $request->path());

            } else {
                return $this->sendError($request->path(), trans('message.coupen_not_apply'));
            }
        }
    }

    public function cancel_coupon(Request $request) {
        $input = $request->all();

        $validator = Validator::make($input, ['token' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $getCarts = Cart::where(["user_id"=>$check_token->user_id])->get();
        if($getCarts) {
            foreach ($getCarts as $getCart) {
                $updateCart = Cart::where(["id"=>$getCart->id])->first();
                $updateCart->discount = 0;
                $updateCart->coupon_code = "";
                $updateCart->save();
            }
        }

        return $this->sendResponse([], trans('message.coupen_cancel'), $request->path());
    }

    public function placeorder(Request $request) {
        $input = $request->all();
        $x = $request->toArray();
        $lang = app()->getLocale();
        $orderData = array();
        $orderProducts = array();
      // return $this->sendResponse1($x, 'test', $request->path());die;
       // return $this->sendResponse(["data" =>$x ],'test data', $request->path());
       // return $this->sendResponse(["status" => "success",'check_toekn'=>123456,'token'=>$input['token'],'new_token'=>$request->token], trans('message.order_placed'), $request->path());
        $validator = Validator::make($input, ['token' => 'required', 'payment_type' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['token' => trim($input['token'])])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

            $findallcartlist = Cart::where(["user_id" => $check_token->user_id])->get();
            $userGet = User::where(['id' => $check_token->user_id])->first();
            $getAddress = Address::where(['id' => $input['address_id']])->first();
            
            $orderNum = 'HONEY-'. $this->random_strings(5);
            $new_final_price  = 0;
            $total_admin_comission = 0;
            $total_seller_comission = 0;
            $sub_tot_price = 0;
            foreach ($findallcartlist as $value) {
                
                $totalPrice = 0;
                $finalPrice = 0;
                $discountAmt = 0;

                $quantity = $value->quantity;
                $findp = Product::find($value->product_id);

                if($findp) {

                    $get_price_sp = $findp->sp; 
                    $get_price_mrp = $findp->mrp;

                    $sellerGet = Seller::where(['id' => $findp->user_id])->first();

                    $totalPrice = $value->quantity * $get_price_sp;
                    $sub_tot_price = $sub_tot_price+$totalPrice;
                    //$adminCommmisionPrice = $totalPrice / 100 * $sellerGet->commission;
                    $total_admin_comission = $totalPrice / 100 * 10;
                   // $total_admin_comission =  $total_admin_comission+$adminCommmisionPrice; //get total admin comission

                    $getShippingPrice = 0;
                    $priceTCS = $totalPrice / 100 * 1;
                    $calculateTax = $total_admin_comission + $getShippingPrice;
                  //  $calculateTax = $calculateTax / 100 * 18;
                    $calculateTax = $calculateTax / 100 * 1;
                   // $sellerEarningAmount = $totalPrice - $adminCommmisionPrice - $getShippingPrice - $priceTCS - $calculateTax;
                    

                    $finalPrice = $totalPrice + $getShippingPrice;
                    $new_final_price = $new_final_price+$finalPrice;
                    $new_final_price1 = $new_final_price-$value->discount; //final


                    $get_final_amt = $new_final_price1+$request->shipping_charges;
                    $total_admin_comission = $get_final_amt / 100 * 10;
                    $sellerEarningAmount = $get_final_amt - $total_admin_comission;
                    $total_seller_comission = $sellerEarningAmount;

                    if(strlen($value->coupon_code) > 0) {
                        $getDiscount = $value->discount;
                        $discountAmt = $getDiscount;
                        $finalPrice = $finalPrice - $getDiscount;

                    } else {
                        $discountAmt = 0;
                    }

                    if(strlen($input['transcation_id']) > 1) {
                        $payStatus = 'completed';
                    } else {
                        $payStatus = 'pending';
                    }

                    $shippingDate = $findp->ships_in;
                    $shipDate = "+$shippingDate days";

                    $get_currentdate_time = strtotime(date('H:i:sa'));
                    $allocateTime = "11:59:59am";
                    $compare_time = strtotime($allocateTime);
                    if($get_currentdate_time <= $compare_time){
                        $slatime = 1;
                    } else {
                        $slatime = 0;
                    }
                    $finalAmt  = $new_final_price1+$request->shipping_charges; 
                    $orderData = array("order_number"=> $orderNum,
                                        "seller_id" => $findp->user_id,
                                        "user_id" => $check_token->user_id,
                                        "location" => $getAddress->address,
                                        "state" => $getAddress->state,
                                        "street" => $getAddress->street,
                                        "city" => $getAddress->city,
                                        "country" => $getAddress->country,
                                        "phone" => $getAddress->phone,
                                        "name" => $getAddress->name,
                                        "address_type" => $getAddress->type,
                                       // "dispatch_at" => date('Y-m-d', strtotime($shipDate)),
                                        "payment_type" => strtolower($input['payment_type']),
                                        "transcation_id" => $input['transcation_id'],
                                        "coupon_code" => $input['coupon_code'],
                                        "sla_time"=>$slatime,
                                        "payment_status" => $payStatus,
                                        "shipping_charges" =>  $request->shipping_charges,
                                        'discount' => $discountAmt,
                                        'final_amt' => $new_final_price1+$request->shipping_charges,
                                        'total_seller_earning' => $total_seller_comission,
                                        'total_admin_comission'=> $total_admin_comission,
                                        'sub_tot_amt' => $sub_tot_price,
                                    );

                    $orderProducts[] = array(
                                        "product_id" => $findp->id,
                                        "quantity" => $value->quantity,
                                        "price" => $get_price_mrp,
                                        "sale_price" => $get_price_sp,
                                        "discount_price" => $discountAmt,
                                        "total_price" => $totalPrice,
                                        "seller_shipping"=> $getShippingPrice,
                                        "valuable_tax" => 0,
                                        "gst_tax" => $calculateTax,
                                        "tcs_tax" => $priceTCS,
                                        "final_price" => $finalPrice,
                                        "seller_amount" => $sellerEarningAmount,
                                        "admin_commision" => $total_admin_comission,
                                        "total_shipping"=> 0
                                    );
                 //seller wallet   
                $check_seller_wallet = SellerWallet::where('seller_id',$findp->user_id)->first();
                $old_payable_amt = 0;
                $old_receivable_amt = 0;
                $old_actual_amt = 0;
                $old_admin_comisssion = 0;
                if($check_seller_wallet){
                $old_payable_amt = $check_seller_wallet->payable_amt;
                $old_receivable_amt = $check_seller_wallet->receivable_amt; 
                $old_actual_amt = $check_seller_wallet->actual_amt; 
                $old_admin_comisssion = $check_seller_wallet->admin_comission; 
                }
                
                $total_payable_amt = $old_payable_amt+$total_seller_comission;
                $total_receivable_amt = $old_receivable_amt+$total_admin_comission;
                $total_admin_com = $old_admin_comisssion+$total_admin_comission;
                $total_actual_amt = $old_actual_amt+$finalAmt;
                if($input['payment_type']=='online'){
                $wallet_data = ['seller_id'=>$findp->user_id,'actual_amt'=>$total_actual_amt,'admin_comission'=>$total_admin_com,'payable_amt'=>$total_payable_amt,'status'=>1];
               }else{
                $wallet_data = ['seller_id'=>$findp->user_id,'actual_amt'=>$total_actual_amt,'admin_comission'=>$total_admin_com,'receivable_amt'=>$total_receivable_amt,'status'=>1];
               }  

                    Cart::where(['id'=>$value->id])->delete();

                    $pQty = $findp->quantity;
                    $updatepquantity = (int)$pQty - (int)$value->quantity;
                    DB::table('products')->where(['id' => $value->product_id])->update(['quantity' => $updatepquantity]);
                }
            } //end loop

            if(!$check_seller_wallet){
             $insert_wallet = SellerWallet::create($wallet_data);
            }else{ //update
             $update_seller = SellerWallet::where('seller_id',$findp->user_id)->update($wallet_data);
            }
            $oid = Order::create($orderData);
            foreach($orderProducts as $orderProduct) {
                $orderProduct['order_id'] = $oid->id;
                OrderDetail::create($orderProduct);
            }
            $msg = "";
            $notifyData = [
                'user_id'=>$check_token->user_id,
                'order_id'=>$oid->id,
                'order_number'=>$orderNum,
                'type'=>'Order',
                'title'=>'Order - '.$orderNum,
                'content'=>'Your new order has been placed',
                'title_ar'=>'ترتيب - '.$orderNum,
                'content_ar'=>'تم تقديم طلبك الجديد',
            ];
            Notification::create($notifyData);
            
            $userDevice = Token::select('deviceType','deviceToken')->where(['user_id'=>$check_token->user_id])->first();
            if($userDevice) {
                $badge = Notification::where(["user_id"=>$check_token->user_id,'status'=>0])->count();
                $this->pushNotificationNew($notifyData, $userDevice, $badge, $lang);
            }

            return $this->sendResponse(["status" => "success"], trans('message.order_placed'), $request->path());
    }
    public function testnotification(){
         $notifyData = [
                'user_id'=>3,
                'order_id'=>6,
                'order_number'=>88,
                'type'=>'Order',
                'title'=>'New Order - ',
                'content'=>'Your new order has been placed'
            ];
         $userDevice = Token::select('deviceType','deviceToken')->where(['user_id'=>32])->first();
        // echo $userDevice->deviceToken;die;
            if($userDevice) {
                $badge = Notification::where(["user_id"=>32,'status'=>0])->count();
                $x = $this->pushNotification($notifyData, $userDevice, $badge);
                echo $x ;die;
            }
    }

    public function upcomingorders(Request $request) {
        $input = $request->all();
        $lang = app()->getLocale();

        $validator = Validator::make($input, ['token' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $details = Order::join('users', 'users.id', '=', 'orders.user_id')->select("orders.id as id","orders.created_at as created", "orders.*", "users.name as buyer")->where(['orders.user_id' => $check_token->user_id])->whereIn('orders.status', ['0','1','2'])->orderBy('orders.id','DESC')->get(); //[4,5,6,7]

        $return_array = array();
    
        foreach ($details as $met) {
            
            $getSeller = Seller::where(['id' => $met->seller_id])->first();
            $orderCount = 1;
            $final_amount = 0;

            $cartfindall = OrderDetail::where(['order_id' => $met->id])->get();
            if($cartfindall) {

                foreach($cartfindall as $cartfind) {
                
                    $final_amount =  $final_amount + $cartfind->final_price;
                    $orderCount++;
                }
            }

            $getD = strtotime($met->created_at);
            $addedDate = strtotime("+7 day", $getD);
            $currentDate = date('d/m/y');
            $currentConvert = strtotime($currentDate);

            if($currentConvert > $addedDate) {
                $exchangeStatus = 0;
            } else {
                $exchangeStatus = 1;
            }

            if($met->status == 0) {
                $order_status = "Pending";
                $ar_order_status = "قيد الانتظار ";
            } else if($met->status == 1) {
                $order_status = "Processing";
                $ar_order_status  = "معالجة ";
            } else if($met->status == 2) {
                $order_status = "Shipped";
                $ar_order_status = "شحنها ";
            } else if($met->status == 3) {
                $order_status = "Delivered";
                $ar_order_status = "تم التوصيل ";
            } else if($met->status==5){
                $order_status = "Cancelled";
                $ar_order_status = "ألغيت ";
            }
                $name = ($lang=='ar')?$lang.'_name':'name';
                $status = ($lang=='ar')?$ar_order_status:$order_status;
                $final_amt = $met->final_amt;
                $image = ($getSeller)?$getSeller->image:"";
                $return_array[] = array(
                    "id" => $met->id,
                    "image" => asset('public/'.$image),
                    "item_count"=> $orderCount,
                    "order_number" => $met->order_number,
                    "seller_name" => ($getSeller)?$getSeller->$name:"", 
                    "dispatch_at" => (!empty($met->dispatch_at))?date("d/m/Y", strtotime($met->dispatch_at)):'',
                    "order_date" => (!empty($met->created))?date("d/m/Y", strtotime($met->created)):'', 
                    "status" => $status, 
                    "payment_type" => $met->payment_type, 
                    //"amount" => $final_amount+$met->shipping_charges,
                    "amount" => $final_amt,
                );

        }
        return $this->sendResponse1($return_array, trans('message.order_retrived'), $request->path());
    }

    public function pastorders(Request $request) {
        $input = $request->all();
        $lang = app()->getLocale();
        $validator = Validator::make($input, ['token' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $details = Order::join('users', 'users.id', '=', 'orders.user_id')->select("orders.id as id","orders.created_at as created", "orders.*", "users.name as buyer")->where(['orders.user_id' => $check_token->user_id])->whereIn('orders.status', ['3','4','5','6','7'])->orderBy('orders.id','DESC')->get(); //whereIn('orders.status', [4,5,6,7])

        $return_array = array();
    
        foreach ($details as $met) {
            
            $getSeller = Seller::where(['id' => $met->seller_id])->first();
            $orderCount = 1;
            $final_amount = 0;

            $cartfindall = OrderDetail::where(['order_id' => $met->id])->get();
            if($cartfindall) {

                foreach($cartfindall as $cartfind) {
                
                    $final_amount =  $final_amount + $cartfind->final_price;
                    $orderCount++;
                }
            }

            $getD = strtotime($met->created_at);
            $addedDate = strtotime("+7 day", $getD);
            $currentDate = date('d/m/y');
            $currentConvert = strtotime($currentDate);

            if($currentConvert > $addedDate) {
                $exchangeStatus = 0;
            } else {
                $exchangeStatus = 1;
            }

           
            $order_status=  '';
            if($met->status == 3) {
                $order_status = "Delivered";
                $ar_order_status = "تم التوصيل ";
            } else if($met->status == 5) {
                $order_status = "Cancelled";
                 $ar_order_status = "ألغيت  ";
            } else if($met->status == 6) {
                $order_status = "Return";
                 $ar_order_status = "إرجاع ";
            } else if($met->status == 7) {
                $order_status = "Exchange";
                 $ar_order_status = "تبادل ";
            } 
                $name = ($lang=='ar')?$lang.'_name':'name';
                $status = ($lang=='ar')?$ar_order_status:$order_status;
                $final_amt = $met->final_amt;
                $image = ($getSeller)?$getSeller->image:'';
                $return_array[] = array(
                    "id" => $met->id,
                    "image" => asset('public/'.$image),
                    "item_count"=> $orderCount,
                    "order_number" => $met->order_number,
                    "seller_name" => ($getSeller)?$getSeller->$name:'', 
                    "dispatch_at" => date("d/m/Y", strtotime($met->dispatch_at)),
                    "order_date" => date("d/m/Y", strtotime($met->created)), 
                    "status" => $status, 
                    "payment_type" => $met->payment_type, 
                   // "amount" => $final_amount,
                    "amount" => $final_amt,
                );

        }
        return $this->sendResponse1($return_array, trans('message.order_retrived'), $request->path());
    }

    public function orderdetail(Request $request) {
        $input = $request->all();
        $lang = app()->getLocale();
        $validator = Validator::make($input, ['token' => 'required', 'order_id' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.order_retrived'));
        }

        $order = Order::join('users', 'users.id', '=', 'orders.user_id')->select("orders.id as id","orders.created_at as created", "orders.*", "users.name as buyer")->where(['orders.user_id'=>$check_token->user_id, 'orders.id'=>$input['order_id']])->first();

        $return_array = array();
    
        $getSeller = Seller::where(['id' => $order->seller_id])->first();
        $orderCount = 1;
        $total_amount = 0;
        $final_amount = 0;
        $discount_amount = 0;

        $cartfindall = OrderDetail::where(['order_id' => $order->id])->get();
        if($cartfindall) {

            $detailArray = array();
            
            foreach($cartfindall as $cartfind) {
                
                $product = Product::where('id', $cartfind->product_id)->first();
                
                if(!empty($product->images)){
                 $explodeImg = explode(',', $product->images);
                 $product->images = asset('public'.$explodeImg[0]);
               }else{
                 $product->images = asset("public/img/honey_app.png");
               }
                $name = ($lang=='ar')?$lang.'_name':'name';
                $category_id = ($lang=='ar')?$lang.'_category_id':'category_id';


                $getCats = Category::where("id", $product->$category_id)->get();
                if($getCats) {
                  foreach($getCats as $getCat) {
                      if($lang == "ar") {
                          $product->$category_id = $getCat->ar_name;
                      } else {
                          $product->$category_id = $getCat->name;
                      }
                  }
                }

                $total_pricee = $cartfind->total_price;
                $detailArray[] = [
                        "product_name" => $product->$name,
                        "image" => $product->images,
                        "category" => $product->$category_id,
                      //  "price" => $cartfind->final_price,
                        "price" => $total_pricee,
                        'discount' => $cartfind->discount_price,
                        'quantity' => $cartfind->quantity,
                ];

                $total_amount =  $total_amount + $cartfind->total_price;
                $final_amount =  $final_amount + $cartfind->final_price;
               // $discount_amount =  $discount_amount + $cartfind->discount_price;
                  $discount_amount   = $cartfind->discount_price;
                $orderCount++;
            }
        }

        $getD = strtotime($order->created_at);
        $addedDate = strtotime("+7 day", $getD);
        $currentDate = date('d/m/y');
        $currentConvert = strtotime($currentDate);

        if($currentConvert > $addedDate) {
            $exchangeStatus = 0;
        } else {
            $exchangeStatus = 1;
        }

        if($order->status == 0) {
            $order_status = "Pending";
            $ar_order_status = "قيد الانتظار ";
        } else if($order->status == 1) {
            $order_status = "Processing";
            $ar_order_status  = "معالجة ";
        } else if($order->status == 2) {
            $order_status = "Shipped";
            $ar_order_status = "شحنها ";
        } else if($order->status == 3) {
            $order_status = "Delivered";
            $ar_order_status = "تم التوصيل ";
        } else if($order->status == 5) {
            $order_status = "Cancelled";
            $ar_order_status = "ألغيت ";
        } else if($order->status == 6) {
            $order_status = "Return";
             $ar_order_status = "إرجاع ";
        } else if($order->status == 7) {
            $order_status = "Exchange";
            $ar_order_status = "تبادل ";
        }
        $status = ($lang=='ar')?$ar_order_status:$order_status;
        $seller_name = ($lang=='ar')?$lang.'_name':'name';
        $final_amt = $order->final_amt;
        $total_amt = $total_amount;
        $discount_amount = $discount_amount;
        $shipping_charges = $order->shipping_charges;
        $image = ($getSeller)?$getSeller->image:'';
        $return_array = array(
            "id" => $order->id,
            "image" => asset('public/'.$image),
            'seller_address' => ($getSeller)?$getSeller->address:'',
            "item_count"=> $orderCount,
            "order_number" => $order->order_number,
            "seller_name" => ($getSeller)?$getSeller->$seller_name:'', 
            "dispatch_at" => (!empty($order->dispatch_at))?date("d/m/Y", strtotime($order->dispatch_at)):'00/00/0000',//expected_delivery_date
           // "dispatch_at" => ($order->expected_delivery_date)?date("d/m/Y", strtotime($order->expected_delivery_date)):'',
            "order_date" => date("d/m/Y", strtotime($order->created)), 
             "order_time" => date("H:i:s", strtotime($order->created)), 
            "status" => $status, 
            "payment_type" => $order->payment_type, 
            "order_items"=>$detailArray,
            "customer_name"=>$order->name,
            "customer_address"=>$order->location,
            "customer_state"=>$order->state,
            "customer_city"=>$order->city,
            "customer_country"=>$order->country,
            "customer_phone"=>$order->phone,
            "total_amount" => $total_amt,
            "tax" => 0,
            //"final_amount" => $final_amount+$order->shipping_charges,
            'final_amount' => $final_amt,
            'total_discount' => $discount_amount,
            'shipping_charge' => $shipping_charges,
            'tracking_id' => ($order->tracking_id)?$order->tracking_id:"",
            'tracking_url' => ($order->tracking_url)?$order->tracking_url:"",
        ); 

        return $this->sendResponse1($return_array,trans('message.order_detail_retrived'), $request->path());
    }

    public function orderrating(Request $request) {
        $input = $request->all();
        $lang = app()->getLocale();
        $validator = Validator::make($input, ['token' => 'required', 'order_id' => 'required', 'rate_on_product' => 'required', 'rate_on_seller' => 'required', 'comment' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $checkRating = OrderRating::where(["user_id"=>$check_token->user_id, "order_id" => $input['order_id']])->first();
        if($checkRating) {

            return $this->sendError($request->path(), trans('message.already_rated'));

        } else {

            $order = Order::where(["id"=>$input['order_id']])->first();
            $seller = Seller::where(["id"=>$order->seller_id])->first();

            OrderRating::create([
                "user_id"=>$check_token->user_id,
                "order_id" => $input['order_id'], 
                "seller_id" => $order->seller_id, 
                "product_rate" => $input['rate_on_product'], 
                "store_rate" => $input['rate_on_seller'], 
                "comment" => $input['comment'], 
            ]);
            $updateRating = OrderDetail::where('order_id',$input['order_id'])->update(['rating'=>$input['rate_on_product']]);  
            $sellerSumRating = OrderRating::where(["seller_id"=>$order->seller_id])->sum('store_rate');
            $sellerCountRating = OrderRating::where(["seller_id"=>$order->seller_id])->count('store_rate');
            $calculateSellerRating = $sellerSumRating / $sellerCountRating;

            $productSumRating = OrderRating::where(["order_id"=>$input['order_id']])->sum('product_rate');
            $productCountRating = OrderRating::where(["order_id"=>$input['order_id']])->count('product_rate');
            $calculateProductRating = $productSumRating / $productCountRating;
            
            $seller->rating = $calculateSellerRating;
            $seller->save();

            $orderDetails = OrderDetail::where(["order_id"=>$order->id])->get();
            foreach($orderDetails as $orderDetail) {

                $product = Product::where('id', $orderDetail->product_id)->first();
                $product->rating = $calculateProductRating;
                $product->save();
            }

            return $this->sendResponse1([],trans('message.rating_saved'), $request->path());
        }
    }

    public function cancelorder(Request $request) {
        $input = $request->all();
        $lang = app()->getLocale();
        $validator = Validator::make($input, ['token' => 'required', 'order_id' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $details = OrderRequest::where(['order_id' => $input['order_id']])->first();
        if ($details) {
            return $this->sendError($request->path(), trans('message.cancel_request'));
        }

        $orderGet = Order::where(['id'=>$input['order_id']])->first();
        $details = OrderDetail::where(['order_id'=>$input['order_id']])->first();

        OrderRequest::create([
            "order_id"=>$input['order_id'],
            "user_id"=>$check_token->user_id,
            "request_type"=>"cancel",
            "reason"=>$request->reason,
            "description"=>$request->description,
            "cancel_by"=>"user",
        ]);

        Order::where(['id'=>$input['order_id']])->update(['status' => '5']);

        return $this->sendResponse1([], trans('message.request_submitted'), $request->path());
    }

    public function reorder(Request $request) {
        $input = $request->all();
        $lang = app()->getLocale();
        $validator = Validator::make($input, ['token' => 'required', 'order_id' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        $orderGet = Order::where(['id'=>$input['order_id']])->first();
        $details = OrderDetail::where(['order_id'=>$input['order_id']])->get();
        foreach($details as $detail) {
            Cart::create([
                "user_id" => $check_token->user_id,
                "product_id" => $detail->product_id,
                "seller_id" => $orderGet->seller_id,
                "quantity" => $detail->quantity,
            ]);
        }

        return $this->sendResponse1([], trans('message.reorder_sucess'), $request->path());
    }

    public function addreview(Request $request) {
        $input = $request->all();
        $lang = app()->getLocale();
        $validator = Validator::make($input, ['token' => 'required', 'review' => 'required','product_id'=>'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }

        Review::create([
            "user_id" => $check_token->user_id,
            "description" => $request->review,
            "product_id" => $request->product_id,
            'type' => $request->type,
        ]);

        return $this->sendResponse1([], trans('message.review_added'), $request->path());
    }

    public function reviewlist(Request $request,$type,$id) {
        
        $reviews = Review::where(['type'=>$type,'product_id'=>$id])->orderBy('id','DESC')->get();
       // print_r($reviews);
        $reviewArray = array();
        if(count($reviews)>0){  
        foreach($reviews as $review) {

            $user = User::select("id","name","image")->where('id', $review->user_id)->first();
            if($user){
            if(strlen($user) > 0) {
                $user->image = asset('public'.$user->image);
            }else{
                $user->image = asset('public/avtar.png');
            }
            if(strlen($user->name) > 0){
                $user->name = $user->name;
            }else{
                $user->name = "";
            }
           }else{
            $user = (object)[];
           }



            $reviewArray[] = ["id"=>$review->id, "review"=>$review->description, "user"=>$user, "created_at" => date('d-M-Y', strtotime($review->created_at))];
    
        }
       } 

        return $this->sendResponse1($reviewArray, trans('message.review_retrived'), $request->path());
    }

    //count unread notification
         public function notiicationCount(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), trans('message.token_expire'));
        }
        $count = Notification::where(['user_id'=>$check_token->user_id,'status'=>0])->count();
        
        return $this->sendResponse1(['count'=>$count], 'Notification Count', $request->path());
    }

    function random_strings($length_of_string) {
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }
}