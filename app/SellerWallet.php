<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerWallet extends Model
{
    
    protected $fillable = [ 'seller_id','actual_amt','admin_comission','receivable_amt','payable_amt','status'];
}
