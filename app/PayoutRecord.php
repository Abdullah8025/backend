<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayoutRecord extends Model
{
    //

    protected $fillable = [
        'seller_id','date','orders_count','payout','transcation_id','commission','adjustments','gst','tcs'
    ];
}
