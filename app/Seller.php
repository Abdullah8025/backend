<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    //

    protected $fillable = [
         'name', 'email', 'phone', 'password', 'status', 'address','image','cover_image','latitude','longitude','city','pincode','country','commission','delivery_time','shipping','type','rating','ar_name','ar_type','ar_address'
    ];
}
