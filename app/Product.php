<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
    protected $fillable = [ 'user_id','sku','ar_sku', 'name','ar_name', 'sp','ar_sp', 'commision','tax','quantity','ar_quantity', 'mrp','ar_mrp', 'status','category_id','ar_category_id','description','ar_description','weight','ar_weight','ships_in','images','ar_size' ,'size', 'discount','user_type','deleteStatus', 'payment_mode','rating','ar_ships_in'];
    //protected $guarded  = [];
}
