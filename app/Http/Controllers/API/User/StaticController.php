<?php
namespace App\Http\Controllers\API\User;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use DB;

use App\User;
use App\Token;
use App\Content;
use App\Faq;
use App\UserContact;

use Image;
use Mail;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class StaticController extends APIBaseController {

	public function __construct() {

  }

    public function aboutus(Request $request,$lang=null) {
      $detail = Content::where(['type'=>'about'])->first();
      return view('About1',compact('detail','lang'));
    }

    public function tandc(Request $request,$lang=null) {
      $detail = Content::where(['type'=>'tandc'])->first();
      return view('tandc1',compact('detail','lang'));
    }

    public function faq(Request $request) {
      $detail = Faq::get();

      return $this->sendResponse($detail, 'FAQ retrieved successfully', $request->path());
    }

    public function privacypolicy(Request $request,$lang=null) {
      $detail = Content::where(['type'=>'privacypolicy'])->first();
      return view('privacypolicy1',compact('detail','lang'));
    }

    public function supprot(Request $request) {
      //$detail = Content::where(['type'=>'privacypolicy'])->first();
      return view('support',compact('detail'));
    }

    public function marketing(Request $request) {
      //$detail = Content::where(['type'=>'privacypolicy'])->first();
      return view('marketing');
    }

    public function contact(Request $request) {
        
        // $validator = Validator::make($request->all(), ['reason' => 'required', 'query' => 'required']);
        // if ($validator->fails()) {
        //     return $this->sendError($request->path(), $validator->errors()->first());
        // }

        // if ($request->image) {
        //     $filename = substr(md5('cont'.time()), 0, 15) . '.' . $request->file('image')->getClientOriginalExtension();
        //     $path = public_path('users-photos/' . $filename);
        //     Image::make($request->image)->save($path);
        //     $contact_image = '/users-photos/' . $filename;
        // } else {
        //     $contact_image =  "";
        // }

        // UserContact::create([
        //     "reason" => $request->reason,
        //     "query"=>$request->query,
        //     //"image"=>$contact_image,
        // ]);

        return $this->sendResponse([], 'Query posted successfully', $request->path());
    }
}