<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        a:hover{
          text-decoration: none;  
        }
        .contactUs{
              background: url("{{asset('public/backgroundhonney.jpg')}}") no-repeat center;
    padding: 25px 0px;
    background-size: cover;
            position: relative;
        }
        .contactInner h2{
            font-size: 42px;
    color: #fff;
    font-weight: 700;
    position: relative;
    text-align: center;
        }
        .bgColor{
                width: 100%;
    height: 100%;
    background: radial-gradient(6.06% 21.03% at 53.16% 49.59%, rgba(115, 115, 115, 0) 0%, #000000 100%);
    opacity: 0.4;
    top: 0;
    left: 0;
    position: absolute;
        }
        .detailsAll{
            display: block;
            padding: 20px;
            box-shadow: 0 6px 12px rgb(0 0 0 / 18%);
            text-align: center;
            min-height: 200px;
        }
        .detailHead h2{
            color: #000;
            font-size: 24px;
            font-weight: 500;
            text-align: center;
        }
        .detailImg{
            width: 35px;
            margin: 20px auto;
        }
        .detailImg img{
            width: 100%;
        }
        .detailsAll p{
            font-size: 18px;
            color: #000;
    
        }
        .contactDetails{
            padding-top: 150px;
        }
        .detailsAll:hover img{
                transform: scaleY(1.3) scaleX(1.3);
    transition: 0.35s ease-in-out;
        }
        .detailsAll:hover{
                box-shadow: 0px 3px 60px #00000029;
    transition: all 0.35s ease 0s;
        }
        @media(max-width:600px){
   .contactDetails{
            padding: 75px 0px;
        }
        }
    </style>
</head>
<body>

    <section class="contactUs">
        <span class="bgColor"></span>
    <div class="container">
        <div class="contactInner">
            <h2>Contact Us</h2>
        </div>
        </div>
    </section>
    
    <section class="contactDetails">
  <div class="container">
            <div class="row">
                
            <div class="col-sm-4">
                <a href ="#0" class="detailsAll">
                    <div class="detailHead">
                        <h2>Reach Us</h2>
                    </div>
                <div class="detailImg">
                    <img src="{{asset('public/location.svg')}}" class="img-fluid" alt="location">
                    </div>
                    
                    <p>Hail , Saudi arabia</p>
                </a>
            </div>
                  <div class="col-sm-4">
                <a href ="tel:+966553343973" class="detailsAll">
                    <div class="detailHead">
                        <h2>Call To</h2>
                    </div>
                <div class="detailImg">
                    <img src="{{asset('public/phone.svg')}}" class="img-fluid" alt="location">
                    </div>
                    
                    <p>+966553343973</p>
                </a>
            </div>
                  <div class="col-sm-4">
                <a href ="mailto:admin@frzah.com" class="detailsAll">
                    <div class="detailHead">
                        <h2>Email</h2>
                    </div>
                <div class="detailImg">
                    <img src="{{asset('public/message.svg')}}" class="img-fluid" alt="location">
                    </div>
                    
                    <p>admin@frzah.com</p>
                </a>
            </div>
       
      </div>
        </div>
    </section>


    
</body>
</html>
