<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use DB;
use App\Seller;
use App\User;
use App\Token;
use App\Address;
use App\Product;
use App\Order;
use App\OrderDetail;
use App\OrderRequest;
use App\Admin;
use App\Banner;
use App\Wishlist;
use App\Cart;
use App\Cuponcode;
use App\Content;
use App\Category;
use App\Faq;
use App\Notification;
use App\Promotion;
use App\PromotionList;
use App\Attribute;
use App\Support;
use App\ProductImageColor;
use App\Manifest;
use App\SellerPerformance;
use App\SellerMistake;
use App\PayoutRecord;
use App\SellerApprove;
use App\AttributeGroup;
use App\AttributeOption;
use App\Size;
use App\ProductOffer;
use App\OrderRating;
use App\Offer;
use Image;
use Validator;
use Mail;
use Intervention\Image\ImageServiceProvider;
use Softon\Indipay\Facades\Indipay;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use PDF;
error_reporting(0);

class SellerController extends APIBaseController {

    private $baseurlslash;
    private $baseurl;
    
    public function __construct() {
        $this->baseurlslash = "https://mobuloustech.com/yodapi/public/";
        $this->baseurl = "https://mobuloustech.com/yodapi/public";
    }

    public function checkemailseller(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['phone' => 'required', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $userdetails = Seller::where('email', $input['phone'])->first();
        if (!empty($userdetails)) {
            return $this->sendResponse(['status' => '1'], 'Email Already Exist', $request->path());
        } else {
            return $this->sendResponse(['status' => '0'], 'Email Not Exist', $request->path());
        }
    }
    public function checkphoneseller(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['phone' => 'required', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $userdetails = Seller::where('phone', $input['phone'])->first();
        if (!empty($userdetails)) {
            return $this->sendResponse(['status' => '1'], 'Phone Number Already Exist', $request->path());
        } else {
            return $this->sendResponse(['status' => '0'], 'Phone Number Not Exist', $request->path());
        }
    }

    public function signup(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['email' => 'required', 'phone' => 'required','name' => 'required','address' => 'required','city' => 'required','country' => 'required','pincode' => 'required','password' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $obj = Seller::create($input);

        return $this->sendResponse($obj, "Your account created successfully", $request->path());
    }

    public function signupother(Request $request) {
        $input = $request->all();
        $obj = Seller::create($input);

        return $this->sendResponse($obj, "Your account created successfully", $request->path());
    }

    public function sellerlogin(Request $request) {

        $input = $request->all();
        $validator = Validator::make($input, ['username' => 'required', 'password' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $detailsother = Seller::where(['email' => $input['username'], 'password' => $input['password']])->first();
        
        if (!empty($detailsother)) {
            
            if(empty($detailsother->status)) {
               return $this->sendError($request->path(),"Your account is inactive");
            }

            $catdetails = Category::where('seller_id',$detailsother->id)->orderBy('id','DESC')->get();
            $cat_array = "";
            $cat_array_ar = "";
            if($catdetails) {
                $xx=1;
                foreach ($catdetails as $value) {

                    if($xx == 1) {
                        $cat_array = $value->name;
                        $cat_array_ar = $value->ar_name;
                    } else {
                        $cat_array = $cat_array.", ".$value->name;
                        $cat_array_ar = $cat_array_ar.", ".$value->ar_name;
                    }
                    $xx++;
                }
                $detailsother->type = $cat_array;
                $detailsother->ar_type = $cat_array_ar;
            }

            return $this->sendResponse($detailsother, 'Login successfully.', $request->path());
        } else {
            return $this->sendError($request->path(), "Your username or Password is Incorrect");
        }
    }

    function test($str){
       
        $result = [];
        $strArray = explode(',',$str);
        foreach($strArray as $key=>$values){
            $result[] = (object)['name'=>$values];
           
        }
        return  $result;
       

    }
    
    public function sellerlogin1(Request $request) {
        $input = $request->all();
        $detailsother = Seller::where(['email' => $input['username'], 'password' => $input['password']])->first();
        if (!empty($detailsother)) {
            return $this->sendResponse($detailsother, 'You login successfully.', $request->path());
        } else {
            return $this->sendError($request->path(), "Your username or Password is Incorrect");
        }
    }

    public function getsellerlogin(Request $request) {
        $input = $request->all();
        $detailsother = Seller::where(['id' => $input['seller_id']])->first();
        if (!empty($detailsother)) {

            $catdetails = Category::where('seller_id',$detailsother->id)->orderBy('id','DESC')->get();
            $cat_array = "";
            $cat_array_ar = "";
            if($catdetails) {
                $xx=1;
                foreach ($catdetails as $value) {

                    if($xx == 1) {
                        $cat_array = $value->name;
                        $cat_array_ar = $value->ar_name;
                    } else {
                        $cat_array = $cat_array.", ".$value->name;
                        $cat_array_ar = $cat_array_ar.", ".$value->ar_name;
                    }
                    $xx++;
                }
                $detailsother->type = $cat_array;
                $detailsother->ar_type = $cat_array_ar;
            }

            return $this->sendResponse($detailsother, 'Data fetched successfully.', $request->path());
        } else {
            return $this->sendError($request->path(), "Incorrect");
        }
    }

    public function forgotpasswordseller(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['email' => 'required', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        if ($this->checkemail($input['email']) == 0) {
            return $this->sendError($request->path(), "Email Id does not Exist");
        }
        $userdetails = Seller::where('email', $input['email'])->first();
        $otp = $userdetails->password;
        $email = $input['email'];
        $subject = "Request for Forgot Password";
        $postData = "";
        try {
            Mail::send('emails.otpverify', ['otp' => $otp], function ($message) use ($postData, $email) {
                $message->from('asecclass@asecenglish.awsapps.com', 'Hony App');
                $message->to($email, 'Hony App')->subject('Request for Forgot Password');
            });
        }
        catch(Exception $e) {
        }
        return $this->sendResponse(['status' => 'success'], 'Password sent to your email successfully', $request->path());
    }

    public function updatesellerprofile(Request $request, $id) {
        $input = $request->all();
        $details = Seller::find($id);
        $details->name = $input['name'];
        $details->ar_name = $input['ar_name'];
        if (strlen($input['image']) > 0) {
            $details->image = $input['image'];
        }
        if (strlen($input['cover_image']) > 0) {
            $details->cover_image =  $input['cover_image'];
        }

        $findemailonissue = Seller::where('email', $input['email'])->WhereNotIn('id', [$id])->first();
        if (!empty($findemailonissue)) {
            return $this->sendError($request->path(), "Email Id already exist.");
        }
        $findemailonissue1 = Seller::where('phone', $input['phone'])->WhereNotIn('id', [$id])->first();
        if (!empty($findemailonissue1)) {
            return $this->sendError($request->path(), "Phone Number is already exist.");
        }
        $details->phone = $input['phone'];
        $details->email = $input['email'];
        $details->address = $input['address'];
        $details->ar_address = $input['ar_address'];
        $details->latitude = $input['latitude'];
        $details->longitude = $input['longitude'];
        $details->city = $input['city'];
        // $details->shipping = $input['shipping'];
        $details->delivery_time = $input['delivery_time'];
        $details->country = $input['country'];
       // $details->type = implode(',',$input['type']);
       // $details->ar_type = implode(',',$input['ar_type']);
        $details->save();

        return $this->sendResponse1($details, 'Profile updated successfully', $request->path());
    }


    public function changepasswordforseller(Request $request, $id) {
        $input = $request->all();
        $post = Seller::find($id);
        if ($post->password != $input['oldpassword']) {
            return $this->sendError($request->path(), "You enter incorrect old password");
        }
        $post->password = $input['password'];
        $post->save();
        return $this->sendResponse(['status' => 'success'], 'Password changed successfully.', $request->path());
    }

    public function dashoardforseller(Request $request, $id) {
        $result = array();

        $orderids = array();
        // $getOrderids = Order::select('id')->where(["seller_id"=>$id])->get();
        // foreach($getOrderids as $getOrderid) {
        //     $orderids[] = $getOrderid->order_id;
        // }

        // $orderid1s = array();
        // $getOrderid1s = Order::select('id')->where(["seller_id"=>$id, "status"=>"delivered"])->get();
        // foreach($getOrderid1s as $getOrderid1) {
        //     $orderid1s[] = $getOrderid1->order_id;
        // }

        if(isset($request->dateframe)) {
            $dateFrame = $request->dateframe;

            if($dateFrame == 6) {

                $totalOrder = Order::where('seller_id',$id)->where("created_at",">", Carbon::now()->subMonths(6))->count();
                $revenue  = Order::where(['seller_id'=>$id,'status'=>'3'])->where("created_at",">", Carbon::now()->subMonths(6))->sum('final_amt');
                $livePro = Product::where(["status"=>1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(6))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(6))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(6))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(6))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 12) {

                $totalOrder = Order::where('seller_id',$id)->where("created_at",">", Carbon::now()->subMonths(12))->count();
                $revenue  = Order::where(['seller_id'=>$id,'status'=>'3'])->where("created_at",">", Carbon::now()->subMonths(12))->sum('final_amt');
                $livePro = Product::where(["status"=>1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(12))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(12))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(12))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(12))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 18) {

                $totalOrder = Order::where('seller_id',$id)->where("created_at",">", Carbon::now()->subMonths(18))->count();
                $revenue  = Order::where(['seller_id'=>$id,'status'=>'3'])->where("created_at",">", Carbon::now()->subMonths(18))->sum('final_amt');
                $livePro = Product::where(["status"=>1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(18))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(18))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(18))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(18))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 24) {

                $totalOrder = Order::where('seller_id',$id)->where("created_at",">", Carbon::now()->subMonths(24))->count();
                $revenue  = Order::where(['seller_id'=>$id,'status'=>'3'])->where("created_at",">", Carbon::now()->subMonths(24))->sum('final_amt');
                $livePro = Product::where(["status"=>1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(24))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(24))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(24))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(24))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 30) {

                $totalOrder = Order::where('seller_id',$id)->where("created_at",">", Carbon::now()->subMonths(30))->count();
                $revenue  = Order::where(['seller_id'=>$id,'status'=>'3'])->where("created_at",">", Carbon::now()->subMonths(30))->sum('final_amt');
                $livePro = Product::where(["status"=>1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(30))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(30))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(30))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(30))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }
            }

        } else {

            $startd = date('d-m-Y H:i:s', strtotime($request->startdate));
            $endd = date('d-m-Y H:i:s', strtotime($request->enddate));

            $totalOrder = Order::where('seller_id',$id)->where("created_at",">", $startd)->where("created_at","<", $endd)->count();
            $revenue  = Order::where(['seller_id'=>$id,'status'=>'3'])->where("created_at",">", $startd)->where("created_at","<", $endd)->sum('final_amt');
            $livePro = Product::where(["status"=>1])->where(["user_id"=>$id])->where("created_at",">", $startd)->where("created_at","<", $endd)->count();
            $nonlivePro = Product::whereNotIn("status", [1])->where(["user_id"=>$id])->where("created_at",">", $startd)->where("created_at","<", $endd)->count();

            $stateRecords = array();
            $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", $startd)->where("created_at","<", $endd)->get();
            $getOrderCount = Order::where("created_at",">", $startd)->where("created_at","<", $endd)->whereIn('id', $orderids)->count();

            foreach($getStates as $getState) {
                $stateCount = Order::where(["state"=>$getState->state])->count();
                $orderper = round(($stateCount/$getOrderCount) * 100);
                $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
            }
        }

        $orderRecords = OrderDetail::select(
            DB::raw('sum(final_price) as sums'), 
            DB::raw("DATE_FORMAT(created_at,'%m') as months"))->whereYear('created_at', date('Y'))->whereIn('order_id', $orderids)->groupBy('months')->get();
        if(count($orderRecords) > 0) {
            for($i=0;$i<12;$i++) {
                $c = $i+1;
                if(isset($orderRecords[$i])) {
                    $graphArr[] = ["g$c" => (int)$orderRecords[$i]['sums']];
                } else {
                    $graphArr[] = ["g$c" => 0];
                }
            }
        } else {
            for($i=0;$i<12;$i++) {
                $c = $i+1;
                $graphArr[] = ["g$c" => 0];
            }
        }


        $offerCurrentDate = date('d-m-Y H:i:s');
        $strCurrentDate = strtotime($offerCurrentDate);
        $offerData = array();
        $objs = Offer::all();
        
        foreach($objs as $obj) {

            $startdate = date('d-m-Y H:i:s', strtotime($obj->startdate));
            $strStartdate = strtotime($startdate);
            $enddate = date('d-m-Y H:i:s', strtotime($obj->enddate));
            $strEnddate = strtotime($enddate);

            if($strCurrentDate > $strStartdate && $strCurrentDate < $strEnddate) {

                $offerData[] = ["id"=>$obj->id, "desc"=>$obj->description];
            }
        }

        $result = [
            "orders" => $totalOrder,
            "revenue" => $revenue,
            "live_product" => $livePro,
            "nonlive_product" => $nonlivePro,
            "state_records"=>$stateRecords,
            "graph"=>$graphArr,
            "offerData"=>$offerData
        ];
        return $this->sendResponse($result, "Dashboard Response", $request->path());
    }


    public function productlistforseller(Request $request, $userid) {

        $details = Product::select("sku", "name", "category_id", "sp", "mrp", "size", "quantity", "status", "id")->where(['user_id' => $userid, 'deleteStatus'=>1])->orderBy('id','DESC')->get();
        $return_array = array();
        foreach ($details as $met) {

            $getSeller = Seller::where(["id"=>$userid])->first();
            $comm = $getSeller->commission;

            $getProPriceid = 0;
            if(strlen($met->size) > 0) {
                $sizeValue = $met->size;
            } else {
                $sizeValue = "-";
            }

            $getCat = Category::where(["id"=>$met->category_id])->first();
            if($getCat) {
                $getCatname = $getCat->name;
            } else {
                $getCatname = "";
            }

            $return_array[] = [$met->sku, $met->name, $getCatname, $sizeValue, $met->sp, $met->mrp, $met->quantity, [$met->id,$getProPriceid, $met->quantity], [$met->id,$getProPriceid,$met->sp,$comm, $met->quantity], [$met->status, $met->id], $met->id,$met->id];
            
        }
        return $this->sendResponse1($return_array, 'Product list retrieved successfully', $request->path());
    }

    public function csv_uploade_for_product(Request $request, $id) {
        $input = $request->all();
        $counting = 0;
        $userid = $id;
        $checkSellerCategoty = Seller::where(["id"=>$userid])->first();
        if(is_null($checkSellerCategoty->type)){
            return $this->sendError($request->path(), "Please complete your account details first"); 
        }
        if(count($input['data']) > 0) {
            foreach ($input['data'] as $value) {
                if ($counting > 0) {
                  //  if (sizeof($value) > 11) {
                      if($value[1]!=''){
                       //print_r($value);
                        $percentage = 100 - ($value[3] * 100) / $value[2];

                        $scom = Seller::select("commission")->where(["id"=>$userid])->first();
                        $calComm = ($value[3]  * $scom->commission) / 100;
                        $gstRate = ($calComm * 18) / 100;
                        $inputcommision = $calComm;
                        $inputtax = $gstRate;

                        $return_array = array(
                                        "sku" => $value[0], 
                                        "name" => $value[1],
                                        "mrp" => $value[2],
                                        "sp" => $value[3],
                                        "commission" => $inputcommision, 
                                        "tax" => $inputtax, 
                                        "discount" => $percentage, 
                                        "user_type" => "seller", 
                                        "quantity" => $value[4], 
                                        "size" => $value[5], 
                                        "category_id" => $value[6], 
                                        "description" => $value[7],
                                        "weight" => $value[8], 
                                        "ships_in" => $value[9],
                                        "status" => '',
                                        "user_id" => $userid,
                                        "payment_mode" => 1,
                                    );
                        $details = Product::create($return_array);
                   // }
                     }   
                }
                $counting++;
            }

            return $this->sendResponse1($input['data'], 'All Product added successfully', $request->path());
        } else {

            return $this->sendResponse1([], 'Products not found', $request->path());
        }
        
    }

    public function checkcategoryapproval(Request $request, $id) {
        $getData = SellerApprove::where(["seller_id"=>$id, "status"=>2])->get();
        if(count($getData) > 0) {
            return $this->sendResponse(["status" => "success"], "Category Found", $request->path());
        } else {
            return $this->sendError($request->path(), 'Category not approved yet for products!');
        }
    }

    public function categoryapproval(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['seller_id'=>'required', 'category' => 'required','subcategory'=>'required','subsubcategory'=>'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check = SellerApprove::where(["seller_id"=>$id, "subsubcategory"=>$request->subsubcategory])->first();
        if($check) {

            if($check->status == 1) {
                return $this->sendError($request->path(), 'Category already in approval');
            } else {
                return $this->sendError($request->path(), 'You are already been Approved for this Category');
            }
        }
        SellerApprove::create([
            "seller_id"=>$request->seller_id,
            "category"=>$request->category,
            "subcategory"=>$request->subcategory,
            "subsubcategory"=>$request->subsubcategory,
        ]);

        return $this->sendResponse(["status" => "success"], "Approval for category  is saved successfully", $request->path());
    }

    public function sellerCategoryList(Request $request) {
        $details = SellerApprove::select("category")->where(["seller_id"=>$request->seller_id, "status"=>2])->groupBy('category')->get();
        $return_array = array();
        foreach ($details as $value) {
            $return_array[] = [$value->category];
        }
        return $this->sendResponse($details, 'Category list retrieved successfully', $request->path());
    }

    public function sellerSubCategoryList(Request $request) {
        $details = Subcategory::where(['category_id' => $request->id])->get();
        $return_array = array();
        foreach ($details as $value) {

            $check = SellerApprove::where(["category"=>$request->id, "subcategory"=>$value->id, "seller_id"=>$request->seller_id, "status"=>2])->first();
            if($check) {
                $return_array[] = [$value->id, $value->name];
            }
        }
        return $this->sendResponse($return_array, 'Subcategory list retrieved successfully', $request->path());
    }

    public function sellerSubSubCategoryList(Request $request) {
        $details = Subsubcategory::where(['subcategory_id' => $request->id])->get();
        $return_array = array();
        foreach ($details as $value) {

            $check = SellerApprove::where(["subcategory"=>$request->id, "subsubcategory"=>$value->id, "seller_id"=>$request->seller_id, "status"=>2])->first();
            if($check) {
                $return_array[] = [$value->id, $value->name];
            }
        }
        return $this->sendResponse($return_array, 'Sub-subcategory list retrieved successfully', $request->path());
    }

    public function uniquesku(Request $request) {
        $details = Product::where(['sku'=>$request->sku])->first();
        
        if($details) {
            return $this->sendResponse([], 'SKU already in use', $request->path());   
        } else {
            return $this->sendResponse([], 'SKU available', $request->path());    
        }
    }

    public function addproductbyseller(Request $request, $userid) {
        $input = $request->all();
        $colorWithImageArr = [];
        //return $this->sendResponse1($colorWithImageArr, 'Product added successfully', $request->path());die;
        $colorWithImageArr = [];
        $checkSellerCategoty = Seller::where(["id"=>$userid])->first();
        // if(is_null($checkSellerCategoty->type)){
        //     return $this->sendError($request->path(), "Please complete your account details first"); 
        // }
        $return_array = array();
        $input['user_id'] = $userid;
        $percentage = 100 - ($input['sp'] * 100) / $input['mrp'];
        $input['discount'] = $percentage;

        if(isset($input['image']) && count($input['image']) > 0) {
            $input['images'] = implode(',', $input['image']);
        }else {
            $input['images'] = "";
        }

        $scom = Seller::select("commission")->where(["id"=>$userid])->first();
        $calComm = ($input['sp']  * $scom->commission) / 100;
        $gstRate = ($calComm * 18) / 100;
        $input['commision'] = $calComm;
        $input['tax'] = $gstRate;

        $details = Product::create($input);

        if(!empty($input['attribute'])) {
            $attris = $input['attribute'];
            foreach($attris as $attri) {
                Attribute::create([
                    "product_id"=>$details->id,
                    "label"=>$attri['labal'],
                    "ar_label"=>$attri['ar_labal'],
                    "labeltext"=>$attri['text'],
                    "ar_labeltext"=>$attri['ar_text']
                ]);
            }
        }

        return $this->sendResponse1($colorWithImageArr, 'Product added successfully', $request->path());
    }

    public function editproductbyseller(Request $request, $idk) {
        $input = $request->all();

        $finddetails = Product::find($idk);

        if(isset($input['image']) && count($input['image']) > 0) {
            $finddetails->images = implode(',', $input['image']);
        }else {
            $finddetails->images = $finddetails->images;
        }

        $finddetails->name = $input['name'];
        $finddetails->ar_name = $input['ar_name'];
        $finddetails->category_id = $input['category_id'];
        $finddetails->ar_category_id = $input['ar_category_id'];
        $finddetails->description = $input['description'];
        $finddetails->ar_description = $input['ar_description'];
        $finddetails->mrp = $input['mrp'];
        $finddetails->ar_mrp = $input['ar_mrp'];
        $finddetails->sp = $input['sp'];
        $finddetails->ar_sp = $input['ar_sp'];
        $percentage = 100 - ($input['sp'] * 100) / $input['mrp'];
        $finddetails->discount = $percentage;
        $finddetails->quantity = $input['quantity'];
        $finddetails->ar_quantity = $input['ar_quantity'];
        $finddetails->weight = $input['weight'];
        $finddetails->ar_weight = $input['ar_weight'];
        $finddetails->ships_in = $input['ships_in'];
        $finddetails->sku = $input['sku'];
        $finddetails->ar_sku = $input['ar_sku'];
        $finddetails->size = $input['size'];
        $finddetails->ar_size = $input['ar_size'];
        if(isset($input['payment_mode'])) {
            $finddetails->payment_mode = $input['payment_mode'];
        }
        
        $finddetails->save();

        if(isset($input['attribute'])) {
            if(count($input['attribute']) > 0) {
                Attribute::where('product_id', $idk)->delete();

                $attris = $input['attribute'];
                foreach($attris as $attri) {
                    Attribute::create([
                        "product_id"=>$idk,
                        "label"=>$attri['labal'],
                        "ar_label"=>$attri['ar_labal'],
                        "labeltext"=>$attri['text'],
                        "ar_labeltext"=>$attri['ar_text'],
                    ]);
                }
            }
        }

        return $this->sendResponse1($finddetails, $input['name'] . ' updated successfully', $request->path());
    }

    public function addsize(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['size' => 'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $input = $request->all();
        $verifySize = Size::where(["size"=>$input['size']])->first();

        if($verifySize) {
            return $this->sendError($request->path(), 'Size Already Added');
        } else {
            $arrayData = [
                "size"=> strtoupper($input['size']),
                'ar_size' => $input['ar_size'],
            ];

            $obj = Size::create($arrayData);
            return $this->sendResponse($obj, "Size added successfully", $request->path());
        }
    }

    public function getsize(Request $request) {
        $input = $request->all();
        $sizeData = array();

        $input = $request->all();

        $objs = Size::select('size')->get();

        foreach ($objs as $value) {
            $sizeData[] = ["value"=>$value->size, "label"=>$value->size];
        }

        return $this->sendResponse($sizeData, "Size listing", $request->path());
    }

    public function sizelist(Request $request) {

        $objs = Size::select('id', 'size','created_at')->orderBy('id','DESC')->get();
        foreach ($objs as $value) {
            $sizeData[] = [$value->id, $value->size, date('d-m-Y', strtotime($value->created_at))];
        }
        return $this->sendResponse($sizeData, "Size listing", $request->path());
    }

    public function sizelistfordropdown(Request $request) {

        $objs = Size::select('id', 'size')->orderBy('id','DESC')->get();
        foreach ($objs as $value) {
            $sizeData[] = ["id"=>$value->size, "name"=>$value->size];
        }
        return $this->sendResponse($sizeData, "Size listing", $request->path());
    }

    public function sizedelete(Request $request, $id) {
        $getsize = Size::where(["id"=>$id])->first();

        if($getsize) {
            $verifySize = Product::where("size", 'like', '%'.$getsize->size.'%')->first();

            if($verifySize) {
                return $this->sendError($request->path(), 'Size not able to delete because related product available');
            } else {
                Size::where(["id"=>$id])->delete();
                $objs = "delete";
                return $this->sendResponse($objs, "Size deleted", $request->path());
            }
        } else {
            return $this->sendError($request->path(), 'Size not found');
        }
    }

    public function payreport(Request $request, $id) {
        $input = $request->all();
        $orderArr = array();
        $orderDetailArr = array();
        $query = Order::query();

        if(isset($request->pay_type)) {
            if($request->pay_type == "COD") {       
                $query->where(["payment_type"=>"Cash On Delivery"]);
            } else {
                $query->where(["payment_type"=>"Online"]);
            }
        }
        if($request->has('pay_from_date')) {

            $start = Carbon::parse($request->pay_from_date);
            $end = Carbon::parse($request->pay_to_date);

            $query->whereDate('orders.created_at','<=',$end->format('Y-m-d'))->whereDate('orders.created_at','>=',$start->format('Y-m-d'));
        }

        $getOrders = $query->where(["seller_id"=>$id])->orderBy("id","DESC")->get();
        $y=1;
        $total_payoutAmt = 0;
        $total_priceAmt = 0;

        foreach($getOrders as $getOrder) {

            $oderDetail = OrderDetail::where(["order_id"=>$getOrder->id])->first();

            $orderDetailArr[] = [
                            "order_number"=>$getOrder->order_number,
                            "order_date"=>date("d-m-Y H:i:s",strtotime($getOrder->created_at)),
                            "invoice_amount"=>$oderDetail->final_price,
                            "discount"=>$oderDetail->discount_price + $oderDetail->refer_discount,
                            "commission"=>$oderDetail->admin_commision,
                            "shipping_cost"=>$oderDetail->seller_shipping,
                            "reverse_shipping_cost"=>0,
                            "gst"=>$oderDetail->gst_tax,
                            "tcs"=>$oderDetail->tcs_tax,
                            "protection_fund"=>$oderDetail->final_price,
                            "net_earning"=>$oderDetail->seller_amount,
                            "payout_type"=>"",

                        ];
            $y++;
        }

        $payoutRecords = PayoutRecord::where(["seller_id"=>$id])->orderBy('id','DESC')->get();
        if($payoutRecords) {

            foreach($payoutRecords as $payoutRecord) {
                $orderArr[] = ["seller_id"=>$payoutRecord->seller_id, "date"=>$payoutRecord->date, "orders_count"=>$payoutRecord->orders_count, "payout"=>$payoutRecord->payout, "transcation_id"=>$payoutRecord->transcation_id, "commission"=>$payoutRecord->commission, "adjustments"=>$payoutRecord->adjustments, "gst"=>$payoutRecord->gst, "tcs"=>$payoutRecord->tcs];
            }
        } else {

            $orderArr = [];    
        }
    
        return $this->sendResponse(["orderDetail"=>$orderDetailArr, "orders"=>$orderArr], "Payout report", $request->path());
    }

    public function performanceseller(Request $request, $id) {

        $getPerformance = array();
        $performanceData = SellerPerformance::where(["seller_id"=>$id])->orderBy('id','DESC')->get();
        if($performanceData) {

            foreach($performanceData as $data) {
                $getPerformance[] = ["seller_id"=>$data->seller_id, "from_date"=>date('d-m-Y',strtotime($data->from_date)), "to_date"=>date('d-m-Y',strtotime($data->to_date)), "total_order"=>$data->total_order, "metric"=>$data->metric, "target"=>$data->target, "rating"=>$data->rating];
            }

        } else {
            $getPerformance = [];
        }


        $performanceDataGraph = SellerPerformance::where(["seller_id"=>$id])->whereMonth('created_at', Carbon::now()->month)->get();
        if($performanceDataGraph) {
            $returnGraph = array();
            $breachGraph = array();
            $reattemptGraph = array();

            foreach($performanceDataGraph as $performanceGraph) {

                $getReturnPercent = ($performanceGraph->total_return / $performanceGraph->total_order) * 100;
                $rating1 = 5;
                if($getReturnPercent == 6) {
                    $rating1 = $rating1 - 1;
                
                } else if($getReturnPercent == 7) {
                    $rating1 = $rating1 - 2;
                
                } else if($getReturnPercent == 8) {
                    $rating1 = $rating1 - 3;
                
                } else if($getReturnPercent == 9) {
                    $rating1 = $rating1 - 4;
                
                }  else if($getReturnPercent > 9) {
                    $rating1 = $rating1 - 5;
                }
            
                $getBreachesPercent = ($performanceGraph->total_breach / $performanceGraph->total_order) * 100;
                $rating2 = 5;
                if($getBreachesPercent == 6) {
                    $rating2 = $rating2 - 1;
                
                } else if($getBreachesPercent == 7) {
                    $rating2 = $rating2 - 2;
                
                } else if($getBreachesPercent == 8) {
                    $rating2 = $rating2 - 3;
                
                } else if($getBreachesPercent == 9) {
                    $rating2 = $rating2 - 4;
                
                }  else if($getBreachesPercent > 9) {
                    $rating2 = $rating2 - 5;
                }

                $getReattemptPercent = ($performanceGraph->total_reattempt / $performanceGraph->total_order) * 100;
                $rating3 = 5;
                if($getReattemptPercent == 6) {
                    $rating3 = $rating3 - 1;
                
                } else if($getReattemptPercent == 7) {
                    $rating3 = $rating3 - 2;
                
                } else if($getReattemptPercent == 8) {
                    $rating3 = $rating3 - 3;
                
                } else if($getReattemptPercent == 9) {
                    $rating3 = $rating3 - 4;
                
                }  else if($getReattemptPercent > 9) {
                    $rating3 = $rating3 - 5;
                }

                $returnGraph[] = ["total"=>$performanceGraph->total_order, "rating"=>$rating1, "date"=>date('d-m-Y',strtotime($performanceGraph->created_at))];

                $breachGraph[] = ["total"=>$performanceGraph->total_order, "rating"=>$rating2, "date"=>date('d-m-Y',strtotime($performanceGraph->created_at))];

                $reattemptGraph[] = ["total"=>$performanceGraph->total_order, "rating"=>$rating3, "date"=>date('d-m-Y',strtotime($performanceGraph->created_at))];

            }
        }

        return $this->sendResponse(['performance' => $getPerformance, "graphone"=>$returnGraph, "graphtwo"=>$breachGraph,"graphthree"=>$reattemptGraph], 'Performance Listing', $request->path());
    }


    public function sellersupportticketlist(Request $request, $id) {
        $supportArr = array();
        $getSupports = Support::where(["seller_id"=>$id])->orderBy('id','DESC')->get();
        foreach($getSupports as $getSupport) {

            $supportArr[] = [$getSupport->ticket_id, $getSupport->title, $getSupport->content,$getSupport->status, $getSupport->id];
        }

        return $this->sendResponse($supportArr, "Support Listing Retrive successfully", $request->path());
    }

    public function supportticketadd(Request $request, $id) {
        $input = $request->all();
        $validator = Validator::make($input, ['title'=>'required','content'=>'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $ticketrandom = 'ticket-'.$this->random_strings(4);

        $arrayData = [
            "seller_id"=>$id,
            "ticket_id"=>$ticketrandom,
            "title"=>$input['title'],
            "content"=>$input['content'],
            "status"=>"Pending YOD action"
        ];

        $obj = Support::create($arrayData);
        return $this->sendResponse($obj, "Ticket added with ticket number:".$ticketrandom." successfully", $request->path());
    }
   //seller rating list
     public function ratingList(Request $request,$id){
        $query = OrderRating::query();
        if(!empty($id)){
        $query->where('seller_id',$id);
        }
        $query->orderBy('id','DESC');
        $return_array = [];
        $ratings = $query->get();
        if(count($ratings)>0){
            foreach($ratings as $rating){
             $seller  = Seller::where('id',$rating->seller_id)->first();
             $seller_name = ($seller)?$seller->name:'';
             $order  = Order::where('id',$rating->order_id)->first();
             $order_number = ($order)?$order->order_number:'';
             $return_array[] = array($order_number,$rating->product_rate,$rating->store_rate,$rating->comment);
        
            }
        }  
        return $this->sendResponse1($return_array, 'Rating list retrieved successfully.', $request->path());    
    }
    // Order Listing

    public function sellerorderlisting(Request $request, $id) {   // Pending
        
        $query = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["seller_id"=>$id])->whereIn("orders.status", ['0']);
        
        if (($request->has('start_date') && !empty($request->start_date)) && ($request->has('end_date') && !empty($request->end_date))) {
            $start = Carbon::parse($request->start_date);
            $end = Carbon::parse($request->end_date);
            $query->whereDate('orders.created_at','<=',$end->format('Y-m-d'))->whereDate('orders.created_at','>=',$start->format('Y-m-d'));
        }

        $details = $query->orderBy('orders.id','DESC')->get();
       // echo "<pre>";print_r($details);die;
        $return_array = array();
        
        foreach ($details as $met) {

            $detail = OrderDetail::where(['order_id'=>$met->id])->first();

            $product = Product::select("name","ships_in","sku","sp")->where(['id'=>$detail->product_id])->first();
            if($product) {
                
                $get_price_sp = $product->sp; 
                $get_price_mrp = $product->mrp;

                if($product) {
                    $productName = $product->name;
                } else {
                    $productName = "";
                }

                $get_current_date = strtotime(date('Y-m-d H:i:s'));
                $get_order_date = strtotime($met->created_at);
                $datediff = $get_current_date - $get_order_date;
                $getdayDiff = round($datediff / (60 * 60 * 24));
                 
                $return_array[] = array($met->order_number,$met->final_amt, $met->buyer, $met->state, $met->city, $met->payment_type,'Pending', date('d-m-Y H:i',strtotime($met->created_at)), $met->id, $met->id);
            }
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function sellerorderlisting1(Request $request, $id) {  //  Confirmed processing
        
        $details = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["orders.status"=>'1', "seller_id"=>$id])->orderBy('orders.id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {

            $detail = OrderDetail::where(['order_id'=>$met->id])->first();
            $product = Product::select("name","ships_in","sku","sp")->where(['id'=>$detail->product_id])->first();
            
            $get_price_sp = (!empty($product->sp))?$product->sp:''; 
            $get_price_mrp = (!empty($product->mrp))?$product->mrp:'';

            if($product) {
                $productName = $product->name;
                $ships_in = $product->ships_in;
            } else {
                $productName = "";
                $ships_in = '';
            }

            $shipsINDAYS = "+". $ships_in ."days";
            $oDate = date('d-m-y',strtotime($met->created_at));
            $shipsIN = date('d-m-Y', strtotime($oDate . $shipsINDAYS));

            $mistakeData = SellerMistake::where(['order_id'=>$met->id, 'type'=>'breache'])->first();
            if($mistakeData) {
                $hasMistake = "SLA Breached";
            } else {
                $hasMistake = "";
            }

            //$return_array[] = array($met->order_number, $productName, $product->sku, $get_price_sp, $met->buyer, $met->state, $met->city, $met->payment_type, $detail->quantity ,'Processing', date('d-m-Y H:i',strtotime($met->created_at)),date('d-m-Y',strtotime($met->dispatch_at)), $met->id, $met->id);
          $return_array[] = array($met->order_number,$met->final_amt, $met->buyer, $met->state, $met->city, $met->payment_type,'Processing', date('d-m-Y H:i',strtotime($met->created_at)),date('d-m-Y',strtotime($met->dispatch_at)), $met->id, $met->id);
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function sellerorderlisting2(Request $request, $id) {  //  Ready   //shipped
        
        $details = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["orders.status"=>'2', "seller_id"=>$id])->orderBy('orders.id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {
            $detail = OrderDetail::where(['order_id'=>$met->id])->first();
            $product = Product::select("name","ships_in","sku","sp")->where(['id'=>$detail->product_id])->first();
            
            $get_price_sp = $product->sp; 
            $get_price_mrp = $product->mrp;

            if($product) {
                $productName = $product->name;
            } else {
                $productName = "";
            }

            $shipsINDAYS = "+". $product->ships_in ."days";
            $oDate = date('d-m-y',strtotime($met->created_at));
            $shipsIN = date('d-m-Y', strtotime($oDate . $shipsINDAYS));

            $mistakeData = SellerMistake::where(['order_id'=>$met->id, 'type'=>'breache'])->first();
            if($mistakeData) {
                $hasMistake = "SLA Breached";
            } else {
                $hasMistake = "";
            }


            //$return_array[] = array($met->order_number, $productName, $product->sku, $get_price_sp, $met->buyer, $met->state, $met->city, $met->payment_type, $detail->quantity, 'Shipped' , date('d-m-Y H:i',strtotime($met->created_at)), date('d-m-Y',strtotime($met->dispatch_at)), $hasMistake, $met->id, $met->id);
            $return_array[] = array($met->order_number,$met->buyer, $met->state, $met->city, $met->payment_type,'Shipped' , date('d-m-Y H:i',strtotime($met->created_at)), date('d-m-Y',strtotime($met->dispatch_at)), $met->id, $met->id);
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function sellerorderlisting3(Request $request, $id) {  //  Out for delivery  //deliverd
        
        $details = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["orders.status"=>'3', "seller_id"=>$id])->orderBy('orders.id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {

            $detail = OrderDetail::where(['order_id'=>$met->id])->first();

            $product = Product::select("name")->where(['id'=>$detail->product_id])->first();
            if($product) {
                $productName = $product->name;
            } else {
                $productName = "";
            }

            $return_array[] = array($met->order_number, $met->buyer, $met->state, $met->city, $met->payment_type,'Delivered' , date('d-m-y H:i',strtotime($met->created_at)),date('d-m-Y',strtotime($met->dispatch_at)),$met->id, $met->id);
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function sellerorderlisting4(Request $request, $id) {  //  Delivered
        
        $details = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["orders.status"=>'4', "seller_id"=>$id])->orderBy('orders.id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {

            $detail = OrderDetail::where(['order_id'=>$met->id])->first();

            $product = Product::select("name")->where(['id'=>$detail->product_id])->first();
            if($product) {
                $productName = $product->name;
            } else {
                $productName = "";
            }

            $return_array[] = array($met->order_number, $productName, $met->buyer, $met->state, $met->city, $met->payment_type, $detail->quantity ,$met->status , date('d-m-y H:i',strtotime($met->created_at)),date('d-m-Y',strtotime($met->dispatch_at)),$met->id, $met->id);
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    // public function attributegroupList(Request $request, $id) {
    //     $details = AttributeGroup::where(['seller_id'=>$id])->get();
    //     $return_array = array();
        
    //     if($details) {
    //         foreach ($details as $value) {
    //             $return_array[] = [$value->id, $value->name, $value->status];            
    //         }
    //     }
    //     return $this->sendResponse($return_array, 'Attribute Group list retrieved successfully', $request->path());
    // }

    // public function attributegroupadd(Request $request) {
    //     $input = $request->all();
    //     $validator = Validator::make($input, ['seller_id'=>'required', 'name' => 'required', 'status' => 'required']);
    //     if ($validator->fails()) {
    //         return $this->sendError($request->path(), $validator->errors()->first());
    //     }

    //     AttributeGroup::create([
    //         "seller_id"=>$request->seller_id,
    //         "name"=>$request->name,
    //         "status"=>$request->status,
    //     ]);

    //     return $this->sendResponse(["status" => "success"], "Attribute Group is saved successfully", $request->path());
    // }

    // public function attributegroupedit(Request $request, $id) {
    //     $details = AttributeGroup::where(['id'=>$id])->first();
    //     $return_array = array();
        
    //     if($details) {
    //         $return_array = [$details->id, $details->name, $details->status];
    //     }
    //     return $this->sendResponse($return_array, 'Attribute Group retrieved successfully', $request->path());
    // }

    // public function attributegroupupdate(Request $request) {
    //     $input = $request->all();
    //     $validator = Validator::make($input, ['id'=>'required', 'name' => 'required', 'status' => 'required']);
    //     if ($validator->fails()) {
    //         return $this->sendError($request->path(), $validator->errors()->first());
    //     }

    //     $details = AttributeGroup::where(['id'=>$id])->first();
    //     $details->name = $request->name;
    //     $details->statu = $request->status;
    //     $details->save();

    //     return $this->sendResponse(["status" => "success"], "Attribute Group is uppdated successfully", $request->path());
    // }

    // public function attributeoptionList(Request $request, $id) {
    //     $details = AttributeOption::where(['group_id'=>$id])->get();
    //     $return_array = array();
        
    //     if($details) {
    //         foreach ($details as $value) {
    //             $return_array[] = [$value->id, $value->name];
    //         }
    //     }
    //     return $this->sendResponse($return_array, 'Attribute Option list retrieved successfully', $request->path());
    // }

    // public function attributeoptionadd(Request $request) {
    //     $input = $request->all();
    //     $validator = Validator::make($input, ['seller_id'=>'required', 'group_id'=>'required', 'name' => 'required']);
    //     if ($validator->fails()) {
    //         return $this->sendError($request->path(), $validator->errors()->first());
    //     }

    //     AttributeOption::create([
    //         "group_id"=>$request->group_id,
    //         "seller_id"=>$request->seller_id,
    //         "name"=>$request->name,
    //     ]);

    //     return $this->sendResponse(["status" => "success"], "Attribute Option is saved successfully", $request->path());
    // }

    // public function attributeoptionedit(Request $request, $id) {
    //     $details = AttributeOption::where(['id'=>$id])->first();
    //     $return_array = array();
        
    //     if($details) {
    //         $return_array = [$details->id, $details->name];
    //     }
    //     return $this->sendResponse($return_array, 'Attribute Option fetched successfully', $request->path());
    // }

    // public function attributeoptiondate(Request $request) {
    //     $input = $request->all();
    //     $validator = Validator::make($input, ['id'=>'required', 'name' => 'required']);
    //     if ($validator->fails()) {
    //         return $this->sendError($request->path(), $validator->errors()->first());
    //     }

    //     $details = AttributeOption::where(['id'=>$id])->first();
    //     $details->name = $request->name;
    //     $details->save();

    //     return $this->sendResponse(["status" => "success"], "Attribute Option is updated successfully", $request->path());
    // }

    public function promotionoffer(Request $request) {
        $arrayData = array();
        $objs = Offer::where(["offer_type"=>2])->get();

        foreach($objs as $obj) {

            $date1 = strtotime($obj->enddate);
            $date2 = strtotime(date("d-m-Y H:i:s"));
            if($date2 <= $date1) {

                $arrayData[] = ["id"=>$obj->id, "select"=>$obj->description];
            }
        }
        return $this->sendResponse($arrayData, "Offer added successfully", $request->path());
    }

    public function promotion(Request $request, $id) {
        $input = $request->all();
        $validator = Validator::make($input, ['offer_id' => 'required', 'csv_data' => 'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $input = $request->all();
        $arrayData = [
            "seller_id"=>$id,
            "offer_id"=>$input['offer_id'],
        ];
        $getOffer = Offer::where('id',$input['offer_id'])->first();

        $counting = 0;
        if(count($input['csv_data']) > 0) {

            $obj = Promotion::create($arrayData);
            $xx = 0;
            foreach ($input['csv_data'] as $value) {

                if($xx == 0) {} else {
                    if(strlen($value[0]) > 0) {
                        $getPro = Product::select('id','subsubcategory_id')->where(["sku"=>$value[0]])->first();
                        if($getPro) {
                            if($getOffer->catid == $getPro->subsubcategory_id) {
                                PromotionList::create([
                                    "seller_id" => $id,
                                    "promotion_id" => $obj->id,
                                    "sku" => $value[0],
                                    "product_id" => $getPro->id
                                ]);
                            }
                        }
                    }
                }
                $xx++;
            }
        }

        return $this->sendResponse($obj, "Promotion Added successfully", $request->path());
    }

    public function promotionlist(Request $request, $id) {

        $getPros = Promotion::where(["seller_id"=>$id])->orderBy('id','DESC')->get();
        $arrData = array();

        if($getPros) {
            foreach($getPros as $getPro) {
                $getOffer = Offer::select('catid','description','startdate','enddate','coupon_id')->where(["id"=>$getPro->offer_id])->first();
                $getcoupon = Cuponcode::where(["id"=>$getOffer->coupon_id])->first();
                $getcat = Subsubcategory::where(["id"=>$getOffer->catid])->first();

                if($getOffer) {

                    if($getcoupon) {
                        $getcouponname = $getcoupon->name;
                    } else {
                        $getcouponname = "";
                    }

                    if($getcat) {
                        $getcatname = $getcat->name;
                    } else {
                        $getcatname = "";
                    }

                        $date1 = date("d-m-Y", strtotime($getOffer->enddate));
                        $date2 = date("d-m-Y");
                        
                        $diff = abs(strtotime($date2) - strtotime($date1));
                        $years = floor($diff / (365*60*60*24));
                        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                        $diffdays = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

                        $arrData[] = [
                            'title'=>$getcatname,
                            'content'=>"",
                            'ongoing'=>$diffdays,
                            'offertype'=>$getOffer->description,
                            'startdate'=>$getOffer->startdate,
                            'enddate'=>$getOffer->enddate,
                            'couponcode'=>$getcouponname,
                        ];
                    
                }
            }
        }

        return $this->sendResponse($arrData, "Promotion listing", $request->path());
    }

    public function promotiondelete(Request $request, $id) {
        $arrayData = array();
        $objs = Promotion::where(["id"=>$id])->delete();
        $objs = PromotionList::where(["promotion_id"=>$id])->delete();
        return $this->sendResponse(["status" => "success"], "Promotion deleted successfully", $request->path());
    }

    //offer mang
     public function productlistforoffer(Request $request){
        $input = $request->all();
        $validator = Validator::make($input, ['seller_id'=>'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $query = Product::query();
        $query->select('id','name');
        $query->where(['user_type'=>'seller','user_id'=>$request->seller_id]);
        if(!empty($request->keywords)){
         $query->where('name', 'LIKE', "%$request->keywords%") ; 
        }
        $details =$query->get();
        $return_array = array();
        foreach ($details as $met) {
            $return_array[] = array($met->name, $met->id);
        }
        return $this->sendResponse1($details, 'Product list retrieved successfully', $request->path());
    }

    //add offer product
    public function addproductoffer(Request $request){
        $input = $request->all();
        $validator = Validator::make($input, ['seller_id'=>'required','offer_name'=>'required', 'offer_item'=>'required','mrp_price'=>'required','offer_price'=>'required','description'=>'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $obj =  ProductOffer::create([
            'seller_id' => $request->seller_id,
            'name' => $request->offer_name,
            'product_id' => implode(',',$request->offer_item),
            'mrp' => $request->mrp_price,
            'offer' => $request->offer_price,
            'description' => $request->description,
            'image' => 'bb',
          ]);
        return $this->sendResponse([], "offer product added successfully", $request->path());
    }

    public function offerlistingforseller(Request $request,$id){
        $offers = ProductOffer::where('seller_id',$id)->orderBy('id','DESC')->get();
        if(count($offers)>0){
            foreach($offers as $offer){
                $product_id = explode(',',$offer->product_id);
                $product = Product::select('name')->whereIn('id',$product_id)->get();
                $offer->product = $product;
                $offer->created_at = $offer->created_at;
            }
            $return_array[] = [$offer->name, $offer->mrp, $offer->offer, $offer->description, date('d-m-Y',strtotime($offer->created_at)), [$offer->status, $offer->id],$offer->id];
        }

       // echo "hi";die;
        return $this->sendResponse($return_array, "offer product retrieved successfully", $request->path());
    }

  //payment list
   public function paymentList(Request $request,$id){
        $query = Order::query();
        $query->select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id');
        $query->where('orders.seller_id',$id);
        if (($request->has('to') && !empty($request->to)) && ($request->has('from') && !empty($request->from))) {
           $to = Carbon::parse($request->to);
           $from = Carbon::parse($request->from);
           $query->whereDate('orders.created_at','<=',$to->format('Y-m-d'))->whereDate('orders.created_at','>=',$from->format('Y-m-d'));
        }
       if(($request->has('payment_type') && !empty($request->payment_type))){
         $query->where('payment_type',$request->payment_type);
        }

        $details = $query->orderBy('orders.id','DESC')->get();
        // print_r($details->toArray());die;
        $return_array = array();
        $return_array = [];
        $total_earning = 0;
        if(count($details)>0){    
        foreach ($details as $met) {
            $detail = OrderDetail::where(['order_id'=>$met->id])->first();
            $total_earning = $total_earning+$met->total_seller_earning;
            $invoic_amt = $met->shipping_charges+$met->final_amt;
            $return_array[] = array($met->order_number,date('d/m/y',strtotime($met->created_at)),$met->buyer,$met->sub_tot_amt,$met->shipping_charges,$met->discount,$met->final_amt,round($met->total_seller_earning),round($met->total_admin_comission),$met->payment_type,$met->transcation_id,$met->id);

                //print_r($return_array);die;
            //}
              }  

        }
         //$result = ['total_earning'=>round($total_earning),'list'=>$return_array];

     return $this->sendResponse1($return_array, 'Payment list retrieved successfully.', $request->path());
        }

    function random_strings($length_of_string) {
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }

    public function pushNotification($notifyData, $userDevice, $badge) {
        $url = 'https://fcm.googleapis.com/fcm/send';  
        
        $message = array("id"=>$notifyData['order_id'],'title' => $notifyData['title'], "body"=>$notifyData['content'], "type"=>$notifyData['type'], 'sound' => 'default', 'badge'=>$badge);
        
        $registatoin_ids = array($userDevice->deviceToken);
        $fields = array('registration_ids' => $registatoin_ids, 'data' => $message, 'notification'=>$message);

        $GOOGLE_API_KEY = "AAAAvtlLWfU:APA91bHkKDPg59guB8VXYN8zeJc5w8vfrYuCRYL6J4Hlmdz2SSo15JotADzXH-RBF9fdGxPgci7W_422oGlJz6I_KW1mZ99dy7fqJHxtiDy3_FT199aH8VkUgZmA5Zf8NaGeL6rnnKW8";   
        $headers = array('Authorization: key=' . $GOOGLE_API_KEY , 'Content-Type: application/json',);
        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);    
        
        if ($result === false) {
            return $abc = 'FCM Send Error: ' . curl_error($ch);
        } else {
            return $abc = "Notification Send";
        }
        curl_close($ch);
    }
}

?>