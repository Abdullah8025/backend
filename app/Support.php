<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    //

    protected $fillable = [
        'seller_id','ticket_id','title','content','status'
    ];
}
