<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class APIBaseController extends Controller
{
    //

    public function sendResponse($result, $message,$requestkey)
    {
    	$response = [
            'status' => 'SUCCESS',
             explode("/",str_replace("api/","",$requestkey))[0]   => $result,
            'message' => $message,
            'requestKey'=>$requestkey,
        ];


        return response()->json($response, 200,[],JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    public function sendResponse1($result, $message,$requestkey)
    {
        $response = [
            'status' => 'SUCCESS',
            'response'    => $result,
            'message' => $message,
            'requestKey'=>$requestkey,
        ];


        return response()->json($response, 200,[],JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
    
    public function sendResponse5($resulttotal,$result)
    {
        $response = [
            'searchSuccess' => 'SUCCESS',
            'dataTotalSize'    => $resulttotal,
            'data' =>$result
        ];


        return response()->json($response, 200,[],JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
    
     public function sendError1($requestkey,$errorMessages)
    {
    	$response = [
            'searchSuccess' => 'FAILURE',
        ];


        return response()->json($response, 200);
    }
    
    public function sendResponse3($result,$result1, $message,$requestkey)
    {
        $response = [
            'status' => 'SUCCESS',
           explode("/",str_replace("api/","",$requestkey))[0]   => $result,
           'record'=>$result1,
            'message' => $message,
            'requestKey'=>$requestkey,
        ];


        return response()->json($response, 200,[],JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }



    public function sendError($requestkey,$errorMessages)
    {
    	$response = [
            'status' => 'FAILURE',
            'message' => $errorMessages,
            'requestKey'=>$requestkey,
        ];


        return response()->json($response, 200);
    }

     public function checkemail($email) {
        $email_inmembers = DB::table('sellers')->where('email', $email)->first();
        if (!empty($email_inmembers)) {
            return 1;
        } else {
            return 0;
        }
    }

    // binary search with time complexcity O(logn) other hand linear search of time complexcity is O(n)

    public function fastsearch($arr,$firstindex,$lastindex,$search_element)
    {
        
        if($lastindex >= $firstindex)
        {
             $mid = ceil($firstindex + ($lastindex - $firstindex) / 2);
             //dd($mid);
             if($arr[$mid] == $search_element)
             {
                return floor($mid);
             } else if($arr[$mid] > $search_element){
                 return $this->fastsearch($arr, $firstindex,$mid - 1, $search_element); 
             } else {
                 return $this->fastsearch($arr, $mid + 1,$lastindex, $search_element); 
             }

        }

        return -1;
    }  

    public function pushNotificationNew($notifyData, $userDevice, $badge, $lang) {
        $url = 'https://fcm.googleapis.com/fcm/send';  
        
        if($lang == "en") {
            $message = array("id"=>$notifyData['order_id'],'title' => $notifyData['title'], "body"=>$notifyData['content'], "type"=>$notifyData['type'], 'sound' => 'default', 'badge'=>$badge);
        } else {
            $message = array("id"=>$notifyData['order_id'],'title' => $notifyData['title_ar'], "body"=>$notifyData['content_ar'], "type"=>$notifyData['type'], 'sound' => 'default', 'badge'=>$badge);
        }
        
        $registatoin_ids = array($userDevice->deviceToken);
        $fields = array('registration_ids' => $registatoin_ids, 'data' => $message, 'notification'=>$message);

        $GOOGLE_API_KEY = "AAAAKT7Oo5k:APA91bEcQ-uqB42hPjRktyQv2CMKrLXdsJhYWGjy0_PDhGRmsFD8OBqJ0cErEJp0rStYoHBJa1jrmZjeMblMCM6SQ1JxmdT00lZg7e4aBungPYiUHPTY7PaXhfGXzlhQ857agWaJ9Z7P";   
        $headers = array('Authorization: key=' . $GOOGLE_API_KEY , 'Content-Type: application/json',);
        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);    
        
        if ($result === false) {
            return $abc = 'FCM Send Error: ' . curl_error($ch);
        } else {
            return $abc = "Notification Send";
        }
        curl_close($ch);
    }


     public function pushNotification($notifyData, $userDevice, $badge) {
        $url = 'https://fcm.googleapis.com/fcm/send';  
        
        $message = array("id"=>$notifyData['order_id'],'title' => $notifyData['title'], "body"=>$notifyData['content'], "type"=>$notifyData['type'], 'sound' => 'default', 'badge'=>$badge);
        
        $registatoin_ids = array($userDevice->deviceToken);
        $fields = array('registration_ids' => $registatoin_ids, 'data' => $message, 'notification'=>$message);

        $GOOGLE_API_KEY = "AAAAKT7Oo5k:APA91bEcQ-uqB42hPjRktyQv2CMKrLXdsJhYWGjy0_PDhGRmsFD8OBqJ0cErEJp0rStYoHBJa1jrmZjeMblMCM6SQ1JxmdT00lZg7e4aBungPYiUHPTY7PaXhfGXzlhQ857agWaJ9Z7P";   
        $headers = array('Authorization: key=' . $GOOGLE_API_KEY , 'Content-Type: application/json',);
        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);    
        
        if ($result === false) {
            return $abc = 'FCM Send Error: ' . curl_error($ch);
        } else {
            return $abc = "Notification Send";
        }
        curl_close($ch);
    }

    public function android_push($deviceToken = null, $message = null, $type = null,$badge = null,$batch = array())
    {
        $this->autoRender = false;
        $this->layout     = false;
        $url              = 'https://android.googleapis.com/gcm/send';
        $message          = array("batch"=>$batch,'badge'=>$badge,'sound' => 'default','type'=>$type,"message" => $message);
        $registatoin_ids  = array($deviceToken);
        $fields           = array('registration_ids' => $registatoin_ids, 'data' => $message);
        
        //$GOOGLE_API_KEY   = "AIzaSyCT1hAiELV9ogaDsrPfyTlBJ9ocI5jRqE0";
        $GOOGLE_API_KEY   = "AAAAKT7Oo5k:APA91bEcQ-uqB42hPjRktyQv2CMKrLXdsJhYWGjy0_PDhGRmsFD8OBqJ0cErEJp0rStYoHBJa1jrmZjeMblMCM6SQ1JxmdT00lZg7e4aBungPYiUHPTY7PaXhfGXzlhQ857agWaJ9Z7P";
        $headers          = array(
            'Authorization: key=' . $GOOGLE_API_KEY,
            'Content-Type: application/json',
        );
       // echo json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === false) {
           // die('Curl failed: ' . curl_error($ch));
        } else {
           // print_r("success");die;
        }
        curl_close($ch);
    }


    public function iphone_push($deviceToken = null, $message = null, $type = null,$badge = null,$batch = array())
    {
       
         $deviceToken      = $deviceToken; //"01FE59D85A1F62728541988192F92AF840D098F89254D607D6D7B97F250D12BB";//$deviceToken;

        //636A67B61425C7C677704334D40B852844445D63CD5921DC6B151F9D9F20E7A5
        $passphrase       = '1234';
        $Text             = $message;
        $this->autoRender = false;
        $this->layout     = false;
        $basePath         = public_path().'/Escalate.pem';
        
        if (file_exists($basePath)) {
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $basePath);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client(
                'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx
            );
            if (!$fp) {
                exit("Failed to connect: $err $errstr" . PHP_EOL);
            }
            $body['aps'] = array('alert' => array("body" => $Text, 'type' => $type),'badge' => $badge,'sound' => 'default',"batch"=>$batch);
           
            $payload     = json_encode($body);
            $msg         = chr(0) . pack('n', 32) . pack("H*", $deviceToken) . pack('n', strlen($payload)) . $payload;
            $result      = fwrite($fp, $msg, strlen($msg));
            if (!$result) {
           /*     echo 'Message not delivered' . PHP_EOL;
            echo $result;
            echo "Failure";*/
            } else {
               //]  var_dump($result);
            //  print_r($msg);

                //echo "success";die;
            }
            fclose($fp);
        }
    }

    public function convertNumbers($str){
    $western_arabic = array('0','1','2','3','4','5','6','7','8','9');
    $eastern_arabic = array('٠','١','٢','٣','٤','٥','٦','٧','٨','٩');
    $str = str_replace($western_arabic, $eastern_arabic, $str);
    return $str;
    }
}
