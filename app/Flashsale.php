<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flashsale extends Model
{
    //

    protected $fillable = [
         'time', 'endtime', 'products','status'
    ];
}
