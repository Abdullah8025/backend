<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WishlistStore extends Model
{
    
    protected $fillable = [ 'user_id','store_id'];
}
