<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Token;
use App\Address;
use App\Product;
use App\Subcategory;
use App\Banner;
use App\Theme;
use App\Brand;
use App\Flashsale;
use App\Wishlist;
use App\Subsubcategory;
use App\Priority;
use Image;
use Auth;
use Session;
use Validator;
use Intervention\Image\ImageServiceProvider;

class InnerPageController extends Controller {

  private $baseurlslash;
  private $baseurl;

  public function __construct() {
      $this->baseurlslash = "http://mobuloustech.com/yodapi/public/";
      $this->baseurl = "http://mobuloustech.com/yodapi/public";
  }

  public function innerPage(Request $request, $id) {
      $currencyValue = Session::get('currency_value');
      if(empty($currencyValue)) {
          $currencyValue = 1;
      }

      $shopType = $id;
      $input = $request->all();
      $section1Arr = array();
      $othercatlist = array();
      $ocassionList = array();
      $flash_sale = array();
      $bannerlist = array();

      //SUB CAT PRODUCT LIST
      $subCatlists = Subcategory::where('category_id', $id)->get();

      foreach ($subCatlists as $subCatlist) {
      
          if(empty($subCatlist->image)) {
              $subCatlist->image = url('/public/') . "/img/imagenotfound.jpg";
          }

          $section1Arr[] = ["subcatid"=>$subCatlist->id, "image"=>$subCatlist->image, "name"=>$subCatlist->name];
          
          $productdetails = Product::where(['subcategory_id' => $subCatlist->id])->where(["deleteStatus"=>1,"status"=>1])->limit(20)->orderBy('id','DESC')->get();
          $prodatalist = array();
            
          foreach ($productdetails as $value) {
              $imagearray = explode(",", $value->images);
      
              if (!empty($imagearray)) {
      
                  if (!empty($imagearray[0])) {
      
                      if (strpos($imagearray[0], $this->baseurlslash) == false) {
                          $imagearray[0] = $this->baseurl . $imagearray[0];
                      }
                  } else {
                      $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                  }
              } else {
                  $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
              }

              if(Auth::check()) {
                  $findflagofwishlist = Wishlist::where(['user_id' => Auth::user()->id, "product_id" => $value->id])->first();
                  
                  if (!empty($findflagofwishlist)) {
                      $like_Status = "1";
                  } else {
                      $like_Status = "0";
                  }
              } else {
                $like_Status = "0";
              }
              $percentagetemp = 100 - ($value->sp * $currencyValue * 100) / $value->mrp * $currencyValue;
              
              if (!empty($value->status)) {
                  $prodatalist[] = ["image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp * $currencyValue, "mrp" => $value->mrp * $currencyValue, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status];
              }
          }
          $othercatlist[] = ["section_name" => $subCatlist->name, "subcat_id" => $subCatlist->id, "data" => $prodatalist];
      }

      // echo "<PRE>";
      // var_dump($othercatlist);die;

      //FLASH SALE LIST
      $flash_sale = array();
        $findflashdetails = Flashsale::where(['id' => 1, 'status' => '1'])->first();
        
        if (!empty($findflashdetails)) {
            $flashdatenew = date('Y-m-d H:i:s', strtotime($findflashdetails->time));
            $flashStr = strtotime($flashdatenew);
            $currentDate = date('Y-m-d H:i:s');
            $currentStr = strtotime($currentDate);

            if($currentStr >= $flashStr) {

                $flashdateend = date('Y-m-d H:i:s', strtotime($findflashdetails->endtime));
                $flashStrEnd = strtotime($flashdateend);
                $currentDateEnd = date('Y-m-d H:i:s');
                $currentStrEnd = strtotime($currentDateEnd);

                if($currentStrEnd <= $flashStrEnd) {

                    $findflashdetailsstatus = $findflashdetails->status;
                    $flashtime = date('d-m-Y, h:i:s', strtotime($findflashdetails->endtime));
                    $flashtime = strtotime($findflashdetails->endtime);
                    $currentDate = date('d-m-Y, h:i:s');
                    $currentDateStr = strtotime($currentDate);

                    $timeDiff = abs($flashtime - $currentDateStr);
                    $findflashdetailstime = $timeDiff;
                    $flashsaleendtimeget = $findflashdetails->endtime;

                    if(strlen(trim($findflashdetails->products))>=1) {
                      $productdetails = Product::whereIn('id', explode(",", $findflashdetails->products))->where(['category_id'=>$id, "deleteStatus"=>1,"status"=>1])->orderBy('id','DESC')->get();
                    } else {
                      $productdetails = Product::where(['subsubcategory_id'=>$findflashdetails->subsubcategory])->where(['category_id'=>$id, "deleteStatus"=>1,"status"=>1])->orderBy('id','DESC')->get();
                    }
                    
                    foreach ($productdetails as $value) {
                        $imagearray = explode(",", $value->images);
                        if (!empty($imagearray)) {
                            if (!empty($imagearray[0])) {
                                if (strpos($imagearray[0], $this->baseurlslash) == false) {
                                    $imagearray[0] = $this->baseurl . $imagearray[0];
                                }
                            } else {
                                $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                            }
                        } else {
                            $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                        }
                        
                        if(Auth::check()) {
                          $findflagofwishlist = Wishlist::where(['user_id' => Auth::user()->id, "product_id" => $value->id])->first();
                          
                          if (!empty($findflagofwishlist)) {
                              $like_Status = "1";
                          } else {
                              $like_Status = "0";
                          }
                        } else {
                           $like_Status = "0";
                        }

                        $percentagetemp = 100 - ($value->sp * $currencyValue * 100) / $value->mrp * $currencyValue;
                        $flash_sale[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp * $currencyValue, "mrp" => $value->mrp * $currencyValue, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
                    }
                }  else {
                    $flash_sale = [];
                    $findflashdetailsstatus = '';
                    $findflashdetailstime = '';
                    $flashsaleendtimeget = '';
                } 
            } else {
                $flash_sale = [];
                    $findflashdetailsstatus = '';
                    $findflashdetailstime = '';
                    $flashsaleendtimeget = '';
            }
        } else {
            $flash_sale = [];
                    $findflashdetailsstatus = '';
                    $findflashdetailstime = '';
                    $flashsaleendtimeget = '';
        }

      //OCASSION LIST
      $subcategories_list1 = Subsubcategory::where(['category_id' => $id, 'occasion_status' => '1'])->orderBy('updated_at','DESC')->get();
      foreach ($subcategories_list1 as $value) {
          if($value->status == 1) {
            if (empty($value->image)) {
                $value->image = url('/public/') . "/img/imagenotfound.jpg";
            }
            $ocassionList[] = array("filter_data" => $value->id, "image" => $value->image, "name" => $value->name);
          }
      }

      //BANNERS
      $allinnerbanner = Banner::where('type', $id . "_inner")->orderBy('id', 'ASC')->get();
      
      foreach ($allinnerbanner as $value) {
          if (empty($value->image)) {
              $value->image = url('/public/') . "/img/imagenotfound.jpg";
          } else {
              $value->image = url('/public/') . $value->image;
          }
          $getCat = Subcategory::select("name")->where(['id'=>$value->subcategory_id])->first();
          if($getCat->status == 1) {
            $bannerlist[] = ["filter_data" => $value->subsubcategory_id, "image" => $value->image, "name" => $getCat->name];
          }
      }

      //Categories all
      $catArray = array();
      $catLists = Subcategory::where('category_id',$shopType)->orderBy('id', 'ASC')->limit(4)->get();
      
      foreach($catLists as $catList) {
          $subcatLists = Subsubcategory::where(['subcategory_id'=>$catList->id, 'category_id'=>$shopType])->orderBy('id', 'ASC')->get();
          $subcatArray = array();
          
          if($catList->status == 1) {
            if (empty($catList->image)) {
                $catList->image = url('/public/') . "/img/imagenotfound.jpg";
            } else {
                $catList->image = $catList->image;
            }

            foreach($subcatLists as $subcatList) {
              $subcatArray[] = ["subname"=>$subcatList->name,"subid"=>$subcatList->id];
            }
            $catArray[] = ["catname"=>$catList->name,"catid"=>$catList->id, 'catimg'=>$catList->image, "subcat"=>$subcatArray];
          }
      }

      $urlname = $shopType;

      return view('shop', compact('section1Arr', 'othercatlist','flash_sale', 'findflashdetailsstatus', 'flashsaleendtimeget', 'ocassionList', 'bannerlist', 'urlname','catArray'));
  } 
}
