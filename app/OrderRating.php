<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderRating extends Model
{
    
    protected $fillable = [ 'user_id','order_id','seller_id','product_rate','store_rate','comment','product_id'];
}
