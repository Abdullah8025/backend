<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{
    //

    protected $fillable = [
        'reason', 'query', 'image',
    ];
}
