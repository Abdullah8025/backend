<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerHistory extends Model
{
    
    protected $fillable = [ 'seller_id','amount','type'];
    protected $table = 'seller_history';

}
