<?php
use Session as Session;
use App\Review;
use App\Product;
use App\OrderDetail;
use DB;
	
	function getCount() {

		if(Auth::check()) {
	    	return $cartCount = \App\Cart::where(['user_id'=>Auth::user()->id])->count();
	    } else {
	    	if (Request()->session()->has('cartsession')) {
                $session_id = session::get('cartsession');
            } else {
                $session_id = Session::getId();
            }
	    	return $cartCount = \App\Cart::where(['user_id'=>$session_id])->count();
	    }
	}

	function getFooterContent() {
	    $getContent = \App\Content::where(['id'=>13])->first();
	    return $footerContent = $getContent->value;
	}

	function productReview($product_id){
     $query =  DB::table('order_ratings');
                    $query->select('comment','name','image','order_ratings.created_at');
                    $query->join('users','users.id','=','order_ratings.user_id');
                    $query->where(['product_id'=>$product_id]);
                    $query->orderBy('order_ratings.id','DESC');
                    $review = $query->get();
                   // $rating_sum = Review::where(['type'=>0,'product_id'=>$id->id])->avg('rating'); 
                    foreach($review as $rev){
                        $rev->image = asset('public/'.$rev->image);
                        $rev->created_at = date('d-M-Y', strtotime($rev->created_at));
                    }
                   return $review; 
	}

	function getReview($id,$type){
		$query =  DB::table('reviews');
                    $query->select('description','name','image','reviews.created_at');
                    $query->join('users','users.id','=','reviews.user_id');
                    $query->where(['type'=>$type,'product_id'=>$id]);
                    $query->orderBy('reviews.id','DESC');
                    $review = $query->get();
                   // $rating_sum = Review::where(['type'=>0,'product_id'=>$id->id])->avg('rating'); 
                    foreach($review as $rev){
                        $rev->image = asset('public/'.$rev->image);
                        $rev->created_at = date('d-M-Y', strtotime($rev->created_at));
                    }
                   return $review; 
	}

	function getRating($id,$type){

		$rating = Review::where(['type'=>$type,'product_id'=>$id])->avg('rating');
		return ($rating)?$rating:0;
	}

    function store_review_count($store_id){
     $review = Review::where(['product_id'=>$store_id,'type'=>1])->count("id");
     return $review;
    }

    //product rating
    function product_rating($product_id){
      $rating = OrderDetail::where('product_id',$product_id)->avg('rating');
      return round($rating,1);  
    }

    function check_category_product($cat){
     $product = Product::where('category_id',$cat)->count();
     if($product>0){
      return 1;
     }else{
      return 0;
     }
    }

     function check_category_product_ar($cat){
     $product = Product::where('ar_category_id',$cat)->count();
     if($product>0){
      return 1;
     }else{
      return 0;
     }
    }

?>