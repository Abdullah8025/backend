<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    //
    
    protected $fillable = [
  		'user_id','order_id','order_number','type','title','content','title_ar','content_ar'
    ];
}

?>
