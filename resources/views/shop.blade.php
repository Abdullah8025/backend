@extends('layouts.website')
@section('content')
<?php
$currencyCode = Session::get('currency_code');
if($currencyCode == "INR") {
    $currencyCodeLabel = "Rs";
} else {
    $currencyCodeLabel = $currencyCode;
}
?>
<style type="text/css">
   .BannerBoxss{height: 400px; margin: 20px 0 20px 0; overflow: hidden;}
   .BannerBoxss img{ width: 100%; height: 100% }
   .Discountlist a figure { height: 150px; }
   .Discountlist a figure img{ height: 100% }
   .CollectionBox figure{ height: 310px }
   .CollectionBox figure img{ height: 100% } 
   .OccasionArea .Occasionbox figure{ height: 225px }
   .OccasionArea .Occasionbox figure img{ height: 100% }
   .Discountlist a p { 
    min-height: 63px;
    align-items: center;
    justify-content: center;
    display: flex;
}

.DiscountArea .DiscountBox .row{ display: flex;align-items: center; justify-content: center; }

</style>

<section>
   <div class="DiscountArea">
      <div class="container">
         <div class="DiscountBox" style="background-image: url('{{url('/')}}/public/images/Women.png');">
            <!--<h1>GET 10 % EXTRA DISCOUNT</h1>
            <h2>USE CODE: <span>WELCOME 10</span></h2>-->

            <div class="row">
            @if(count($section1Arr) > 0)
            @foreach($section1Arr as $section1)
               <div class="col-sm-2">
                  <div class="Discountlist">
                     <a href="{{ route('products', [$urlname, base64_encode('subcat'), base64_encode($section1['subcatid'])]) }}">
                        <figure><img src="{{ $section1['image'] }}"></figure>
                        <p>{{ $section1['name'] }}</p>
                     </a>
                  </div>
               </div>
            @endforeach
            @endif
            </div>
         </div>
      </div>
   </div>
</section>

@if(count($flash_sale) > 0)
<section>
   <div class="CollectionArea FlashArea">
      <div class="container">
         <h1><img src="{{url('/')}}/public/images/flash.png"> Flash Sale <a href="{{ route('products', [$urlname, base64_encode('sale'), base64_encode('0')]) }}">View all</a></h1>

         <div class="CollectionList FlashBody">
            <div id="demos">
               <div class="owl-carousel owl-theme" id="Ethnic">
               @if(count($flash_sale) > 0)
               @foreach ($flash_sale as $flash)
                  <div class="item">
                     <div class="FlashBox">
                        <a href="{{ route('product/description/', base64_encode($flash['product_id'])) }}">
                           <figure>
                              @if($flash['percentage'] != 0)
                                  <span>{{$flash['percentage']}}%</span>
                              @endif
                              <img src="{{ $flash['image'] }}">
                           </figure>
                           <aside>
                              <p>{{ $currencyCodeLabel }} {{ $flash['sp'] }}   
                                 @if($flash['percentage'] != 0)
                                      <del>{{ $currencyCodeLabel }} {{ $flash['mrp'] }}</del>
                                 @endif
                              </p>
                           </aside>
                        </a>
                     </div>
                  </div>
                @endforeach
                @endif
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endif

@if(count($ocassionList) > 0)
<section>
   <div class="container">
      <div class="OccasionArea">
         <h2>Shop by Occasion</h2>
         <div class="row">
         @if(count($ocassionList) > 0)
         @foreach($ocassionList as $ocassion)
            <div class="col-sm-3">
               <a href="{{ route('products', [$urlname, base64_encode('subsubcat'), base64_encode($ocassion['filter_data'])]) }}">
                  <div class="Occasionbox">
                     <figure><img src="{{ $ocassion['image'] }}"></figure>
                     <p> {{ $ocassion['name'] }} </p>
                  </div>
               </a>
            </div>
         @endforeach
         @endif
         </div>
      </div>

   </div>
</section>
@endif

<section>
   @if(isset($bannerlist[0]))
   <div class="BannerBoxss">
       <a href="{{ route('products', [$urlname, base64_encode('subsubcat'), base64_encode($bannerlist[0]['filter_data'])]) }}"> <img src="{{ $bannerlist[0]['image']}}"> </a>
   </div>
   @endif
</section>


@if(count($othercatlist) > 0)
<section>
   <div class="CollectionArea">
      <div class="container">
        @if(count($othercatlist) > 0)
        @foreach($othercatlist as $otherlist)
         <div class="CollectionList">
            @if(count($otherlist['data']) > 0)
               <h2> {{ $otherlist['section_name'] }} </h2>
               <div id="demos">
                  <div class="owl-carousel owl-theme" id="Ethnic">
                   @php $otherDatas = $otherlist['data']; @endphp
                   @foreach($otherDatas as $otherData)
                     <div class="item">
                        <div class="CollectionBox">
                           <a href="{{ route('product/description/', base64_encode($otherData['product_id'])) }}">
                              <figure><img src="{{ $otherData['image'] }}"></figure>
                              <figcaption>
                                 <h3>{{ $otherData['brand'] }}</h3>
                                 <h4> {{ $otherData['name'] }} </h4>
                                 <p>{{ $currencyCodeLabel }} {{ $otherData['sp'] }} @if($otherData['percentage'] > 0) <del>{{ $currencyCodeLabel }} {{ $otherData['mrp'] }}</del> <span>({{ $otherData['percentage'] }}%OFF)</span> @endif </p>
                              </figcaption>
                           </a>
                        </div>
                     </div>
                   @endforeach
                  </div>
               </div>
            @endif
         </div>
        @endforeach
        @endif
      </div>
   </div>
</section>
@endif

<section>
   @if(isset($bannerlist[1]))
   <div class="BannerBoxss">
      <a href="{{ route('products', [$urlname, base64_encode('subsubcat'), base64_encode($bannerlist[1]['filter_data'])]) }}"> <img src="{{ $bannerlist[1]['image']}}"> </a>
   </div> 
   @endif
</section>

@if(count($catArray) > 0)
<section>
   <div class="ClothingArea">
      <div class="container">
         <div class="row">
         @if(count($catArray) > 0)
         @foreach($catArray as $cat)
            <div class="col-sm-6">
               <div class="Clothing">
                  <figure><img src="{{ $cat['catimg'] }}" style="width:100px;height:94px;"></figure>
                  <h3> {{ $cat['catname'] }} </h3>
                  <ul>
                  @foreach($cat['subcat'] as $sub)
                     <li><a href="{{ route('products', [$urlname, base64_encode('subsubcat'), base64_encode($sub['subid'])]) }}"> {{ $sub['subname'] }} </a></li>
                  @endforeach
                  </ul>
               </div>
            </div>
         @endforeach
         @endif
            
         </div>
      </div>
   </div>
</section>
@endif

@endsection