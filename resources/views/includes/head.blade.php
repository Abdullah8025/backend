 <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{ asset('/public/images/apple-touch-icon.png') }}">
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="{{ asset('/public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/fonts/D-DIN/') }}">
    <!-- Main-Stylesheets -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('/public/css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/overright.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/responsive.css') }}">
    <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
