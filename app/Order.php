<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    
    protected $fillable = ['order_number','seller_id','user_id', 'location','state', 'name','phone','city','country','address_type','dispatch_at','coupon_code','sla_time','payment_type', 'transcation_id', 'payment_status', 'status','shipping_charges','tracking_id','tracking_url','expected_delivery_date','final_amt','discount','total_seller_earning','total_admin_comission','sub_tot_amt'];
}
