<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderRequest extends Model
{
    
    protected $fillable = [ 'order_id', 'user_id', 'request_type','reason','description', 'cancel_by', 'status'];
}
